<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<?php echo easy_wp_pagenavigation(); ?>
		</div><!-- .site-content -->
<?php $ara = get_option( 'theme_mods_outnabout' );
					//print_r($ara); 
					?>
<footer>

      <div class="calltoaction">
   <ul class="action-list">
            <li class="reg_artist"><a href="/registeration" class="banner-button-style">register as artist</a></li>
            <li><span><img src="/assets/artists/images/artist_icon.png" alt="artist_icon"></span></li>
            <li class="hire_artist"><a class="banner-button-style showlogin" href="javascript:void(0);">login as artist</a></li>
        </ul>
    </div>
   
<div class="container">
   <div class="row">
   <div class="footer-wrap">
            <ul class="footer-list footer-nav">
                <li><a href="/HowItwork" class="footer-link"><i class="fa fa-briefcase" aria-hidden="true"></i> how it works</a></li>
				<li><a href="/partner" class="footer-link"><i class="fa fa-sun-o" aria-hidden="true"></i> Featured Partners</a></li>
                <li><a href="/career" class="footer-link"><i class="fa fa-user" aria-hidden="true"></i> join our team</a></li>
                <li><a href="/advertise" class="footer-link"><i class="fa fa-random" aria-hidden="true"></i> Advertise with us</a></li>
                 <li><a href="/Welcome/allstate" class="footer-link"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Cultural Points</a></li>
                 <li><a href="/allevents" class="footer-link"><i class="fa fa-calendar" aria-hidden="true"></i> events</a></li>
            </ul>
        </div>
		
		<div class="download-share">
		<div class="center-block">
            <ul>
                <li><a href="/sitemap">sitemap</a></li>
               <li><a href="/contact" class="footer-link">Contact</a></li>
                <li><a href="/blog">blog</a></li>
                <li><a href="#">press</a></li>
            </ul>
		  </div>
		 </div>
		 
		 <div class="bottom-footer">
		 <div class="col-sm-6">
		 <div class="copyright">
		 <p>© Hi Artist | All Rights Reserved</p>
		   </div>
		   </div>
		   <div class="col-sm-6">
		   <div class="social-share">
			<ul>
			<li><a href="https://www.facebook.com"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
			<li><a href="https://www.twitter.com"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			<li><a href="https://www.linkedin.com"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
			<li><a href="https://www.youtube.com"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
			</ul>
		   </div>
		   </div>
		   </div>
    </div>
    </div>
	
<input value="/" id="base_url" type="hidden">
<input value="" id="showmapornot" type="hidden">
<input value="" id="showfancyornot" type="hidden">
    </footer>
	</div><!-- .site-inner -->
</div><!-- .site -->

<?php wp_footer(); ?>
</body>
</html>
