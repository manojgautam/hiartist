<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	
	<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">

		<?php wp_head(); ?> 

<style>

.page_not_found #message-frame { background: #f7f7f7;border: 1px solid #f7f7f7;  margin: 120px auto 0 auto; box-sizing: border-box;  border-radius: 8px; box-shadow: 2px 3px 9px rgba(0, 0, 0, .5);   padding: 60px; width: 80%; }
.page_not_found h3 { font-size: 25px;}
.page_not_found .frame-container p {    margin: 20px 0;    font-size: 17px;    color: #474747;    line-height: 23px;    letter-spacing: 1px;  font-weight: bold;}
.page_not_found a.btn.btn-primary.btn-large {    border: none;    border-radius: 0;    margin-right: 7px;    color: #fff;    font-size: 14px;    font-weight: normal;    letter-spacing: 1px;}
.page_not_found .btn:hover, select:hover {
background-color: #ed217c;}
.page_not_found .btn:active, .btn:focus, select:active, select:focus {
-moz-box-shadow: none;
-webkit-box-shadow: none;
box-shadow: none;
background-color: #ed217c;}
.btn-primary {
  background-color: #ed217c;
  background-image: linear-gradient(to bottom, #ed217c, #ed217c);
  background-repeat: repeat-x;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
}

</style>

</head>
<body >
<div class="page-content page_not_found"> 
           <div id="main" class="col-xs-12 col-md-12 container"> 
		   <div id="message-frame" class="frame-container">   
		   <h3>Page Not Found - Error 404</h3>     
		   <p>  Unfortunately the page you requested does not exist. Please check your browser URL or head to another location using the buttons below. </p>
		   <a href="http://agiledevelopers.in/thecut/home" class="btn btn-primary btn-large">  <i class="icon-home icon-white"></i>  
		   The Cut Home Page                 
		   </a>   
		   <a href="http://agiledevelopers.in/thecut/contact" class="btn btn-primary btn-large">                        <i class="icon-envelope icon-white"></i>
		   Contact Us
		   </a> 
		   </div> 
           </div> 
		   </div>

</body>
</html>
