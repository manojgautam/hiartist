<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<div class="main-banner">
<?php //twentysixteen_excerpt(); ?>
<?php twentysixteen_post_thumbnail(); ?>

	<div class="single-banner-text">
	<div class="container">
	<div class="row">
	<div class="col-sm-12">
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->
	
		<footer class="entry-footer">
		<div class="single-post-meta-content">
			
				<div class="single-post-category"><a class="eg-herbert-hoover-element-1 e" href="<?php echo the_permalink(); ?>" title="View all posts in Articles" rel="category tag"><?php the_category(); ?></a></div>
				<div class="single-post-date"><?php the_time('F jS, Y') ?></div>
				<div class="single-post-author"><?php the_author_posts_link(); ?></div>
             		  	
				
</div>
		<?php //twentysixteen_entry_meta(); ?>
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
			</div>
			</div>
			</div>
			</div>
	
	
	</div>

<div class="container">
<div class="row">
<div class="col-sm-12">

	<div class="entry-content">
	<div class="single-post-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			if ( '' !== get_the_author_meta( 'description' ) ) {
				get_template_part( 'template-parts/biography' );
			}
		?>
		 <?php //echo do_shortcode('[hupso]'); ?>
<span class="all-blog"><a href="<?php echo get_home_url(); ?>">All Blog</a></span>
	</div>
	</div><!-- .entry-content -->


	
	</div>
	</div>
	</div>
</article><!-- #post-## -->
