<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
  
?>
<?php $ara = get_option( 'theme_mods_outnabout' );
					//print_r($ara); 
					?>
<div class="vc_custom_heading wpb_content_element vc_custom_1413475234349"><h2 style="font-size: 60px;color: #c4a26c;line-height: 64px;text-align: center;font-family:'Montserrat', sans-serif;font-weight:700;font-style:normal"><?php echo $ara['Title']; ?></h2></div>
		<div class="vc_custom_heading wpb_content_element vc_custom_1413476468924"><h2 style="font-size: 30px;color: #000000;line-height: 34px;text-align: center;font-family:'Montserrat', sans-serif;font-weight:700;font-style:normal"><?php echo $ara['SubTitle']; ?></h2></div>

  <div class="container">
		<div class="row">
		<div class="esg-grid blog_cat">	 <?php //wp_list_categories();  ?> </div> </div></div>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<div class="container">
		<div class="row">
		<div class="esg-grid cat_post_page">

  
	

	<div class="esg-entry-media"><?php twentysixteen_post_thumbnail(); ?></div>
	
	<div class="esg-entry-content eg-herbert-hoover-content">
		<header class="entry-header">
		<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
			<span class="sticky-post"><?php _e( 'Featured', 'twentysixteen' ); ?></span>
		<?php endif; ?>

		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</header><!-- .entry-header -->
<div class="esg-entry-content1 eg-herbert-hoover-content1">
			
				<div class="esg-content e eg-herbert-hoover-element-1-a"><a class="eg-herbert-hoover-element-1 e" href="<?php echo the_permalink(); ?>" title="View all posts in Articles" rel="category tag"><?php the_category(); ?></a></div>
				<div class="esg-content e eg-herbert-hoover-element-26"><?php the_time('F jS, Y') ?></div>
              <div class="esg-content eg-herbert-hoover-element-3 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
				<div class="esg-content e eg-herbert-hoover-element-4"><?php the_author_posts_link(); ?></div>
              			  	
				
</div>
	<div class="entry-content">
	<?php twentysixteen_excerpt(); ?>
	

	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php //twentysixteen_entry_meta(); ?>
		<div class="share-social"><?php echo do_shortcode('[hupso]'); ?></div>
	</footer><!-- .entry-footer -->
	</div>
	
	</div>
	</div>
	</div>
	
	
</article><!-- #post-## -->

