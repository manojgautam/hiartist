<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	
	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css' />
	<link href="http://agiledevelopers.in/thecut/assets/fonts/font-awesome.css" rel="stylesheet" type="text/css" />
	<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css' type="text/css" />


		
		<link rel='stylesheet' href='<?php bloginfo("stylesheet_directory"); ?>/css/settings.css' type="text/css" />
		<link rel='stylesheet' href='<?php bloginfo("stylesheet_directory"); ?>/css/blog-css.css' type="text/css" />

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  		<link rel='stylesheet' href='<?php bloginfo("stylesheet_directory"); ?>/css/sweetalert2.css' type="text/css" />
		 <link rel='stylesheet' href='<?php bloginfo("stylesheet_directory"); ?>/css/sweetalert2.min.css' type="text/css" />
		 <script type='text/javascript' src='<?php bloginfo('template_directory'); ?>/js/sweetalert2.js'></script>
		 <script type='text/javascript' src='<?php bloginfo('template_directory'); ?>/js/sweetalert2.min.js'></script>
		<?php wp_head(); ?> 

</head>

<body class="page page-id-769 page-child parent-pageid-51 page-template-default _masterslider _msp_version_2.16.0 ozy-page-model-full ozy-page-model-no-sidebar ozy-classic no-page-title wpb-js-composer js-comp-ver-4.3.4 vc_responsive">

		<div class="header">
                <div class="container">
                <div class="row">
                    <div class="brand">
                        <?php twentysixteen_the_custom_logo(); ?>
                    </div>
                                     
                </div>
                </div>
</div>

		<div id="content" class="site-content">
		



	
	              

