<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Footerpagemodel extends CI_Model
{
public function addAdvertise($data)
{
 $this->db->insert('advertise',$data);
}
public function getallartistsORdirectors($roleid){
$this->db->distinct();
$this->db->select('sub_role_id');
$this->db->where('role_id',$roleid);
$this->db->order_by('sub_role_id','ASC');
$this->db->where('active',1);
$query = $this->db->get('users');
return $query->result();
}
public function getallArtistsDistricts($role){
$this->db->select('district');
$this->db->where('sub_role_id',$role);
$this->db->where('active',1);
$this->db->group_by('district');
$this->db->order_by('district','ASC');
$query = $this->db->get('users');
return $query->result();
}
public function getallthedistricts(){
        $this->db->select('cities.name,shops.district_id');
        $this->db->from('shops');
        $this->db->join('cities','cities.id = shops.district_id');
        $this->db->group_by('shops.district_id');
        $this->db->order_by('cities.name','ASC');
        $q=$this->db->get();
        return $q->result();
}
public function addCareer($data)
{
$this->db->insert('career',$data);
}
public function RegisteredUsers()
{
$this->db->where('active',1);
$query=$this->db->get('users');
return $query->result();
}
public function Artist()
{
$query=$this->db->get_where('users',array('role_id'=>1,'active'=>1));
return $query->result();
}
public function Director()
{
$query=$this->db->get_where('users',array('role_id'=>2,'active'=>1));
return $query->result();
}
public function events()
{
$query=$this->db->get('events');
return $query->result();
}
public function shops()
{
$query=$this->db->get('shops');
return $query->result();
}
public function testimonial()
{
$query=$this->db->get('testimonial');
return $query->result();

}
public function Admindetail()
{
$query=$this->db->get('admin');
return $query->result();

}
}
?>
