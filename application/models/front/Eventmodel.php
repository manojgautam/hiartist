<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Eventmodel extends CI_Model
{

	public function getallEvents($limt=0){
      $this->db->limit(6,$limt);
     $this->db->order_by('start_date','ASC');
   $this->db->where('end_date>=',date('Y-m-d'));
    $q=$this->db->get('events');
    return $q->result();
 }
public function getevent($id)
{
     $query=$this->db->get_where('events',array('event_id'=>$id));
    if($query->row_array()){
            return $query->row_array();
        }else{
            return $balnk = array();
        }
    }
 public function geteventcount()
 {
     $count=$this->db->get_where('events',array('end_date>='=>date('Y-m-d')))->num_rows();
      return $count;
 }
  public function ajaxeventPage($searchdata,$limt=0)
    {
       $miles = '20000';//your search radius
       if($searchdata['latitude']!='')
        {$this->db->select('*, (((acos(sin(('.$searchdata['latitude'].'*pi()/180)) * 
            sin((`latitude`*pi()/180))+cos(('.$searchdata['latitude'].'*pi()/180)) * 
            cos((`latitude`*pi()/180)) * cos((('.$searchdata['longitude'].'- `longitude`)* 
            pi()/180))))*180/pi())*60*1.1515
        ) AS distance');}
      else
        {$this->db->select('*');}
      $this->db->from('events');
    if($searchdata['date']!='')  
  {$startenddate=preg_split("/[\s-]+/",$searchdata['date']);
  $startdate=date_format(date_create($startenddate[0]),"Y-m-d");
  $enddate=date_format(date_create($startenddate[3]),"Y-m-d");
  $srchsrttme=date("H:i:s", strtotime($startenddate[1]." ".$startenddate[2]));
  $srchendtme=date("H:i:s", strtotime($startenddate[4]." ".$startenddate[5]));
  $wher = "(start_date>='".$startdate."' AND start_date<='".$enddate."' AND start_time>='".$srchsrttme."') OR (start_date<='".$startdate."' AND end_date>='".$startdate."'  AND start_time>='".$srchsrttme."')";}
  if($searchdata['date']!='')
  $this->db->where($wher);
 if($searchdata['date']=='')
 $this->db->where('end_date>=',date('Y-m-d'));
  $this->db->limit(6,$limt);
  if($searchdata['latitude']!='')
     $this->db->order_by('distance', 'ASC');
     $this->db->order_by('start_date', 'ASC');
      $query=$this->db->get();
      return $query->result();
      }

       public function ajaxeventPagecount($searchdata)
    {
       $miles = '20000';//your search radius
       if($searchdata['latitude']!='')
        {$this->db->select('*, (((acos(sin(('.$searchdata['latitude'].'*pi()/180)) * 
            sin((`latitude`*pi()/180))+cos(('.$searchdata['latitude'].'*pi()/180)) * 
            cos((`latitude`*pi()/180)) * cos((('.$searchdata['longitude'].'- `longitude`)* 
            pi()/180))))*180/pi())*60*1.1515
        ) AS distance');}
      else
        {$this->db->select('*');}
      $this->db->from('events');
  if($searchdata['date']!='')
  {$startenddate=preg_split("/[\s-]+/",$searchdata['date']);
  $startdate=date_format(date_create($startenddate[0]),"Y-m-d");
  $enddate=date_format(date_create($startenddate[3]),"Y-m-d");
  $srchsrttme=date("H:i:s", strtotime($startenddate[1]." ".$startenddate[2]));
  $srchendtme=date("H:i:s", strtotime($startenddate[4]." ".$startenddate[5]));
  $wher = "(start_date>='".$startdate."' AND start_date<='".$enddate."' AND start_time>='".$srchsrttme."') OR (start_date<='".$startdate."' AND end_date>='".$startdate."'  AND start_time>='".$srchsrttme."')";}
  if($searchdata['date']!='')
  $this->db->where($wher);
if($searchdata['date']=='')
 $this->db->where('end_date>=',date('Y-m-d'));
  if($searchdata['latitude']!='')
     $this->db->order_by('distance', 'ASC');
     $this->db->order_by('start_date', 'ASC');
      $query=$this->db->get();
      return $query->result();
      }



}

