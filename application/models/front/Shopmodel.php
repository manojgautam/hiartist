<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shopmodel extends CI_Model
{

public function get_shop_list($id,$limt=0)
{
    $this->db->limit(6,$limt);
   $query =$this->db->get_where('shops',array('district_id'=>$id));
   
/*echo $this->db->last_query();die;*/
     return $query->result();
}

public function get_destrict_from_id($id){
$query =$this->db->get_where('cities',array('id'=>$id));
  return $query->result();
}
public function get_thestate_from_id($id){
$query =$this->db->get_where('states',array('id'=>$id));
return $query->result();
}
public function getShopImages($id){
  $query =$this->db->get_where('shop_images',array('shop_id'=>$id));
  return $query->result();
}

public function getstateid($id)
{
    $this->db->select('state_id');
    $this->db->group_by('state_id');
    $query =$this->db->get_where('shops',array('district_id'=>$id));
     return $query->result();
}

public function getalldistrcts($stateid){
        $this->db->select('shops.district_id,cities.name');
        $this->db->from('shops');
        $this->db->join('cities','shops.district_id = cities.id');
        $this->db->where('shops.state_id',$stateid);
        $this->db->group_by('shops.district_id');
        $q=$this->db->get();
        return $q->result();

}

public function get_shop_list_ajax($stateid,$limt=0){
   $this->db->limit(6,$limt);
   $query =$this->db->get_where('shops',array('state_id'=>$stateid));
   return $query->result();
}

public function get_shop_list_ajax_count($stateid){
 $query =$this->db->get_where('shops',array('state_id'=>$stateid));
   return $query->result();
}


public function get_states_list(){
        $this->db->select('states.id,states.name,COUNT(shops.state_id) AS citycount');
        $this->db->from('shops');
        $this->db->join('states','states.id = shops.state_id');
        $this->db->group_by('shops.state_id');
        $q=$this->db->get();
        return $q->result();
}

public function get_district_list_for_selected_state($stateid){
 $this->db->select('cities.id,cities.name');
        $this->db->from('shops');
        $this->db->join('cities','cities.id = shops.district_id');
        $this->db->where('shops.state_id',$stateid);
        $this->db->group_by('shops.district_id');
        $q=$this->db->get();
        return $q->result();
}

 public function getshopcount($id)
 {
     $count=$this->db->get_where('shops',array('district_id'=>$id))->num_rows();
      return $count;
 }
    public function getbyshop($id)
    {
        $query=$this->db->get_where('shops',array('shop_id'=>$id));
        if($query->row_array()){
            return $query->row_array();
        }else{
            return $balnk = array();
        }
    }
 
}
?>

         

