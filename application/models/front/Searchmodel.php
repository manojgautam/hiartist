<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Searchmodel extends CI_Model
{
    

public function get_allroles_front_menu()//header menu artist
{
  $wher="(role_id='1' OR role_id='2') AND active='1'";
  $this->db->where($wher);
  $query = $this->db->get('user_sub_role');
  return $query->result();
}


public function get_user_detail($role_name,$limt=0,$type="")
    {
        $str=str_replace('%20',' ',$role_name);
        $this->db->limit(8,$limt);
        if($role_name!='Profile')
        $this->db->like('sub_role_id',$str);
        else{$this->db->order_by('profile_view','DESC');}
        if($type!='' && $type!='all')
        $this->db->where('district',str_replace('%20',' ',$type));
        $this->db->where('active',1);
        $query = $this->db->get('users');
         return $query->result();
        
}
    public function getrolecount($role_name,$type="")
 {
        $str=str_replace('%20',' ',$role_name);
        if($role_name!='Profile')
        $this->db->like('sub_role_id',$str);
        else{$this->db->order_by('profile_view','DESC');}
        if($type!='' && $type!='all')
        $this->db->where('district',str_replace('%20',' ',$type));
        $this->db->where('active',1);
        $query = $this->db->get('users');
        $count=$query->num_rows();
      return $count;
 }
    public function roleDirector($role_name)
    {    if($role_name==''){$role_name='Profile';}
         $str=str_replace('%20',' ',$role_name);
            $this->db->select('district');
              $this->db->from('users');
            //if($role_name!='Profile')
            //$this->db->where('sub_role_id',$str);
            $this->db->where('active',1);
            $this->db->group_by('district');
            $query = $this->db->get(); 
            return $query->result();
    }
    public function ajaxserachPage($role_name,$data,$limt=0,$therole='')
    {
         $str=str_replace('%20',' ',$role_name);
        $this->db->limit(8,$limt);
              $this->db->select('*');
              $this->db->from('users');
              $this->db->where('active',1);
               if($data['allvalues'][0]!='' && $data['allvalues'][0] != "Profile"){
          $this->db->like('sub_role_id',$data['allvalues'][0]);
		}else if($therole == "Artists" || $therole == "Director"){
                if($therole == "Artists")
			    $this->db->where('role_id',1);
                if($therole == "Director")
			    $this->db->where('role_id',2);
		}else{}
        if($data['allvalues'][1]!='')
         $this->db->where('gender',$data['allvalues'][1]);

         if($data['allvalues'][2]!='')
         {$agerange=explode('-',$data['allvalues'][2]);
         $startage=(int)date("Y")-(int)$agerange[1];
         $endage=(int)date("Y")-(int)$agerange[0];
         $this->db->where('dob>=',$startage.'-01-01');
         $this->db->where('dob<=',$endage.'-12-31');
        }

        if($data['allvalues'][3]!='')
         $this->db->where('district',$data['allvalues'][3]);
        
            if($data['allvalues'][4]!='')
         $this->db->order_by('reviews',$data['allvalues'][4]);
            if($data['allvalues'][5]!='')
         $this->db->order_by('profile_view',$data['allvalues'][5]);
            if($data['allvalues'][6]!='')
         $this->db->order_by('created_on',$data['allvalues'][6]);
            $query = $this->db->get(); 
            return $query->result();
    }
      public function ajaxserachPagecount($role_name,$data,$therole='')
    {
         $str=str_replace('%20',' ',$role_name);
      $this->db->select('*');
              $this->db->from('users');
              $this->db->where('active',1);
     if($data['allvalues'][0]!='' && $data['allvalues'][0] != "Profile"){
          $this->db->like('sub_role_id',$data['allvalues'][0]);
		}else if($therole == "Artists" || $therole == "Director"){
                if($therole == "Artists")
			    $this->db->where('role_id',1);
                if($therole == "Director")
			    $this->db->where('role_id',2);
		}else{}
             if($data['allvalues'][0]!='')
         $this->db->like('sub_role_id',$data['allvalues'][0]);
        if($data['allvalues'][1]!='')
         $this->db->where('gender',$data['allvalues'][1]);

        if($data['allvalues'][2]!='')
         {$agerange=explode('-',$data['allvalues'][2]);
         $startage=(int)date("Y")-(int)$agerange[1];
         $endage=(int)date("Y")-(int)$agerange[0];
         $this->db->where('dob>=',$startage.'-01-01');
         $this->db->where('dob<=',$endage.'-12-31');}

        if($data['allvalues'][3]!='')
         $this->db->where('district',$data['allvalues'][3]);
          
            if($data['allvalues'][4]!='')
         $this->db->order_by('reviews',$data['allvalues'][4]);
            if($data['allvalues'][5]!='')
         $this->db->order_by('profile_view',$data['allvalues'][5]);
            if($data['allvalues'][6]!='')
         $this->db->order_by('created_on',$data['allvalues'][6]);
            $query = $this->db->get(); 
            return $query->num_rows();
    }
    

}
?>
