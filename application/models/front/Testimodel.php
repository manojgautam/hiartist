<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Testimodel extends CI_Model
{
   public function getTestimonial($start=0)
{
        $this->db->select('testimonial.*,users.*');
        $this->db->from('testimonial');
        $this->db->join('users','users.user_id = testimonial.testimonialAddedBy');
        $this->db->where('testimonial.is_active ',1);
        $this->db->where('users.active ',1);
        $this->db->limit('6',$start);
        $q=$this->db->get();
        return $q->result();
  
}
    

 public function getTestimonialcount()
{
        $this->db->select('testimonial.*,users.*');
        $this->db->from('testimonial');
        $this->db->join('users','users.user_id = testimonial.testimonialAddedBy');
        $this->db->where('testimonial.is_active ',1);
        $this->db->where('users.active ',1);
        $q=$this->db->get();
        return $q->result();
  
}


}
?>
