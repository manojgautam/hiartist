<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Homemodel extends CI_Model
{
public function getUserForHome(){
     $this->db->order_by('profile_view','DESC');
        $this->db->limit(5);
          $q=$this->db->get_Where('users',array('active'=>1));
            return $q->result();
 }
    public function Usercount(){
       $count=$this->db->get_where('users',array('active'=>1))->num_rows();
        return $count;
    }
    
public function get_artist_front_menu()//header menu artist
{
          $query = $this->db->get_where('user_sub_role',array('role_id'=>1,'active'=>1));
          return $query->result();
}
public function get_director_front_menu()//header menu director
{
     $query = $this->db->get_where('user_sub_role',array('role_id'=>2,'active'=>1));
          return $query->result();
}
public function get_groups_front_menu()//header menu group
{
     $query = $this->db->get('groups');
          return $query->result();
}
    
public function get_state_front_menu()//header menu state
{
        
        $this->db->select('*');
        $this->db->from('states');
        $this->db->join('shops','shops.state_id = states.id');
        $this->db->group_by('shops.state_id');
        $q=$this->db->get();
        return $q->result();


}

public function get_state_front_menu_forallstates()//header menu state
{
   $this->db->select('*');
    $this->db->from('states');
  $query = $this->db->get(); 
    return $query->result();
}
public function getallState()
 {
   $this->db->select('*');
    $this->db->from('states');
     $query = $this->db->get(); 
    return $query->result();
}
    public function get_state_cities_front_menu($id)//header menu state
{
      $query = $this->db->get_where('cities',array('state_id'=>$id));
    $result_city = $query->result();
    
    $i=0;
     foreach($result_city as $city)
    {
     $count_shop=$this->db->get_where('shops',array('district_id'=>$city->id))->num_rows();
         $result_city[$i]->count_shop = $count_shop;
         $i++;
    }
    return $result_city;
}


public function getEventsForHome(){
   $this->db->order_by('event_id','DESC');
   //$this->db->limit(5);
   $this->db->where('end_date>=',date('Y-m-d'));
  $q=$this->db->get('events');
    return $q->result();
 }


public function getBannerForHome()
{    $this->db->order_by('home_id','DESC');
     $this->db->limit(3);
     $query = $this->db->get_where('homepage_media',array('media_type'=>1));
          return $query->result();
}


public function getLogosForHome()
{    $this->db->order_by('home_id','DESC');
     $query = $this->db->get_where('homepage_media',array('media_type'=>4));
          return $query->result();
}


public function getvideosForHome()
{    $this->db->order_by('home_id','DESC');
     $query = $this->db->get_where('homepage_media',array('media_type'=>2));
          return $query->result();
}
public function getaddsForHome()
{    $this->db->order_by('home_id','DESC');
     $query = $this->db->get_where('homepage_media',array('media_type'=>3));
          return $query->result();
}
    public function getTestimonials(){    
        $this->db->order_by('testimonialId','DESC');
        $this->db->limit(7);
        $query = $this->db->get_where('testimonial',array('is_active'=>1));
        $testimonial_array = $query->result();
        $i=0;
        foreach($testimonial_array as $testimoial){
            $user_data = $this->getUserDetails($testimoial->testimonialAddedBy);
            $testimonial_array[$i]->first_name = $user_data['first_name'];
            $testimonial_array[$i]->last_name = $user_data['last_name'];
            $testimonial_array[$i]->profile_pic = $user_data['profile_pic'];
            $testimonial_array[$i]->sub_role = $user_data['sub_role_id'];
            $i++;
        }
       return $testimonial_array;
    }
    
    public function getUserDetails($UserId){
        $query = $this->db->get_where('users', array('user_id'=>$UserId) )->result_array();
        return $query[0];
    }

 public function getRecommendedArtists(){
 
 $this->db->select('*');
 $this->db->where('active','1');
 $this->db->where('role_id','1');
 $this->db->group_by('sub_role_id');
 $this->db->order_by('profile_view','DESC');
 $qu=$this->db->get('users');
    // echo "<pre>";print_r($qu->result());
     if(count($qu->result()) > 8){
         return $qu->result(); 
     }else{
         $role_array = array();
         foreach($qu->result() as $key => $value){
             $role_array[] = $value->sub_role_id;
         }
         $result = $this->getRecommendedArtistsmore($role_array);
        // echo "<pre>";print_r($result);die;
         return $result;
     }

 
}

function getRecommendedArtistsmore($role_arraya){
    $newarray = array();
    if(count($role_arraya) > 4){
        $limit = 2;
    }else{
        $limit = 3;
    }
    foreach($role_arraya as $role_array){
         $this->db->select('*');
         $this->db->limit($limit);
         $this->db->where('sub_role_id',$role_array);
         $this->db->where('active','1');
         $this->db->where('role_id','1');
         
         $this->db->order_by('profile_view','DESC');
         $qu=$this->db->get('users');
        foreach($qu->result() as $key => $values){
            $newarray[] = $values;
        }
    }
     return  $newarray;
}


}
?>
