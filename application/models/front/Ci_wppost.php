<?php
 
 if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Ci_Wppost extends CI_Model {
 
    public function __construct(){
        parent::__construct();
    }
    
    private $postLimit = 5;
    private $imagesPerPost = 1;
    
    public function getPosts() {
        $this->db->where('wp_posts.post_parent', 0);
        $this->db->where('wp_posts.post_status', 'publish');
        $this->db->where('post_type','post');
        //$this->db->limit($this->postLimit);
        $this->db->order_by('post_date', 'DESC');
 
        $query = $this->db->get('wp_posts');
 
        $data = $query->result();
 
        $post = array();
 
        for ($i = 0; $i < count($data); $i++) {
            array_push($post, 
                array(
                    'post' => $data[$i],
                    'author' =>$this->getAuthorDetails($data[$i]->post_author),
                    'image' => $this->getPostImages($data[$i]->ID)
                )
            );     
        }
 
       
 
        return $post;
    }
 
    private function getPostImages ($idPost) {
        
        $this->db->select('wp_posts.guid')
         ->from('wp_posts')
         ->join('wp_postmeta', 'wp_posts.ID = wp_postmeta.meta_value')
        ->where(array('wp_postmeta.post_id'=>$idPost,'meta_key'=>'_thumbnail_id'));
        $image = $this->db->get();
 
        if ($this->imagesPerPost > 1) {
            return $image->result();	
      	} else {
      	    return $image->row();
      	}
    }
    
     private function getAuthorDetails ($id) {
        
        $this->db->select('meta_value')
         ->from('wp_usermeta')
        ->where('user_id', $id);
         $where = '(meta_key="first_name" or meta_key = "last_name")';
       $this->db->where($where);
        $image = $this->db->get();
 
        return $image->result();	
    }
}
