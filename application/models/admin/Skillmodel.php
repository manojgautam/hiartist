<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Skillmodel extends CI_Model
{

	public function getAllExpertDetails(){
		$this->db->select('user_sub_role.sub_role,all_expertise.expert_id,all_expertise.expertise');
        $this->db->from('all_expertise');
        $this->db->join('user_sub_role','user_sub_role.sub_role_id = all_expertise.sub_role_id');
        $this->db->where('user_sub_role.active !=',0);
        $q=$this->db->get();
        return $q->result();
     
 }

    public function getAllrolesDetails(){
    	$this->db->select('*');
    	$wher="(role_id='1' OR role_id='2') AND active!=0";
        $this->db->where($wher);
        $q=$this->db->get('user_sub_role');
        return $q->result();

    }

    public function addskill($data){

    	$this->db->insert('all_expertise',$data);
    }

    public function editskill($id,$data){
        $this->db->where('expert_id',$id);
        $this->db->update('all_expertise',$data);

    }

    public function deleteskill($id){
    $this->db->where('expert_id',$id);
    $this->db->delete('all_expertise');

      $this->db->where('expert_id',$id);
    $this->db->delete('user_expetise');

    }

    public function getparticularSkillDetail($id){
        $this->db->where('expert_id',$id);
        $q=$this->db->get('all_expertise');
        return $q->result();

    }
}
