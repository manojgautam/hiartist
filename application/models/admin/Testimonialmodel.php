<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Testimonialmodel extends CI_Model
{
    public function getTestimonial()
    {
    $this->db->select('testimonial.testimonialId,testimonial.testimonialText,testimonial.is_active,users.user_id,users.first_name,users.last_name');
          $this->db->from('testimonial');
         $this->db->join('users','users.user_id=testimonial.testimonialAddedBy');
          $this->db->order_by('testimonial.testimonialId','DESC');
        $query= $this->db->get(); 
        return $query->result();
 }
    public function delete($id)
    {
        $this->db->where('testimonialId',$id);
        return $this->db->delete('testimonial');
    }
 public function changeTestimonialStatus($testimonialId,$status){
      $data=array('is_active'=>$status);
      $this->db->where('testimonialId',$testimonialId);
         $this->db->update('testimonial',$data);
}
}
