<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class GroupShopmodel extends CI_Model
{
public function getGroupImages($grpid){
$this->db->where('group_id',$grpid);
$q=$this->db->get('group_images');
return $q->result();
}

public function getShopImages($shpid){
$this->db->where('shop_id',$shpid);
$q=$this->db->get('shop_images');
return $q->result();
}

public function deleteshopimages($id){
$eventimage=$this->getparticularshopimage($id);
@unlink('uploads/'.$eventimage[0]->image_name);
$this->db->where('shop_image_id', $id);
$this->db->delete('shop_images');
}

public function getparticularshopimage($id){
$this->db->where('shop_image_id', $id);
$q=$this->db->get('shop_images');
return $q->result();
}

public function deletegroupimages($id){
$eventimage=$this->getparticularGroupimage($id);
@unlink('uploads/'.$eventimage[0]->image_name);
$this->db->where('group_image_id', $id);
$this->db->delete('group_images');
}

public function getparticularGroupimage($id){
$this->db->where('group_image_id', $id);
$q=$this->db->get('group_images');
return $q->result();
}

public function addGroup($data){
$this->db->insert('groups',$data);
}
public function addGroupImages($data1)
{
   $this->db->insert('group_images',$data1); 
}


public function addshoppImages($data1)
{
   $this->db->insert('shop_images',$data1); 
}

public function ListGroups(){
$this->db->order_by('group_id','DESC');	
$q=$this->db->get('groups');
return $q->result();
}

public function changeGroupStatus($grpid,$status){
$data=array('active'=>$status);
$this->db->where('group_id',$grpid);
$this->db->update('groups',$data);
}

public function deleteGroup($id){

$eventimage=$this->GroupDetail($id);
if($eventimage[0]->image!='')
@unlink('uploads/'.$eventimage[0]->image);

$groupimage=$this->GroupAllImagesDetail($id);
foreach($groupimage as $val){
@unlink('uploads/'.$val->image_name);
}

$this->db->where('group_id', $id);
$this->db->delete('group_images');

$this->db->where('group_id', $id);
$this->db->delete('groups');
}

public function GroupAllImagesDetail($id){
$this->db->where('group_id', $id);
$q=$this->db->get('group_images');
return $q->result();
}

public function GroupDetail($id){
 $q=$this->db->get_Where('groups',array('group_id'=>$id));
return $q->result();
}

public function EditGroup($id,$data){
$this->db->where('group_id', $id);
$this->db->update('groups', $data);
}

public function getAllStates(){
$q=$this->db->get('states');
return $q->result();
}

public function getDistricts($stateid){
 $q=$this->db->get_Where('cities',array('state_id'=>$stateid));
return $q->result();
}

public function addShop($data){
$this->db->insert('shops',$data);
}

public function ListShops(){
$this->db->order_by('shops.shop_id','DESC');
 $this->db->select('shops.*,cities.name');
$this->db->from('shops');
$this->db->join('cities','cities.id = shops.district_id');
$q=$this->db->get();
return $q->result();
}

public function changeShopStatus($shopid,$status){
$data=array('active'=>$status);
$this->db->where('shop_id',$shopid);
$this->db->update('shops',$data);
}

public function getShopDetail($sid){
 $q=$this->db->get_Where('shops',array('shop_id'=>$sid));
return $q->result();
}

public function getSelectedDistrict($districtid){
 $q=$this->db->get_Where('cities',array('id'=>$districtid));
return $q->result();
}

public function editShop($id,$data){
$this->db->where('shop_id', $id);
$this->db->update('shops', $data);
}

public function deleteShop($id){

$eventimage=$this->getShopDetail($id);
if($eventimage[0]->image!='')
@unlink('uploads/'.$eventimage[0]->image);

$groupimage=$this->ShopAllImagesDetail($id);
foreach($groupimage as $val){
@unlink('uploads/'.$val->image_name);
}

$this->db->where('shop_id', $id);
$this->db->delete('shop_images');

$this->db->where('shop_id', $id);
$this->db->delete('shops');
}

public function ShopAllImagesDetail($id){
$this->db->where('shop_id', $id);
$q=$this->db->get('shop_images');
return $q->result();
}

}
