<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Usermodel extends CI_Model
{

public function getAllUsers(){
$this->db->order_by('users.user_id','DESC');
 $this->db->select('users.*,user_role.role');
$this->db->from('users');
$this->db->join('user_role','users.role_id = user_role.role_id');   
$q=$this->db->get();
return $q->result();
}

public function changeUserStatus($userid,$status){
$data=array('active'=>$status);
$this->db->where('user_id',$userid);
$this->db->update('users',$data);
}

public function deleteUser($userid){

$this->db->where('user_id',$userid);
$this->db->delete('reviews');

$this->db->where('added_by',$userid);
$this->db->delete('reviews');

$this->db->where('user_id',$userid);
$this->db->delete('user_education');

$this->db->where('user_id',$userid);
$this->db->delete('user_experience');

$this->db->where('user_id',$userid);
$this->db->delete('user_physical_stats');

$this->db->where('user_id',$userid);
$this->db->delete('user_expetise');

$this->db->where('user_id',$userid);
$this->db->delete('user_work_pref');

$this->db->where('testimonialAddedBy',$userid);
$this->db->delete('testimonial');

$mediaimage=$this->getUserMedia($userid);
foreach($mediaimage as $val){
if($val->media_type==1 || $val->media_type==4)
@unlink('uploads/'.$val->url);
}

$this->db->where('user_id',$userid);
$this->db->delete('user_media');

$profimage=$this->getUser($userid);
if($profimage[0]->profile_pic!='')
@unlink('uploads/'.$profimage[0]->profile_pic);

$this->db->where('user_id',$userid);
$this->db->delete('users');
}
public function getUser($id){
$q=$this->db->get_Where('users',array('user_id'=>$id));
return $q->result();
}

public function getUserMedia($id){
$q=$this->db->get_Where('user_media',array('user_id'=>$id));
return $q->result();
}
public function updateAdmininfo($data,$id)
{
 $this->db->where('id',$id);
 $this->db->update('admin',$data);   
}
public function getUserTestimonial()
{
$q=$this->db->get_Where('testimonial');
 return $q->result();
}
public function getTestimonial()
{
$q=$this->db->get_Where('testimonial',array('seen'=>0));
 return $q->result();
}
public function getUsers()
{
$q=$this->db->get_where('users',array('seen'=>0));
return $q->result();
}
public function updateUserSeen($data)
{
   $this->db->where('seen',0);
   $this->db->update('users',$data); 
}
     public function eventsforGraph()
{
    $q=$this->db->get('events');
 return $q->result();
}
public function shopsforGraph()
{
    $q=$this->db->get('shops');
 return $q->result();
}
public function groupforGraph()
{
    $q=$this->db->get('groups');
 return $q->result();
}
 public function getArtist()
{
 $q=$this->db->get_where('users',array('role_id'=>1));
return $q->result(); 
}
public function getnewReviews()
{
 $q=$this->db->get_where('reviews',array('seen'=>0));
return $q->result(); 
}
    public function updateReviewSeen($data)
{
   $this->db->where('seen',0);
   $this->db->update('reviews',$data); 
}
 public function updateTestiSeen($data)
{
   $this->db->where('seen',0);
   $this->db->update('testimonial',$data); 
}
}
