<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reviewmodel extends CI_Model
{

public function getReviewsaddedTo()
{ 
$this->db->select('reviews.review_id,reviews.review,reviews.added_by,reviews.description,users.user_id,users.first_name,users.last_name');
        $this->db->from('reviews');
         $this->db->join('users ','users.user_id=reviews.user_id');
        $this->db->order_by('reviews.review_id','DESC');
           $query= $this->db->get();
		$data_array = $query->result(); $i = 0;
         foreach($data_array as $result_data){
			$data_array[$i]->added_by_user = $this->getReviewsaddedBy($result_data->added_by);
		$i++;}
return $data_array;
}
public function getReviewsaddedBy($user_id)
{ 
$this->db->select('first_name,last_name');
        $this->db->from('users')->where('user_id',$user_id);
           $query= $this->db->get();
if(count($query->result())>0){ 
         return $query->row_array()['first_name']." ".$query->row_array()['last_name'];
}else{
return $array = "";
}
}
public function delete($id)
{
    $this->db->where('review_id',$id);
        return $this->db->delete('reviews');
}
    public function getallReviews()
    {
        $query=$this->db->get('reviews');
        return $query->result();
    }
}
?>
