<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rolemodel extends CI_Model
{
   


public function getAllRoleDetails($role){
$wher="((active='1' OR active='2' ) AND role_id=".$role.")";
$this->db->where($wher);
$this->db->order_by('sub_role_id','DESC');
$query = $this->db->get('user_sub_role');
return $query->result();
}

public function getRoleDetails($id){
 $q=$this->db->get_Where('user_sub_role',array('sub_role_id'=>$id));
return $q->result();
}

public function editrole($id,$data){
 $this->db->where('sub_role_id', $id);
 $this->db->update('user_sub_role', $data);
}

public function editroleinusers($oldrole,$datanew){
 $this->db->where('sub_role_id',$oldrole);
 $this->db->update('users', $datanew);
}

public function deleterole($id){
    $this->db->where('sub_role_id', $id);
    $this->db->delete('user_sub_role');
}

public function addrole($data){
$this->db->insert('user_sub_role', $data);
}

public function changerolestatus($subroleid,$data){
 $this->db->where('sub_role_id',$subroleid);
 $this->db->update('user_sub_role', $data);
}

public function DuplicateEntry($rolename){
$q=$this->db->get_Where('user_sub_role',array('sub_role'=>$rolename));
return $q->result();
}


}
