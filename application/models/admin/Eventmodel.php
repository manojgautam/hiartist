<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Eventmodel extends CI_Model
{

 public function addevent($data){
$this->db->insert('events',$data);
}

public function editEvent($id,$data){
$this->db->where('event_id', $id);
$this->db->update('events', $data);
}

public function deleteEvent($id){

$eventimage=$this->getEventDetails($id);
if($eventimage[0]->image!='')
@unlink('uploads/'.$eventimage[0]->image);

$this->db->where('event_id', $id);
$this->db->delete('events');
}

public function getAllEvents(){
$this->db->order_by('event_id','DESC');	
$q=$this->db->get('events');
return $q->result();
}

public function getEventDetails($id){
 $q=$this->db->get_Where('events',array('event_id'=>$id));
return $q->result();
}

}