<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mediamodel extends CI_Model
{


public function getAllMedia($id){
$this->db->order_by('home_id','DESC');
 $q=$this->db->get_Where('homepage_media',array('media_type'=>$id));
return $q->result();
}

public function addMedia($data){
$this->db->insert('homepage_media', $data);
}


public function getEditMedia($mediaID){
 $q=$this->db->get_Where('homepage_media',array('home_id'=>$mediaID));
return $q->result();
}


public function editMedia($mediaID,$data){
 $this->db->where('home_id',$mediaID);
 $this->db->update('homepage_media', $data);
}

public function deleteMedia($mediaid){

$eventimage=$this->getEditMedia($mediaid);
if($eventimage[0]->path!='' && $eventimage[0]->media_type!=2)
@unlink('uploads/'.$eventimage[0]->path);

    $this->db->where('home_id', $mediaid);
    $this->db->delete('homepage_media');
}

}
