<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Artist extends CI_Model
{
   
		public function getListing($role_id){
			   $q=$this->db->get_Where('user_sub_role', array('role_id'=>$role_id , 'active'=>1));
			return $q->result_array();
		}

		public function getAllRole(){
			   $q=$this->db->get_Where('user_role',array('active'=>1));
			return $q->result();
		}

		public function getRoleId($selectRole){
			$q=$this->db->get_Where('user_role', array('role'=>$selectRole));
			return $q->result();

		}
		public function insertUser($data){
			$this->db->insert('users', $data);
            return $this->db->insert_id();
		}

       public function userExist($mail){
			$q=$this->db->get_Where('users', array('email'=>$mail));
			return $q->result();
		}
		
		public function addNewSubrole($ro_field, $ro_id){
			$data = array(
			'sub_role' => $ro_field,
			'role_id' => $ro_id,
			'active' => 0
	        );
			$this->db->insert('user_sub_role', $data);
			$returnid=$this->db->insert_id();
			if($ro_id==1)
			$arr=array("Ambitious","Flexibility","Observant");
		    if($ro_id==2)
			$arr=array("Communication","Imagination","Innovation");
		    foreach($arr as $val)
            {$ndata = array(
			'sub_role_id' => $returnid,
			'expertise' =>$val,
			'created_at' => date('Y-m-d H:i:s')
	        );
	        $this->db->insert('all_expertise', $ndata);}
			return $returnid;
		}
    public function updateUserActive($id,$data)
    {
        $this->db->where('user_id',$id);
        $this->db->update('users',$data);
    }
    public function getlastuserdeatil($last_id)
    {
       $q=$this->db->get_Where('users', array('user_id'=>$last_id));
			return $q->result(); 
    }

}
