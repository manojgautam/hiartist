<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller
{

 var $sociallogin;

     public function __construct()
	{
		parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
  $this->load->model('Artist', 'artist');
  $this->load->model('admin/AdminLogin', 'loginadmin');
  $this->load->model('front/Homemodel','home');
 
   
  $this->load->helper('string');
  
 	$this->load->library('facebook');
    
         }



    public function isLoggedUser()
    {
      if($this->session->userdata('usersession') != '')
      {
        return true;
     }
      else
     {
      return false;
     }
    }

   public function isLoggedAdmin()
    {
      if($this->session->userdata('adminsession') != '')
      {
        return true;
     }
      else
     {
      return false;
     }
    }


 /* Function is used to login with facebook and google. */
   public function social_login()
   {

            $login_array= array();
      
            /*  FB LOGIN SCRIPT STARTS HERE!  */
         $social_user = $this->facebook->is_authenticated(); // Get Facebook User
       
       	// Check if user is logged in
		if($social_user){
			// Get user facebook profile details
			$data['user_profile'] = $this->facebook->request('get', '/me?fields=friends,id,name,birthday,email,first_name,last_name,about,gender,location,picture.width(800).height(800)');

     $this->session->unset_userdata('fbsessn');

if(isset($data['user_profile']['error']))
{redirect(base_url(),'refresh');}


                    $first_name = $data['user_profile']['first_name'];
                    $last_name  = $data['user_profile']['last_name'];
                    $fblogin_id = $data['user_profile']['id'];
                    $emaill   = $data['user_profile']['email'];
                    $gender   = $data['user_profile']['gender'];
                    $password = random_string('alnum', 6);
       
                 $data = array(
'first_name' => $first_name,
'last_name' => $last_name ,
'email' => $emaill,
'password' => md5($password),
'access_token'=>md5($emaill.$password),
'active'=>0,
'profile_pic' => $data['user_profile']['picture']['data']['url'],
'gender' => ucfirst($gender),
'facebook_id'=>$fblogin_id,
'created_on' => date('Y-m-d H:i:s'),
'updated_on' => date('Y-m-d H:i:s')
);


                    $fbuser = $this->facebooklogin($data,$fblogin_id);
                    
                    if($fbuser == 1)
                    {   
                         redirect(base_url()."Welcome/AddMore");
                          
                    }
              
                $fb_login_url ='';
		}else{
        
         $fb_login_url = $this->facebook->login_url();
           
        }
		
            $login_array = array($fb_login_url);
       
            /*  FB LOGIN SCRIPT ENDS HERE!  */

            /*  Login With Google Starts  */
      
            if ($social_user == 0) 
            {

                $this->load->library('Google'); 
                $googleobj = new Google();
                $new_googleobj =   $googleobj->googlelog();    

                if (isset($_SESSION['google_access_token']) && $_SESSION['google_access_token']) 
                {
                    $googleuser = $new_googleobj;
                    
                    if($googleuser)
                    {    
                            $usermail =  $googleuser->email;
                            $user_first_name = $googleuser->givenName;
                            $user_last_name =  $googleuser->familyName;
                            $google_user_id = $googleuser->id;
                            $user_name = $googleuser->name;
                            //$user_gender = $googleuser->gender;
                            $user_gender = '';
                            $user_image = $googleuser->picture;
                            $password = random_string('alnum', 6);
                     
                          
$data = array(
'first_name' => $user_first_name,
'last_name' => $user_last_name ,
'email' => $usermail,
'password' => md5($password),
'access_token'=>md5($usermail.$password),
'active'=>0,
'profile_pic' => $user_image,
'gender' => $user_gender,
'google_id'=>$google_user_id,
'created_on' => date('Y-m-d H:i:s'),
'updated_on' => date('Y-m-d H:i:s')
);
                            $google_user = $this->google_login($data,$usermail);

                            if($google_user == 2)
                            {  
                             redirect(base_url()."Welcome/AddMore");
                            }
                         }
                         $googlelogin_url = '';
                } 
                else 
                {
                  $googlelogin_url = $new_googleobj;
                }


                /*  Login With Google ENDs  */



            }
            else
            {
                    $googlelogin_url = '';
            }
            array_push($login_array,$googlelogin_url);
            return $login_array;
   }
   
   
   /* Function is used for the insert the new user loggdin with Facebook  */
  public function facebooklogin($data,$fbid)
  {  
            $getfbuser = $this->artist->userExist($data['email']);
            
             if(count($getfbuser) == 0)
            {  
                $url = $data['profile_pic'];
                $fullname = basename($url);
                $name=explode('?',$fullname);
                file_put_contents("uploads/$name[0]", file_get_contents($url));
                $data['profile_pic']=$name[0];
                
                $user_id = $this->artist->insertUser($data);
    if ($user_id) 
    {
            
                $userdata = array (
                        'user_id'=> $user_id,
                        'name'=>$data['first_name']." ".$data['last_name'],
                        'profile_pic'=>$data['profile_pic'],
                        'created_on'=>$data['created_on']
                   );
         $this->session->set_userdata('usersession',$userdata);
                    $social_user = 0;


        
              $this->facebook->destroy_session();
                     return 1;  
    }
            }
            else
            {  

              
                $fbuserid = $getfbuser[0]->user_id;
                
               $userdata = array (
                        'user_id'=> $fbuserid,
                        'name'=>$getfbuser[0]->first_name." ".$getfbuser[0]->last_name,
                        'profile_pic'=>$getfbuser[0]->profile_pic,
                        'created_on'=>$getfbuser[0]->created_on
                   );
         $this->session->set_userdata('usersession',$userdata);
               $social_user = 0;
            $this->facebook->destroy_session();
                 return 1;  
            }
  }

/*  Function is used for the insert the new user loggdin with Google  */
  public function google_login($data,$mailid)
  {   
      $getuser = $this->artist->userExist($mailid);
    if(count($getuser) == 0)
    { 
                $url = $data['profile_pic'];
                //$fullname = basename($url);
                $fullname=random_string('alnum',50).".jpg";
                file_put_contents("uploads/$fullname", file_get_contents($url));
                $data['profile_pic']=$fullname;

      $user_id = $this->artist->insertUser($data);
      
      if ($user_id) 
      {
        $userdata = array (
                      'user_id'=> $user_id,
                        'name'=>$data['first_name']." ".$data['last_name'],
                        'profile_pic'=>$data['profile_pic'],
                        'created_on'=>$data['created_on']
                      );
              
              $this->session->set_userdata('usersession',$userdata);
              }

  unset($_SESSION['google_access_token']);
      
                return 2;
    }
    else
    {   
         $g_userid = $getuser[0]->user_id;
         $userdata = array (
                          'user_id'=> $g_userid,
                        'name'=>$getuser[0]->first_name." ".$getuser[0]->last_name,
                        'profile_pic'=>$getuser[0]->profile_pic,
                        'created_on'=>$getuser[0]->created_on
                       
                      );
              
              $this->session->set_userdata('usersession',$userdata);
               unset($_SESSION['google_access_token']);
              return 2;
       
    }
  }

public function globalmailfunction($email,$subject,$message)
{
     $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'karan.techindustan@gmail.com',
            'smtp_pass' => 'karan@123',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1',
            'wordwrap' => TRUE
        );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->set_mailtype("html");
            $this->email->from('karan.techindustan@gmail.com', 'Hiartist');
            $this->email->to($email);
            $this->email->subject($subject);
            $this->email->message($message);
            $this->email->send();
}

}
