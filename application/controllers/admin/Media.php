<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media extends MY_Controller
{

 public function __construct()
    {
        parent::__construct();
      $this->load->model('admin/Mediamodel', 'media');
      if (!$this->isLoggedAdmin()) {
            redirect(base_url() . 'admin/login');
        }
    }
   
    public function index()
    {

        
    }

    public function home($id)
    { 
       $data['details']=$this->loginadmin->getDetails();
        if($id==1){$data['title']='Banner';}
        if($id==2){$data['title']='Videos';}
        if($id==3){$data['title']='Ads';}
        if($id==4){$data['title']='Logos';}
       $data['media_type']=$id;
       $data['homemedia']=$this->media->getAllMedia($id);

     if($this->input->post('bulk_delt') == TRUE){
         if($this->input->post('hiddendel')==1){
          foreach($this->input->post('checked_events') as $media_id){
          $this->media->deleteMedia($media_id);
           }
          }
        $this->session->set_flashdata('deleteit',"Deleted successfully!");
       redirect(base_url()."admin/media/home/".$id);
       }

       $this->load->view('admin/head',$data);
       $this->load->view('admin/medialist',$data);
       $this->load->view('admin/foot');
    }


   public function addmedia($mediatype)
    {  $data['mediatype']=$mediatype;
       if ($this->input->post('submit') == TRUE) {
$description='';
if($mediatype!=2){
if($mediatype==3){$description=$this->input->post('desc');}
        $config['upload_path']='./uploads';
        $config['allowed_types']='jpg|jpeg|png|JPG|JPEG|PNG';
        $config['file_name']=random_string('alnum', 50).".jpg";
        $this->load->library('upload',$config);
        if(!$this->upload->do_upload())
        { $imageORvideo='';}
        else
        { $imgdata=$this->upload->data();
        $imageORvideo=$imgdata['file_name'];  }
}
else
{
$imageORvideo=$this->input->post('url');
}

    
            $data          = array(
                'title' => $this->input->post('title'),
                'path'=>$imageORvideo,
                'media_type'=>$mediatype,
                'description'=>$description
               );
            $this->media->addMedia($data);
            $this->session->set_flashdata('addedit',"Added Successfully");
            redirect(base_url()."admin/media/home/".$mediatype);
          }
       echo $this->load->view('admin/addmedia',$data,TRUE);
    }

public function editmedia($mediatype,$mediaID)
    {  $data['mediatype']=$mediatype;
       $data['mediaid']=$mediaID;
       $data['editdata']=$this->media->getEditMedia($mediaID);
       if ($this->input->post('submit') == TRUE) {
$description='';
if($mediatype!=2){
if($mediatype==3){$description=$this->input->post('desc');}
        $config['upload_path']='./uploads';
        $config['allowed_types']='jpg|jpeg|png|JPG|JPEG|PNG';
        $config['file_name']=random_string('alnum', 50).".jpg";
        $this->load->library('upload',$config);
        if(!$this->upload->do_upload())
        { $imageORvideo=$this->input->post('hiddenname');}
        else
        { $imgdata=$this->upload->data();
        $imageORvideo=$imgdata['file_name'];  }
}
else
{$imageORvideo=$this->input->post('url');}

       
            $data          = array(
                'title' => $this->input->post('title'),
                'path'=>$imageORvideo,
                'media_type'=>$mediatype,
                'description'=>$description
               );
            $this->media->editMedia($mediaID,$data);
            $this->session->set_flashdata('addedit',"Edited Successfully");
            redirect(base_url()."admin/media/home/".$mediatype);
          }
       echo $this->load->view('admin/editmedia',$data,TRUE);
    }

public function deletemedia($mediatype,$mediaid){
    $this->media->deleteMedia($mediaid);
    $this->session->set_flashdata('deleteit',"Deleted successfully!");
    redirect(base_url()."admin/media/home/".$mediatype);
}

}
