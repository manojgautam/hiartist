<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller
{

 public function __construct()
    {
        parent::__construct();
      $this->load->model('admin/Usermodel', 'user');
      if (!$this->isLoggedAdmin()) {
            redirect(base_url() . 'admin/login');
        }
    }
   
    public function index()
    {

        
    }

    
    public function ListUsers()
    {
       $data['details']=$this->loginadmin->getDetails();
       $data['title']='Users';
        $udata=array(
        'seen'=>1);
        $this->user->updateUserSeen($udata);
       $data['users']=$this->user->getAllUsers();

       if($this->input->post('bulk_delt') == TRUE){
         if($this->input->post('hiddendel')==1){
          foreach($this->input->post('checked_events') as $user_id){
           $this->user->deleteUser($user_id);}
          }
        $this->session->set_flashdata('deleteit',"Deleted successfully!");
       redirect(base_url()."admin/users/ListUsers");
       }

       $this->load->view('admin/head',$data);
       $this->load->view('admin/userslist',$data);
       $this->load->view('admin/foot');
        
    }

    public function changestatus($userid,$status)
    { $this->user->changeUserStatus($userid,$status);
      $this->session->set_flashdata('addedit',"Status changed successfully!");
      redirect(base_url()."admin/users/ListUsers");
     }

    public function deleteuser($userid)
    {
      $this->user->deleteUser($userid);
      $this->session->set_flashdata('deleteit',"Deleted successfully!");
      redirect(base_url()."admin/users/ListUsers");
    }

}
