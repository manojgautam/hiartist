<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends MY_Controller
{

 public function __construct()
    {
        parent::__construct();
      $this->load->model('admin/Rolemodel', 'role');
      if (!$this->isLoggedAdmin()) {
            redirect(base_url() . 'admin/login');
        }
    }
   
    public function index()
    {

        
    }

    public function checkDuplicate(){
     $rolename=$this->input->post('rolename');
     $value=$this->role->DuplicateEntry($rolename);
     if(count($value)){echo 'notadd';}
     else{echo 'add';}
    }

    public function changestatus($type,$subroleid,$status){
            $data=array('active'=>$status);
            $this->role->changerolestatus($subroleid,$data);
            $this->session->set_flashdata('addedit',"Status Changed Successfully");
            if($type==1)
            redirect(base_url()."admin/role/artists");
            if($type==2)
            redirect(base_url()."admin/role/directors");
            
   }

     public function artists()
    {   $data['details']=$this->loginadmin->getDetails();
        $data['artistdetails']=$this->role->getAllRoleDetails($role=1);
        $data['title']='Artists';

 if($this->input->post('bulk_delt') == TRUE){
     

         if($this->input->post('hiddendel')==1){
          foreach($this->input->post('checked_events') as $artist_id){
           $this->role->deleterole($artist_id);}
          }
    
       $this->session->set_flashdata('deleteit',"Deleted successfully!");
       redirect(base_url()."admin/role/artists");
    }


        $this->load->view('admin/head',$data);
        $this->load->view('admin/artistlist');
        $this->load->view('admin/foot');
        
    }
    
      public function editartist($id)
    {   
        $data['subrole']=$this->role->getRoleDetails($id);
        $data['id']=$id;
       if ($this->input->post('submit') == TRUE) {

            $artistrole = trim($this->input->post('artistrole'));
            $data          = array(
                'sub_role' => $artistrole
            );
        if($this->input->post('artistrole')!=$this->input->post('hiddenrole')){
 $datanew=array('sub_role_id'=>trim($this->input->post('artistrole')));
 $this->role->editroleinusers($this->input->post('hiddenrole'),$datanew);
}
            $this->role->editrole($id,$data);
            $this->session->set_flashdata('addedit',"Edited Successfully");
            redirect(base_url()."admin/role/artists");
          }
       echo $this->load->view('admin/editartist',$data,TRUE);
    }

      public function addartist()
    {   $data['title']='addartist';
       if ($this->input->post('submit') == TRUE) {

            $artistrole = trim($this->input->post('artistrole'));
            $data          = array(
                'sub_role' => $artistrole,
                'role_id'=>1,
                'active'=>1,
                'created_on'=>date("Y-m-d H:i:s")
            );
            $this->role->addrole($data);
            $this->session->set_flashdata('addedit',"Added Successfully");
            redirect(base_url()."admin/role/artists");
          }
       echo $this->load->view('admin/editartist',$data,TRUE);
    }


public function deleteartist($id){
    $this->role->deleterole($id);
        $this->session->set_flashdata('deleteit',"Deleted successfully!");
       redirect(base_url()."admin/role/artists");
}

 public function directors()
    {   $data['details']=$this->loginadmin->getDetails();
        $data['directordetails']=$this->role->getAllRoleDetails($role=2);
        $data['title']='Directors';

 if($this->input->post('bulk_delt') == TRUE){
     

         if($this->input->post('hiddendel')==1){
          foreach($this->input->post('checked_events') as $director_id){
           $this->role->deleterole($director_id);}
          }
    
       $this->session->set_flashdata('deleteit',"Deleted successfully!");
       redirect(base_url()."admin/role/directors");
    }


        $this->load->view('admin/head',$data);
        $this->load->view('admin/directorlist');
        $this->load->view('admin/foot');
        
    }

    
      public function adddirector()
    {   $data['title']='adddirector';
       if ($this->input->post('submit') == TRUE) {

            $directorrole = trim($this->input->post('directorrole'));
            $data          = array(
                'sub_role' => $directorrole,
                'role_id'=>2,
                'active'=>1,
                'created_on'=>date("Y-m-d H:i:s")
            );
            $this->role->addrole($data);
            $this->session->set_flashdata('addedit',"Added Successfully");
            redirect(base_url()."admin/role/directors");
          }
       echo $this->load->view('admin/addeditdirector',$data,TRUE);
    }

    public function editdirector($id)
    {   
        $data['subrole']=$this->role->getRoleDetails($id);
        $data['id']=$id;
       if ($this->input->post('submit') == TRUE) {

            $directorrole = trim($this->input->post('directorrole'));
            $data          = array(
                'sub_role' => $directorrole
            );
          if($this->input->post('directorrole')!=$this->input->post('hiddenrole')){
 $datanew=array('sub_role_id'=>trim($this->input->post('directorrole')));
 $this->role->editroleinusers($this->input->post('hiddenrole'),$datanew);
}
            $this->role->editrole($id,$data);
            $this->session->set_flashdata('addedit',"Edited Successfully");
            redirect(base_url()."admin/role/directors");
          }
       echo $this->load->view('admin/addeditdirector',$data,TRUE);
    }

    
    public function deletedirector($id){
    $this->role->deleterole($id);
        $this->session->set_flashdata('deleteit',"Deleted successfully!");
       redirect(base_url()."admin/role/directors");
}

}
