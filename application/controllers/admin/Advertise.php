<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Advertise extends MY_Controller
{

 public function __construct()
    {
        parent::__construct();
      $this->load->model('admin/Advertisemodel', 'advert');
      if (!$this->isLoggedAdmin()) {
            redirect(base_url() . 'admin/login');
        }
    }
   
    public function index()
    {

        
    }

 public function Listadvertise(){
       $data['details']=$this->loginadmin->getDetails();
       $data['title']='Advertise';
       $data['users']=$this->advert->Listadv();

       if($this->input->post('bulk_delt') == TRUE){
       if($this->input->post('hiddendel')==1){
          foreach($this->input->post('checked_events') as $add_id){
           $this->advert->deleteAdv($add_id);}
          }
        $this->session->set_flashdata('deleteit',"Deleted successfully!");
       redirect(base_url()."admin/Advertise/Listadvertise");
       }

       $this->load->view('admin/head',$data);
       $this->load->view('admin/advertise',$data);
       $this->load->view('admin/foot');
    }

  public function deleteAdv($add_id){
    $this->advert->deleteAdv($add_id);
    $this->session->set_flashdata('deleteit',"Deleted successfully!");
    redirect(base_url()."admin/Advertise/Listadvertise");
}

}
