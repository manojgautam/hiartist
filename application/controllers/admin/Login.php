<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
     public function __construct()
	{
		parent::__construct();
              $this->load->model('admin/AdminLogin', 'adminlogin');
  		
    
         }

	public function index()
	{
  
		$this->load->view('admin/header');
		$this->load->view('admin/login');
		$this->load->view('admin/footer');
	}



/* Login Function */
   public function logout(){
                unset($_SESSION['adminsession']);
       			redirect(base_url().'admin/login');

}

      public function login()
	{  
		$this->form_validation->set_rules('emailorname', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == TRUE)
		{
			 $email=trim($this->input->post('emailorname')); 
	        $password=$this->input->post('password');

	       
	        $data= array(
			    'email' => $email,
			    'password'=>$password			 
	                  );
	               $getval=$this->adminlogin->checkdetails($data);


          	if(count($getval) == 0)
          	{
            	$this->session->set_flashdata('msg','Email or password does not match');
            	redirect(base_url()."admin/login");
            }
	        else{
            	$userdata = array (
                        'id'=> $getval[0]->id,
                        'name'=>$getval[0]->name,
                        'email' => $getval[0]->email,
                        'image'=>$getval[0]->image
                    );
            	$this->session->set_userdata('adminsession',$userdata);
       			redirect(base_url().'admin/dashboard');
			}

		}
		else
		{ 
			$this->load->view('admin/login');
			
		}    
		
	}




}
