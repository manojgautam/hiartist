<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonial extends MY_Controller
{

 public function __construct()
    {
        parent::__construct();
      $this->load->model('admin/testimonialmodel', 'testimonial');
           $this->load->model('admin/Usermodel', 'user');
     
 }
     public function index()
    {
     }
  public function listTestimonial()
    {
        $data['title']='Testimonials';
         $data['details']=$this->loginadmin->getDetails();

     $tdata=array(
        'seen'=>1);
     $this->user->updateTestiSeen($tdata);

        $data['testimonials']=$this->testimonial->getTestimonial();
        if($this->input->post('bulk_delt') == TRUE){
         if($this->input->post('hiddendel')==1){
          foreach($this->input->post('checked_testimonial') as $id){
           $this->testimonial->delete($id);}
          }
         $this->session->set_flashdata('delete',"Deleted successfully!");
         redirect(base_url()."admin/Testimonial/listTestimonial");
       }
         $this->load->view('admin/head',$data);
       $this->load->view('admin/testimonialList',$data);
       $this->load->view('admin/foot');
    }
    public function deleteTestimonial($id) 
    { 
    $this->testimonial->delete($id);
         $this->session->set_flashdata('delete',"Deleted successfully!");
    redirect(base_url()."admin/Testimonial/listTestimonial");
  }
   public function changeStatus($testimonialId,$status)
   {
       $this->testimonial->changeTestimonialStatus($testimonialId,$status);
        $this->session->set_flashdata('status',"Status changed successfully!");
        redirect(base_url()."admin/Testimonial/listTestimonial");
   }
}
