<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Review extends MY_Controller
{

 public function __construct()
    {
        parent::__construct();
     $this->load->model('admin/testimonialmodel', 'testimonial');
     $this->load->model('admin/reviewmodel', 'review');
     $this->load->model('admin/Usermodel', 'user');
       
 }
public function index()
{
}
public function reviewsList()
{

        $data['title']='Reviews';
        $data['details']=$this->loginadmin->getDetails();
     $rdata=array(
        'seen'=>1);
     $this->user->updateReviewSeen($rdata);
        $data['reviewsaddTo']=$this->review->getReviewsaddedTo();
         if($this->input->post('bulk_delt') == TRUE){
         if($this->input->post('hiddendel')==1){
          foreach($this->input->post('checked_review') as $id){
           $this->review->delete($id);}
          }
         $this->session->set_flashdata('delete',"Deleted successfully!");
          redirect(base_url()."admin/Review/reviewsList");
       }
      $this->load->view('admin/head',$data);
       $this->load->view('admin/reviewlist',$data);
       $this->load->view('admin/foot');
    }
 public function deleteReview($id) 
    { 
    $this->review->delete($id);
    $this->session->set_flashdata('delete',"Deleted successfully!");
    redirect(base_url()."admin/Review/reviewsList");
}
}
?>
