<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

 public function __construct()
    {
        parent::__construct();
     $this->load->model('admin/Usermodel', 'user');
     $this->load->model('admin/Reviewmodel', 'review');
       if (!$this->isLoggedAdmin()) {
            redirect(base_url() . 'admin/login');
        }
     
    }

    public function index()
    {
        $users =$this->user->getAllUsers();
        $data['user_count']=count($users);
        $testimonail=$this->user->getUserTestimonial();
        $data['user_testimonial']=count($testimonail);
        $user=$this->user->getUsers();
        $data['usercount']=count($user);
        $testi=$this->user->getTestimonial();
        $data['testi_count']=count($testi);
        $reviews=$this->review->getallReviews();
        $data['user_reviews']=count($reviews);
        $artist=$this->user->getArtist();
        $data['artist_count']=count($artist);
        $events=$this->user->eventsforGraph();
        $data['event_count']=count($events);
        $shops=$this->user->shopsforGraph();
        $data['shop_count']=count($shops);
        $groups=$this->user->groupforGraph();
        $data['group_count']=count($groups);
        $Reviewnew=$this->user->getnewReviews();
        $data['new_review_count']=count($Reviewnew);
        $data['details']=$this->loginadmin->getDetails();
        $data['title']='Admin Dashboard';
        $this->load->view('admin/head',$data);
        $this->load->view('admin/index',$data);
        $this->load->view('admin/foot');
        
    }

public function chkoldpwd(){
$pwd=$this->input->post('password');
$getresult=$this->loginadmin->chkpwd($pwd);
if(count($getresult)){echo "exist";}
else{echo "notexist";}
}


  public function editProfile()
    {
if($this->input->post('update')==TRUE){
$config['upload_path']='./uploads';
$config['allowed_types']='jpg|jpeg|png|JPG|JPEG|PNG';
$config['file_name']=random_string('alnum', 50).".jpg";
$this->load->library('upload',$config);
        if($this->upload->do_upload())
        {
          $imgdata=$this->upload->data();
           $image=$imgdata['file_name'];
         }
        else
        {  
         $image=$this->input->post('hidden');
       }

if($this->input->post('newpassword')){$pass=md5($this->input->post('newpassword'));}
else{$pass=$this->input->post('chkoldpass');}
$data=array(
'name'=>$this->input->post('name'),
'email'=>$this->input->post('email'),
'password'=>$pass,
'image'=>$image
);
$this->loginadmin->updateDetails($data);
redirect(base_url()."admin/dashboard");
}
$data['details']=$this->loginadmin->getDetails();
echo $this->load->view('admin/editprofile',$data,TRUE);
}
public function addAdminaddress()
{     
    
       $data['title']="admin Address Info";
       $data['details']=$this->loginadmin->getDetails();
     $this->load->view('admin/head',$data);
        $this->load->view('admin/addaddress');
        $this->load->view('admin/foot');
}
public function updateAdminInfo()
{
    if($this->input->post('submit')==true)
    {
        $id=$_POST['id'];
        $data=array(
        'street_address'=>$this->input->post('street_address'),
        'city'=>$this->input->post('city'),
        'state'=>$this->input->post('state'),
        'country'=>$this->input->post('country'),
        'weekst'=>$this->input->post('weekst'),
         'weeket'=>$this->input->post('weeket'),
   'latitude'=>$this->input->post('latitude'),
            'longitude'=>$this->input->post('longitude'),
            'weekendst'=>$this->input->post('weekendst'),
            'weekendet'=>$this->input->post('weekendet'),
         'phone1'=>$this->input->post('phone_no'),
            'phone2'=>$this->input->post('mobile_no'),
               'facebook'=>$this->input->post('facebook'),
             'google'=>$this->input->post('google'),
             'twitter'=>$this->input->post('twitter'),
             'youtube'=>$this->input->post('youtube')
        );
        $this->user->updateAdmininfo($data,$id);
        redirect(base_url()."admin/dashboard/addAdminaddress/".$id);
    }
}
    public function UpdateSeen()
    {
     $data=array(
        'seen'=>1);
     $this->user->updateUserSeen($data);
        redirect(base_url()."admin/users/ListUsers"); 
  }
 public function UpdatereviewSeen()
    {
     $data=array(
        'seen'=>1);
     $this->user->updateReviewSeen($data);
        redirect(base_url()."admin/Review/reviewsList"); 
  }
 public function updateSeentestimonial()
 {
     $data=array(
        'seen'=>1);
     $this->user->updateTestiSeen($data);
        redirect(base_url()."admin/testimonial/listTestimonial");
 }


}
