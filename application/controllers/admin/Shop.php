<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends MY_Controller
{
 public function __construct()
	{
		parent::__construct();     
          $this->load->model('front/Shopmodel', 'shop');
      
    }
public function shop()
{
    $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		 $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
        $data['googlelogin'] = $sociallogin[1];
     $id=$this->uri->segment(2);
    $data['id']=$id;
    $data['count']=$this->shop->getshopcount($id);
  $data['shop']=$this->shop->get_shop_list($id);
  $data['disrtict']=$this->shop->get_destrict_from_id($id);
     $data['shop_count']=count($data['shop']);
$data['artist']=$this->home->get_artist_front_menu();
       $data['director']=$this->home->get_director_front_menu();
    $data['groups']=$this->home->get_groups_front_menu();
        $data['state']=$this->shop->get_states_list();
    $this->load->view('front/header',$data);
       $this->load->view('front/shoplist',$data);
        $this->load->view('front/footer');
}
public function Viewshop()
    {
     $id=$this->uri->segment(2);
    echo $id;die;
    $data['artist']=$this->home->get_artist_front_menu();
    $data['director']=$this->home->get_director_front_menu();
    $data['groups']=$this->home->get_groups_front_menu();
    $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
       
        $this->load->view('front/header',$data);
        $this->load->view('front/shopview');
        $this->load->view('front/footer');
    }   
public function load_more()
{
    
    $limt=$_POST['limit'];
    $id=$_POST['id'];
    $data['shop']=$this->shop->get_shop_list($id,$limt);
    $data['disrtict']=$this->shop->get_destrict_from_id($id);
  echo $this->load->view('front/load_more_shop',$data,true);
 }


public function load_more_aftershopfilter()
{
    
    $limt=$_POST['limit'];
    $statid=$_POST['statid'];
    $shpid=$_POST['shpid'];
    if($shpid!='')
    {$data['shop']=$this->shop->get_shop_list($shpid,$limt);
    $data['disrtict']=$this->shop->get_destrict_from_id($shpid);
  echo $this->load->view('front/load_more_shop_ajax',$data,true);}
  else{

    $shops=$this->shop->get_shop_list_ajax($statid,$limt);
$i=0;
foreach($shops as $val){
  $dist=$this->shop->get_destrict_from_id($val->district_id);
$shops[$i]->districtname=$dist[0]->name;
$i++;
}
$data['shop']=$shops;
$data['districtlist']=$this->shop->get_district_list_for_selected_state($statid);
echo $this->load->view('front/shoplistajaxstate',$data,true);
}
 }

public function onchangeval(){

    $stateid=$_POST['sid'];
    $shops=$this->shop->get_shop_list_ajax($stateid);
$i=0;
foreach($shops as $val){
  $dist=$this->shop->get_destrict_from_id($val->district_id);
$shops[$i]->districtname=$dist[0]->name;
$i++;
}
$data['shopcount']=$this->shop->get_shop_list_ajax_count($stateid);
$data['shop']=$shops;
$data['districtlist']=$this->shop->get_district_list_for_selected_state($stateid);?>
<option value="">Select District</option>
<?php foreach($data['districtlist'] as $val){ ?>
<option value="<?php echo $val->id;?>"><?php echo $val->name;?></option> 
<?php }
echo "##@@##";
  echo $this->load->view('front/shoplistajax',$data,true);
}

public function onchangevalshop(){

    $shopid=$_POST['sid'];
    $data['shop']=$this->shop->get_shop_list($shopid);
    $data['shopcount']=$this->shop->getshopcount($shopid);
    $data['disrtict']=$this->shop->get_destrict_from_id($shopid);
  echo $this->load->view('front/shoplistajaxshop',$data,true);

}
    
}
?>
	

