<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Footerpages extends MY_Controller
{

     public function __construct()
	{
		parent::__construct();
      $this->load->model('front/Profilemodel','profile');
  $this->load->model('front/Footerpagemodel','footermodel');
      $this->load->model('front/Groupmodel','group');
       ob_start();
        ob_flush();
      /* if (!$this->isLoggedUser()) {
            redirect(base_url());
        }*/
                
         }

    public function index()
    {
    }

   public function career()
    { $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		 $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
        $data['googlelogin'] = $sociallogin[1];
     $data['artist']=$this->home->get_artist_front_menu();
       $data['director']=$this->home->get_director_front_menu();
       $data['groups']=$this->group->getAllGroups();
        $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
        $data['fronttitle']="Join Us";
        $this->load->view('front/header',$data);
        $this->load->view('front/career',$data);
        $this->load->view('front/footer');
    }

  public function partner()
    { $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		 $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
        $data['googlelogin'] = $sociallogin[1];
     $data['artist']=$this->home->get_artist_front_menu();
       $data['director']=$this->home->get_director_front_menu();
        $data['groups']=$this->group->getAllGroups();
        $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
       $data['fronttitle']="Partner";
        $data['partners']=$this->profile->getPartners();
      $this->load->view('front/header',$data);
        $this->load->view('front/partner',$data);
        $this->load->view('front/footer');
    }

 public function sitemap()
    { $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		 $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
        $data['googlelogin'] = $sociallogin[1];
     $data['artist']=$this->home->get_artist_front_menu();
       $data['director']=$this->home->get_director_front_menu();
         $data['groups']=$this->group->getAllGroups();
        $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
$data['fronttitle']="Site Map";
$allartists=$this->footermodel->getallartistsORdirectors('1');
$j=0;
foreach($allartists as $val){
$allartists[$j]->alldistricts=$this->footermodel->getallArtistsDistricts($val->sub_role_id);
$j++;
}
$data['allartists']=$allartists;

$alldirectors=$this->footermodel->getallartistsORdirectors('2');
$k=0;
foreach($alldirectors as $val){
$alldirectors[$k]->alldistricts=$this->footermodel->getallArtistsDistricts($val->sub_role_id);
$k++;
}
$data['alldirectors']=$alldirectors;

$data['alldistricts']=$this->footermodel->getallthedistricts();


    
        $this->load->view('front/header',$data);
        $this->load->view('front/sitemap',$data);
        $this->load->view('front/footer');
    }


  public function advertise()
  { $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		 $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
        $data['googlelogin'] = $sociallogin[1];
     $data['artist']=$this->home->get_artist_front_menu();
       $data['director']=$this->home->get_director_front_menu();
        $data['groups']=$this->group->getAllGroups();
        $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
              $data['fronttitle']="Advertise";
         $users =$this->footermodel->RegisteredUsers();
         $data['registered_user']=count( $users);
          $artist=$this->footermodel->Artist();
          $data['registered_artist']=count($artist);
          $director=$this->footermodel->Director();
          $data['registered_director']=count($director);
         $events=$this->footermodel->events();
          $data['registered_events']=count($events);
         $shops=$this->footermodel->shops();
          $data['registered_shops']=count($shops);
          $testimonial=$this->footermodel->testimonial();
          $data['registered_testimonial']=count($testimonial);
          $data['adminContact']=$this->footermodel->Admindetail();
          //echo "<pre>";print_r( $data['adminContact']);die;
         $this->load->view('front/header',$data);
         $this->load->view('front/advertise',$data);
         $this->load->view('front/footer');
    }
    public function HowItwork()
    {
        $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		 $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
        $data['googlelogin'] = $sociallogin[1];
     $data['artist']=$this->home->get_artist_front_menu();
       $data['director']=$this->home->get_director_front_menu();
        $data['groups']=$this->group->getAllGroups();
        $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
            $data['fronttitle']="How It Works";
         $this->load->view('front/header',$data);
         $this->load->view('front/howitwork');
         $this->load->view('front/footer'); 
    }
public function advertisePage()
{         
         
          $data_email['heading']="Here is the detail of Person who want to Advertise with us";
          $data_email['message']='<table>'
'<tr><th>Name:</th>'
'<td>'.$_POST['name'].'</td></tr>'
'<tr><th>Contact:</th><td>'.$_POST['contact'].'</td></tr><tr><th>Email</th>'
              '<td>'.$_POST['email'].'</td></tr><tr><th>Description</th>'
              '<td>'.$_POST['description'].'</td></tr></table>';
        $message=$this->load->view('front/advemail',$data_email,true);
        $data=$this->footermodel->Admindetail();
        $email=$data[0]->email;
       
if($this->input->post('submit')==true)
{
 $data=array(
   'name'=>$this->input->post('name'),
   'email'=>$this->input->post('email'),
    'contact'=>$this->input->post('contact'),
    'description'=>$this->input->post('description')
);
$this->footermodel->addAdvertise($data);
 
 $this->globalmailfunction($email,"Hiartist Advetise ",$message);
$this->session->set_flashdata('Advertise','Your Mesaage submitted successfully');
redirect(base_url()."advertise");

}
}
public function careerPage()
{
          $data_email['heading']="Here is the detail of Person who want to JoinOur Team";
         // $data_email['message']="Name: ".$_POST['name']."  Contact:".$_POST['contact']." </br> Email:".$_POST['email']." </br> Description:".$_POST['description']."";
$data_email['message']='<table>
<tr><th>Name:</th>
<td>'.$_POST['name'].'</td></tr>
<tr><th>Contact:</th><td>'.$_POST['contact'].'</td></tr><tr><th>Email</th><td>'.$_POST['email'].'</td></tr><tr><th>Description</th><td>'.$_POST['description'].'</td></tr></table>';
          $message=$this->load->view('front/advemail',$data_email,true);
         $data=$this->footermodel->Admindetail();
        $email=$data[0]->email;
if($this->input->post('submit')==true)
{
 $data=array(
   'name'=>$this->input->post('name'),
   'email'=>$this->input->post('email'),
    'contact'=>$this->input->post('contact'),
    'description'=>$this->input->post('description')
);
$this->footermodel->addCareer($data);
$this->globalmailfunction($email,"Hiartist Career ",$message);
$this->session->set_flashdata('Career','Your Mesaage submitted successfully');
redirect(base_url()."career");
}
}
}
