<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skill extends MY_Controller
{

 public function __construct()
    {
        parent::__construct();
      $this->load->model('admin/Skillmodel', 'skill');
      if (!$this->isLoggedAdmin()) {
            redirect(base_url() . 'admin/login');
        }
    }
   
    public function index()
    {

        
    }

 public function expertise(){
    $data['details']=$this->loginadmin->getDetails();
        $data['expertdetails']=$this->skill->getAllExpertDetails();
        $data['title']='Expertise';

 if($this->input->post('bulk_delt') == TRUE){
     

         if($this->input->post('hiddendel')==1){
          foreach($this->input->post('checked_events') as $expert_id){
           $this->skill->deleteskill($expert_id);}
          }
    
       $this->session->set_flashdata('deleteit',"Deleted successfully!");
       redirect(base_url()."admin/Skill/expertise");
    }


        $this->load->view('admin/head',$data);
        $this->load->view('admin/expertlist');
        $this->load->view('admin/foot');
    }

      public function addskill()
    {   $data['title']='addskill';
        $data['roles']=$this->skill->getAllrolesDetails();
       if ($this->input->post('submit') == TRUE) {

            $data          = array(
                'sub_role_id' =>$this->input->post('therole'),
                'expertise'=>$this->input->post('expert'),
                'created_at'=>date("Y-m-d H:i:s")
            );
            $this->skill->addskill($data);
            $this->session->set_flashdata('addedit',"Added Successfully");
            redirect(base_url()."admin/Skill/expertise");

          }
       echo $this->load->view('admin/addskill',$data,TRUE);
    }

    public function editskill($id)
    {   $data['roles']=$this->skill->getAllrolesDetails();
        $data['getexpert']=$this->skill->getparticularSkillDetail($id);
        $data['id']=$id;
       if ($this->input->post('submit') == TRUE) {

      
            $data          = array(
                'expertise' => $this->input->post('expert')
            );
            $this->skill->editskill($id,$data);
            $this->session->set_flashdata('addedit',"Edited Successfully");
            redirect(base_url()."admin/Skill/expertise");
          }
       echo $this->load->view('admin/addskill',$data,TRUE);
    }

    public function deleteskill($id){
    $this->skill->deleteskill($id);
        $this->session->set_flashdata('deleteit',"Deleted successfully!");
       redirect(base_url()."admin/Skill/expertise");
}
}