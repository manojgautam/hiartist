<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends MY_Controller
{

 public function __construct()
    {
        parent::__construct();
      $this->load->model('admin/Careermodel', 'career');
      if (!$this->isLoggedAdmin()) {
            redirect(base_url() . 'admin/login');
        }
    }
   
    public function index()
    {

   }
 public function ListCareer()
{
      $data['details']=$this->loginadmin->getDetails();
     $data['title']='Career';
      $data['users']=$this->career->Listcareer();

   if($this->input->post('bulk_delt') == TRUE){
       if($this->input->post('hiddendel')==1){
          foreach($this->input->post('checked_events') as $career_id){
           $this->career->deleteCareer($career_id);}
          }
        $this->session->set_flashdata('deleteit',"Deleted successfully!");
       redirect(base_url()."admin/Career/ListCareer");
       }
      $this->load->view('admin/head',$data);
       $this->load->view('admin/career',$data);
       $this->load->view('admin/foot');
}
public function deletecareer($career_id){
    $this->career->deleteCareer($career_id);
    $this->session->set_flashdata('deleteit',"Deleted successfully!");
    redirect(base_url()."admin/Career/ListCareer");
}

}
