<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MY_Controller
{

     public function __construct()
	{
		parent::__construct();
      $this->load->model('front/Profilemodel','profile');

       ob_start();
        ob_flush();
      /* if (!$this->isLoggedUser()) {
            redirect(base_url());
        }*/
                
         }

    public function index()
    {
    }

    public function profile($name,$id)
    {
		$checkSession = $this->session->userdata('usersession');
		if($checkSession['user_id'] != $id){
			redirect(base_url());
		}
        $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		$data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
        $data['googlelogin'] = $sociallogin[1];
		
		$this->profile->increaseViews($id); //profile view
		
		$sociallogin = $this->social_login(); // Return Fb and google login urls array from main controller
		$data['login_url'] = $sociallogin[0]; // Login_url is used to get FB Login Url from main controller
$data['artist']=$this->home->get_artist_front_menu();
       $data['director']=$this->home->get_director_front_menu();
      $data['groups']=$this->home->get_groups_front_menu();
      $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
	 $data['profdetail'] = $this->profile->getUserProfileDetails($id); 
        $data['testimonialtext']=$this->profile->testimonial($id);

        $data['edudetail'] = $this->profile->getEDucationDetails($id);
        $data['imagedetail'] = $this->profile->getUserImages($id);
        $data['video'] = $this->profile->getUserVideos($id);
        $data['experiencedetail'] = $this->profile->getUserExperience($id);
        $data['allreviews'] = $this->profile->getallReviews($id);
        $data['physicalstats'] = $this->profile->getPhysicalStats($id);
        $data['workpref'] = $this->profile->getWorkPrefrence($id);
        $subrolename=$data['profdetail'][0]->sub_role_id;
        $subroleID = $this->profile->getIdbyName($subrolename);
        $data['getexpertiselist'] = $this->profile->getExpertiseList($subroleID[0]->sub_role_id);
$userlanguages = $data['profdetail'][0]->languages;
$userexpertise = $this->profile->getUserExpertise($id);
$arr=array();
foreach($userexpertise as $val){$arr[]=$val->expertise;}
if(count($arr)){$str=implode(',',$arr);}
else{$str='';}
        $data['skills'] = array();
        $data['skills'][]=array('lang'=>$userlanguages,'expert'=>$str);

        $this->load->view('front/header',$data);
        $this->load->view('front/profile', $data);
        $this->load->view('front/footer');
 
    }

   public function ajaxsave(){
    $Sess = $this->session->userdata('usersession');
    $key=$this->input->post('key');
    $value=$this->input->post('value');
    if($key=='dob')
     {$value=date_format(date_create($value),'Y-m-d');}
    $data=array($key=>$value);
    $this->profile->UpdateProfile($Sess['user_id'],$data);
    if($key=='first_name' || $key=='last_name'){
    $result=$this->profile->getUserProfDetails($Sess['user_id']);
    echo $result[0]->first_name.' '.$result[0]->last_name;
    }
    else
    {echo $value;}
   }

   public function educationsaveajax(){
    $Sess = $this->session->userdata('usersession');
 $data=array(
'user_id'=>$Sess['user_id'],
'course_name'=>$this->input->post('course'),
'institute_name'=>$this->input->post('institute'),
'start_date'=>date_format(date_create($this->input->post('startdate')),'Y-m-d'),
'end_date'=>date_format(date_create($this->input->post('enddate')),'Y-m-d'),
'created_at'=>date('Y-m-d H:i:s')
);
 $eduid=$this->profile->addEDucation($data);
 echo $eduid;
}

public function experiencesaveajax(){
    $Sess = $this->session->userdata('usersession');
 $data=array(
'user_id'=>$Sess['user_id'],
'project_name'=>$this->input->post('project'),
'role'=>$this->input->post('role'),
'start_date'=>date_format(date_create($this->input->post('stdate')),'Y-m-d'),
'end_date'=>date_format(date_create($this->input->post('eddate')),'Y-m-d'),
'description'=>$this->input->post('desc'),
'created_at'=>date('Y-m-d H:i:s')
);
 $expid=$this->profile->addExperience($data);
 echo $expid;
}

public function editeducationsaveajax(){
   $Sess = $this->session->userdata('usersession');
$eduid=$this->input->post('eduid');
 $data=array(
'user_id'=>$Sess['user_id'],
'course_name'=>$this->input->post('alldetails')[0],
'institute_name'=>$this->input->post('alldetails')[1],
'start_date'=>date_format(date_create($this->input->post('alldetails')[2]),'Y-m-d'),
'end_date'=>date_format(date_create($this->input->post('alldetails')[3]),'Y-m-d'),
'created_at'=>date('Y-m-d H:i:s')
);
 $this->profile->editEDucation($eduid,$data);
}


public function editexperiencesaveajax(){
   $Sess = $this->session->userdata('usersession');
$expid=$this->input->post('expid');
 $data=array(
'user_id'=>$Sess['user_id'],
'project_name'=>$this->input->post('alldetails')[0],
'role'=>$this->input->post('alldetails')[1],
'start_date'=>date_format(date_create($this->input->post('alldetails')[2]),'Y-m-d'),
'end_date'=>date_format(date_create($this->input->post('alldetails')[3]),'Y-m-d'),
'description'=>$this->input->post('alldetails')[4],
'created_at'=>date('Y-m-d H:i:s')
);
 $this->profile->editExperience($expid,$data);
}

public function deleteeducationajax(){
$delEduId=$this->input->post('deleteid');
$this->profile->deleteEDucation($delEduId);
}


public function deleteexperienceajax(){
$delExpId=$this->input->post('deleteid');
$this->profile->deleteEXperience($delExpId);
}

public function deleteimageajax(){
$deluserimageId=$this->input->post('deleteid');
$this->profile->deleteUserImage($deluserimageId);
}

public function videoupload(){
$Sess = $this->session->userdata('usersession');
$video=$this->input->post('url');
 $data=array(
'user_id'=>$Sess['user_id'],
'media_title'=>'',
'url'=>$video,
'file_extension'=>'mp4',
'media_type'=>2,
'created_on'=>date('Y-m-d H:i:s')
);
$insertid=$this->profile->addUserVideos($data);
echo $insertid.','.$video;
}

public function loadreviews(){
$Sess = $this->session->userdata('usersession');
$uid=$Sess['user_id'];
$startlimit=$this->input->post('start');
$showcount=3;
$data['reviewsdata']=$this->profile->ajaxReviews($uid,$startlimit,$showcount);
echo $this->load->view('front/ajaxreviews',$data,TRUE);
}

public function addphysicalstats(){
$Sess = $this->session->userdata('usersession');
$detailarray=$this->input->post('details');
$data=array(
'user_id'=>$Sess['user_id'],
'hair_length'=>$detailarray[0],
'hair_type'=>$detailarray[1],
'biceps'=>$detailarray[2],
'chest'=>$detailarray[3],
'hips'=>$detailarray[4],
'waist'=>$detailarray[5],
'bust'=>$detailarray[6],
'weight'=>$detailarray[7],
'height'=>$detailarray[8],
'created_at'=>date('Y-m-d H:i:s')
);
$this->profile->addUpdateUserPhysicalStats($data,$Sess['user_id']);
//echo $insertid.','.$video;

}


public function addwork(){
$Sess = $this->session->userdata('usersession');
$loc=implode(',',$this->input->post('loc'));
$hrs=$this->input->post('hrs');
$data=array(
'user_id'=>$Sess['user_id'],
'work_places'=>$loc,
'work_hours'=>$hrs
);
$this->profile->addupdatework($data,$Sess['user_id']);
}


public function addskills(){
$Sess = $this->session->userdata('usersession');
$lang=$this->input->post('lang');
$expertisearray=$this->input->post('exprt');
$data=array(
'user_id'=>$Sess['user_id'],
'work_places'=>$loc,
'work_hours'=>$hrs
);
$this->profile->updateLangUser($lang,$Sess['user_id']);
$this->profile->addupdateskills($expertisearray,$Sess['user_id']);
}

public function imageupload(){
if(is_array($_FILES)) {
if(is_uploaded_file($_FILES['userImage']['tmp_name'])) {
$sourcePath = $_FILES['userImage']['tmp_name'];
$imagname = random_string('alnum', 50).".jpg";
$targetPath = "uploads/".$imagname; 
//by default the image is uploaded in base url and you only have to give the further path
$target="../../uploads/".$imagname;
//localhost/outnabout/welcome and the uploaded file is in localhost/outnabout/uploads 
if(move_uploaded_file($sourcePath,$targetPath)) {
 $Sess = $this->session->userdata('usersession');
 $data=array(
'user_id'=>$Sess['user_id'],
'media_title'=>'',
'url'=>$imagname,
'file_extension'=>'jpg',
'media_type'=>1,
'created_on'=>date('Y-m-d H:i:s')
);
$insertid=$this->profile->addUserImages($data);
echo $insertid.','.$imagname;

}
}
}
}
public function addTestimonial()
{
$Sess = $this->session->userdata('usersession');
$data=array(
'testimonialAddedBy'=>$Sess['user_id'],
'testimonialText'=>$testimonial

);
//print_r($data);die;
$this->profile->testimonialdata($data,$Sess['user_id']);

}


}
?>
