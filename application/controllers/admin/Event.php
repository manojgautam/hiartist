<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends MY_Controller
{

 public function __construct()
    {
        parent::__construct();
      $this->load->model('admin/Eventmodel', 'event');
      if (!$this->isLoggedAdmin()) {
            redirect(base_url() . 'admin/login');
        }
    }
   
    public function index()
    {

        
    }

    public function addEvent(){
   $data['details']=$this->loginadmin->getDetails();
   $data['title']='Add Event';

if($this->input->post('addevent')==TRUE){
     
       $this->form_validation->set_rules('title', '', 'required');
       $this->form_validation->set_rules('venue', '', 'required');
       $this->form_validation->set_rules('state', '', 'required');
       $this->form_validation->set_rules('startenddate', 'Date Range', 'required');
       $this->form_validation->set_rules('longitude', 'Selection of any nearby location from drop down box', 'required');
       $this->form_validation->set_rules('latitude', '', 'required');
       $this->form_validation->set_error_delimiters('<span style="color:red"><br>','</span>');
    
      if ($this->form_validation->run() == TRUE){
        
        $config['upload_path']='./uploads';
        $config['allowed_types']='jpg|jpeg|png|JPG|JPEG|PNG';
        $config['file_name']=random_string('alnum', 50).".jpg";
        $this->load->library('upload',$config);
        if(!$this->upload->do_upload())
        { $image='';}
        else
        { $imgdata=$this->upload->data();
        $image=$imgdata['file_name'];  }
   
          $fulldate=preg_split("/[\s-]+/",$this->input->post('startenddate'));
          $start_date=date_format(date_create($fulldate[0]),"Y-m-d");
          $end_date=date_format(date_create($fulldate[1]),"Y-m-d");
          if($this->input->post('starttime'))
          $start_time=date_format(date_create($this->input->post('starttime')),"H:i:s");
          else{$start_time='';}
          if($this->input->post('endtime'))
          $end_time=date_format(date_create($this->input->post('endtime')),"H:i:s");
          else{$end_time='';}         
          $ttt=$this->input->post('title');
          $newtitle = str_replace("'", "", $ttt);
          $newtitle = str_replace('"', "", $newtitle);


           $data= array(
          'title' =>$newtitle,
          'description' =>$this->input->post('description'),
          'category'=>$this->input->post('category'),
          'place'=>$this->input->post('state'),
          'venue' =>$this->input->post('venue'),
          'start_date'=>$start_date,
          'start_time' =>$start_time,
          'end_date' =>$end_date,
          'end_time'=>$end_time,
          'event_type'=>$this->input->post('eventtype'),
          'image'=>$image,
          'cost'=>$this->input->post('cost'),
          'currency'=>$this->input->post('currency'),
          'contact_no'=>$this->input->post('phone'),
          'email'=>$this->input->post('email'),
          'latitude'=>$this->input->post('latitude'),
          'longitude'=>$this->input->post('longitude'),
          'created_at'=>date("Y-m-d",time())
            );
                        
            $this->event->addevent($data);
            $this->session->set_flashdata('success',"Event Added Successfully");
            redirect(base_url()."admin/event/addEvent");
              }
     }
       $this->load->view('admin/head',$data);
       $this->load->view('admin/addevent',$data);
       $this->load->view('admin/foot');
      }

      public function ListEvent(){
       $data['details']=$this->loginadmin->getDetails();
       $data['title']='Events';
       $data['events']=$this->event->getAllEvents();

       if($this->input->post('bulk_delt') == TRUE){
         if($this->input->post('hiddendel')==1){
          foreach($this->input->post('checked_events') as $event_id){
           $this->event->deleteEvent($event_id);}
          }
        $this->session->set_flashdata('deleteit',"Deleted successfully!");
       redirect(base_url()."admin/event/ListEvent");
       }
       $this->load->view('admin/head',$data);
       $this->load->view('admin/eventslist',$data);
       $this->load->view('admin/foot');


      }

      
      public function editEvent($id){
   $data['details']=$this->loginadmin->getDetails();
   $data['event']=$this->event->getEventDetails($id);
   $data['eid']=$id;
   $data['title']='Events';

if($this->input->post('addevent')==TRUE){
     
       $this->form_validation->set_rules('title', '', 'required');
       $this->form_validation->set_rules('venue', '', 'required');
       $this->form_validation->set_rules('state', '', 'required');
       $this->form_validation->set_rules('startenddate', 'Date Range', 'required');
       $this->form_validation->set_rules('longitude', 'Selection of any nearby location from drop down box', 'required');
       $this->form_validation->set_rules('latitude', '', 'required');
       $this->form_validation->set_error_delimiters('<span style="color:red"><br>','</span>');
    
      if ($this->form_validation->run() == TRUE){
        
        $config['upload_path']='./uploads';
        $config['allowed_types']='jpg|jpeg|png|JPG|JPEG|PNG';
        $config['file_name']=random_string('alnum', 50).".jpg";
        $this->load->library('upload',$config);
        if(!$this->upload->do_upload())
        { $image=$this->input->post('hiddenname');}
        else
        { $imgdata=$this->upload->data();
        $image=$imgdata['file_name'];  }
   
          $fulldate=preg_split("/[\s-]+/",$this->input->post('startenddate'));
          $start_date=date_format(date_create($fulldate[0]),"Y-m-d");
          $end_date=date_format(date_create($fulldate[1]),"Y-m-d");
          if($this->input->post('starttime'))
          $start_time=date_format(date_create($this->input->post('starttime')),"H:i:s");
          else{$start_time='';}
          if($this->input->post('endtime'))
          $end_time=date_format(date_create($this->input->post('endtime')),"H:i:s");
          else{$end_time='';}         
          $ttt=$this->input->post('title');
          $newtitle = str_replace("'", "", $ttt);
          $newtitle = str_replace('"', "", $newtitle);


           $data= array(
          'title' =>$newtitle,
          'description' =>$this->input->post('description'),
          'category'=>$this->input->post('category'),
          'place'=>$this->input->post('state'),
          'venue' =>$this->input->post('venue'),
          'start_date'=>$start_date,
          'start_time' =>$start_time,
          'end_date' =>$end_date,
          'end_time'=>$end_time,
          'event_type'=>$this->input->post('eventtype'),
          'image'=>$image,
          'cost'=>$this->input->post('cost'),
          'currency'=>$this->input->post('currency'),
          'contact_no'=>$this->input->post('phone'),
          'email'=>$this->input->post('email'),
          'latitude'=>$this->input->post('latitude'),
          'longitude'=>$this->input->post('longitude'),
          'created_at'=>date("Y-m-d",time())
            );
                        
            $this->event->editEvent($id,$data);
            $this->session->set_flashdata('success',"Event Edited Successfully");
            redirect(base_url()."admin/event/ListEvent");
              }
     }
       $this->load->view('admin/head',$data);
       $this->load->view('admin/editevent',$data);
       $this->load->view('admin/foot');
      }

public function deleteEvent($id){
    $this->event->deleteEvent($id);
    $this->session->set_flashdata('deleteit',"Deleted successfully!");
    redirect(base_url()."admin/event/ListEvent");
}


}
