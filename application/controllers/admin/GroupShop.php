<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GroupShop extends MY_Controller
{

 public function __construct()
    {
        parent::__construct();
      $this->load->model('admin/GroupShopmodel', 'grpshop');
      if (!$this->isLoggedAdmin()) {
            redirect(base_url() . 'admin/login');
        }
    }
   
    public function index()
    {

        
    }

    public function addGroup(){
    $data['details']=$this->loginadmin->getDetails();
    $data['title']='Add Group';
if($this->input->post('gpsubmit')==TRUE){
     
       $this->form_validation->set_rules('name', 'Group Name', 'required');
       $this->form_validation->set_error_delimiters('<span style="color:red"><br>','</span>');
    
      if ($this->form_validation->run() == TRUE){
         $groupMember_image=$_FILES['userfile1']['name'];
          $banner_image=$_FILES['userfilephp']['name'];
 
          if($groupMember_image[0]!='')
          {
            $files = $_FILES;
               $count = count($_FILES['userfile1']['name']);
                for($i=0; $i<$count; $i++)
                {
                $_FILES['userfile']['name']= $files['userfile1']['name'][$i];
                $_FILES['userfile']['type']= $files['userfile1']['type'][$i];
                $_FILES['userfile']['tmp_name']= $files['userfile1']['tmp_name'][$i];
                $_FILES['userfile']['error']= $files['userfile1']['error'][$i];
                $_FILES['userfile']['size']= $files['userfile1']['size'][$i];
                $config['upload_path'] = './uploads';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2000000';
                $config['remove_spaces'] = true;
                $config['overwrite'] = false;
                $config['max_width'] = '';
                $config['max_height'] = '';
                $config['file_name']=random_string('alnum', 50).".jpg";
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload();
                $theimgdata=$this->upload->data();
                $images[] = $theimgdata['file_name'];
                    
                    }
       
          }
       
   

  if($banner_image!=''){
        $imgname=random_string('alnum', 50).".jpg";
        $file_path = 'uploads/'.$imgname;
        move_uploaded_file($_FILES['userfilephp']['tmp_name'], $file_path);
        $image=$imgname; 
        }
        else{
       $image='';
       }


        
         $data= array(
          'name' =>$this->input->post('name'),
          'description' =>$this->input->post('desc'),
          'image'=>$image,
          'group_members'=>$this->input->post('members'),
          'website' =>preg_replace('#^https?://#', '', $this->input->post('website')),
          'blog_link'=>preg_replace('#^https?://#', '', $this->input->post('blog')),
          'active' =>1,
          'created_at'=>date("Y-m-d",time())
            );
            $this->grpshop->addGroup($data);
            $insert_id = $this->db->insert_id();
if(isset($images)){
          foreach($images as $val){
        $data1=array(
           'group_id'=>$insert_id,
          'image_name'=>$val);
          
           $this->grpshop->addGroupImages($data1);}
}
           $this->session->set_flashdata('addedit',"Group Added Successfully");
            redirect(base_url()."admin/groupShop/ListGroups");
        }
     }
        $this->load->view('admin/head',$data);
        $this->load->view('admin/addgroup');
        $this->load->view('admin/foot');
    }

    public function ListGroups(){
       $data['details']=$this->loginadmin->getDetails();
       $data['title']='Groups';

       $grplist=$this->grpshop->ListGroups();
$i=0;
foreach($grplist as $val){
        $grplist[$i]->grpimages=$this->grpshop->getGroupImages($val->group_id);
$i++;
}
$data['groups']=$grplist;

       if($this->input->post('bulk_delt') == TRUE){
         if($this->input->post('hiddendel')==1){
          foreach($this->input->post('checked_events') as $group_id){
           $this->grpshop->deleteGroup($group_id);}
          }
        $this->session->set_flashdata('deleteit',"Deleted successfully!");
       redirect(base_url()."admin/groupShop/ListGroups");
       }

       $this->load->view('admin/head',$data);
       $this->load->view('admin/groupslist',$data);
       $this->load->view('admin/foot');
    }


public function deletegrpimages(){
$gpimgid=$_POST['gpimgid'];
$this->grpshop->deletegroupimages($gpimgid);
}


public function deleteshpimages(){
$shpimgid=$_POST['shpimgid'];
$this->grpshop->deleteshopimages($shpimgid);
}

    
    public function changestatus($grpid,$status)
    { $this->grpshop->changeGroupStatus($grpid,$status);
      $this->session->set_flashdata('addedit',"Status changed successfully!");
      redirect(base_url()."admin/groupShop/ListGroups");
     }

     
     public function deleteGroup($id){
    $this->grpshop->deleteGroup($id);
    $this->session->set_flashdata('deleteit',"Deleted successfully!");
    redirect(base_url()."admin/groupShop/ListGroups");
}

 public function editGroup($id){
    $data['details']=$this->loginadmin->getDetails();
    $data['groups']=$this->grpshop->GroupDetail($id);
    $data['title']='Groups';
    $data['id']=$id;
if($this->input->post('gpsubmit')==TRUE){
     
       $this->form_validation->set_rules('name', 'Group Name', 'required');
       $this->form_validation->set_error_delimiters('<span style="color:red"><br>','</span>');
    
      if ($this->form_validation->run() == TRUE){
    $groupMember_image=$_FILES['userfile1']['name'];
          $banner_image=$_FILES['userfilephp']['name'];

  
 
          if($groupMember_image[0]!='')
          {
            $files = $_FILES;
               $count = count($_FILES['userfile1']['name']);
                for($i=0; $i<$count; $i++)
                {
                $_FILES['userfile']['name']= $files['userfile1']['name'][$i];
                $_FILES['userfile']['type']= $files['userfile1']['type'][$i];
                $_FILES['userfile']['tmp_name']= $files['userfile1']['tmp_name'][$i];
                $_FILES['userfile']['error']= $files['userfile1']['error'][$i];
                $_FILES['userfile']['size']= $files['userfile1']['size'][$i];
                $config['upload_path'] = './uploads';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2000000';
                $config['remove_spaces'] = true;
                $config['overwrite'] = false;
                $config['max_width'] = '';
                $config['max_height'] = '';
               $config['file_name']=random_string('alnum', 50).".jpg";
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload();
                $theimgdata=$this->upload->data();
                $images[] = $theimgdata['file_name'];
                    
                    }
             
          }


    
      if($banner_image!=''){
      $imgname=random_string('alnum', 50).".jpg";
      $file_path = 'uploads/'.$imgname;
      move_uploaded_file($_FILES['userfilephp']['tmp_name'], $file_path);
      $image=$imgname;
        }
        else{
       $image=$this->input->post('hiddenname');
       }

           $data= array(
          'name' =>$this->input->post('name'),
          'description' =>$this->input->post('desc'),
          'image'=>$image,
          'group_members'=>$this->input->post('members'),
          'website' =>preg_replace('#^https?://#', '',$this->input->post('website')),
          'blog_link'=>preg_replace('#^https?://#', '',$this->input->post('blog')),
          'created_at'=>date("Y-m-d",time())
            );


if(isset($images)){
         foreach($images as $val){
        $data1=array(
           'group_id'=>$id,
          'image_name'=>$val);
     
           $this->grpshop->addGroupImages($data1);}
}
                        
            $this->grpshop->EditGroup($id,$data);
            $this->session->set_flashdata('addedit',"Group Edited Successfully");
            redirect(base_url()."admin/groupShop/ListGroups");
              }
     }
        $this->load->view('admin/head',$data);
        $this->load->view('admin/editgroup');
        $this->load->view('admin/foot');
    }

    
    public function addShop(){
    $data['details']=$this->loginadmin->getDetails();
    $data['stateslist']=$this->grpshop->getAllStates();
    $data['title']='Add Shop';
if($this->input->post('shopsubmit')==TRUE){
     
       $this->form_validation->set_rules('name', 'Shop Name', 'required');
       $this->form_validation->set_rules('state', '', 'required');
       $this->form_validation->set_rules('district', '', 'required');
       $this->form_validation->set_error_delimiters('<span style="color:red"><br>','</span>');
      if ($this->form_validation->run() == TRUE){
      $shopMember_image=$_FILES['userfile1']['name'];
          $banner_image=$_FILES['userfilephp']['name'];
 
          if($shopMember_image[0]!='')
          {
            $files = $_FILES;
               $count = count($_FILES['userfile1']['name']);
                for($i=0; $i<$count; $i++)
                {
                $_FILES['userfile']['name']= $files['userfile1']['name'][$i];
                $_FILES['userfile']['type']= $files['userfile1']['type'][$i];
                $_FILES['userfile']['tmp_name']= $files['userfile1']['tmp_name'][$i];
                $_FILES['userfile']['error']= $files['userfile1']['error'][$i];
                $_FILES['userfile']['size']= $files['userfile1']['size'][$i];
                $config['upload_path'] = './uploads';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2000000';
                $config['remove_spaces'] = true;
                $config['overwrite'] = false;
                $config['max_width'] = '';
                $config['max_height'] = '';
                $config['file_name']=random_string('alnum', 50).".jpg";
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload();
                $theimgdata=$this->upload->data();
                $images[] = $theimgdata['file_name'];
                    
                    }
       
          }
       
   

  if($banner_image!=''){
        $imgname=random_string('alnum', 50).".jpg";
        $file_path = 'uploads/'.$imgname;
        move_uploaded_file($_FILES['userfilephp']['tmp_name'], $file_path);
        $image=$imgname; 
        }
        else{
       $image='';
       }

       
      

           $data= array(
          'shop_name' =>$this->input->post('name'),
          'shop_desc' =>$this->input->post('desc'),
          'district_id'=>$this->input->post('district'),
          'state_id'=>$this->input->post('state'),
          'owner'=>$this->input->post('owner'),
          'address' =>$this->input->post('address'),
          'image'=>$image,
          'website'=>preg_replace('#^https?://#', '',$this->input->post('website')),
            'contact'=>$this->input->post('contact'),
            'email'=>$this->input->post('email'),
          'active' =>1,
          'created_at'=>date("Y-m-d",time())
            );
        
                        
            $this->grpshop->addShop($data);
            $insert_id = $this->db->insert_id();

      if(isset($images)){
         foreach($images as $val){
            $data1=array(
           'shop_id'=>$insert_id,
           'image_name'=>$val);

           $this->grpshop->addshoppImages($data1);}
}
            $this->session->set_flashdata('addedit',"Shop Added Successfully");
            redirect(base_url()."admin/groupShop/ListShops");
              }
     }
        $this->load->view('admin/head',$data);
        $this->load->view('admin/addshop');
        $this->load->view('admin/foot');
    }

    public function getdistricts(){
    $stateid=$this->input->post('stateid');
    $allDistricts=$this->grpshop->getDistricts($stateid);
    ?><option value="">Select District</option><?php
    foreach($allDistricts as $val){ ?>
    <option value="<?php echo $val->id;?>"><?php echo $val->name;?></option>
    <?php }
    }

    public function ListShops(){
       $data['details']=$this->loginadmin->getDetails();
       $data['title']='Shops';

$shplist=$this->grpshop->ListShops();
$i=0;
foreach($shplist as $val){
        $shplist[$i]->shpimages=$this->grpshop->getShopImages($val->shop_id);
$i++;
}
$data['shops']=$shplist;

       if($this->input->post('bulk_delt') == TRUE){
         if($this->input->post('hiddendel')==1){
          foreach($this->input->post('checked_events') as $shop_id){
           $this->grpshop->deleteShop($shop_id);}
          }
        $this->session->set_flashdata('deleteit',"Deleted successfully!");
       redirect(base_url()."admin/groupShop/ListShops");
       }

       $this->load->view('admin/head',$data);
       $this->load->view('admin/shopslist',$data);
       $this->load->view('admin/foot');
    }

     public function changeshopstatus($shopid,$status)
    { $this->grpshop->changeShopStatus($shopid,$status);
      $this->session->set_flashdata('addedit',"Status changed successfully!");
      redirect(base_url()."admin/groupShop/ListShops");
     }

       public function editShop($sid){
    $data['details']=$this->loginadmin->getDetails();
    $data['stateslist']=$this->grpshop->getAllStates();
    $data['shop']=$this->grpshop->getShopDetail($sid);
    $data['tdistrict']=$this->grpshop->getSelectedDistrict($data['shop'][0]->district_id);
    $data['districtlist']=$this->grpshop->getDistricts($data['tdistrict'][0]->state_id);
    $data['selectedstateid']=$data['tdistrict'][0]->state_id;
    $data['sid']=$sid;
    $data['title']='Shops';
if($this->input->post('shopsubmit')==TRUE){
     
       $this->form_validation->set_rules('name', 'Shop Name', 'required');
       $this->form_validation->set_rules('state', '', 'required');
       $this->form_validation->set_rules('district', '', 'required');
       $this->form_validation->set_error_delimiters('<span style="color:red"><br>','</span>');
    
      if ($this->form_validation->run() == TRUE){
    $shopMember_image=$_FILES['userfile1']['name'];
          $banner_image=$_FILES['userfilephp']['name'];

     if($shopMember_image[0]!='')
          {
            $files = $_FILES;
               $count = count($_FILES['userfile1']['name']);
                for($i=0; $i<$count; $i++)
                {
                $_FILES['userfile']['name']= $files['userfile1']['name'][$i];
                $_FILES['userfile']['type']= $files['userfile1']['type'][$i];
                $_FILES['userfile']['tmp_name']= $files['userfile1']['tmp_name'][$i];
                $_FILES['userfile']['error']= $files['userfile1']['error'][$i];
                $_FILES['userfile']['size']= $files['userfile1']['size'][$i];
                $config['upload_path'] = './uploads';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size'] = '2000000';
                $config['remove_spaces'] = true;
                $config['overwrite'] = false;
                $config['max_width'] = '';
                $config['max_height'] = '';
               $config['file_name']=random_string('alnum', 50).".jpg";
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                $this->upload->do_upload();
                $theimgdata=$this->upload->data();
                $images[] = $theimgdata['file_name'];
                    
                    }
             
          }


    
      if($banner_image!=''){
      $imgname=random_string('alnum', 50).".jpg";
      $file_path = 'uploads/'.$imgname;
      move_uploaded_file($_FILES['userfilephp']['tmp_name'], $file_path);
      $image=$imgname;
        }
        else{
       $image=$this->input->post('hiddenname');
       }
        
        

           $data= array(
          'shop_name' =>$this->input->post('name'),
          'shop_desc' =>$this->input->post('desc'),
          'district_id'=>$this->input->post('district'),
             'state_id'=>$this->input->post('state'),
          'owner'=>$this->input->post('owner'),
          'address' =>$this->input->post('address'),
          'image'=>$image,
            'contact'=>$this->input->post('contact'),
            'email'=>$this->input->post('email'),
          'website'=>preg_replace('#^https?://#', '',$this->input->post('website')),
          'created_at'=>date("Y-m-d",time())
            );
   if(isset($images)){             
    foreach($images as $val){
        $data1=array(
           'shop_id'=>$sid,
          'image_name'=>$val);
     
           $this->grpshop->addshoppImages($data1);}
}
        
            $this->grpshop->editShop($sid,$data);
            $this->session->set_flashdata('addedit',"Shop Edited Successfully");
            redirect(base_url()."admin/groupShop/ListShops");
              }
     }
        $this->load->view('admin/head',$data);
        $this->load->view('admin/editshop');
        $this->load->view('admin/foot');
    }

    public function deleteShop($id){
    $this->grpshop->deleteShop($id);
    $this->session->set_flashdata('deleteit',"Deleted successfully!");
    redirect(base_url()."admin/groupShop/ListShops");
}

}
