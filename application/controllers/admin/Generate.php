<?php

class Generate extends CI_Controller
{

    function Generate()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('url');
       
        
    }

        function create_csv($role){

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=Exported_data.csv');

// create a file pointer connected to the output stream
$output = fopen('php://output', 'w');

// output the column headings
$resultt = $this->db->list_fields('users');
$res=array();
foreach($resultt as $vall){ 
if($vall!="password" && $vall!="access_token" && $vall!="paid" && $vall!="active" && $vall!="review_count" && $vall!="facebook_id" && $vall!="google_id" &&  $vall!="seen")
$res[]=ucfirst($vall);}
fputcsv($output, $res);

$this->db->select('user_id,first_name,last_name,email,dob,gender,language,location,country,state,district,phone_no,profile_pic,role_id,sub_role_id,profile_view,description,reviews,languages,created_on,updated_on');
$this->db->where('role_id',$role);
$quer = $this->db->get('users');
$query=$quer->result_array();

// loop over the rows, outputting them

foreach($query as $val){ 
if($role==1)
$val['role_id']='Artist';
if($role==2)
$val['role_id']='Directors';
fputcsv($output, $val);
}

}


}
