<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Groups extends MY_Controller
{

     public function __construct()
	{
		parent::__construct();
        $this->load->model('front/Groupmodel','group');
}
public function index()
    {
    }
public function allgroups()
{
     $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		 $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
        $data['googlelogin'] = $sociallogin[1];
     $data['artist']=$this->home->get_artist_front_menu();
       $data['director']=$this->home->get_director_front_menu();
       $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
       $data['fronttitle']="Groups";
        $data['grouptab']="active";
        $data['groups']=$this->group->getAllGroups();
           $data['count']=$this->group->getGroupCount();
           $data['noshowmap']="yes";
$data['fbfail']="yes";
if($this->session->userdata('fbsessn')!='')
{redirect($data['login_url']);}
        $this->load->view('front/header',$data);
        $this->load->view('front/allgroups',$data);
        $this->load->view('front/footer');
}
public function viewgroup()
{
$sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		 $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
        $data['googlelogin'] = $sociallogin[1];
     $data['artist']=$this->home->get_artist_front_menu();
       $data['director']=$this->home->get_director_front_menu();
         $id=$this->uri->segment(2);
         $data['group']=$this->group->getGroup($id);
      // echo "<pre>";print_r($data['group']);die;
         $data['group_images']=$this->group->getGroupimages($id);
    $data['group_image_count']=count($data['group_images']);
       $data['groups']=$this->group->getAllGroups();
        $data['state']=$this->home->get_state_front_menu();
        $data['grouptab']="active";
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
          $data['fronttitle']="Group";
$data['fbfail']="yes";
if($this->session->userdata('fbsessn')!='')
{redirect($data['login_url']);}
        $this->load->view('front/header',$data);
        $this->load->view('front/viewgroup',$data);
        $this->load->view('front/footer');
}
    public function load_more()
{
    $limt=$_POST['limit'];
     $data['groups']=$this->group->getAllGroups($limt);
  echo $this->load->view('front/load_more_groups',$data,true);
 }

}
?>
