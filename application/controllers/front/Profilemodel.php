<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Profilemodel extends CI_Model
{
   
     public function getUserProfileDetails($id){
          $q=$this->db->get_Where('users', array('user_id'=>$id));
			return $q->result();
       }

public function updateLangUser($lang,$uid){
$data=array('languages'=>$lang);
$this->db->where('user_id',$uid);
 $this->db->update('users',$data);
}

public function updateprofileImage($uid,$data){
  $this->db->where('user_id',$uid);
 $this->db->update('users',$data);
}


	 public function getIdbyName($subrolename){
          $namearray=explode(',',$subrolename);
          $res=array()
          foreach($namearray as $val){
           $this->db->where('sub_role',$val));
           $this->db->get('user_sub_role');
	   $res[]=$q->result();
               }
            echo "<pre>";print_r($res);die;
         return $res;
       }  


 public function getExpertiseList($subroleID){
          $q=$this->db->get_Where('all_expertise', array('sub_role_id'=>$subroleID));
			return $q->result();
       } 

    public function getUserProfDetails($id){
          $q=$this->db->get_Where('users', array('user_id'=>$id));
			return $q->result();
       }

	public function increaseViews($id){
		   $this->db->set('profile_view', 'profile_view+1', FALSE);
		   $this->db->where('user_id',$id);
		   $this->db->update('users');
		   }

		   	public function UpdateProfile($uid,$data){
		  
		   $this->db->where('user_id',$uid);
		   $this->db->update('users',$data);
		   }

public function addEDucation($data){
$this->db->insert('user_education',$data);
return $this->db->insert_id();
}


public function addExperience($data){
$this->db->insert('user_experience',$data);
return $this->db->insert_id();
}

public function getUserExpertise($id){
$this->db->select('all_expertise.expertise');
        $this->db->from('user_expetise');
        $this->db->join('all_expertise','all_expertise.expert_id = user_expetise.expert_id');
        $this->db->where('user_expetise.user_id ',$id);
        $q=$this->db->get();
        return $q->result();
}

public function addupdateskills($expertisearray,$uid){
$q=$this->db->get_Where('user_expetise', array('user_id'=>$uid));
$r=$q->result();
if(count($r)){
  $this->db->where('user_id',$uid);
   $this->db->delete('user_expetise');

foreach($expertisearray as $val){
$data=array('user_id'=>$uid,'expert_id'=>$val);
$this->db->insert('user_expetise',$data);
}

}
else{
foreach($expertisearray as $val){
$data=array('user_id'=>$uid,'expert_id'=>$val);
$this->db->insert('user_expetise',$data);
}
}
}


public function addupdatework($data,$uid){
$q=$this->db->get_Where('user_work_pref', array('user_id'=>$uid));
$r=$q->result();
if(count($r)){
   $this->db->where('user_id',$uid);
   $this->db->update('user_work_pref',$data);
}
else{
$this->db->insert('user_work_pref',$data);
}
}


public function addUpdateUserPhysicalStats($data,$uid){
$q=$this->db->get_Where('user_physical_stats', array('user_id'=>$uid));
$r=$q->result();
if(count($r)){
   $this->db->where('user_id',$uid);
   $this->db->update('user_physical_stats',$data);
}
else{
$this->db->insert('user_physical_stats',$data);
//return $this->db->insert_id();
}
}

public function getoldReview($id,$addedby){
$q=$this->db->get_Where('reviews', array('user_id'=>$id,'added_by'=>$addedby));
return $q->result();
}

public function addEditReviews($userid,$addedby,$data){
$q=$this->db->get_Where('reviews', array('user_id'=>$userid,'added_by'=>$addedby));
$r=$q->result();
if(count($r)){
$g=$this->db->get_Where('users', array('user_id'=>$userid));
$re=$g->result();
$oldreviews=$re[0]->reviews;
$newreviews=(float)$oldreviews-(float)$r[0]->review+(float)$data['review'];
$newdata=array('reviews'=>$newreviews);
   $this->db->where('user_id',$userid);
   $this->db->update('users',$newdata);


   $this->db->where('user_id',$userid);
   $this->db->where('added_by',$addedby);
   $this->db->update('reviews',$data);
}
else{
$g=$this->db->get_Where('users', array('user_id'=>$userid));
$re=$g->result();
$oldreviews=$re[0]->reviews;
$newreviews=(float)$oldreviews+(float)$data['review'];
$oldreviewcount=$re[0]->review_count;
$newreviewcount=$oldreviewcount+1;
$newdata=array('reviews'=>$newreviews,'review_count'=>$newreviewcount);
   $this->db->where('user_id',$userid);
   $this->db->update('users',$newdata);
$this->db->insert('reviews',$data);
}
}


public function getPhysicalStats($id){
$q=$this->db->get_Where('user_physical_stats', array('user_id'=>$id));
return $q->result();
}

public function getEDucationDetails($id){
$this->db->order_by('edu_id','DESC');
$q=$this->db->get_Where('user_education', array('user_id'=>$id));
return $q->result();
}


public function getWorkPrefrence($id){
$q=$this->db->get_Where('user_work_pref', array('user_id'=>$id));
return $q->result();
}


	public function editEDucation($eid,$data){
		  
		   $this->db->where('edu_id',$eid);
       $this->db->where('user_id',$data['user_id']);
		   $this->db->update('user_education',$data);
		   }



	public function editExperience($expid,$data){
		  
		   $this->db->where('exp_id',$expid);
       $this->db->where('user_id',$data['user_id']);
		   $this->db->update('user_experience',$data);
		   }

public function deleteEDucation($delEduId){
   $this->db->where('edu_id',$delEduId);
   $this->db->where('user_id',$this->getUserIdFromSession());
   $this->db->delete('user_education');
}

public function getUserIdFromSession(){
  $Sess = $this->session->userdata('usersession');
  $uid=$Sess['user_id'];
  return $uid; 
}
public function deleteEXperience($delExpId){
   $this->db->where('exp_id',$delExpId);
   $this->db->where('user_id',$this->getUserIdFromSession());
   $this->db->delete('user_experience');
}


public function deleteUserImage($Id){

$eventimage=$this->getUserMediaDetails($Id);
if(count($eventimage)){
if($eventimage[0]->url!='')
@unlink('uploads/'.$eventimage[0]->url);
}

   $this->db->where('media_id',$Id);
   $this->db->where('user_id',$this->getUserIdFromSession());
   $this->db->delete('user_media');
   if(count($eventimage)){return 1;}else{return 0;}
}


public function getUserMediaDetails($mediaid){
$q=$this->db->get_Where('user_media', array('media_id'=>$mediaid,'user_id'=>$this->getUserIdFromSession()));
return $q->result();
}


public function addUserImages($data){
$this->db->insert('user_media',$data);
return $this->db->insert_id();}

public function getUserImages($id){
$this->db->order_by('media_id','DESC');
$q=$this->db->get_Where('user_media', array('user_id'=>$id,'media_type'=>1));
return $q->result();}


public function getUserVideos($id){
$this->db->order_by('media_id','DESC');
$q=$this->db->get_Where('user_media', array('user_id'=>$id,'media_type'=>2));
return $q->result();}


public function addUserVideos($data){
$this->db->insert('user_media',$data);
return $this->db->insert_id();}

public function getUserExperience($id){
$this->db->order_by('exp_id','DESC');
$q=$this->db->get_Where('user_experience', array('user_id'=>$id));
return $q->result();}

public function getallReviews($id){
$this->db->select('users.first_name,users.last_name,users.profile_pic,reviews.*');
        $this->db->from('reviews');
        $this->db->join('users','users.user_id = reviews.added_by');
        $this->db->where('reviews.user_id',$id);
        $this->db->order_by("reviews.created_at","desc");
        $q=$this->db->get();
        return $q->result();
}


public function ajaxReviews($uid,$startlimit,$showcount){
$this->db->select('users.first_name,users.last_name,users.profile_pic,reviews.*');
        $this->db->from('reviews');
        $this->db->join('users','users.user_id = reviews.added_by');
        $this->db->where('reviews.user_id',$uid);
        $this->db->limit($showcount,$startlimit);
        $this->db->order_by("reviews.created_at","desc");
        $q=$this->db->get();
        return $q->result();
}
public function testimonial($id)
{
$query=$this->db->get_where('testimonial',array('testimonialAddedBy'=>$id));
return $query->row_array();
}
public function testimonialdata($data,$id)
{
$query=$this->db->get_where('testimonial',array('testimonialAddedBy'=>$id));
$res=$query->result();
if(count($res)){
   $this->db->where('testimonialAddedBy',$id);
   $this->db->update('testimonial',$data);
}
else{
$this->db->insert('testimonial',$data);
}
}
public function getPartners()
{
    $query=$this->db->get_where('homepage_media',array('media_type'=>4));
    return $query->result();
}

}
?>
