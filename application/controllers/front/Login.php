<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller
{

     public function __construct()
	{
		parent::__construct();
  $this->load->model('front/Loginmodel', 'login');  
         $this->load->model('front/Groupmodel','group');
                
         }

    public function index()
    {
    }

    public function login()
    {

     $emailorname=strtolower($this->input->post('emailorname')); 
         $password=$this->input->post('password');
         $check=preg_match('/^[A-z0-9_\-.]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z.]{1,4}$/', $emailorname);
              if(!$check){ echo "error"; }
         else{ 
            $logindetails= $this->login->checkLoginDetails($emailorname, $password);
              if(count($logindetails)){
 $logindetailsactive= $this->login->checkLoginDetailsactive($emailorname, $password);
if(count($logindetailsactive)){
              $userdata = array (
                        'user_id'=> $logindetails[0]->user_id,
                        'name'=>ucfirst($logindetails[0]->first_name).' '.ucfirst($logindetails[0]->last_name),
                        'profile_pic'=>$logindetails[0]->profile_pic,
                        'created_on'=>$logindetails[0]->created_on
                   );
         $this->session->set_userdata('usersession',$userdata);
 $sessiondata = $this->session->userdata('usersession');
                   echo $sessiondata['user_id']; 
} else{
echo "notverified";
}

               }
              else{ echo "error"; }
            }
    }
     public function logout(){

   
       unset($_SESSION["usersession"]);

       redirect(base_url());
    }

public function checkemail()
    {
     $email=strtolower($_POST['email']);
        $data=$this->login->Checkemail($email);

        if($data)
        {$id=base64_encode($data->user_id);
         $data_email['name']=$data->first_name;
         $data_email['message']="<p>You  recently requested to reset your password for your hiartist Account. Click the button below to reset it</p> ";
         $data_email['link']=base_url().'front/Login/forgetpassword/'.$id;
          $data_email['message1']="<p> if you dont request a password reset,Please ignore this email or reply to let us know.</p>";
           $data_email['link_text']="Click here to reset your password";
          $message=$this->load->view('front/email',$data_email,true);
          $this->globalmailfunction($data->email,"Hiartist - Reset Password",$message);
         }
        else{
        echo "not match";
            }
}
 public function forgetpassword()
    {
     $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		 $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
        $data['googlelogin'] = $sociallogin[1];

     $data['artist']=$this->home->get_artist_front_menu();
       $data['director']=$this->home->get_director_front_menu();
       $data['groups']=$this->group->getAllGroups();
        $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }

     
          $data['fronttitle']="Forget Password";
         //$id=$this->uri->segment(4);
        $id=base64_decode($this->uri->segment(4));
         if($this->input->post('submit')==true)
         {
            $this->form_validation->set_rules('password','Password','required');
            $this->form_validation->set_rules('confirm_password','Confirm Password','required|matches[password]');
            $this->form_validation->set_message('required','The passwords are not the same');
            $this->form_validation->set_error_delimiters('<span style="color:red">','</span>');
            if($this->form_validation->run()==true)
           {
          $data=array(
          'password'=>md5($this->input->post('password'))
          );
            $this->login->updateForgetpassword($id,$data);
           redirect(base_url());
         }
              else{
            $this->load->view('front/header',$data);
            $this->load->view('front/forget_password');
            $this->load->view('front/footer');;
    }
         }
         else{
            $this->load->view('front/header',$data);
            $this->load->view('front/forget_password');
            $this->load->view('front/footer');;
    }
         }

}

?>
