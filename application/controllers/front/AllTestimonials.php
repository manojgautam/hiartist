<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AllTestimonials extends MY_Controller
{

     public function __construct()
	{
		parent::__construct();
$this->load->model('front/Testimodel', 'testimonial');
         $this->load->model('front/Groupmodel','group');
         }

    public function index()
    {
    }

    public function all()
    {
        $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		 $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
        $data['googlelogin'] = $sociallogin[1];
     $data['artist']=$this->home->get_artist_front_menu();
       $data['director']=$this->home->get_director_front_menu();
        $data['groups']=$this->group->getAllGroups();
        $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
 
     $data['testi']=$this->testimonial->getTestimonial();
     $allvalues=$this->testimonial->getTestimonialcount();
     $data['count']=count($allvalues);
$data['fbfail']="yes";
if($this->session->userdata('fbsessn')!='')
{redirect($data['login_url']);}

      $data['fronttitle']="Testimonials";
        $this->load->view('front/header',$data);
        $this->load->view('front/alltestimonial',$data);
        $this->load->view('front/footer');
    }


public function load_more()
{
    $limt=$_POST['limit'];
     $data['testi']=$this->testimonial->getTestimonial($limt);
  echo $this->load->view('front/load_more_testimonials',$data,true);
 }



}
?>
