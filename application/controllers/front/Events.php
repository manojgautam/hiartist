<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Events extends MY_Controller
{

     public function __construct()
	{  
		parent::__construct(); 
		$this->load->model('front/Eventmodel','frontevent'); 
          $this->load->model('front/Groupmodel','group');
    }

    public function index()
    {  
	}
	
	public function event()
         {   
        $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		 $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
        $data['googlelogin'] = $sociallogin[1];
  $data['count']=$this->frontevent->geteventcount();
 $data['eventsdata']=$this->frontevent->getallEvents();
    $data['artist']=$this->home->get_artist_front_menu();
       $data['director']=$this->home->get_director_front_menu();
        $data['groups']=$this->group->getAllGroups();
        $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }   
        $data['fronttitle']="Events";
$data['fbfail']="yes";
if($this->session->userdata('fbsessn')!='')
{redirect($data['login_url']);}
		  $this->load->view('front/header',$data);
        $this->load->view('front/events',$data);
        $this->load->view('front/footer');
	}
    public function ViewEvent()
    {
        $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		 $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
        $data['googlelogin'] = $sociallogin[1];
         $id=$this->uri->segment(2);
        $data['artist']=$this->home->get_artist_front_menu();
       $data['director']=$this->home->get_director_front_menu();
         $data['groups']=$this->group->getAllGroups();
        $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
        $data['fronttitle']="Event";
$data['fbfail']="yes";
if($this->session->userdata('fbsessn')!='')
{redirect($data['login_url']);}
        $data['data']=$this->frontevent->getevent($id);
        $this->load->view('front/header',$data);
        $this->load->view('front/eventview',$data);
        $this->load->view('front/footer');
    }
public function load_more()
{
    $limt=$_POST['limit'];
     $data['eventsdata']=$this->frontevent->getallEvents($limt);
  echo $this->load->view('front/load_more_events',$data,true);
 }


    public function onChangeEventvalue()
    {
        $searchdata['date']=$_POST['datetime'];
        $searchdata['latitude']=$_POST['lat'];
        $searchdata['longitude']=$_POST['lon'];
        $data['eventsdata']=$this->frontevent->ajaxeventPage($searchdata);
        $data['eventsdatacount']=$this->frontevent->ajaxeventPagecount($searchdata);
        echo $this->load->view('front/eventsajax',$data,true);
    }

    
     public function load_more_searchevent()
    {   $limt=$_POST['limit'];
        $searchdata['date']=$_POST['datetime'];
        $searchdata['latitude']=$_POST['lat'];
        $searchdata['longitude']=$_POST['lon'];
        $data['eventsdata']=$this->frontevent->ajaxeventPage($searchdata,$limt);
        echo $this->load->view('front/load_more_events',$data,true);
    }
}
	
