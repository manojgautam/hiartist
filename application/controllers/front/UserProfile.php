<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserProfile extends MY_Controller
{

     public function __construct()
	{
		parent::__construct();
      $this->load->model('front/Profilemodel','profile');
           $this->load->model('front/Groupmodel','group');
       ob_start();
        ob_flush();

         }

    public function index()
    {
    }

    public function userprof($id)
    {
        $useridarray = explode('.',$id);
        if(isset($useridarray[1]))
        {$id = $useridarray[1];}
        else{$id='';}
        $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
        $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
        $data['googlelogin'] = $sociallogin[1];
        $Sess = $this->session->userdata('usersession');
        $addedby=$Sess['user_id'];

        if(isset($_SESSION['userip'])){}
        else{
        $this->profile->increaseViews($id);} 

        $data['userid'] =$id;
       
	    $data['userprofdetail'] = $this->profile->getUserProfDetails($id);
        $data['edudetail'] = $this->profile->getEDucationDetails($id);
        $data['imagedetail'] = $this->profile->getUserImages($id);
        $data['video'] = $this->profile->getUserVideos($id);
        $data['experiencedetail'] = $this->profile->getUserExperience($id);
        $data['allreviews'] = $this->profile->getallReviews($id);
        $data['physicalstats'] = $this->profile->getPhysicalStats($id);
        $data['workpref'] = $this->profile->getWorkPrefrence($id);
        $data['currentprofid'] =$id;
        $data['checkoldreview']=$this->profile->getoldReview($id,$addedby);

        if(count($data['userprofdetail'])){
             $data['userprofilepage'] = 'userprofilepage';
        $subrolename=$data['userprofdetail'][0]->sub_role_id;
        $subroleID = $this->profile->getIdbyName($subrolename);
        $expertisse = $this->profile->getExpertiseList($subroleID);
        $expppp=array();
        foreach($expertisse as $expp){
         foreach($expp as $innerexpp){
         $expppp[]= $innerexpp;
         }
        }
        $data['getexpertiselist']=$expppp; 
        $userlanguages = $data['userprofdetail'][0]->languages;
        $userexpertise = $this->profile->getUserExpertise($id);
        $arr=array();
        foreach($userexpertise as $val){$arr[]=$val->expertise;}
        if(count($arr)){$str=implode(',',$arr);}
        else{$str='';}
        $data['skills'] = array();
        $data['skills'][]=array('lang'=>$userlanguages,'expert'=>$str);
        }
        
        $data['artist']=$this->home->get_artist_front_menu();
        $data['director']=$this->home->get_director_front_menu();
       $data['groups']=$this->group->getAllGroups();
        $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
if(count($data['userprofdetail'])){
$data['fronttitle']=ucfirst($data['userprofdetail'][0]->first_name).' '.ucfirst($data['userprofdetail'][0]->last_name).'|'.$data['userprofdetail'][0]->sub_role_id;}
else{$data['fronttitle']='User Profile';}
        
        $data['noshowmap']="no";
$data['fbfail']="yes";
if($this->session->userdata('fbsessn')!='')
{redirect($data['login_url']);}
        $this->load->view('front/header',$data);
        $this->load->view('front/userprofile',$data);
        $this->load->view('front/footer');
 
    }

    public function addreviews(){
      if($this->input->post('submit')==TRUE)
      {
$Sess = $this->session->userdata('usersession');
$data=array(
'user_id'=>$this->input->post('currentprof'),
'added_by'=>$Sess['user_id'],
'review'=>$this->input->post('rating'),
'description'=>$this->input->post('comment'),
'created_at'=>date('Y-m-d H:i:s')
);
$userprofdetail = $this->profile->getUserProfDetails($this->input->post('currentprof'));
$this->profile->addEditReviews($this->input->post('currentprof'),$Sess['user_id'],$data);
redirect(base_url().'userprofile/'.$userprofdetail[0]->last_name.$userprofdetail[0]->first_name.'.'.$this->input->post('currentprof'));
 }
 }

public function loadreviewsuserprof(){
$uid=$this->input->post('userid');
$startlimit=$this->input->post('start');
$showcount=3;
$data['reviewsdata']=$this->profile->ajaxReviews($uid,$startlimit,$showcount);
echo $this->load->view('front/ajaxreviews',$data,TRUE);
}

}
?>
