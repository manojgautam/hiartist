<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends MY_Controller
{

     public function __construct()
	{
		parent::__construct();
                 $this->load->model('front/Searchmodel', 'search'); 
             $this->load->model('front/Homemodel', 'home'); 
           $this->load->model('front/Groupmodel','group');
         }

    public function index()
    {
    }

     public function search()
    {    
         $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		 $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
        $data['googlelogin'] = $sociallogin[1];
          $data['role']=$this->uri->segment(2);
         $role_name=$this->uri->segment(3);
        // print_r($role_name);die;
         $data['district']=$this->search->roleDirector($role_name);
         $data['role_name']=$this->uri->segment(3);
         $data['thetype']=$this->uri->segment(4);
//contains 'all' if all are to be shown and contains 'district name' if want to show of particular subcategory of particular district
        $data['count']=$this->search->getrolecount($role_name,$data['thetype']);
        $data['data']=$this->search->get_user_detail($role_name,0,$data['thetype']);
        $data['user']=count($data['data']);
        $data['artist']=$this->home->get_artist_front_menu();
       $data['director']=$this->home->get_director_front_menu();
       $data['allroles']=$this->search->get_allroles_front_menu();
         $data['groups']=$this->group->getAllGroups();
        $data['state']=$this->home->get_state_front_menu();
        if($data['role']=="Director")
        {$data['searchdirectortab']="active";}
        else{$data['searcartisthtab']="active";}
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
          $data['fronttitle']=$data['role'];
          $data['noshowmap']="no";
$data['fbfail']="yes";
if($this->session->userdata('fbsessn')!='')
{redirect($data['login_url']);}
          $this->load->view('front/header',$data);
        $this->load->view('front/search',$data);
        $this->load->view('front/footer');
    }
    public function load_more()
{          
      
        $limt=$_POST['limit'];
        $role_name=$_POST['id'];
        $thetype=$_POST['type'];
        $data['role_name']=$role_name;
        $data['role']=$this->search->get_user_detail($role_name,$limt,$thetype);
        echo $this->load->view('front/load_more_role',$data,true);
 }
    public function onChangeSearchvalue()
    {         $therole=$_POST['therole'];
          $role_name=$_POST['id'];
         $data['allvalues']=$this->input->post('allvalues');
         $data['district']=$this->search->roleDirector($data['allvalues'][0]);
         $data['role_count']=$this->search->ajaxserachPagecount($role_name,$data,$therole);
       $data['result']=$this->search->ajaxserachPage($role_name,$data,$therole);
        $data['role_name']=$data['allvalues'][0];
        $array = array(
                'select'=>$data['district'],
                'view' => $this->load->view('front/searchafterajax',$data,true) 
        );
       /* print_r($array);die;*/
        echo json_encode($array);
    }
    public function load_more_searchafterajax()
    {    $therole=$_POST['therole'];
        $data['allvalues']=$this->input->post('allvalues');
        $limt=$_POST['limit'];
        $role_name=$_POST['id'];
        $data['role_name']=$role_name;
    $data['role']=$this->search->ajaxserachPage($role_name,$data,$limt,$therole);
        echo $this->load->view('front/load_more_ajaxsearch',$data,true);
    }

}
?>
