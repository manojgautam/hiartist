<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registeration extends MY_Controller
{

     public function __construct()
	{
		parent::__construct();     
         $this->load->model('front/Groupmodel','group');
         $this->load->model('front/Eventmodel','frontevent'); 
         
    }

    public function index()
    {  
      $sociallogin = $this->social_login(); // Return Fb and google login urls array from main controller
		$data['login_url'] = $sociallogin[0]; // Login_url is used to get FB Login Url from main controller
        $data['googlelogin'] = $sociallogin[1]; // Login_url is used to get Google Login Url from main controller
        
	/* Registeration Process */
          if($this->input->post('submit') == true){ 
            // echo "<pre>"; print_r($_POST);die;
			   $this->form_validation->set_rules('first_name', 'First name', 'required');
			   $this->form_validation->set_rules('last_name', 'Last name', 'required');
			   $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
			   $this->form_validation->set_rules('phone_no', 'Phone No.', 'required');
			   $this->form_validation->set_rules('password', 'password', 'required');
			   $this->form_validation->set_rules('password_confirmation', 'Confirm password', 'required|matches[password]');
			   $this->form_validation->set_error_delimiters('<span style="color: red;font-size: 12px;display: inline-block;line-height: 18px;margin-top: 6px;">','</span>');
			
				  if ($this->form_validation->run() == TRUE){
					 $selectRole = $this->input->post('radio');
					 $role_select = $selectRole.'_select[]';
					 $role_field = $selectRole.'_field';
                                         if($this->input->post($role_select))
					 {$ro_select = implode(',',$this->input->post($role_select));}
                                         else
                                         {$ro_select ='';} 
					 $ro_field = $this->input->post($role_field);   //role_name
					  $ro_id = $this->artist->getRoleId($selectRole);
					 if( $ro_field != ''){
						 $var = 1;
						 $inser_id = $this->artist->addNewSubrole($ro_field, $ro_id[0]->role_id);
                                                 if($ro_select!='')
						 {$artistsubrole = $ro_select.','.$ro_field;}
                                                 else
                                                 {$artistsubrole = $ro_field;}
						 
					 }else{
						 $var = 0;
						 $artistsubrole = $ro_select;
					 }
					/* image upload start */
					$config['upload_path']='./uploads';
					$config['allowed_types']='jpg|jpeg|png|JPG|JPEG|PNG';
					$config['file_name']=random_string('alnum', 40).".jpg";
					$this->load->library('upload',$config);
					
				if(!$this->upload->do_upload())
				{ 
				  $image='';
					 
				}
				else
				{ 
					 $imgdata=$this->upload->data();
					 $image=$imgdata['file_name'];  }
					 /* image upload end */
					 
					$dob= $this->input->post('date_day')." ".$this->input->post('date_month')." ".$this->input->post('date_year');
					$dobformat= date_format(date_create($dob),"Y-m-d");
					
					
				 $data = array(
				 'first_name' => $this->input->post('first_name'),
				 'last_name' => $this->input->post('last_name') ,
				 'email' => strtolower($this->input->post('email')),
				 'password' => md5($this->input->post('password')),
				 'access_token'=>md5($this->input->post('email').$this->input->post('password')),
				  'active'=>0,
				 'phone_no' => $this->input->post('phone_no'),
				 'profile_pic' => $image,
				 'role_id' => $ro_id[0]->role_id,
				 'sub_role_id' => $artistsubrole,
				 'dob' => $dobformat,
				 'gender' => $this->input->post('gender'),
				 'location' => $this->input->post('location'),
                 'country' => $this->input->post('country'),
                 'state' => $this->input->post('administrative_area_level_1'),
                 'district' => $this->input->post('locality'),
				 'created_on' => date('Y-m-d H:i:s'),
				 'updated_on' => date('Y-m-d H:i:s')		 
				 
				 );
				 $this->artist->insertUser($data);
                  $last_id=$this->db->insert_id();
                
                     $emailUser=$this->input->post('email');
                 $data_email['name']=$this->input->post('first_name').' '.$this->input->post('last_name');
                 $data_email['message']="<p>You have been successfully registered with Hiartist. Click the button below to Activate your account</p> ";
                  $data_email['link']=base_url().'front/Registeration/userActive/'.$last_id;
                 $data_email['message1']="<p>please visit the site</p>";
                 $data_email['link_text']="Activate Account";
               // $message=$this->load->view('front/email',$data_email,true);
                 //$this->globalmailfunction($emailUser,"Hiartist user Activation ",$message);
				 //$this->session->set_flashdata('success',SUCCESS_REGISTER);
                                 $this->session->set_flashdata('success','You have registered Successfully and Your Profile will be activated after approval by admin');
				 redirect(base_url().'registeration');
				 }
		 
           }

		$allRole = $this->artist->getAllRole(); //user_role data get
		foreach($allRole as $value){
			$result[$value->role]=$this->artist->getListing($value->role_id);
		}

        
		$data['result'] = $result;
                  $data['registertab']="active";
       $data['artist']=$this->home->get_artist_front_menu();
        $data['director']=$this->home->get_director_front_menu();
        $data['groups']=$this->group->getAllGroups();
    $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
         $data['fronttitle']="Register";
$data['fbfail']="yes";
if($this->session->userdata('fbsessn')!='')
{redirect($data['login_url']);}
   
        $this->load->view('front/header',$data);
        $this->load->view('front/register',$data);
        $this->load->view('front/footer');
    }
    public function userActive()
    {
         $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		 $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
        $data['googlelogin'] = $sociallogin[1];
     $data['artist']=$this->home->get_artist_front_menu();
       $data['director']=$this->home->get_director_front_menu();
        $data['groups']=$this->group->getAllGroups();
        $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
            $data['fronttitle']="User Activation";
         $id=$this->uri->segment(4);
         $ndata=array('active'=>1);
        $this->artist->updateUserActive($id,$ndata);
        $this->session->set_flashdata('Active',"You are Successfully Registered");
         $this->load->view('front/header',$data);
        $this->load->view('front/active_message');
        $this->load->view('front/footer');
        
    }
}
?>
