<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
    public function __construct()
	{
		parent::__construct();
         $this->load->model('front/Loginmodel', 'login');
        $this->load->model('front/Groupmodel','group');
 
    
         }
	public function index()
	{ 
         $this->load->model('front/Ci_wppost','post');
         $data['post']=$this->post->getPosts();
         $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		 $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
         $data['googlelogin'] = $sociallogin[1];// googlelogin is used to get Google Login Url from main controller
         $data['homeslider'] = $this->home->getBannerForHome();
$data['recommended'] = $this->home->getRecommendedArtists();
$data['testimonials']=$this->home->getTestimonials();
$data['logos'] = $this->home->getLogosForHome();
$data['video'] = $this->home->getvideosForHome();
$data['adds'] = $this->home->getaddsForHome();
    $data['events'] = $this->home->getEventsForHome();
 $data['homedetails'] = $this->home->getUserForHome();
        $data['user_count']=$this->home->Usercount();
$data['artist']=$this->home->get_artist_front_menu();
$data['director']=$this->home->get_director_front_menu();
$data['groups']=$this->group->getAllGroups();
$data['fronttitle']="Hi Artist";
$data['homepagetab']="active";
$data['noshowmap']="no";
$data['fancybox_and_editor_hide_script']="yes";
$data['fbfail']="yes";

if($this->session->userdata('fbsessn')!='')
{redirect($data['login_url']);}

        $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }

//echo "<pre>";print_r($data['state']);die;
  
                $this->load->view('front/header.php',$data);
                $this->load->view('index.php');
                $this->load->view('front/footer.php');
	}

public function createfbsession(){

$value=$this->input->post('check');
if($value=='yes'){
$this->session->set_userdata('fbsessn','setsession');
echo 'reload';
}
else{echo 'noreload';}


}


public function allstate()
    {             $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		 $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
         $data['googlelogin'] = $sociallogin[1];// googlelogin is used to get Google Login Url from main controller
         $data['artist']=$this->home->get_artist_front_menu();
         $data['director']=$this->home->get_director_front_menu();
         $data['groups']=$this->group->getAllGroups();
         $data['allthestate']=$this->home->get_state_front_menu_forallstates();
         $data['state']=$this->home->get_state_front_menu();
       $data['shoptab']="active";
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
 
        $j=0;
        foreach($data['allthestate'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['allthestate'][$j]->cities=$val;
            $j++;
        }
         $data['fronttitle']="States";
$data['fbfail']="yes";
if($this->session->userdata('fbsessn')!='')
{redirect($data['login_url']);}
        $this->load->view('front/header.php',$data);
        $this->load->view('front/allstate',$data);
        $this->load->view('front/footer.php');
    }
public function contact_message(){
       $to ="hiartist@gmail.com";
          $subject = "Contact Us";
         $message='<h5>contact us form <br> Name : '.$_POST['name'].' Email:'.$_POST['email'].'</h5>
            <p><b> Subject: '.$_POST['subject'].' </b></p>
	    <p><b> Message: '.$_POST['message'].' </b></p>';
         $headers = "From:".$_POST['email']." \r\n";
         $headers .= "MIME-Version: 1.0" . "\r\n";
         $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
         $retval = mail($to,$subject,$message,$headers);
         if($retval == true ) {
            //echo "Message sent successfully...";
        $this->session->set_flashdata('Email','Your message have been sent');
         redirect(base_url()."contact"); 
         
         }else {
            echo "Message could not be sent...";
         }
       }
    public function AddMore()
    {  $sess=$this->session->userdata('usersession');
           if($this->session->userdata('usersession')==''){
           redirect(base_url()); 
            }
        $id=$sess['user_id'];
        $UserData=$this->login->getUserdetailbyId($id);
    if($UserData['sub_role_id']!=''){
           redirect(base_url()."profile/".$id); 
        }
        if($this->input->post('submit')==true)
        {
          //  echo "<pre>";print_r($_POST);die;
           $selectRole = $this->input->post('radio');
					 $role_select = $selectRole.'_select[]';
					 $role_field = $selectRole.'_field';
				        
                                         if($this->input->post($role_select))
					 {$ro_select = implode(',',$this->input->post($role_select));}
                                         else
                                         {$ro_select ='';}  

					 $ro_field = $this->input->post($role_field);   //role_name
					 $ro_id = $this->artist->getRoleId($selectRole);
					 if( $ro_field != ''){
						 $var = 1;
						 $inser_id = $this->artist->addNewSubrole($ro_field, $ro_id[0]->role_id);
						 if($ro_select!='')
						 {$artistsubrole = $ro_select.','.$ro_field;}
                                                 else
                                                 {$artistsubrole = $ro_field;}
						  }else{
						 $var = 0;
						 $artistsubrole = $ro_select;}
                         $id=$this->input->post('user_id');
                     $data=array(
                          'role_id' => $ro_id[0]->role_id,
                 'sub_role_id' => $artistsubrole,
                 'active'=>1,
                'location' => $this->input->post('location'),
                 'country' => $this->input->post('country'),
                 'state' => $this->input->post('administrative_area_level_1'),
                 'district' => $this->input->post('locality')
            );
            $this->login->UpdateUserInfo($id,$data);
            redirect(base_url()."profile/".$id);
        }
        else{
        
       $data['artist']=$this->home->get_artist_front_menu();
        $data['director']=$this->home->get_director_front_menu();
        $data['groups']=$this->group->getAllGroups();
        $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
        $allRole = $this->artist->getAllRole(); //user_role data get
		foreach($allRole as $value){
			$result[$value->role]=$this->artist->getListing($value->role_id);
		}
         $data['result'] = $result;
      $this->load->view('front/header',$data);
        $this->load->view('front/addmoreInfo',$data);
      $this->load->view('front/footer');
        }
    }
public function Page404()
{        $sociallogin = $this->social_login();	// Return Fb and google login urls array from main controller
		 $data['login_url'] = $sociallogin[0];	// Login_url is used to get FB Login Url from main controller
         $data['googlelogin'] = $sociallogin[1];// googlelogin is used to get Google Login Url from main controller
         $data['artist']=$this->home->get_artist_front_menu();
         $data['director']=$this->home->get_director_front_menu();
         $data['groups']=$this->group->getAllGroups();
         $data['state']=$this->home->get_state_front_menu();
        $i=0;
        foreach($data['state'] as $value)
        {
            $val=$this->home->get_state_cities_front_menu($value->id);
            $data['state'][$i]->cities=$val;
            $i++;
        }
         $data['fronttitle']="Page Not Found";
$data['fbfail']="yes";
if($this->session->userdata('fbsessn')!='')
{redirect($data['login_url']);}
        $this->load->view('front/header',$data);
        $this->load->view('front/View404');
      $this->load->view('front/footer');
}
   

   
}
