<style>
    .main-menu li a{
        text-transform: uppercase !important;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/fancybox/css/fullscreen.css" media="screen">
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/fancybox/css/slideshow.css" media="screen">
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/fancybox/css/thumbs.css" media="screen">
<div class="slickslide home_slide">
<?php $u=1;foreach( $homeslider as $value ) { ?>
  <div><img src="<?php echo base_url();?>uploads/<?php echo $value->path;?>" /></div>
<?php } ?>
</div>
<div class="container">
    <ul class="nav nav-tabs nav-justified">
      <li class="active"><a href="#tab1" class="text-uppercase"><i class="fa fa-user" aria-hidden="true"></i> Top Profiles</a></li>
      <li><a href="#tab2" class="text-uppercase"><i class="fa fa-newspaper-o" aria-hidden="true"></i> News</a></li>
      <li><a href="#tab3" class="text-uppercase"><i class="fa fa-calendar" aria-hidden="true"></i> Event Management</a></li>
    </ul>

    <div class="tab-content">
      <div id="tab1" class="content-pane is-active">
        
        <div class="inner_tab_content">
     <?php  if(count($homedetails)==0){ ?> <p class="error-home">No Profile Found</p> <?php } ?>
             <?php  foreach( $homedetails as $value ) {
              ?>
		 <div class="tab_grid">
		 <div class="boxing">
		<a href="<?php echo base_url();?>userprofile/<?php echo trim($value->last_name) ; ?><?php echo trim($value->first_name) ; ?>.<?php echo $value->user_id ; ?>"> <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php if($value->profile_pic==''){echo 'user.jpeg';}else{echo $value->profile_pic;}?>&h=280&w=275" alt="artist"/></a>
		 </div>
		 <div class="artist-ribbon">
		 <span><i style="padding-right:2px;" class="fa fa-eye" aria-hidden="true"></i><?php echo $value->profile_view ; ?></span>
		  </div>
		 <div class="artist-content">
		 <h2><?php echo $value->first_name ; ?> <?php echo $value->last_name ; ?></h2>
		 <p><?php if($value->sub_role_id!=''){$categarray=explode(',',$value->sub_role_id);$thcount=count($categarray)-1;if($thcount){if($thcount==1)$othermore=' + '.$thcount.' more skill';else$othermore=' + '.$thcount.' more skills';}else{$othermore='';}echo $categarray[0].$othermore;}else{echo '';} ?></p>
		 </div>
		 </div>
		<?php } ?>
            
        </div>
        <!-- /.login-box-body -->
		<div class="view_all">
		 <?php  
            if($user_count > 5) {?>
		  <a  href="<?php echo base_url();?>search/Top/Profile/all" class="viewall_tab">View All ></a>
                <?php } ?>
				</div>
      </div>
        
      <!-- /#tab1 -->

      <div id="tab2" class="content-pane">
	 
        <div class="inner_tab_content">
     <?php  if(count($post)==0){ ?> <p class="error-home">No News Found</p> <?php } ?>
		<?php $u=1;foreach( $post as $value ) { if($u<6){ ?>
		
		 <div class="tab_grid">
		 <div class="boxing">
              <div class="itemThumbnail">    
       <a href="<?php echo $value['post']->guid ; ?>"> <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo $value['image']->guid ; ?>&h=280&w=275" alt="events"/></a>
             
       </div>
		<!--<a href="<?php echo $value['post']->guid ; ?>"> <img src="<?php echo $value['image']->guid ; ?>" alt="artist"/></a>-->
		 </div>
          
		 <div class="artist-ribbon">
		 <span>#<?php echo $u; ?></span>
		  </div>
		 <div class="artist-content">
		 <h2><?php echo $value['post']->post_title ; ?> </h2>
		 </div>
		 </div>
		<?php } else{break;} $u++;} 
           ?>
        </div>
        <!-- /.form-box -->
		 <div class="view_all">
          <?php if(count($post)>5) {?>
		 <a href="<?php echo base_url();?>blog" class="viewall_tab">View All ></a>
        <?php } ?>
		</div>
      </div>
      <!-- /#tab2 -->
 
      <div id="tab3" class="content-pane">
	  
        <div class="inner_tab_content">
<?php  if(count($events)==0){ ?> <p class="error-home">No Events Found</p> <?php } ?>
<?php $e=1;foreach( $events as $value ) { if($e<6){ ?>
         <div class="tab_grid">
		 <div class="boxing">
		 <div class="itemThumbnail">    
       <a href="<?php echo base_url();?>ViewEvent/<?php echo $value->event_id;?>"> <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php if($value->image==''){echo 'noimage.png';}else{echo $value->image ;} ?>&h=280&w=275" alt="events"/></a>
        
       <div class="ev_time home_evnts">
       <i aria-hidden="true" class="fa fa-clock-o"></i> <?php echo date_format(date_create($value->start_date),'d F Y');?> |
       <?php echo date_format(date_create($value->start_time),'h:i A');?> 
        </div>     
       </div>
		 </div>
		 <div class="artist-ribbon">
		 <span>#<?php echo $e;?></span>
		  </div>
		 <div class="artist-content">
		 <h2><?php if(strlen($value->title)>=18){echo substr($value->title,0,18);echo "[..]";}else echo $value->title;?></h2>
		 <p><i aria-hidden="true" class="fa fa-map-marker" style="padding-right:2px;"></i><?php echo $value->place ; ?></p>
		 </div>
		 </div>
		<?php } else{break;}  $e++;} 
             ?>
        </div>
          <div class="view_all">
          <?php if(count($events)>5) {?>
          <a href="<?php echo base_url();?>allevents" class="viewall_tab">View All ></a>
            <?php } ?>
			</div>
        <!-- /.form-box -->
      </div>
      <!-- /#tab2 -->
       
    </div>
    <!-- /.tab-content -->
  </div><!-- /.container -->
  
  
  <div class="container">
  <div class="row">
  <div class="col-sm-6">
  <div class="block01 wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0s">
  <?php foreach( $video as  $value){
  if($value->title == "Video-1") {
$lastvalue=explode('/',$value->path);
          $lastval=$lastvalue[count($lastvalue)-1];?>
<a  class="fancyboxvideo fancybox.iframe" href="<?php echo $value->path;?>?autoplay=1&wmode=opaque"><img style="width:300px;height:200px;"  src="http://img.youtube.com/vi/<?php echo $lastval;?>/0.jpg" alt=""/><img id="oneblack" style="width:50px;height:30px;position:absolute;left:130px;top:100px;"  src="<?php echo base_url();?>/assets/artists/images/youtubeblack.png" alt=""/><img id="onered" style="width:50px;height:30px;display:none;position:absolute;left:130px;top:100px;"  src="<?php echo base_url();?>/assets/artists/images/youtubered.png" alt=""/></a>
	<!--<iframe width="300" height="200" src="<?php echo $value->path ; ?>" frameborder="0" allowfullscreen></iframe>-->
  <? } } ?>
  </div>
  <a href="<?php echo base_url();?>HowItwork"><div class="block02 wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0s">
	<h3>How we work</h3>
      </div></a>
  <div style="margin-top:10px;" class="block03 wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0s">
   <?php foreach( $video as  $value){
  if( $value->title == "Video-2") {
$lastvalue=explode('/',$value->path);
          $lastval=$lastvalue[count($lastvalue)-1];?>
<a  class="fancyboxvideo fancybox.iframe" href="<?php echo $value->path;?>?autoplay=1&wmode=opaque"><img style="width:560px;height:315px;"  src="http://img.youtube.com/vi/<?php echo $lastval;?>/0.jpg" alt=""/><img id="twoblack" style="width:50px;height:30px;position:absolute;left:260px;top:350px;"  src="<?php echo base_url();?>/assets/artists/images/youtubeblack.png" alt=""/><img id="twored" style="width:50px;height:30px;display:none;position:absolute;left:260px;top:350px;"  src="<?php echo base_url();?>/assets/artists/images/youtubered.png" alt=""/></a>
<!--<iframe width="560" height="315" src="<?php echo $value->path ; ?>" frameborder="0" allowfullscreen></iframe>-->
 <? } } ?>
	</div>
  </div>
  <div class="col-sm-3 wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0s">
	<a href="#"><div class="block04">
	  <?php foreach( $adds as  $value){
  if( $value->title == "Add-1") {?>
	<img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo $value->path ; ?>&h=357&w=283" alt="image"/>
	  <?php } } ?>
        </div></a>
  <a href="/hiartist/allevents"><div class="block05">
	<h3>Upcoming Events</h3>
  </div></a>
  </div>
  <div class="col-sm-3">
<div class="block06 wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0s">
<div class="testimonial-wrap">

					<div class="testi-slide">
						<?php foreach( $testimonials as $value ) { ?>
						<div>
							<div class="testimonial-img"><span class="border-style-img">
                                <a href="<?php echo base_url();?>userprofile/<?php echo $value->testimonialAddedBy;?>"><img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php if($value->profile_pic){echo $value->profile_pic;} else {echo 'user.jpeg';}?>&h=280&w=275" alt="events"/></a></span></div>
							<p class="testi-quote"><?php if(strlen($value->testimonialText)>=115){echo substr($value->testimonialText,0,115);echo "[..]";}else echo $value->testimonialText;?></p>
							<a href="<?php echo base_url();?>userprofile/<?php echo $value->testimonialAddedBy;?>"><div class="testi-name"><?php echo $value->first_name ; ?> <?php echo $value->last_name ; ?></div></a>
							<div class="testi-cat"><?php echo $value->sub_role ; ?></div>
						</div>
						<?php } ?>
					</div>
<?php  if(count($testimonials)) { ?>
<a href="<?php echo base_url();?>allTestimonials" class="testimonial-viewall">View all</a>
<?php } else { ?>
<span style="color:white"><h4>No Testimonials Yet</h4></span>
<?php } ?>
				</div>
  </div>
  </div>
  </div>
  </div>
	<?php  if(count($recommended) > 8){?>
	<section class="aoh">
   <div class="container">
  <div class="row">

<div style="display:inline-block;float:left" class="col-md-12">
<div class="subtitle search_result h1">
<h1>Recommended Artists</h1>
</div>
</div>

<div class="col-sm-9">
<div class="h_artist">
<div class="row">
	<?php $r=1;foreach($recommended as $value){ if($r<13){ ?>
 <div class="col-sm-3 col-xs-6 nopadding">
 <div class="h_artist_inner">
 <div class="boxing">
<?php if($value->profile_pic ==''){?>
      <a href="<?php echo base_url();?>userprofile/<?php echo trim($value->last_name) ; ?><?php echo trim($value->first_name) ; ?>.<?php echo $value->user_id ; ?>"> <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo 'user.jpeg' ; ?>&h=275&w=275" alt="artist"/></a>
    <?php } else {?>
		 <a href="<?php echo base_url();?>userprofile/<?php echo trim($value->last_name) ; ?><?php echo trim($value->first_name) ; ?>.<?php echo $value->user_id ; ?>"> <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo $value->profile_pic ; ?>&h=275&w=275" alt="artist"/></a>
     <?php } ?>
		 </div>
<div class="artist-ribbon">
		 <span><?php 		  if($value->sub_role_id!=''){$categarray=explode(',',$value->sub_role_id);$thcount=count($categarray)-1;if($thcount){if($thcount==1)$othermore=' + '.$thcount.' more skill';else$othermore=' + '.$thcount.' more skills';}else{$othermore='';}echo $categarray[0].$othermore;}else{echo '';}		 ?></span>
		  </div>
<h2><?php echo $value->first_name.' '.$value->last_name;?></h2>
 </div>
 

 </div>
<?php } else{break;} $r++;} ?>
  </div>
 
 </div>

 </div>
 <div class="col-sm-3 nopadding">
 <div class="advertisement">
	  <?php foreach( $adds as  $value){
  if( $value->title == "Add-2") {?>
	<img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo $value->path ; ?>&h=674&w=284" alt="add"/>
	  <?php } } ?>
 </div>
 </div>

	 </div>
  </div>
  </section>
<?php } ?>
	
  
 <!-- <section class="partners">

      
  <div class="container">
  <div class="row">
       <h2 class="title">Featured partners</h2>
  <div class="our_partners">
	<div class="slick-list">
<?php //$e=1;foreach( $logos as $value ) { ?>
 <div><img src="<?php //echo base_url();?>uploads/<?php //echo $value->path;?>" alt="partner"/></div>
<?php //} ?>
</div>
   </div>
   </div>
   </div>
   </section>-->
   
