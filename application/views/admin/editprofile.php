
 <!-- Content Header (Page header) -->
  

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-sm-12">
          
          <div class="box">
            <div style="display:none;" class="box-header success">
            <div class="alert alert-success fade in">
              <h3 class="box-title"></h3>
              </div>
            </div><!-- /.box-header -->
            <div class="box-body">
     

   
    
            <form class="form-horizontal" id="adddynamicimg" name="addartist" action="<?php echo base_url();?>admin/dashboard/editProfile"  enctype="multipart/form-data"  method="post">
      
 <p id="msgg" style="color:green;"></p>
     

              <div class="box-body">   
                     <div class="form-group">
                  <label class="col-sm-4 control-label required-field" for="name"><b>Name</b></label>
                   <div class="col-sm-8">
                  <input class="form-control" type="text" size="35" name="name" id="name" value="<?php echo $details[0]->name;?>"/>
                <span style='color:red'><?php echo form_error("name"); ?></span>
                </div>
                </div>
                 

<div class="changepswdiv" style="display:none">
                          <div class="form-group">
                  <label class="col-sm-4 control-label" for="password"><b>Old Password</b></label>
                  <div class="col-sm-8">
                  <input class="form-control" type="password" size="35" name="password" id="password"  onfocus="$('#pwdnotmatch').text('');"/>
        <p id="oldpwddd" style='color:red;font-size:11px;'></p>
                <span style='color:red'><?php echo form_error("password"); ?></span>
                 </div>
                </div>



                <div class="form-group">
                  <label class="col-sm-4 control-label" for="newpassword"><b>New Password</b></label>
                  <div class="col-sm-8">
                  <input class="form-control" type="password" size="35" name="newpassword" id="newpassword"  onfocus="$('#confpwdnotmatch').text('');"/>
                 <p id="confpwdnotmatch" style='color:red;font-size:11px;'></p>
                <span style='color:red'><?php echo form_error("password"); ?></span>
                 </div>
                </div>
                  
                <div class="form-group">
                  <label class="col-sm-4 control-label" for="confpassword"><b>Confirm Password</b></label>
                  <div class="col-sm-8">
                  <input class="form-control" type="password" size="35" name="confpassword" id="confpassword" onfocus="$('#confpwdn').text('');"/>
                
                <span style='color:red'><?php echo form_error("password"); ?></span>
                 </div>
                </div>

</div>

  <div class="form-group">
                  <label class="col-sm-4 control-label required-field" for="exampleproductname1"><b>Email</b></label>
                   <div class="col-sm-8">
                  <input class="form-control" type="email" size="35" name="email" id="email" value="<?php echo $details[0]->email;?>"/>
                <span style='color:red'><?php echo form_error("email"); ?></span>
                </div>
                </div>


<div class="form-group">
                      <label class="col-sm-4 control-label"for="exampleInputFile"><b>Image</b></label>
                         <div class="col-sm-8">
                       <img  src="<?php echo base_url();?>uploads/<?php if($details[0]->image!=''){echo $details[0]->image;}else echo 'user.jpeg';?>"><br/><b>Change Image</b> 
                       <input type="file" name="userfile"> 
                   
                    </div>
                    </div>
    <input type="hidden" name="hidden" value="<?php echo $details[0]->image;?>"/>
                 <input type="hidden" name="chkoldpass" id="chkoldpass" value="<?php echo $details[0]->password;?>"/>

                <div class="box-footer">
                   <input type="submit" value="Update" class="btn btn-info pull-left" id="editp" name="update"/>
    <input style="color:white;background-color:#ff6060;border:none;height:34px" type="button"  value="Change Password" id="chgpsw"  name="chgpsw"/>
<input style="display:none" type="button"  value="Hide" id="hidechgpsw" class="btn btn-info" name="hidechgpsw"/>
           
                   
                  
                     </div>
                     </div>
                  </form>
              </div>
            
        
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </section><!-- /.content -->



<script>
$("#editp").click(function(e){
var str1=$("#name").val();
var str2=$("#email").val();
var str3=$("#password").val();
var str4=$("#newpassword").val();
var str5=$("#confpassword").val();
var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{1,4})+$/;



if(str1.trim() == '' || str2.trim() == '' || !(regex.test(str2)) || str3.trim() != '' || str4.trim() != '' || str5.trim() != '')
  {
    if(str1.trim() == '')
    $("#name").css('border','1px solid red');
    if(str1.trim() != '')
    $("#name").css('border','1px solid #d2d6de');
    if(str2.trim() == '')
    $("#email").css('border','1px solid red');
  if(str2.trim() != '')
    $("#email").css('border','1px solid #d2d6de');
  if(str3.trim() == '')
    $("#password").css('border','1px solid red');
   else
    $("#password").css('border','1px solid #d2d6de');
  if(str4.trim() == '')
    $("#newpassword").css('border','1px solid red');
   else
    $("#newpassword").css('border','1px solid #d2d6de');
  if(str5.trim() == '')
    $("#confpassword").css('border','1px solid red');
   else
    $("#confpassword").css('border','1px solid #d2d6de');
if(!(regex.test(str2)) && str2.trim()!='')
{$('#email').css('border','1px solid red');}
else
{if(str2.trim()!=''){$('#email').css('border','1px solid #ccc');}}
if(str3.trim() != '' || str4.trim() != '' || str5.trim() != ''){
$.ajax({
        type: "POST",
        url: "<?php echo base_url();?>admin/dashboard/chkoldpwd",
        data:{password:str3},
       success: function(data){if(data=="notexist"){$("#password").css('border','1px solid red');$('#oldpwddd').text("old password does not match.");}
else{ $("#password").css('border','1px solid #d2d6de');$('#oldpwddd').text("");}
},
failure: function(){$('#tab').html("form not submitted");}
});
if(str4!=str5){ $('#confpwdnotmatch').text("Your password does not match confirm password field");}
else{ $('#confpwdnotmatch').text("");}
}
else
{ $("#password").css('border','1px solid #d2d6de'); $("#newpassword").css('border','1px solid #d2d6de'); $("#confpassword").css('border','1px solid #d2d6de');}
var olp=$('#oldpwddd').text();
if(str1.trim() != '' && str2.trim() != '' && str4==str5 && str4.length>0 && regex.test(str2) && olp=='')
{return true;}  
    return false;

  }
});
</script>

<script>
$('#chgpsw').click(function(){
$("#password").val('');
$("#newpassword").val('');
$("#confpassword").val('');
$('.changepswdiv').show();
$(this).hide();
$('#hidechgpsw').show();
});

$('#hidechgpsw').click(function(){
$("#password").val('');
$("#newpassword").val('');
$("#confpassword").val('');
$('.changepswdiv').hide();
$(this).hide();
$('#chgpsw').show();
});
</script>
