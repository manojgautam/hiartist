<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
<script src="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>

 <section class="content-header">
          <h1>
            <small></small>
        </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Manage Events</li>
            <li class="active">Edit Events</li>
          </ol>
        </section> 

<section class="content">
  
<div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step left-align add-width-to-categ">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
        <p>Edit Event</p>
      </div>
      <div class="stepwizard-step center-align">
        <a href="#step-2" type="button" class="btn btn-default btn-circle stopdropp">2</a>
        <p>Edit Cost</p>
      </div>
      <div class="stepwizard-step right-align">
        <a href="#step-3" type="button" class="btn btn-default btn-circle">3</a>
        <p>Edit Location</p>
      </div>
    </div>      
  </div>             
  
  <?php //echo form_open_multipart('admin/dashboard/doaddEvent');?>
  <form  id="data" method="post" enctype="multipart/form-data" action="<?php echo base_url()?>admin/event/editEvent/<?php echo $eid?>">
  <?php if($this->session->flashdata('success')!=''){ ?><div class="isa_success">
    <?php echo "<p style='position:relative;color:green;left:10px;' class='show_msg'>".$this->session->flashdata('success')."</p>";?></div><?php } ?>
    <div class="row setup-content" id="step-1">
      <div class="col-cs-12 col-md-6 col-md-offset-3">
        <div class="col-md-12" style="background:white;">
          <h3>Edit Event</h3>
 <?php if(form_error('longitude')!='') echo "<span style='color:red;'>Please fill correct details</span>"; ?>
           <div class="form-group">
                <label class="required-field">Event Title</label>
                <input type="text"  id="eventtitle"  class="form-control" required="required" placeholder="Title" name="title" value="<?php echo $event[0]->title;?>">
                <?php echo form_error('title'); ?>
            </div>

          <div class="form-group">
                      <label>Event Category</label>
                     
                    <input type="text"  id="category"  class="form-control" placeholder="Category" name="category" value="
                    <?php echo $event[0]->category;?>">
                  

                      
             </div>
          


             <div class="form-group">
            <label>Event Description</label>
              <div class="box-body pad">
              <textarea class="textarea" id="desc" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 0.85em;line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name="description" value="<?php echo $event[0]->description;?>"><?php echo $event[0]->description;?></textarea>
            </div>
            </div>


           <!-- Date range -->
                  <div class="form-group daterange">
                    <label class="required-field">Date range</label>
<input placeholder="Select Date Range" name="startenddate" readonly  required="required" type="text" class="form-control pull-right" id="reservation" style="background:white;" value="<?php echo date_format(date_create($event[0]->start_date),'m/d/Y').' - '.date_format(date_create($event[0]->end_date),'m/d/Y');?>" onfocus="$('#dateformaterror').text('');">
<?php echo form_error('startenddate'); ?>
<p id="dateformaterror" style="color:red"></p>
                 </div>

<br/><br/>



<div class="form-group">
   <div class="row">
<div class="col-xs-6">
<label style="color:black">Start Time</label>
          <div class="get-started bootstrap-timepicker">
            <input type="text"  id="timepicker" placeholder="Select Start Time" class="form-control ajax_filter" name="starttime" value="<?php echo date_format(date_create($event[0]->start_time),"h:i A");?>" onfocus="$('#timeformaterror').text('');">
          </div>
        </div>
 <input type="hidden" id="sttm" value="<?php if($event[0]->start_time=="00:00:00"){echo 2;}else echo 1;?>"/>

<div class="col-xs-6">
<label style="color:black">End Time</label>
          <div class="get-started bootstrap-timepicker">
            <input type="text"  id="timepicker1" placeholder="Select End Time" class="form-control ajax_filter" name="endtime" value="<?php echo date_format(date_create($event[0]->end_time),"h:i A");?>" onfocus="$('#timeformaterror').text('');">
          </div>
        </div>
 </div>
 <input type="hidden" id="edtm" value="<?php if($event[0]->end_time=="00:00:00"){echo 2;}
else echo 1;?>"/>
<p id="timeformaterror" style="color:red"></p>
 </div>



               <label style="color:black;font-size:16px;">Image</label>
<div class="imgss">
                     <img width="300px" height="200px" src="<?php echo base_url();?>uploads/<?php if($event[0]->image)echo $event[0]->image; else echo 'user.jpeg';?>">
</div>
              <div class="form-group">
                      <label for="exampleInputFile">Change Image (max size:2MB)</label>
                      <input type="file" name="userfile" id="userfile">
               </div>
  <input type="hidden" name="hiddenname" value="<?php echo $event[0]->image;?>">
          
              <div class="form-group">
            <label>Event Type</label><br/>
  <input type="radio" name="eventtype" value="0" <?php if($event[0]->event_type==0)echo "checked";?>> Free &nbsp;&nbsp;<input type="radio" name="eventtype" value="1" <?php if($event[0]->event_type==1)echo "checked";?>> Sponsored<br>
  </div>


<div class="form-group">
                  <div class="row">
                    <div class="col-xs-6">
                      <label  style="color:black">Contact Number</label>
                      <div class="amount-group">
                       
           <input type="text"  class="form-control " name="phone"  placeholder="Contact Number" value="<?php echo $event[0]->contact_no;?>">
                      </div><!-- /.input group -->
                     
                    </div>
                    <div class="col-xs-6">
                      <label  style="color:black">Email Id</label>
                      <div class="currency-group ">
                       
                      <input type="email"  class="form-control" id="email" name="email"  placeholder="Email" value="<?php echo $event[0]->email;?>"> 
                      </div><!-- /.input group -->
                   </div>
                    </div>
                  </div>

                
          <div class="box-footer">
          <button class="btn btn-primary nextBtn btn-sm pull-right firstnext" type="button" >Next</button>
         </div>
        </div>
      </div>
    </div>




    <div class="row setup-content" id="step-2">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12" style="background:white">
          <h3> Edit Cost</h3>

<div class="form-group">
                  <div class="row">
                    <div class="col-xs-6">
                      <label style="color:black">Cost</label>
                      <div class="amount-group">
                       
           <input type="text"  id="onlycost" class="form-control thecost" pattern="[0-9]*(\.\d+)?" name="cost"  placeholder="Amount" value="<?php echo $event[0]->cost;?>">
                      </div><!-- /.input group -->
                     
                    </div>
                    <div class="col-xs-6">
                      <label style="color:black">Currency</label>
                      <div class="currency-group thecurr">
                       
                     <input type="text"  id="currency" class="form-control thecost"  name="currency"  placeholder="Currency" value="<?php echo $event[0]->currency;?>">
                      </div><!-- /.input group -->
                   </div>
                    </div>
                  </div>
           
      

         <div class="box-footer">
          <button class="btn btn-primary nextBtn btn-sm pull-right" type="button" >Next</button>
        </div>
        </div>
      </div> 
    </div>


    <div class="row setup-content" id="step-3">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12" style="background:white">
          
          <h3>Edit Location</h3>
           <div class="form-group">
                      <label class="required-field">Location</label>
                    
                      <input type="text" required="required" id="searchTextField" name="state" value="<?php echo $event[0]->place;?>" size="40" class="form-control input pull-right searchTextField"  placeholder="Enter your location">

                   <?php echo form_error('state'); ?><?php echo form_error('longitude'); ?>
                      </div>
                       
              
<?php if(form_error('longitude')){}
else{ ?>
<br><br>
<?php } ?>
<div class="form-group">
                  <div class="row">
                    <div class="col-xs-6">
                      <label style="color:black" class="required-field">Latitude</label>
                      <div class="">
                       
          <input id="lati" pattern="[-+]?[0-9]*(\.\d+)?" class="form-control" required="required" placeholder="Latitude" name="latitude" value="<?php echo $event[0]->latitude;?>" type="text">
<p id="errforlat"></p>
                      </div><!-- /.input group -->
                     
                    </div>
                    <div class="col-xs-6">
                      <label style="color:black" class="required-field">Longitude</label>
                      <div class=" ">
                       
                  <input id="long" pattern="[-+]?[0-9]*(\.\d+)?" class="form-control" required="required" placeholder="Longitude" name="longitude" value="<?php echo $event[0]->longitude;?>" type="text"> 
<p id="errforlong"></p>
                      </div><!-- /.input group -->
                   </div>
                    </div>
                  </div>



                    <input type='hidden' value='1' class='shw_statez'>
                   

                      <div class="form-group">
                      <label class="required-field">Venue</label>
                      <input type="text" class="form-control" id="venue" required="required" placeholder="Venue" name="venue" value="<?php echo $event[0]->venue;?>">
                    <?php echo form_error('venue'); ?>
                    </div>

                   
<div class="box-footer">
<p class="newerrormsgg" style="color:red"></p>
          <button class="btn btn-success nextBtn btn-sm pull-right" type="submit" id="fromabc" value="submit" name="addevent" >Submit</button>
<div>
        </div>
      </div>
    </div>
  </form>
 </section>

<script>
 
    $(document).ready(
   function () {


    //var date = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();

    $( "#datepicker" ).datepicker({
      changeMonth: true,//this option for allowing user to select month
      changeYear: true, //this option for allowing user to select from year range
      dateFormat: 'dd-mm-yy'
    }).val();
  }

);

       $("#timepicker").timepicker({
        showInputs: false
        });
        $("#timepicker1").timepicker({
        showInputs: false
        });

</script>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>

<script>
$(document).ready(function(){
if($("#sttm").val()==2)
$("#timepicker").val('');
if($("#edtm").val()==2)
$("#timepicker1").val('');
});
</script>

<script type="text/javascript">
function handleError()
{
   return true;
}
window.onerror = handleError;
</script>


  
