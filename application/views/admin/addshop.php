<section class="content-header">
          <h1>
            <small></small>
        </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Manage Shops</li>
            <li class="active">Add Shop</li>
          </ol>
</section>
<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Add Shop</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" id="shopform" method="post" enctype="multipart/form-data" action="<?php echo base_url()?>admin/groupShop/addShop">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="shopname" class="required-field">Name</label>
                      <input class="form-control" id="shopname" placeholder="Shop Name" name="name" type="text">
                       <?php echo form_error('name'); ?>
                    </div>
                   <div class="form-group">
                      <label>Description</label>
                      <textarea class="form-control" rows="3" id="desc" name="desc" placeholder="Enter Details.."></textarea>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">Add Banner Image</label>
                      <input id="exampleInputFile" type="file" name="userfilephp">
                    </div>
                     <div class="form-group">
                      <label for="exampleInputFile">Add Shop Images</label>
                      <input id="exampleInputFile" type="file" name="userfile1[]" multiple="multiple">
                    </div>
                    <div class="form-group">
                      <label class="required-field">Select State</label>
                      <select id="thestate" class="form-control" name="state">
                        <option value="">Select State</option>
                        <?php foreach($stateslist as $val){ ?>
                        <option value="<?php echo $val->id;?>"><?php echo $val->name;?></option>
                        <?php } ?>
                      </select>
                      <?php echo form_error('state'); ?>
                    </div>
                    <div class="form-group">
                      <label class="required-field">Select District</label>
                      <select id="thedistricts" class="form-control" name="district">
                        <option value="">Select District</option>
                      </select>
                      <?php echo form_error('district'); ?>
                    </div>
                    <div class="form-group">
                      <label for="owner">Shop Owner</label>
                      <input class="form-control" id="owner" placeholder="Shop Owner" name="owner" type="text">
                    </div>
                     <div class="form-group">
                      <label for="contact">Contact Number</label>
                      <input class="form-control" id="contact" placeholder="Contact Number" name="contact" type="text">
                    </div>
                      <div class="form-group">
                      <label for="email">Email</label>
                      <input class="form-control" id="email" placeholder="Email" name="email" type="email">
                    </div>
                    <div class="form-group">
                      <label for="address">Shop Address</label>
                      <input class="form-control" id="address" placeholder="Shop Address" name="address" type="text">
                    </div>
                    <div class="form-group">
                      <label for="website">Website</label>
                      <input class="form-control" id="website" placeholder="www.example.com" name="website" type="text">
                    </div>
                  
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-info" style="margin-left:10px;" name="shopsubmit" value="shopsubmit">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->              
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section>

        <script>
$(document).ready(function(){
$("#thestate").change(function(){
    $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>admin/groupShop/getdistricts",
        data:{stateid:$('#thestate').val()},
       success: function(data){$('#thedistricts').html(data);},                   
      failure: function(){}                                                        
    });
  });
});
</script>

<script>
$(document).ready(function(){
$("#shopform").submit(function(){
var shopname=$("#shopname").val();
var state=$("#thestate").val();
var city=$("#thedistricts").val();
if(shopname.trim()=='' || state=='' || city==''){
if(shopname.trim() == '')
$("#shopname").css({"border":"1px solid red","width":"100%","height":"31px"});
else
$("#shopname").css({"border":"1px solid #d2d6de","width":"100%","height":"31px"});
if(state == '')
$("#thestate").css({"border":"1px solid red","width":"100%","height":"31px"});
else
$("#thestate").css({"border":"1px solid #d2d6de","width":"100%","height":"31px"});
if(city == '')
$("#thedistricts").css({"border":"1px solid red","width":"100%","height":"31px"});
else
$("#thedistricts").css({"border":"1px solid #d2d6de","width":"100%","height":"31px"});
return false;
}
}); 
});
</script>
