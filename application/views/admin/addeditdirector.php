 <section class="content">
          <div class="row">
            <div class="col-xs-12">
              
              <div class="box ">
            <div style="display:none;" class="box-header success">
            <div class="alert alert-success fade in">
              <h3 class="box-title"></h3>
              </div>
            </div>
                <div class="box-body">
         <?php if(isset($title)){ ?>
                <form  class="form-horizontal" id="eartist" name="adddirector" action="<?php echo base_url();?>admin/role/adddirector" method="post">
                 <?php } else {?> 
                <form  class="form-horizontal" id="eartist" name="editdirector" action="<?php echo base_url();?>admin/role/editdirector/<?php echo $id;?>" method="post">
                 <?php } ?>  
                    <div class="box-body">
                 
                  <div class="form-group">
                 <p class="errduplicate" style="color:red;"></p>   
                 <label class="col-sm-3 control-label" for="exampleInputIsparant1"><b>Director Role</b></label>             
<input type="text" id="edart" class="form-control spl" placeholder="Role Name" name="directorrole" value="<?php if(isset($subrole)){echo $subrole[0]->sub_role;}?>">
<input type="hidden" id="hidd" class="form-control"  name="hiddenrole" value="<?php if(isset($subrole)){echo $subrole[0]->sub_role;}?>">
                <?php echo form_error('title'); ?>
                 </div>
         

                 <input type="hidden" value="<?php if(isset($title)){ echo 'Add';}else{echo 'Update';}?>" id="hidvall"/>

                    <div class="box-footer">
                       <input type="submit" value="<?php if(isset($title)){ echo 'Add';}else{echo 'Update';}?>" class="rolesubmit btn btn-default" name="submit"/>
                  
                        </div>
                  
                  </div>
                      </tr>
                    </thead>
                    <tbody>
                     
                   

                    </tbody>
                    
                  </table>
                </div>
                </form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
<script>
var check =1;
$("#eartist").submit(function(e){ 
var str=$("#edart").val()
if(str.trim() == "")
{
$("#edart").css("border-color", "red");
return false;}
else
$("#edart").css("border-color", "#d2d6de");

if($('#hidvall').val()=='Add'){
if(check == 1){
e.preventDefault();
$.ajax({
        type: "POST",
        url: "<?php echo base_url();?>admin/Role/checkDuplicate",
        data:{rolename:str},
        success: function(data){
if(data=="notadd"){
$('.errduplicate').text('The Role already exists.');
}
else{
check=2;
$('.errduplicate').text('');
$(".rolesubmit").click();
}
}                                                      
});
}

}
});
</script>
