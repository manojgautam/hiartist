<style>
.btn-info,.btn-info:hover,.btn-info.hover,.btn-info:focus,.btn-info:link,.btn-info:visited,.btn-info:active{
  background-color: #ff6060;
  border-color: #ff6060;
  border-radius: 0;
  font-size: 16px;
  font-weight: normal;
  padding: 5px 0;
  text-align: center;
transition: all 0.5s ease-out 0s;
  width: 85px !important;
}


</style>     


<p class="login-box-msg">Log in to start your session</p>   
<div id="tab"></div>
       <?php if(isset($fulldata)) echo $fulldata;?>
       <?php if($this->session->flashdata('msg')!=''){
            echo "<span style='color:red' class='emsgg'>".$this->session->flashdata('msg')."</span>";}?>
        <form name="myform" action="<?php echo base_url();?>admin/login/login" method="post" id='loginform'>
          <div class="form-group has-feedback">
            <input type="text" id="emailorname" name="emailorname" class="form-control" autocomplete="off" placeholder="Email" value="" onfocus='$(".erroremail").text("");'>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            <p class="erroremail" style="color:red;"></p>
            <?php echo form_error('emailorname','<p style="color:red">','</p>'); ?>
          </div>
          
          <div class="form-group has-feedback">
            <input type="password" id="password" name="password" class="form-control" placeholder="Password" value="" onfocus='$(".errorpassword").text("");'>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
             <p class="errorpassword" style="color:red;"></p>
             <?php echo form_error('password','<p style="color:red">','</p>'); ?>
          </div>
          
         
         <div class="row">
            <div class="col-xs-8">
               
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" value="submit" style="position:relative;top:2px;border-color: #f70001;background: #f70001;" class="btn btn-primary btn-block btn-flat btn-info" id="loginsubmit" name="loginsubmit" >Sign In</button>
            </div><!-- /.col -->
          </div>
        </form>

        




      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

<script>
$('#loginsubmit').click(function(){
$('.emsgg').hide();
if($('#emailorname').val().trim()=='' || $('#password').val().trim()==''){
if($('#emailorname').val().trim()==''){$('.erroremail').text('Field is required');}
if($('#password').val().trim()==''){$('.errorpassword').text('Field is required');}
return false;
}
});

</script>
   
