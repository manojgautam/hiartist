 <section class="content">
          <div class="row">
            <div class="col-xs-12">
              
              <div class="box ">
            <div style="display:none;" class="box-header success">
            <div class="alert alert-success fade in">
              <h3 class="box-title"></h3>
              </div>
            </div>
                <div class="box-body">
       
                <form  class="form-horizontal" id="" name="" enctype='multipart/form-data' action="<?php echo base_url();?>admin/media/editmedia/<?php echo $mediatype;?>/<?php echo $mediaid;?>" method="post">
           
                    <div class="box-body">
                 
                  <div class="form-group">   
                 <label class="col-sm-3 control-label" for="exampleInputIsparant1"><b>Title</b></label>             
                 <input type="text" id="edart" class="form-control spl" placeholder="Title" name="title" value="<?php echo $editdata[0]->title;?>">
                 </div>
<?php if($mediatype==2){?>
                 <div class="form-group">   
                 <label class="col-sm-3 control-label" for="videourl"><b>Video URL</b></label>             
                 <input type="text" id="videourl" class="form-control spl" placeholder="Video URL" name="url" value="<?php echo $editdata[0]->path;?>">
                 </div>
<?php } else{ ?>

<?php if($mediatype==3){?>
<div class="form-group">
                      <label>Description</label>
                      <textarea placeholder="Enter Details.." name="desc" id="desc" rows="3" class="form-control" value="<?php echo $editdata[0]->description;?>"><?php echo $editdata[0]->description;?></textarea>
</div>
<?php } ?>
 
<label class="col-sm-3 control-label" for="exampleInputIsparant1"><b>Image</b></label>
<div class="imgss">
                     <img width="300px" height="200px" src="<?php echo base_url();?>uploads/<?php if($editdata[0]->path)echo $editdata[0]->path; else echo 'noimage.png';?>">
</div>
               <div class="form-group">
<label for="exampleInputFile">Change Image</label>
                      <input type="file" name="userfile" id="userfile" class="inputFile">
                      <div class="errimg alert alert-danger" style="display:none;">File is too large max 2 MB</div>
               </div>

<input type="hidden" name="hiddenname"  value="<?php echo $editdata[0]->path;?>">
<?php } ?>

                    <div class="box-footer">
                       <input type="submit" value="Update" class="btn btn-default" name="submit"/>
                  
                        </div>
                  
                  </div>
                      </tr>
                    </thead>
                    <tbody>
                     
                   

                    </tbody>
                    
                  </table>
                </div>
                </form>
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
        
<script>
$(document).ready(function () {
var _URL = window.URL || window.webkitURL;
$('.inputFile').bind('change', function() {
  //this.files[0].size gets the size of your file.

  if(this.files[0].size > 2097152)
  {$('.inputFile').val(''); 
  $('.errimg').show();
} else {
      $('.errimg').hide();
}
  if (!(/\.(gif|jpg|jpeg|png|bmp|GIF|JPG|JPEG|PNG|BMP)$/i).test(this.files[0].name)) {              
    $('.inputFile').val(''); alert('You must select an image file only');               
    }
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        img.onload = function() {
            if(this.width < 350 || this.height < 350)
            {
                
            } 
        };
   
        img.src = _URL.createObjectURL(file);


    }
});});

</script>

