

        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          Groups List
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Groups</li>
            <li class="active"></li>
          </ol>
        </section>

     

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              
              <div class="box">
                <div class="box-header">
                   <h4 style="display:inline;" class="box-title">  <?php if($this->session->flashdata('deleteit')!='') echo "<span class='show_msg withadddelete'>".$this->session->flashdata('deleteit')."</span>";?></h4>
<h4 style="display:inline;" class="box-title">  <?php if($this->session->flashdata('addedit')!='') echo "<div class='show_msg withaddaddoredit' >".$this->session->flashdata('addedit')."</div>";?></h4>
                </div><!-- /.box-header -->
                <form method='post'><input type="submit" value='Delete' class='del_event btn btn-primary'  style='display:none;background:hsl(0, 100%, 69%);border:none;margin:10px;' name='bulk_delt'>
                <div id="no-more-tables" class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                      <th><input type='checkbox' name="deleteall[]" class='checkAll'></th>
                       <th>Group Name</th>
                       <th>Group Members</th>
                       <th>Website</th>
                       <th>Blog</th>
                       <th>Banner Image</th>
                        <th>Actions</th>

                      </tr>
                    </thead>
                    <tbody>
                     <?php foreach($groups as $data){ ?>
                     <tr>
                     <td><input type='checkbox' class='checkdata' value='<?php echo $data->group_id;?>' name='checked_events[]' onclick='chekselctbox()'></td>
                     <td><a href="<?php echo base_url();?>viewgroup/<?php echo $data->group_id;?>" target="_blank"><?php echo $data->name;?></a></td>
                      <td><?php echo $data->group_members;?></td>
                      <td><?php echo $data->website;?></td>
                      <td><?php echo $data->blog_link;?></td>
                      <td style="width:100px;height:100px;"><img src="<?php echo base_url();?>uploads/<?php if($data->image==''){echo 'user.jpeg';}else echo $data->image;?>"/></td>
                      <td><a href="<?php echo base_url();?>admin/groupShop/editGroup/<?php echo $data->group_id;?>"> <i title='Edit' class="fa fa-edit"></i></a>&nbsp;&nbsp;
                      <a href="<?php echo base_url();?>admin/groupShop/deleteGroup/<?php echo $data->group_id;?>" onclick="return confirm('Do you really want to delete?')"><i title='Delete' class="fa fa-trash-o"></i></a>&nbsp;&nbsp;
                     <?php
                     if($data->active == 1){
                      echo "<a class='fa fa-power-off' title='Activated' href='".base_url()."admin/groupShop/changestatus/". $data->group_id."/0'></a>";
                     }
                     else{
                       echo "<a class='fa fa-circle-o-notch' title='Deactivated' href='".base_url()."admin/groupShop/changestatus/". $data->group_id."/1'></a>";
                      }
                      ?>
<?php if(count($data->grpimages)){?>
<a href="javascript:void(0);" data-toggle="modal" data-target="#myModal<?php echo $data->group_id;?>">Images</a>&nbsp;&nbsp;
<?php } ?>
<!-- Modal start-->
  <div class="modal fade" id="myModal<?php echo $data->group_id;?>" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">View/Delete Group Inner Images</h4>
        </div>
        <div class="modal-body">
<?php foreach($data->grpimages as $val){?>
         <div style="display:inline-block;padding:5px;" class="covergpimages<?php echo $val->group_image_id;?>"><img style="width:100px;height:100px;" src="<?php echo base_url();?>uploads/<?php echo $val->image_name;?>"/><input style="background:black;color:white;position:relative;top:-40px" type="button" class="delgpimgs" id="<?php echo $val->group_image_id;?>" value="X"/></div>
<?php } ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
<!-- Modal end-->

                     </td>
                   </tr>
                    <?php } ?> 

                    </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->


 <script src="<?php echo base_url();?>assets/artists/js/jquery.js"></script>
<script>
$(document).ready(function () {
  $("body").on('click',".delgpimgs",function() {
var gpimageid=$(this).attr('id');
    $.ajax({
          url: "<?php echo base_url();?>admin/GroupShop/deletegrpimages",
      type: "POST",
      data:{gpimgid:gpimageid},
      success: function(data)
        {
      $('.covergpimages'+gpimageid).hide();
        },
        error: function() 
        {
        }           
     });
  });
});
</script>

      
         
