<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.css">
<script src="<?php echo base_url();?>assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>

 <section class="content-header">
          <h1>
            <small></small>
        </h1>
          
        </section> 

<section class="content">
  
<div class="stepwizard col-md-offset-3">
    <div class="stepwizard-row setup-panel">
      <div class="stepwizard-step left-align add-width-to-categ">
        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
        <p>Add Address</p>
      </div>
      <div class="stepwizard-step center-align">
        <a href="#step-2" type="button" class="btn btn-default btn-circle stopdropp">2</a>
        <p>Add Contact</p>
      </div>
    
    </div>      
  </div>             
  <form  id="dataaddress" method="post"  action="<?php echo base_url();?>admin/dashboard/updateAdminInfo">
       <?php
   $id =$this->uri->segment(4);?>
      <input type="hidden" name="id" value="<?php echo $id;?>">
      <div class="row setup-content" id="step-1">
      <div class="col-cs-12 col-md-6 col-md-offset-3">
        <div class="col-md-12" style="background:white;">
          <h3>Add Address</h3>
   <div class="form-group">
        <div class="row">
                     <div class="col-xs-6">
                <label style="color:black" class="required-field">Address</label>
                <input type="text"  id="address1"  class="form-control" placeholder="address" name="street_address" value="<?php echo $details[0]->street_address;?>" onfocus="$('#error_name').text('');" ><p id="error_name"></p>
                <?php echo form_error('address1'); ?>
            </div>
            
              <div class="col-xs-6">
                <label style="color:black" class="required-field">City</label>
                <input type="text"  id="city"  class="form-control" placeholder="Enter city" name="city" value="<?php echo $details[0]->city;?>" onfocus="$('#error_city').text('');" ><p id="error_city"></p>
                <?php echo form_error('city'); ?>
            </div>
            
            </div>
            </div>
            <div class="form-group">
                <div class="row">
                     <div class="col-xs-6">
                <label style="color:black"  class="required-field">State</label>
                <input type="text"  id="state"  class="form-control" placeholder="Enter state" name="state" value="<?php echo $details[0]->state;?>" onfocus="$('#error_city').text('')" >
                <?php echo form_error('city'); ?>
                    </div>
             
                  <div class="col-xs-6">
                <label style="color:black" class="required-field">Country</label>
                <input type="text"  id="country"  class="form-control" placeholder="Enter country" name="country" value="<?php echo $details[0]->country;?>" onfocus="$('#error_country').text('');" ><p id="error_country"></p>
                <?php echo form_error('city'); ?>
                    </div>
            </div>
                </div>
   <div class="form-group">
        <div class="row">
                     <div class="col-xs-6">
                <label style="color:black" class="required-field">Latitude</label>
                <input type="text"  id="latitude"  class="form-control" placeholder="30.454444" name="latitude" value="<?php echo $details[0]->latitude;?>" ><p id="erroe"></p>
               
            </div>
            
              <div class="col-xs-6">
                <label style="color:black" class="required-field">Longitude</label>
                <input type="text"  id="longitude"  class="form-control" placeholder="76.98763" name="longitude" value="<?php echo $details[0]->longitude;?>"  ><p id="ery"></p>
 
            </div>
            
            </div>
            </div>
               <div class="form-group">
              <label class="required-field" style="color:black">Monday-Friday:</label>
   <div class="row">
    <div class="col-xs-6">
<label style="color:black">Start Time</label>
          <div class="get-started bootstrap-timepicker">
            <input type="text"  id="timepicker" placeholder="Select Start Time" class="form-control ajax_filter" name="weekst" value="<?php echo $details[0]->weekst;?>" onfocus="$('#timeformaterror').text('');" >
          </div>
        </div>

<div class="col-xs-6">
<label style="color:black">End Time</label>
          <div class="get-started bootstrap-timepicker">
            <input type="text"  id="timepicker1" placeholder="Select End Time" class="form-control ajax_filter" name="weeket" value="<?php echo $details[0]->weeket;?>" onfocus="$('#timeformaterror').text('');" >
          </div>
        </div>
 </div>
<p id="timeformaterror" style="color:red"></p>
 </div>
    <div class="form-group">
        <label style="color:black">Weekends:</label>
   <div class="row">
<div class="col-xs-6">
<label style="color:black">Start Time</label>
          <div class="get-started bootstrap-timepicker">
            <input type="text"  id="timepicker2" placeholder="Select Start Time" class="form-control ajax_filter" name="weekendst" value="<?php echo $details[0]->weekendst ;?>" onfocus="$('#timeformaterror1').text('');">
          </div>
        </div>

<div class="col-xs-6">
<label style="color:black">End Time</label>
          <div class="get-started bootstrap-timepicker">
            <input type="text"  id="timepicker3" placeholder="Select End Time" class="form-control ajax_filter" name="weekendet" value="<?php echo $details[0]->weekendet ;?>" onfocus="$('#timeformaterror1').text('');">
          </div>
        </div>
 </div>
<p id="timeformaterror1" style="color:red"></p>
 </div>
          
<div class="box-footer">
          <button class="btn btn-primary nextBtn btn-sm pull-right firstnext" type="button" >Next</button>
         </div>
          </div>
       </div>
      </div>
    

<div class="row setup-content" id="step-2">
      <div class="col-xs-6 col-md-offset-3">
        <div class="col-md-12" style="background:white">
          <h3> Add contact</h3>
              <div class="form-group">
                <label class="required-field">Mobile No</label>
                <input type="text"  id="mobile_no"  class="form-control ajax_filter" placeholder="" name="mobile_no" value="<?php echo $details[0]->phone2;?>" onfocus="$('#error_mob').text('')"  maxlength="10"><p id="error_mob"></p>
                <?php echo form_error('mobile_no'); ?>
            </div>
           <div class="form-group">
                <label class="">Telephone No</label>
                <input type="text"  id="phone_no"  class="form-control ajax_filter"  placeholder="" name="phone_no" value="<?php echo $details[0]->phone1;?>" onfocus="$('#error_phn').text('')" required maxlength="15">
        <p id="errro_phn"></p>
                <?php echo form_error('phone_no'); ?>
            </div>
           
          
                <div class="form-group">
                <label >Facebook </label>
                <input type="text"  id="facebook"  class="form-control"  placeholder="http://www.example.com/" name="facebook" value="<?php echo $details[0]->facebook;?>" >
                <?php echo form_error('facebook'); ?>
            </div>
            <div class="form-group">
                <label >Google</label>
                <input type="text"  id="google+"  class="form-control"  placeholder="http://www.example.com/" name="google" value="<?php echo $details[0]->google;?>">
                <?php echo form_error('google'); ?>
            </div>
             <div class="form-group">
                <label >Twitter</label>
                <input type="text"  id="twitter"  class="form-control"  placeholder="http://www.example.com/" name="twitter" value="<?php echo $details[0]->twitter;?>" >
                <?php echo form_error('twitter'); ?>
            </div>
             <div class="form-group">
                <label >youtube</label>
                <input type="text"  id="youtube"  class="form-control"  placeholder="http://www.example.com/" name="youtube" value="<?php echo $details[0]->youtube;?>" >
                <?php echo form_error('youtube'); ?>
            </div>
           
      

         <div class="box-footer">
<p class="newerrormsgg" style="color:red"></p>
          <button class="btn btn-success nextBtn btn-sm pull-right" type="submit" id="ffffromabc" value="submit" name="submit">Submit</button>
<div>
        </div>
      </div>
        </div>
      </div> 
    </div>


   

  </form>
 </section>

<script>
 
    $(document).ready(
   function () {


    //var date = $('#datepicker').datepicker({ dateFormat: 'dd-mm-yy' }).val();

    $( "#datepicker" ).datepicker({
      changeMonth: true,//this option for allowing user to select month
      changeYear: true, //this option for allowing user to select from year range
      dateFormat: 'dd-mm-yy'
    }).val();
  }

);

       $("#timepicker").timepicker({
        showInputs: false
        });
        $("#timepicker1").timepicker({
        showInputs: false
        });
     $("#timepicker2").timepicker({
        showInputs: false
        });
     $("#timepicker3").timepicker({
        showInputs: false
        });

</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  
<script>
/*    $(document).ready(function () {
  $("#mobile_no").keypress(function (e) {
   if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
         $("#error_mob").html("[0-9] digits only").show().css("color","red");
               return false;
    }
   });
});*/
    </script>

<script>
 $(document).ready(function () {
$("#dataaddress").submit(function()
{ 
var addrss=$("#address1").val();
var city=$("#city").val();
var state=$("#state").val();
var country=$("#country").val();
var timepicker=$("#timepicker").val();
var timepicker1=$("#timepicker1").val();
var mob=$("#mobile_no").val();
var latt=$("#latitude").val();
var longg=$("#longitude").val();


if( addrss.trim()=='' || city.trim()=='' || state.trim()=='' || country.trim()=='' || 
  timepicker.trim()=='' || timepicker1.trim()=='' || mob.trim()=='' || latt.trim()=='' || longg.trim()==''){



if(addrss.trim()=='')
$("#address1").css("border", "1px solid red");
else
$("#address1").css("border", "1px solid #d2d6de");

if(city.trim()=='')
$("#city").css("border", "1px solid red");
else
$("#city").css("border", "1px solid #d2d6de");

if(latt.trim()=='')
$("#latitude").css("border", "1px solid red");
else
$("#latitude").css("border", "1px solid #d2d6de");

if(longg.trim()=='')
$("#longitude").css("border", "1px solid red");
else
$("#longitude").css("border", "1px solid #d2d6de");

if(state.trim()=='')
$("#state").css("border", "1px solid red");
else
$("#state").css("border", "1px solid #d2d6de");

if(country.trim()=='')
$("#country").css("border", "1px solid red");
else
$("#country").css("border", "1px solid #d2d6de");

if(timepicker.trim()=='')
$("#timepicker").css("border", "1px solid red");
else
$("#timepicker").css("border", "1px solid #d2d6de");

if(timepicker1.trim()=='')
$("#timepicker1").css("border", "1px solid red");
else
$("#timepicker1").css("border", "1px solid #d2d6de");

if(mob.trim()=='')
$("#mobile_no").css("border", "1px solid red");
else
$("#mobile_no").css("border", "1px solid #d2d6de");



if(addrss.trim()=='' || city.trim()=='' || state.trim()=='' || country.trim()=='' || 
  timepicker.trim()=='' || timepicker1.trim()=='' || mob.trim()==''  || latt.trim()=='' || longg.trim()=='')
{$(".newerrormsgg").text("Please check your all details carefully.");
return false;}
}


});
});
</script>
<!--<script type="text/javascript">
function handleError()
{
   return true;
}
window.onerror = handleError;
</script>-->



  
