

        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
         Testimonial List
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Testimonials</li>
            <li class="active"></li>
          </ol>
        </section>

     <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              
              <div class="box">
               <div class="box-header">
                 <h4 style="display:inline;" class="box-title">  <?php if($this->session->flashdata('delete')!='') echo "<span class='show_msg withadddelete'>".$this->session->flashdata('delete')."</span>";?></h4>  
<h4 style="display:inline;" class="box-title">  <?php if($this->session->flashdata('status')!='') echo "<div class='show_msg withaddaddoredit' >".$this->session->flashdata('status')."</div>";?></h4>
                </div>
                <form method='post'><input type="submit" value='Delete' class='del_event btn btn-primary'  style='display:none;background:hsl(0, 100%, 69%);border:none;margin:10px;' name='bulk_delt'>
                <div id="no-more-tables" class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                      <th><input type='checkbox' name="deleteall[]" class='checkAll'></th>
                       <th>User Name</th>
                       <th>Added Testimonial</th>
                        <th>Actions</th>

                      </tr>
                    </thead>
                    <tbody>
                        
                     <?php foreach($testimonials as $data){ ?>
                     <tr>
                     <td><input type='checkbox' class='checkdata' value='<?php echo $data->testimonialId;?>' name='checked_testimonial[]' onclick='chekselctbox()'></td>
                     <td><?php echo $data->first_name.' '.$data->last_name;?></td>
                     <td><?php echo $data->testimonialText;?></td>
                         <td><a href="<?php echo base_url();?>admin/Testimonial/deleteTestimonial/<?php echo $data->testimonialId;?>" onclick="return confirm('Do you really want to delete?')"><i title='Delete' class="fa fa-trash-o"></i></a>
                     <?php
                     if($data->is_active == 1){
                      echo "<a class='fa fa-power-off' title='Activated' href='".base_url()."admin/Testimonial/changeStatus/". $data->testimonialId."/0'></a>";
                     }
                     else{
                       echo "<a class='fa fa-circle-o-notch' title='Deactivated' href='".base_url()."admin/Testimonial/changeStatus/". $data->testimonialId."/1'></a>";
              
                     }
                     ?>
                   

                    </td>
                   </tr>
                    <?php } ?> 

                    </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      
         
