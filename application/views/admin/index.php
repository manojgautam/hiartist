 
<section class="content">
   <h2>Dashboard</h2>
<div class="col-lg-3 col-xs-6 dashboard_content">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $user_count; ?></h3>
              <p>Users</p>
               <span style="position:absolute;color:yellow;top:20px;left:60px;font-weight:bold;font-size:15px;"><?php if($usercount){ echo $usercount." New";}?></span>
            </div>
            <div class="icon">
            <i class="ion ion-person-add"></i>
             
              </div>
            <a href="<?php echo base_url();?>admin/Dashboard/UpdateSeen" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
    <div class="col-lg-3 col-xs-6 dashboard_content">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $user_testimonial;?></h3>
              
             <p>Testimonials</p>
                 <span style="position:absolute;color:yellow;top:20px;left:60px;font-weight:bold;font-size:15px;"><?php if($testi_count){ echo $testi_count." New"; }?></span>
            </div>
            <div class="icon">
            <i class="fa fa-bookmark-o"></i>
             
              </div>
            <a href="<?php echo base_url();?>admin/Dashboard/updateSeentestimonial" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
     <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $event_count; ?></h3>

              <p>Events</p>
            </div>
            <div class="icon">
              <i class="fa fa-flag-o"></i>
            </div>
            <a href="<?php echo base_url(); ?>admin/event/ListEvent" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
    <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $user_reviews; ?></h3>
<span style="position:absolute;color:yellow;top:20px;left:60px;font-weight:bold;font-size:15px;"><?php if($new_review_count){ echo $new_review_count." New"; }?></span>
              <p>Reviews</p>
            </div>
            <div class="icon">
              <i class="fa fa-comments-o"></i>
            </div>
            <a href="<?php echo base_url();?>admin/Dashboard/UpdatereviewSeen" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
      <section class="content">
      <div class="row">
        <div class="col-md-6">
          <!-- AREA CHART -->
        <!--  <div class="box box-primary">
            <div class="box-header with-border">-->
              <h3 class="box-title">Users</h3>
<!--   </div>-->
            <div class="box-body">
              <div class="chart">
                <canvas id="areaChart" style="height:250px"></canvas>
              </div>
         <!--   </div>-->
            <!-- /.box-body -->
          </div>
          </div>
             <div class="col-md-6">
          <!-- AREA CHART -->
         <!-- <div class="box box-primary">-->
           <!-- <div class="box-header with-border">-->
              <h3 class="box-title">Other</h3> <!--</div>-->
            <div class="box-body">
              <div class="chart">
                <canvas id="areaChart1" style="height:250px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          <!--</div>-->
          </div>
        </div>
        
       </section>
   
      </section>
<script>
   $(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.
    var areaChartCanvas = $("#areaChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart = new Chart(areaChartCanvas);

    var areaChartData = {
      labels: ["Users", "Testimonial", "Reviews"],
      datasets: [
        
        {
          label: "user",
          fillColor: "rgba(60,141,188,0.9)",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [<?php echo $user_count; ?>,<?php echo $user_testimonial; ?>, <?php echo $user_reviews; ?>]
        }
      ]
    };
       var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };

    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions);

        var areaChartCanvas = $("#areaChart1").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var areaChart = new Chart(areaChartCanvas);

    var areaChartData = {
      labels: ["Events", "Shops", "Groups"],
      datasets: [
        {
          label: "group",
          fillColor: "rgba(60,141,188,0.9)",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: [<?php echo $event_count; ?>, <?php echo $shop_count; ?>, <?php echo $group_count; ?>]
        },
        
      ]
    };
       var areaChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };

    //Create the line chart
    areaChart.Line(areaChartData, areaChartOptions);
   });
</script>
