<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title ;?> </title>
    <!-- Tell the browser to be responsive to screen width -->
<!--<link rel="icon" type="image/x-icon" href="<?php echo base_url();?>assets/dist/images/fvcon.png">-->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
   
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/skins/_all-skins.min.css">
     <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datepicker/datepicker3.css">
  <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datatables/dataTables.bootstrap.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

      <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
          <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/custum-style.css">
          
   
    <!-- Load jQuery JS -->
    <!--<script src="http://code.jquery.com/jquery-1.9.1.js"></script>-->
    <script src="<?php echo base_url();?>assets/artists/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap/js/custom.js"></script>
    <!-- Load jQuery UI Main JS  -->
    <!--<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>-->
    <script src="<?php echo base_url();?>assets/artists/js/jquery-ui.js"></script>
   
<link rel="shortcut icon" href="<?php echo base_url();?>assets/artists/ico/hiartist_favicon.ico"> 
  </head>
<?php $sess=$this->session->userdata('adminsession');?>
  <body class="hold-transition skin-blue sidebar-mini">
  
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url();?>admin/dashboard" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="<?php echo base_url();?>assets/artists/images/logo.png" class="user-image" alt="User Image"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>
          <img src="<?php echo base_url();?>assets/artists/images/logo.png" alt="logo"/></b></span>
        </a>

<input type="hidden" class="get_url" value="<?php echo base_url();?>">
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
              <!-- Tasks: style can be found in dropdown.less -->
              <li class="dropdown tasks-menu">
            
                  <a target="_blank"  href="<?php echo base_url();?>" style="color:white" ><i class="fa fa-globe"></i>Open Website</a>
              </li>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo base_url();?>assets/artists/images/logo.png" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo "Hi, ".$details[0]->name;?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?php echo base_url();?>uploads/<?php echo $details[0]->image;?>" class="img-circle" alt="User Image">
                    <p>
                    <?php echo $details[0]->name; ?>
               
                    </p>
                  </li>
                  <!-- Menu Body -->
            
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
               <a href="#" onclick="showappointmentInDialog('<?php echo base_url('admin/dashboard/editProfile');?>','Edit Profile'); return false;" class="btn btn-primary btn-block btn-flat" style="border-color: #f70001;background: #f70001;">Edit Profile</a>

                    </div>
                    <div class="pull-right">

                      <form action="<?php echo base_url();?>admin/Login/logout" method="post">
                      <button type="submit" value="submit" class="btn btn-primary btn-block btn-flat" style="border-color: #f70001;background: #f70001;" name="logoutsubmit" >Sign out</button>
                      </form>
                      <!--<a href="#" class="btn btn-default btn-flat">Sign out</a>-->
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
               <!-- <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>-->
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
            <img style="background-color:white;" src="<?php echo base_url();?>assets/artists/images/hiartist_icon.png" class="" alt="User Image">
            </div>
            <div class="pull-left info">
                <a href="<?php echo base_url();?>admin/dashboard/addAdminaddress/<?php echo $details[0]->id;?>" title="Additional Information"> <span style="font-size:16px;">&nbsp;&nbsp;<?php echo $details[0]->name;?></span></a>
               
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              
    
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
         <ul class="sidebar-menu">
            
            <li class="treeview <?php if($title=='Admin Dashboard'){echo 'active';}?>">
              <a href="<?php echo base_url();?>admin/dashboard">
                <i class="fa fa-tachometer" aria-hidden="true"></i>
            <span>Dashboard</span> 
              </a>
            </li>

             <li class="treeview <?php if($title=='Artists'){echo 'active';}?>">
              <a href="<?php echo base_url();?>admin/role/artists">
                <i class="fa fa-user"></i>
                <span>Manage Artist Roles</span>
                
              </a>
             </li>
               </li>


                <li class="treeview <?php if($title=='Directors'){echo 'active';}?>">
              <a href="<?php echo base_url();?>admin/role/directors">
                <i class="fa fa-user"></i>
                <span>Manage Director Roles</span>
                
              </a>
             </li>
               </li>


              <li class="treeview <?php if($title=='Events' || $title=='Add Event'){echo 'active';}?>">
              <a href="#">
                <i class="fa fa-calendar"></i>
                <span>Manage Events</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu leftnavbar">
                 <li class="<?php if($title=='Events'){echo 'active';}?>"><a href="<?php echo base_url();?>admin/event/ListEvent"><i class="fa fa-circle-o"></i> All Events</a></li>
                <li class="<?php if($title=='Add Event'){echo 'active';}?>"><a href="<?php echo base_url();?>admin/event/addEvent"><i class="fa fa-circle-o"></i> Add Event</a></li>
              
              </ul>
            </li>

          <li class="treeview <?php if($title=='Users'){echo 'active';}?>">
              <a href="<?php echo base_url();?>admin/users/ListUsers">
                <i class="fa fa-user"></i>
                <span>Manage Users</span>
                
              </a>
             </li>
                <li class="treeview <?php if($title=='Testimonial'){echo 'active';}?>">
              <a href="<?php echo base_url();?>admin/testimonial/listTestimonial">
                <i class="fa fa-user"></i>
                <span>Manage Testimonials</span>
                
              </a>
             </li>
             <li class="treeview <?php if($title=='Reviews'){echo 'active';}?> ">
              <a href="<?php echo base_url();?>admin/Review/reviewsList">
                <i class="fa fa-user"></i>
                <span>Manage Reviews</span>
                
              </a>
             </li>
         
            <li class="treeview <?php if($title=='Banner' || $title=='Videos' || $title=='Logos' || $title=='Ads'){echo 'active';}?>">
              <a href="#">
                <i class="fa fa-picture-o"></i>
                <span>Manage Homepage Media</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
  <ul class="treeview-menu leftnavbar">
<li class="<?php if($title=='Banner'){echo 'active';}?>"><a href="<?php echo base_url();?>admin/media/home/1"><i class="fa fa-circle-o"></i>Banner</a></li>
<li class="<?php if($title=='Videos'){echo 'active';}?>"><a href="<?php echo base_url();?>admin/media/home/2"><i class="fa fa-circle-o"></i>Video</a></li>
<li class="<?php if($title=='Ads'){echo 'active';}?>"><a href="<?php echo base_url();?>admin/media/home/3"><i class="fa fa-circle-o"></i>Ads</a></li>
<li class="<?php if($title=='Logos'){echo 'active';}?>"><a href="<?php echo base_url();?>admin/media/home/4"><i class="fa fa-circle-o"></i>Logos</a></li>
          </ul>
    
            </li>
               

            <li class="treeview <?php if($title=='Add Group' || $title=='Groups'){echo 'active';}?>">
              <a href="#">
                <i class="fa fa-group"></i>
                <span>Manage Groups</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
          <ul class="treeview-menu leftnavbar">
                <li class="<?php if($title=='Groups'){echo 'active';}?>"><a href="<?php echo base_url();?>admin/groupShop/ListGroups"><i class="fa fa-circle-o"></i> All Groups</a></li>
                <li class="<?php if($title=='Add Group'){echo 'active';}?>"><a href="<?php echo base_url();?>admin/groupShop/addGroup"><i class="fa fa-circle-o"></i> Add Group</a></li>
              
          </ul>
            </li>



            <li class="treeview <?php if($title=='Add Shop' || $title=='Shops'){echo 'active';}?>">
              <a href="#">
                <i class="fa fa-cart-plus"></i>
                <span>Manage Shops</span>
                 <i class="fa fa-angle-left pull-right"></i>
              </a>
         <ul class="treeview-menu leftnavbar">
                <li class="<?php if($title=='Shops'){echo 'active';}?>"><a href="<?php echo base_url();?>admin/groupShop/ListShops"><i class="fa fa-circle-o"></i> All Shops</a></li>
                <li class="<?php if($title=='Add Shop'){echo 'active';}?>"><a href="<?php echo base_url();?>admin/groupShop/addShop"><i class="fa fa-circle-o"></i> Add Shop</a></li>
          </ul>
            </li>

           
             <li class="treeview <?php if($title=='Expertise'){echo 'active';}?>">
              <a href="<?php echo base_url();?>admin/Skill/expertise">
                <i class="fa fa-user"></i>
                <span>Manage Skills</span>
                
              </a>
             </li>

             <li class="treeview <?php if($title=='Advertise'){echo 'active';}?>">
              <a href="<?php echo base_url();?>admin/Advertise/Listadvertise">
                <i class="fa fa-user"></i>
                <span>Manage Advertise Users</span>
                
              </a>
             </li>
<li class="treeview <?php if($title=='Career'){echo 'active';}?>">
              <a href="<?php echo base_url();?>admin/Career/Listcareer">
                <i class="fa fa-user"></i>
                <span>Manage Career Users</span>
                
              </a>
             </li>

      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
    
        
<script>
   var currneturl = window.location.href;
    $('.leftnavbar li a').each(function()
    {
     var myHref= $(this).attr('href');
     if( currneturl == myHref ) 
     {
    $(this).closest("li").addClass("active");
     }
    });

</script>
       
