

        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          Users List
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Users</li>
            <li class="active"></li>
          </ol>
        </section>

     

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              
              <div class="box">
                <div class="box-header">
                   <h4 style="display:inline;" class="box-title">  <?php if($this->session->flashdata('deleteit')!='') echo "<span class='show_msg withadddelete'>".$this->session->flashdata('deleteit')."</span>";?></h4>
<p class="pull-left"><a class="linktobutton" style="width:150px !important;" href="<?php echo base_url();?>admin/Generate/create_csv/1">Generate Artists CSV</a></p>
<p class="pull-right"><a class="linktobutton" style="width:180px !important;" href="<?php echo base_url();?>admin/Generate/create_csv/2">Generate Directors CSV</a></p>
<h4 style="display:inline;" class="box-title">  <?php if($this->session->flashdata('addedit')!='') echo "<div class='show_msg withaddaddoredit' >".$this->session->flashdata('addedit')."</div>";?></h4>
                </div><!-- /.box-header -->
                <form method='post'><input type="submit" value='Delete' class='del_event btn btn-primary'  style='display:none;background:hsl(0, 100%, 69%);border:none;margin:10px;' name='bulk_delt'>
                <div id="no-more-tables" class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                      <th><input type='checkbox' name="deleteall[]" class='checkAll'></th>
                       <th>Name</th>
                       <th>Email</th>
                       <th>Role</th>
                       <th>Sub Role</th>
                       <th>Phone No.</th>
                       <th>Profile Photo</th>
                        <th>Actions</th>

                      </tr>
                    </thead>
                    <tbody>
                     <?php foreach($users as $data){ ?>
                     <tr>
                     <td><input type='checkbox' class='checkdata' value='<?php echo $data->user_id;?>' name='checked_events[]' onclick='chekselctbox()'></td>
                     <td><a href="<?php echo base_url();?>userprofile/<?php echo trim($data->last_name) ; ?><?php echo trim($data->first_name) ; ?>.<?php echo $data->user_id;?>" target="_blank"><?php echo $data->first_name.' '.$data->last_name;?></a></td>
                     <td><?php echo $data->email;?></td>
                     <td><?php echo $data->role;?></td>
                     <td><?php echo $data->sub_role_id;?></td>
                     <td><?php echo $data->phone_no;?></td>
                     <td style="width:130px;height:130px;"><img style="width:130px;height:130px;" src="<?php echo base_url();?>uploads/<?php if($data->profile_pic!=''){echo $data->profile_pic;}else{echo 'user.jpeg';}?>"/></td>
                      <td>
                      <a href="<?php echo base_url();?>admin/users/deleteuser/<?php echo $data->user_id;?>" onclick="return confirm('Do you really want to delete?')"><i title='Delete' class="fa fa-trash-o"></i></a>&nbsp;&nbsp;
                     <?php
                     if($data->active == 1){
                      echo "<a class='fa fa-power-off' title='Activated' href='".base_url()."admin/users/changestatus/". $data->user_id."/0'></a>";
                     }
                     else{
                       echo "<a class='fa fa-circle-o-notch' title='Deactivated' href='".base_url()."admin/users/changestatus/". $data->user_id."/1'></a>";
              
                     }
                     ?>

                     </td>
                   </tr>
                    <?php } ?> 

                    </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      
         
