

        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
     Reviews
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Reviews</li>
            <li class="active"></li>
          </ol>
        </section>

     <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              
              <div class="box">
               <div class="box-header">
                 <h4 style="display:inline;" class="box-title">  <?php if($this->session->flashdata('delete')!='') echo "<span class='show_msg withadddelete'>".$this->session->flashdata('delete')."</span>";?></h4>  
<h4 style="display:inline;" class="box-title">  <?php if($this->session->flashdata('status')!='') echo "<div class='show_msg withaddaddoredit' >".$this->session->flashdata('status')."</div>";?></h4>
                </div>
                <form method='post'><input type="submit" value='Delete' class='del_event btn btn-primary'  style='display:none;background:hsl(0, 100%, 69%);border:none;margin:10px;' name='bulk_delt'>
                <div id="no-more-tables" class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                      <th><input type='checkbox' name="deleteall[]" class='checkAll'></th>
                       <th>Added to</th>
                         <th>Added By</th>
                        <th>Rating</th>
                       <th>Description</th>
                        <th>Actions</th>

                      </tr>
                    </thead>
                    <tbody>
                       <?php foreach($reviewsaddTo as $data){ ?>
                     <tr>
                     <td><input type='checkbox' class='checkdata' value='<?php echo $data->review_id;?>' name='checked_review[]' onclick='chekselctbox()'></td>
                     <td><?php echo $data->first_name.' '.$data->last_name;?></td>
                 <td><?php echo $data->added_by_user;?></td>
                        <td><?php echo $data->review;?></td>
                     <td><?php echo $data->description;?></td>
                         <td><a href="<?php echo base_url();?>admin/Review/deleteReview/<?php echo $data->review_id;?>" onclick="return confirm('Do you really want to delete?')"><i title='Delete' class="fa fa-trash-o"></i></a>
                    
          </td>
                   </tr>
                    <?php } ?> 

                    </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      
         
