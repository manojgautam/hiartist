

        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
          Expertise List
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Expertise</li>
            <li class="active"></li>
          </ol>
        </section>

     

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              
              <div class="box">
                <div class="box-header">
                  <input type="button"  value="Add" class="btn btn-info pull-right" style="position:relative" onclick="showappointmentInDialog('<?php echo base_url();?>admin/Skill/addskill','Add Expertise'); return false;">
                   <h4 style="display:inline;" class="box-title">  <?php if($this->session->flashdata('deleteit')!='') echo "<span class='show_msg withadddelete'>".$this->session->flashdata('deleteit')."</span>";?></h4>
<h4 style="display:inline;" class="box-title">  <?php if($this->session->flashdata('addedit')!='') echo "<div class='show_msg withaddaddoredit' >".$this->session->flashdata('addedit')."</div>";?></h4>
                </div><!-- /.box-header -->
                <form method='post'><input type="submit" value='Delete' class='del_event btn btn-primary'  style='display:none;background:hsl(0, 100%, 69%);border:none;margin:10px;' name='bulk_delt'>
                <div id="no-more-tables" class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                      <th><input type='checkbox' name="deleteall[]" class='checkAll'></th>
                       <th>Role</th>
                       <th>Expertise</th>
                        <th>Actions</th>

                      </tr>
                    </thead>
                    <tbody>
                     <?php foreach($expertdetails as $data){ ?>
                     <tr>
                     <td><input type='checkbox' class='checkdata' value='<?php echo $data->expert_id;?>' name='checked_events[]' onclick='chekselctbox()'></td>
                     <td><?php echo $data->sub_role;?></td>
                     <td><?php echo $data->expertise;?></td>
                      <td><a href="#" onclick="showappointmentInDialog('<?php echo base_url();?>admin/Skill/editskill/<?php echo $data->expert_id;?>','Edit Expertise'); return false;"> <i title='Edit' class="fa fa-edit"></i></a>&nbsp;&nbsp;
             

                      <a href="<?php echo base_url();?>admin/Skill/deleteskill/<?php echo $data->expert_id;?>" onclick="return confirm('Do you really want to delete?')"><i title='Delete' class="fa fa-trash-o"></i></a>&nbsp;&nbsp;
                     

                     </td>
                   </tr>
                    <?php } ?> 

                    </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      
         
