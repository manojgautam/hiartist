

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              
              <div class="box">
                <div class="box-header">
                  <input type="button"  value="Add" class="btn btn-info pull-right" style="position:relative" onclick="showappointmentInDialog('<?php echo base_url();?>admin/media/addmedia/<?php echo $media_type;?>','Add'); return false;">
                   <h4 style="display:inline;" class="box-title">  <?php if($this->session->flashdata('deleteit')!='') echo "<span class='show_msg withadddelete'>".$this->session->flashdata('deleteit')."</span>";?></h4>
<h4 style="display:inline;" class="box-title">  <?php if($this->session->flashdata('addedit')!='') echo "<div class='show_msg withaddaddoredit' >".$this->session->flashdata('addedit')."</div>";?></h4>
                </div><!-- /.box-header -->
                <form method='post'><input type="submit" value='Delete' class='del_event btn btn-primary'  style='display:none;background:hsl(0, 100%, 69%);border:none;margin:10px;' name='bulk_delt'>
                <div id="no-more-tables" class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                      <th><input type='checkbox' name="deleteall[]" class='checkAll'></th>
                       <th>Title</th>
                      <?php if($media_type==3){echo "<th>Description</th>";} ?>
                       <th>Path / Banner</th>
                        <th>Actions</th>

                      </tr>
                    </thead>
                    <tbody>
                     <?php foreach($homemedia as $data){ ?>
                     <tr>
                     <td><input type='checkbox' class='checkdata' value='<?php echo $data->home_id;?>' name='checked_events[]' onclick='chekselctbox()'></td>
                     <td><?php echo $data->title;?></td>
                   <?php if($data->media_type==3){?><td><?php echo $data->description;?></td><?php } ?>
                     <td><?php if($data->media_type==2){echo "<a href='".$data->path."'>".$data->path."</a>";}else{ ?><img style="width:180px;height:80px;" src="<?php echo base_url();?>uploads/<?php if($data->path!=''){echo $data->path;}else{echo 'noimage.png';}?>"/><?php } ?></td>
    
                      <td><a href="#" onclick="showappointmentInDialog('<?php echo base_url();?>admin/media/editmedia/<?php echo $media_type;?>/<?php echo $data->home_id;?>','Edit'); return false;"> <i title='Edit' class="fa fa-edit"></i></a>&nbsp;&nbsp;
             

                      <a href="<?php echo base_url();?>admin/media/deletemedia/<?php echo $media_type;?>/<?php echo $data->home_id;?>" onclick="return confirm('Do you really want to delete?')"><i title='Delete' class="fa fa-trash-o"></i></a>&nbsp;&nbsp;
                     

                     </td>
                   </tr>
                    <?php } ?> 

                    </tbody>
                    
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      
         
