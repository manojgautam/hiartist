<section class="content-header">
          <h1>
            <small></small>
        </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Manage Groups</li>
            <li class="active">Edit Group</li>
          </ol>
</section>
<section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Edit Group</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" id="groupform" method="post" enctype="multipart/form-data" action="<?php echo base_url()?>admin/groupShop/editGroup/<?php echo $id;?>">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="name" class="required-field">Name</label>
                      <input class="form-control" id="name" placeholder="Group Name" name="name" type="text" value="<?php echo $groups[0]->name;?>">
                    </div>
                   <div class="form-group">
                      <label>Description</label>
                      <textarea class="form-control" rows="3" id="desc" name="desc" placeholder="Enter Details.." value="<?php echo $groups[0]->description;?>"><?php echo $groups[0]->description;?></textarea>
                    </div>

               <label style="color:black;font-size:16px;">Image</label>
<div class="imgss">
<img width="200px" height="160px" src="<?php echo base_url();?>uploads/<?php if($groups[0]->image)echo $groups[0]->image; else echo 'user.jpeg';?>">
</div>
                    <div class="form-group">
                      <label for="exampleInputFile">Change Banner Image</label>
                      <input id="exampleInputFile" type="file" name="userfilephp">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputFile">Add Group Images</label>
                      <input id="exampleInputFile" type="file" name="userfile1[]" multiple="multiple">
                    </div>
                    <input type="hidden" name="hiddenname" value="<?php echo $groups[0]->image;?>">
                    <div class="form-group">
                      <label for="members">Group Members</label>
                      <input class="form-control" id="members" placeholder="Group Members Name" name="members" type="text" value="<?php echo $groups[0]->group_members;?>">
                    </div>
                    <div class="form-group">
                      <label for="website">Website</label>
                      <input class="form-control" id="website" placeholder="www.example.com" name="website" type="text" value="<?php echo $groups[0]->website;?>">
                    </div>
                    <div class="form-group">
                      <label for="blog">Blog</label>
                      <input class="form-control" id="blog" placeholder="Blog Link" name="blog" type="text" value="<?php echo $groups[0]->blog_link;?>">
                    </div>
                  
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-info" style="margin-left:10px;" name="gpsubmit" value="gpsubmit">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->              
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
        </section>
