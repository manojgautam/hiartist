<div class="slickslide_img" style="position:relative"><img src="<?php echo base_url();?>assets/artists/images/footer-bg-design.jpg">
<div class="content">
       <div class="container">
           <div class="top_section">
          
        </div>
    </div>
</div>
<section class="contact padding-120">
      <div class="container" style="background:#fff">
         <?php
                if($this->session->flashdata('Email')) {  ?>
        
        <div class="col-md-offset-2 col-md-8 alert alert-success" id="flashdata_email">
        <p><?php echo $this->session->flashdata('Email'); ?></p>
  </div>
       
 <?php } ?> 
        <div class="form_top">
          <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="col-sm-12  contact-form">
              <h3>Get in Touch</h3>
                
    <form method="POST" action="<?php echo base_url();?>welcome/contact_message">
               
                  <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                    <input type="text" name="name" id="name" onfocus="$('#error_name').text('');" placeholder="Full Name" class=" form-control">
                    <p id="error_name" class="error_form_style"></p>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                    <input type="email" name="email" id="email" onfocus="$('#error_email').text('');" placeholder="Email Address" class=" form-control"> <p id="error_email" class="error_form_style"></p>
                  </div>
              
                  <div class="col-md-12 col-sm-6 col-xs-12 form-group">
                <input type="text" name="subject" id="subject" onfocus="$('#error_subject').text('');" placeholder="Subject" class="form-control">
                  <p id="error_subject" class="error_form_style"></p>                 
                </div>
                   <div class="col-md-12 col-sm-6 col-xs-12 form-group">
                  <textarea id="message" name="message" rows="5" onfocus="$('#error_message').text('');" class="form-control" placeholder="Message"></textarea>
                   <p id="error_message" class="error_form_style"></p>
                <input type="submit" name="submit" value="Send Message" class="contact-submit" onclick="return validate();">
                  </div>
              </form>
            </div>
          </div>
           
          <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="contact-info">
              <h3>Contact Info</h3>
              
            
              <ul class="infos">
                <li>
                <p><?php echo $contact[0]->street_address;?> </p>
                    <p><?php echo $contact[0]->city .','.$contact[0]->state .','.$contact[0]->country;?></p>
               
                </li>
                <li >
                    <p><b>Mon-Fri:</b> <?php echo $contact[0]->weekst;?> - <?php echo $contact[0]->weeket;?> </p>
                   <p><b>Sat-Sun:</b> <?php echo $contact[0]->weekendst;?> - <?php echo $contact[0]->weekendet;?> </p>
                  </li>
                <li>
                     <p><?php echo $contact[0]->phone1;?></p>
                    <p><?php echo $contact[0]->phone2;?></p>
        </li>
                <li>
                   <a href="mailto:<?php echo $contact[0]->email;?>"><p><?php echo $contact[0]->email;?></p></a>
                    </li>
              </ul>
                
              <ul class="event-social">
                 <li><a href="<?php echo $contact[0]->google;?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a href="<?php echo $contact[0]->facebook;?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="<?php echo $contact[0]->twitter;?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                 <li><a href="<?php echo $contact[0]->youtube;?>"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
              </ul>
            </div><!-- contact-info -->
          </div>
        </div><!-- row -->
      </div><!-- container -->
    </section>

    <div class="contact_map">
    <div id="map" style="width:100%;height:170px;"></div> 
<div style="text-align:center"> <a  target="_blank" href="https://www.google.com/maps/dir//'<?php echo $contact[0]->latitude;?>,<?php echo $contact[0]->longitude;?>'/@<?php echo $contact[0]->latitude;?>,<?php echo $contact[0]->longitude;?>,17z/"> </a></div></div>
    
    
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
 <script type="text/javascript">
//jQuery(document).ready(function(){
var i=0;
jQuery(function(){
//jQuery('.mapid').click(function(){
if(i==0){
i++;
    // Define the latitude and longitude positions
  var latitude = parseFloat("<?php echo $contact[0]->latitude;?>"); // Latitude get from above variable
  var longitude = parseFloat("<?php echo $contact[0]->longitude;?>"); // Longitude from same
  var latlngPos = new google.maps.LatLng(latitude, longitude);
  
  // Set up options for the Google map
  var myOptions = {
    zoom: 14,
    center: latlngPos,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    zoomControlOptions: true,
    zoomControlOptions: {
      style: google.maps.ZoomControlStyle.LARGE
    }
  };
  
  // Define the map
  map = new google.maps.Map(document.getElementById("map"), myOptions);
  
  // Add the marker
         var contentString = '<div id="content" style="background:yellow">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h3 id="firstHeading" class="firstHeading"><?php echo $contact[0]->city;?></h3>'+
      '<div id="bodyContent">'+
      '<p><?php echo $contact[0]->state .','.$contact[0]->country;?></p>'+
      '</div>'+
      '</div>';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });
  var marker = new google.maps.Marker({
    position: latlngPos,
    map: map,
    title: "Your Location"
  });
        marker.addListener('mouseover', function() {
    infowindow.open(map, marker);
  });
  
}
});
</script>
    
       

    


  
                 
