<div class="slickslide_img" style="position:relative">
    <img src="http://agiledevelopers.in/hiartist/assets/artists/images/footer-bg-design.jpg">
</div>
<section class="oops-page">
    <div class="container">
        <div class="oops-page-left">
            <div class="error-box">
                <h4>404	
                </h4>
                <small>error page</small>
            </div>
        </div>
        <div class="oops-page-right">
            <h2>OOOPS ...</h2>
            <p>We are unable to find the page you are looking for.</p>
            <a href="<?php echo base_url();?>" class="error-home">RETURN TO HOME PAGE</a>
        </div>
    </div>
</section>
