<div class="slickslide_img" style="position:relative"><img src="<?php echo base_url();?>assets/artists/images/footer-bg-design.jpg" />

</div>
 <nav class="navbar navbar-inverse artists_nav">
  <div class="container">
        <!--  <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
       <input type="hidden" id="actualrole" value="<?php //echo $role;?>"/>    
            <h1 class="navbar-brand_tag" href="#"><?php //if($role=='Top'){echo 'Top Profile';}else{if($role=='Director'){ echo "Directors"; } else{ echo $role;}}?></h1> 
            </div> -->
       <input type="hidden" id="actualrole" value="<?php echo $role;?>"/>    
       <div class="search-page-content">
      <ul class="nav navbar-nav navbar-right">
        <li class="sort">Sort by</li>
      <li class="dropdown">
          <?php if($role=='Artists'){?>
          <select id="artist" name="role" attr-id="artist" class="search_change">
               <option value="">Select Category</option>
              <?php foreach($artist as $item){?>
     <option value="<?php echo $item->sub_role;?>" <?php if(str_replace('%20',' ',$role_name)==$item->sub_role){echo "selected";}?>><?php echo $item->sub_role;?></option>
          <?php } ?>
          </select>
           <?php }else if($role=='Director'){?>
          <select id="director" attr-id="director" name="role" class="search_change">
               <option value="">Select Category</option>
              <?php foreach($director as $item){?>
              <option value="<?php echo $item->sub_role;?>" <?php if(str_replace('%20',' ',$role_name)==$item->sub_role){echo "selected";}?>><?php echo $item->sub_role;?></option>
          <?php } ?>
          </select>
        <?php
        } else{ ?>
          <select id="topprofile" attr-id="topprofile" name="role" class="search_change">
               <option value="">Select Category</option>
              <?php foreach($allroles as $item){?>
              <option value="<?php echo $item->sub_role;?>"><?php echo $item->sub_role;?></option>
          <?php } ?>
          </select>
        <?php
        }?>
        </li>
          <li class="dropdown">
           <select id="gender" attr-id="gender"  name="gender" class="search_change">
               <option value="">Select Gender</option>
               <option value="male">Male</option>
               <option value="female">Female</option>
               <option value="other">Other</option>
              </select>
          </li>
          <li class="dropdown">
           <select id="age" attr-id="age"  name="age" class="search_change">
               <option value="">Select Age</option>
               <option value="0-10">less than 10</option>
               <option value="10-15">10-15</option>
               <option value="15-20">15-20</option>
		<option value="20-25">20-25</option>
		<option value="25-30">25-30</option>
		<option value="30-35">30-35</option>
		<option value="35-40">35-40</option>
		<option value="40-45">40-45</option>
		<option value="45-50">45-50</option>
		<option value="50-55">50-55</option>
		<option value="55-60">55-60</option>
		<option value="60-65">60-65</option>
		<option value="65-70">65-70</option>
		<option value="70-75">70-75</option>
		<option value="75-80">75-80</option>
		<option value="80-100">greater than 80</option>

            </select>
          </li>
          <li class="dropdown">
          <select id="location" attr-id="location"  name="role" class="search_change">
               <option value="">Select Location</option>
              <?php foreach($district as $item){?>
             <option value="<?php echo $item->district;?>" <?php if(str_replace('%20',' ',$thetype)==$item->district){echo "selected";}?>><?php echo $item->district;?></option>
             <?php 
        }?>
              </select>
        </li>
         <li class="dropdown">
         <!-- <a class="dropdown-toggle" data-toggle="dropdown" href="#">reviews<span class="caret"></span></a>-->
           <select id="reviews" attr-id="reviews"  name="reviews" class="search_change">
               <option value="">Select Reviews</option>
              <option value="ASC">ASC</option>
                <option value="DESC">DESC</option>
    </select>
        </li>
            <li class="dropdown">
         <!-- <a class="dropdown-toggle" data-toggle="dropdown" href="#">views<span class="caret"></span></a>-->
           <select id="views" attr-id="views"  name="views" class="search_change">
               <option value="">Views</option>
               <option value="ASC">ASC</option>
                <option value="DESC" <?php if($role=='Top'){echo 'selected';}?>>DESC</option>
              </select>
        </li>
           <li class="dropdown">
          <!--<a class="dropdown-toggle" data-toggle="dropdown" href="#">date<span class="caret"></span></a>-->
         <select id="date" attr-id="date"  name="date" class="search_change">
               <option value="">Date</option>
              <option value="ASC">ASC</option>
                <option value="DESC">DESC</option>
              </select>
        </li>
      </ul>
     </div>
     </div>
</nav>
  


<!------------------------artist----------------->
<section class="aoh">
   <div class="container">
  <div class="row">
  <div class="slz-main-title">
<div class="subtitle search_result">
	<span class="subtitle-inner">search result</span>
</div>
</div>

<div class="col-sm-9 searchafterajax">
<div class="h_artist">
<div class="row" id="role_ajax">
  <?php 
    if($user==0)
    {?><h2><?php echo "No Result Found"; ?></h2> 
    <?php
    } else{
       
    foreach($data as $item){
    ?>
    <div class="col-sm-3 nopadding">
 <div class="h_artist_inner ajax_data_role">
 <div class="boxing">
        <a href="<?php echo base_url();?>userprofile/<?php echo trim($item->last_name) ; ?><?php echo trim($item->first_name) ; ?>.<?php echo $item->user_id ; ?>">
         <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php if($item->profile_pic==''){echo 'user.jpeg';} else { echo $item->profile_pic; }?>&h=280&w=275" alt="artist"></a>
   
       
      </div>
    <div class="artist-ribbon">
		 <span><?php
if($item->sub_role_id!=''){
$categarray=explode(',',$item->sub_role_id);
$thcount=count($categarray)-1;
if($thcount)
{if($thcount==1)
$othermore=' + '.$thcount.' more skill';
else
$othermore=' + '.$thcount.' more skills';
}
else
$othermore='';
if(in_array(str_replace('%20',' ',$role_name),$categarray)){echo str_replace('%20',' ',$role_name).$othermore;}
else{echo $categarray[0].$othermore;}
}else{echo '';}
?>
</span>
      
		  </div>
<h2><?php echo $item->first_name.' '.$item->last_name;?></h2>
   <div> 
     </div>   
     <div class="h_artist_inner_hovr">
         <div class="actor-rating">
            <input type="number" value="<?php if($item->review_count==0) {echo 0;} else{echo (float)($item->reviews)/(float)($item->review_count);}?>" class="rating" readonly min=0 max=5 step=0.1 data-size="xs">
             <div class="hover_views">
                <i class="fa fa-eye" aria-hidden="true"></i> <?php echo $item->profile_view;?>
             </div>
             <a href="<?php echo base_url();?>userprofile/<?php echo trim($item->last_name) ; ?><?php echo trim($item->first_name) ; ?>.<?php echo $item->user_id ; ?>" class="view_profilebtn">view profile</a>
         </div>
         
     </div>
</div>
   
  </div>
  
 <?php
}
    }?>
 </div>
 
 </div>
    <?php if($count>8)
{?>
<div class="row">
             <div class="col-md-offset-4 col-md-4 text-center">
                 <a href="javascript:void(0);" id="load_more"  class="show_more">Show More</a>
                  <img id="loading-image" src="<?php echo base_url();?>assets/artists/images/ripple.gif" style="display:none">
               </div>  
              </div>
    <?php }
    ?>
</div>
 <div class="col-sm-3 nopadding">
 <div class="advertisement">
<img src="<?php echo base_url();?>assets/artists/images/add2.jpg" alt="add">
 </div>
 </div>

	 </div>
  </div>
</section>
<!------------------------------client-------------------->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
  $("#load_more").click(function() {
      $('#loading-image').show();
    /*  alert(122);*/
   var divlength = $('.ajax_data_role').length;

     var actualrole=$('#actualrole').val();
 //alert(divlength);
   var sub_role_id="<?php echo $role_name;?>";
   var thetype="<?php echo $thetype;?>";
  /*alert(sub_role_id);*/ 
        $.ajax({
                url:'<?php echo base_url();?>front/Search/load_more',
                 type:'POST',
                data:{limit:divlength,id:sub_role_id,type:thetype,therole:actualrole},
                success:function(data)
                {
                     $('#loading-image').hide();
                  $('#role_ajax').append(data);
           var $input = $('input.rating');
           $input.removeClass('rating-loading').addClass('rating-loading').rating();
                     var divtotal = $('.ajax_data_role').length;
                    var totalrole='<?php echo $count ;?>';
                    if(totalrole<=divtotal)
                        {
                            $('#load_more').hide();
                        }
                 },
            error(error){console.log(error);}
                    });
});
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
  $('.search_change').change(function() {
   var thechangeid=$(this).attr('attr-id');
     var actualrole=$('#actualrole').val();
    var sub_role_id="<?php echo $role_name;?>";  
  var fullarray=[];
      $('.search_change').each(function(){
          var theid=$(this).attr('attr-id');
          fullarray.push($('#'+theid).val());
      });
      var selectedoption=fullarray[2];
   /*alert(fullarray);*/
 $.ajax({
          url:'<?php echo base_url();?>front/Search/onChangeSearchvalue',
          type:'POST',
                data:{allvalues:fullarray,id:sub_role_id,therole:actualrole},
                success:function(data)
                {
                      var obj = JSON.parse(data);
                    var i;var select_array = obj.select;
                    var options = "<option value=''>Select location</option>";
                    for(i=0; i<select_array.length;i++){
                       options += "<option value='"+select_array[i].district+"'>"+select_array[i].district+"</option>"; 
                    }
                    if(thechangeid=='artist' || thechangeid=='director' || thechangeid=='topprofile')
                    {/*$("#location").html(options);
                    $("#location").val(selectedoption);*/}
               $('.searchafterajax').html(obj.view);
           var $input = $('input.rating');
           $input.removeClass('rating-loading').addClass('rating-loading').rating();
                },
            error(error){console.log(error);}
                    });
    
      
  });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        
$('body').on('click',"#load_more_aftersearch",function() {
 var actualrole=$('#actualrole').val();
     /* alert(122);*/
      var fullarray=[];
      $('.search_change').each(function(){
          var theid=$(this).attr('attr-id');
          fullarray.push($('#'+theid).val());
      });
      var ajaxcount =$('#hiddencountval').val();
      var divlength = $('.ajax_data_role_aftersearch').length;
 //alert(divlength);
   var sub_role_id="<?php echo $role_name;?>";
  /*alert(sub_role_id);*/ 
        $.ajax({
                url:'<?php echo base_url();?>front/Search/load_more_searchafterajax',
                 type:'POST',
                data:{allvalues:fullarray,limit:divlength,id:sub_role_id,therole:actualrole},
                success:function(data)
                {
                  $('#role_ajax_aftersearch').append(data);
                      var divtotal = $('.ajax_data_role_aftersearch').length;
                    var totalrole=ajaxcount;
                    if(totalrole<=divtotal)
                        {
                            $('#load_more_aftersearch').hide();
                        }
           var $input = $('input.rating');
           $input.removeClass('rating-loading').addClass('rating-loading').rating();
                 },
            error(error){console.log(error);}
                    });
});
    });
</script>
