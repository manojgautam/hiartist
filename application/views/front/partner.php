<div class="slickslide_img" style="position:relative">
    <img src="<?php echo base_url();?>assets/artists/images/footer-bg-design.jpg">
</div>
<section class="featured_partner">
    <div class="container">
        <h3>featured <span>partners</span></h3>
       <div class="row">
           <?php foreach($partners as $value){?>
           <div class="col-md-3 col-sm-6">
           <div class="partner-text">
               <img src="<?php echo base_url();?>uploads/<?php echo $value->path; ?>" alt="partner">
           </div>
           </div>
           <?php } ?>
            
        </div>
    </div>
</section>
