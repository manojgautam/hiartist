<div class="slickslide_img" style="position:relative"><img src="<?php echo base_url();?>assets/artists/images/footer-bg-design.jpg">
<div class="content">
   
           <div class="top_section">
          
        </div>
</div>
</div>

<section class="shoplist_main">
    <div class="container filterajax">
        <h2>All Groups</h2>
        <?php  if(count($groups)==0){?>
       <div class="row">
       <h2>&nbsp;No Group Found</h2> 
        </div>
        <?php } ?>
        <div class="shoplist_cover" >
            <div class="row" id="groups_ajax">
                <?php $i=0; foreach($groups as $item){ ?>
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="shoplist_bx ajax_data">
                        <?php if($item->image=='')
                            { ?><div class="shoplist_bx_bg">
                        <a href="<?php echo base_url();?>viewgroup/<?php echo $item->group_id;?>"><img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo 'noimage.png'; ?>&h=320&w=375" alt="group">
                           
                        </a>
                      </div>
                        <?php
                            } else { ?>
                        <div class="shoplist_bx_bg">
                            <a href="<?php echo base_url();?>viewgroup/<?php echo $item->group_id;?>"><img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo $item->image; ?>&h=320&w=375" alt="group">
                            
                          </a>
                               
                        </div>
                        <?php } ?>
                        <div class="shoplist_bx_detail">
                            <a href="<?php echo base_url();?>viewgroup/<?php echo $item->group_id;?>"><?php if(strlen($item->name)>=21){echo substr($item->name,0,21);echo "[..]";}else echo $item->name;?></a>
                        </div>
                         <div class="shoplist_bx_description">
                             <p><?php $re = $item->description;$tags=array("<p>","</p>","<br/>","<br>","<b>","</b>","<i>","</i>","<u>","</u>","<li>","</li>","<ul>","</ul>","<ol>","</ol>","<small>","</small>","<h1>","</h1>","<h2>","</h2>","<h3>","</h3>","<h5>","</h5>","<h6>","</h6>");
            $thedesc=str_replace($tags, "", $re);if(strlen($thedesc)>=60){echo substr($thedesc,0,60);echo "[..]";}else echo $thedesc;?></p>
                        </div>
                    </div>
                </div>
                <?php $i++; } ?>
            </div>
        </div>
 <?php if($count > 6)
{?>
    <div class="row">
             <div class="col-md-offset-4 col-md-4 text-center">
                 <a href="javascript:void(0);" id="load_more" class="show_more">Show More</a>
                 <img id="loading-image" src="<?php echo base_url();?>assets/artists/images/ripple.gif" style="display:none">
                 
                </div>  
              </div>
        <?php } ?>
        
    </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
  $( "#load_more" ).click(function() {
     $('#loading-image').show();
   // alert(12);
  var divlength =$('.ajax_data').length;
     // alert(divlength);
          $.ajax({
                url:'<?php echo base_url();?>front/Groups/load_more',
                 type:'POST',
                data:{limit:divlength},
                success:function(data)
                {
                     $('#loading-image').hide();
                    $('#groups_ajax').append(data);
                      var divtotal = $('.ajax_data').length;
                    var totalgroup='<?php echo $count ;?>';
                    if(totalgroup<=divtotal)
                        {
                            $('#load_more').hide();
                        }
                 },
            error(error){console.log(error);}
                    });
});
    });
</script>
