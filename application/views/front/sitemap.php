<div class="slickslide_img" style="position:relative">
    <img src="<?php echo base_url();?>assets/artists/images/footer-bg-design.jpg" />

</div>
<section>
    <div class="container">
        <div class="row">
            <div class="thesitemap text-left">
                <div class="stemap sitemap-other">
                    <div class="col-md-3 col-sm-3"><a href="<?php echo base_url();?>HowItwork">How it works</a></div>
                    <div class="col-md-3 col-sm-3"><a href="<?php echo base_url();?>partner">Featured Partners</a></div>
                    <div class="col-md-3 col-sm-3"><a href="<?php echo base_url();?>career">Join our team</a></div>
                    <div class="col-md-3 col-sm-3"><a href="<?php echo base_url();?>advertise">Advertise with us</a></div>
                    <div class="col-md-3 col-sm-3"><a href="<?php echo base_url();?>Welcome/allstate">Cultural Points</a></div>
                    <div class="col-md-3 col-sm-3"><a href="<?php echo base_url();?>allevents">Events</a></div>
                </div>
                <div class="clearfix"></div>
                <div class="stemap sitemap-artist">
                    <div class="text-center state_heading"><h3>Artists</h3></div>
                    <?php foreach($allartists as $key => $vals){

  foreach($vals as $dist){?>
                        <div class="col-md-3 col-sm-3">
                            <a href="<?php echo base_url();?>search/Artists/<?php echo $key;?>/<?php echo $dist; ?>">
                                <p><?php echo $key;?> in
                                    <?php echo $dist;?>
                                    </p>
                            </a>
                        </div>
                        <?php }} ?>
                </div>
                <div class="clearfix"></div>

                <div class="stemap sitemap-direct">
                    <div class="text-center state_heading"><h3>Directors</h3></div>
                    <?php foreach($alldirectors as $key => $vals){
                    foreach($vals as $dist){ ?>
                        <div class="col-md-3 col-sm-3">
                            <a href="<?php echo base_url();?>search/Director/<?php echo $key;?>/<?php echo $dist; ?>">
                                <?php echo $key;?> in
                                    <?php echo $dist;?>
                            </a>
                        </div>
                        <?php }} ?>
                </div>
                <div class="clearfix"></div>

                <div class="stemap sitemap-shop">
                    <div class="text-center state_heading"><h3>Cultural Points</h3></div>

                    <?php foreach($alldistricts as $val){?>
                        <div class="col-md-3 col-sm-3"><a href="<?php echo base_url();?>shop/<?php echo $val->district_id;?>">Shops in <?php echo $val->name;?></a></div>
                        <?php } ?>
                </div>
            </div>
        </div>
    </div>

</section>