 <footer>

<?php if($this->session->userdata('usersession')==''){?>
      <div class="calltoaction">
   <ul class="action-list">
            <li class="reg_artist"><a href="<?php echo base_url();?>registeration" class="banner-button-style">register as artist</a></li>
            <li><span><img src="<?php echo base_url();?>assets/artists/images/artist_icon.png" alt="artist_icon"></span></li>
            <li class="hire_artist"><a  class="banner-button-style showlogin" href="javascript:void(0);">login as artist</a></li>
        </ul>
    </div>
<?php } ?>
   
   <div class="container">
   <div class="row">
   <div class="footer-wrap">
            <ul class="footer-list footer-nav">
                <li><a href="<?php echo base_url();?>HowItwork" class="footer-link"><i class="fa fa-briefcase" aria-hidden="true"></i> how it works</a></li>
				<li><a href="<?php echo base_url();?>partner" class="footer-link"><i class="fa fa-sun-o" aria-hidden="true"></i> Featured Partners</a></li>
                <li><a href="<?php echo base_url();?>career" class="footer-link"><i class="fa fa-user" aria-hidden="true"></i> join our team</a></li>
                <li><a href="<?php echo base_url();?>advertise" class="footer-link"><i class="fa fa-random" aria-hidden="true"></i> Advertise with us</a></li>
                 <li><a href="<?php echo base_url();?>Welcome/allstate" class="footer-link"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Cultural Points</a></li>
                 <li><a href="<?php echo base_url();?>allevents" class="footer-link"><i class="fa fa-calendar" aria-hidden="true"></i> events</a></li>
                <!--<li><a href="#" class="footer-link">The Magazine</a></li>
                <li><a href="#" class="footer-link">Conceirge & store</a></li>
                <li><a href="#" class="footer-link">Institutions</a></li>
                <li><a href="#" class="footer-link">Launchpad</a></li>
                <li><a href="#" class="footer-link">Sitemap</a></li>
                <li><a href="#" class="footer-link">Featured Partners</a></li>
                <li><a href="#" class="footer-link">Platinum Artist</a></li>
                <li><a href="#" class="footer-link">Contact</a></li>
                <li><a href="#" class="footer-link">Careers</a></li>-->
            </ul>
        </div>
		
		<div class="download-share">
		<div class="center-block">
            <!--<ul>
                <li><a href="#"><img src="<?php echo base_url();?>assets/artists/images/google_play.png" alt="google_play"></a></li>
                <li><a href="#"><img src="<?php echo base_url();?>assets/artists/images/app_store.png" alt="app_store"></a></li>
            </ul>-->
            <ul>
                <li><a href="<?php echo base_url();?>sitemap">sitemap</a></li>
               <li><a href="<?php echo base_url();?>contact" class="footer-link">Contact</a></li>
                <li><a href="<?php echo base_url();?>blog">blog</a></li>
                <li><a href="#">press</a></li>
            </ul>
		  </div>
		 </div>
		 
		 <div class="bottom-footer">
		 <div class="col-sm-6">
		 <div class="copyright">
		 <p>&copy; Hi Artist | All Rights Reserved</p>
		   </div>
		   </div>
		   <div class="col-sm-6">
		   <div class="social-share">
			<ul>
			<li><a href="https://www.facebook.com"><i class="fa fa-facebook-f" aria-hidden="true"></i></a></li>
			<li><a href="https://www.twitter.com"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
			<li><a href="https://www.linkedin.com"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
			<li><a href="https://www.youtube.com"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
			</ul>
		   </div>
		   </div>
		   </div>
    </div>
    </div>
<input type="hidden" value="<?php echo base_url();?>" id="base_url"/>
<input type="hidden" value="<?php if(isset($noshowmap)){echo '';} else{echo 'yes';}?>" id="showmapornot"/>
<input type="hidden" value="<?php if(isset($fancybox_and_editor_hide_script)){echo '';} else{echo 'yes';}?>" id="showfancyornot"/>
    </footer>



<!-- Javascript -->
<script src="<?php echo base_url();?>assets/artists/js/jquerylibraryfooter.js"></script>
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>-->
<script src="<?php echo base_url();?>assets/artists/js/contact_page_validation.js"></script>
<script src="<?php echo base_url();?>assets/artists/js/advertise_Validation.js"></script>
<script src="<?php echo base_url();?>assets/artists/js/career_Validation.js"></script>
<script src="<?php echo base_url();?>assets/artists/js/bootstrap.min.js"></script>

<!-- <script src="<?php echo base_url();?>assets/artists/js/scripts.js"></script> -->
<script src="<?php echo base_url();?>assets/artists/js/wow.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/artists/js/modernizr.custom.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/artists/js/jquerymigrate1.2.js"></script>
<!--<script type="text/javascript" src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>-->
<script type="text/javascript" src="<?php echo base_url();?>assets/artists/js/slick.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/artists/js/jquery.navgoco.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/artists/js/bootstrap-filestyle.min.js"></script>
<?php if(isset($noshowmap)){} else{ ?>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDTz6OY7wyoVgnpu7073kcDVhsdOtUQv6o&libraries=places" type="text/javascript"></script>
<?php } ?>

<script type="text/javascript" src="<?php echo base_url();?>assets/artists/js/fancyybox.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>-->

<script type="text/javascript" src="<?php echo base_url();?>assets/artists/fancybox/js/media.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/artists/fancybox/js/thumbs.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/artists/js/front.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap/js/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="<?php echo base_url();?>assets/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
  

<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<?php if(isset($fancybox_and_editor_hide_script)){} else{ ?>
<script type="text/javascript" src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
<?php } ?>
<script src="<?php echo base_url();?>assets/artists/js/jquery.mosaicflow.min.js"></script>
<script src="<?php echo base_url();?>assets/bootstrap/js/star-rating.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/plugins/daterangepicker/daterangepicker.js"></script>
<!-- custom scroll script-->
<script type="text/javascript" src="<?php echo base_url();?>assets/artists/js/customscrollbar.js"></script>
<!--<script src="http://cdn.jsdelivr.net/jquery.mcustomscrollbar/3.0.6/jquery.mCustomScrollbar.concat.min.js"></script>-->
    <script>
    (function($){
       jQuery(window).on("load",function(){
            jQuery(".content").mCustomScrollbar({
				
				scrollInertia:300
				
			});
        });
    })(jQuery);

</script>
 
<!--[if lt IE 10]>
<script src="<?php echo base_url();?>assets/artists/js/placeholder.js"></script>
<![endif]-->
<script>

$(document).ready(function () {
$("input").keypress(function(event) {
    if (event.which == 13) {
$( "#login_ajax" ).trigger( "click" );
$( "#forget_submit" ).trigger( "click" );
event.preventDefault();
}
});});



//profile edit showhide
$(document).ready(function () {
  $('.hover-tab').hover(function () {
  $('.edit-icon').css({"visibility":"visible","opacity":"1"});
  }, function () {
  $('.edit-icon').css({"visibility":"hidden","opacity":"0"});
  });
});


$(document).ready(function () {
  $('.block01').hover(function () {
  $('#onered').show();
  $('#oneblack').hide();
  }, function () {
   $('#onered').hide();
    $('#oneblack').show();
  });
});

$(document).ready(function () {
  $('.block03').hover(function () {
   $('#twored').show();
  $('#twoblack').hide();
  }, function () {
   $('#twored').hide();
  $('#twoblack').show();
  });
});


$(document).ready(function () {
var _URL = window.URL || window.webkitURL;
$('.inputFile').bind('change', function() {
  //this.files[0].size gets the size of your file.

  if(this.files[0].size > 2097152)
  {$('.inputFile').val(''); alert("The uploaded file exceeds the max file size specified");}
  if (!(/\.(gif|jpg|jpeg|png|bmp|GIF|JPG|JPEG|PNG|BMP)$/i).test(this.files[0].name)) {              
    $('.inputFile').val(''); alert('You must select an image file only');               
    }
    var file, img;
    if ((file = this.files[0])) {
        img = new Image();
        img.onload = function() {
            if(this.width < 350 || this.height < 350)
            {$('.inputFile').val(''); alert('The width and height of the image should be at least 350px');} 
        };
   
        img.src = _URL.createObjectURL(file);


    }
});});


$(document).ready(function (e) {
	$("#uploadForm").on('submit',(function(e) {
		e.preventDefault();
		$.ajax({
        	url: "<?php echo base_url();?>front/Profile/imageupload",
			type: "POST",
			data:  new FormData(this),
			contentType: false,
    	    cache: false,
			processData:false,
			success: function(data)
		    {var baseurl=$('#base_url').val();
             var newdata=data.split(',');
			$(".gallerys").prepend('<div class="col-md-4"><div class="gallery"><div class="view view-first"><a class="fancybox" rel="ligthbox" href="'+baseurl+'uploads/'+newdata[1]+'"><img style="width:300px;height:250px;" src="'+baseurl+'uploads/'+newdata[1]+'" alt="artist" class="img-responsive"><div class="mask"><div class="info"><i class="fa fa-plus" aria-hidden="true"></i></div></div></a><div class="removeimage" id="'+newdata[0]+'"><span>X</span></div></div></div></div>');
$('.addimagetext').hide();
$('.closeimg').click();
		    },
		  	error: function() 
	    	{
	    	} 	        
	   });
	}));
});

$(document).ready(function (e) {
  $("#profileuploadForm").on('submit',(function(e) {
    e.preventDefault();
    $.ajax({
          url: "<?php echo base_url();?>front/Profile/proflieimageupload",
      type: "POST",
      data:  new FormData(this),
      contentType: false,
          cache: false,
      processData:false,
      success: function(data)
        {var baseurl=$('#base_url').val();
      $("#prof-image").html('<img src="'+baseurl+'uploads/'+data+'"  style="height:400px;" alt="artist">');
      $('.closepimg').click();
        },
        error: function() 
        {
        }           
     });
  }));
});

$(document).ready(function (e) {
  $("#videouploadForm").on('submit',(function(e) {
    e.preventDefault();
     if(/(https:\/\/www.youtube.com\/watch)/.test($('#videourl').val())){$('#youtubeerr').text('');}else{$('#youtubeerr').show();return false;}
    var upperurl = $('#videourl').val();
    var rest = upperurl.split("=");
    var embedurl="https://www.youtube.com/embed/"+rest[1];
    var videourl=embedurl;
    $.ajax({
          url: "<?php echo base_url();?>front/Profile/videoupload",
      type: "POST",
      data:{url:videourl},
      success: function(data){
        var baseurl=$('#base_url').val();
             var newdata=data.split(',');
             var URLendPortion=newdata[1].split('/');
             var lastelement=URLendPortion[URLendPortion.length-1];
      $(".videogallerys").prepend('<div class="col-md-4"><div class="gallery"><div class="view view-first"><a class="fancyboxvideo fancybox.iframe" href="'+newdata[1]+'?autoplay=1&wmode=opaque"><img src="http://img.youtube.com/vi/'+lastelement+'/0.jpg" alt=""/><div class="mask"><div class="info"><i class="fa fa-plus" aria-hidden="true"></i></div></div></a><div class="removeimage" id="'+newdata[0]+'"><span>X</span></div></div></div></div>');
    $(".fancyboxvideo").attr('rel', 'gallery').fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        nextEffect  : 'none',
        prevEffect  : 'none',
        padding     : 0,
        margin      : [20, 60, 20, 60] // Increase left/right margin
    });
    $('.addvideotext').hide();
    $('.closevid').click();
        },
        error: function() 
        {
        }           
     });
  }));
});

$(document).ready(function () {
$(".fancyboxvideo")
    .attr('rel', 'gallery')
    .fancybox({
        openEffect  : 'none',
        closeEffect : 'none',
        nextEffect  : 'none',
        prevEffect  : 'none',
        padding     : 0,
        margin      : [20, 60, 20, 60] // Increase left/right margin
    });

$("body").on('click',".removeimage",function(){
		   var delid = $(this).attr('id');
	$.ajax({
        type: "POST",
        url: "<?php echo base_url();?>front/Profile/deleteimageajax",
        data:{deleteid:delid},
        success: function(data){
          if(data=="yes")
          {$('#'+delid).parent().parent().parent().remove();}
},                   
failure: function(){}                                                        
});
});


$("body").on('click',"#physicalstats",function(){
       var fullarray=[];
       var cunt=0;
       $('.attributes').each(function() {
        var theid=$(this).attr('attr-id');
        if($(this).val()==''){$('#'+theid).hide();}
          else{cunt=cunt+1;$('#'+theid).show();$('#span_'+theid).text($(this).val());}
       fullarray.push($(this).val());
       });
    
  $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>front/Profile/addphysicalstats",
        data:{details:fullarray},
        success: function(data){
if(cunt>0){
$('#physicaladdedit').removeClass('fa-plus');
$('#physicaladdedit').addClass('fa-pencil-square-o');
$('#notupdatedphysical').hide();
}
else{$('#physicaladdedit').removeClass('fa-pencil-square-o');
$('#physicaladdedit').addClass('fa-plus');
$('#notupdatedphysical').show();}
$('.close').click();
},                   
failure: function(){}                                                        
});
});


$("body").on('click',"#worksubmit",function(){

   var locations=$('#attr_locations').val();
   var hours=$('#attr_hours').val();

if($('#attr_locations').val()==null)
{$('#attr_locations').css("border","1px solid red");
return false;
}
else{$('#attr_locations').css("border","1px solid #999");}


  if(locations.length==0){}
  else{$('#workplace').show();$('#span_workplace').text(locations.toString());}
  if(hours==''){$('#workhour').hide();}
  else{$('#workhour').show();$('#span_workhour').text(hours);}
   
  $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>front/Profile/addwork",
        data:{loc:locations,hrs:hours},
        success: function(data){
$('#workaddedit').removeClass('fa-plus');
$('#workaddedit').addClass('fa-pencil-square-o');
$('#notupdatedwork').hide();
$('.close').click();
},                   
failure: function(){}                                                        
});

});



$("body").on('click',"#theskill",function(){

if($('#attr_expertise').val()==null || $('#attr_language').val()==null)
{if($('#attr_expertise').val()==null){$('#attr_expertise').css("border","1px solid red");}else{$('#attr_expertise').css("border","1px solid #999");}
if($('#attr_language').val()==null){$('#attr_language').css("border","1px solid red");}else{$('#attr_language').css("border","1px solid #999");}
return false;
}
else{$('#attr_expertise').css("border","1px solid #999");$('#attr_language').css("border","1px solid #999");}

   var expertise=$('#attr_expertise').val();
   var languages=$('#attr_language').val();

   var option_all = $("#attr_expertise option:selected").map(function () { return $(this).text();}).get().join(',');

$('#span_expertise').text(option_all);
$('#span_languages').text(languages.toString());
$('#expertise').show();
$('#languages').show();
 
   
  $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>front/Profile/addskills",
        data:{lang:languages.toString(),exprt:expertise},
        success: function(data){
$('#skilladdedit').removeClass('fa-plus');
$('#skilladdedit').addClass('fa-pencil-square-o');
$('#notupdatedskill').hide();
$('.close').click();
},                   
failure: function(){}                                                        
});

});
  
     $("body").on('click',".edit-icon",function(){
		   var show_divs = $(this).attr('attr-name');
			var sh_ids = $(this).attr('attr-id');
if(show_divs=="eduplussign" || show_divs=="expepuussign"){}
else{
      if(show_divs!="physical_skill_work_text" && show_divs!="img_text")
		   $(this).addClass("hide_divs");
			$('.icon_'+show_divs+''+sh_ids).addClass("hide_divs");		   
			$('.'+show_divs).show()
		   $('.hide_'+show_divs).hide();
}
	   });
			
$("body").on('click',".input-cancel",function(){
		var hide_divs = $(this).attr('attr-name');
		$('.icon_'+hide_divs).removeClass("hide_divs");
		$('.'+hide_divs).hide()
		$('.hide_'+hide_divs).show();
    });

$("body").on('click',".hide_cancel",function(){
		var hide_divs_names = $(this).attr('attr-name');
		var hide_divs_ids = $(this).attr('attr-id');
		$('.icon_'+hide_divs_names+''+hide_divs_ids).removeClass("hide_divs");
		$('.edu_sectin_'+hide_divs_ids).show()
		$('.edu_input_'+hide_divs_ids).hide();
    $('#err_edit_education_'+hide_divs_ids).text('');
    });


$("body").on('click',".hide_cancel_exp",function(){
		var hide_divs_names = $(this).attr('attr-name');
		var hide_divs_ids = $(this).attr('attr-id');
		$('.icon_'+hide_divs_names+''+hide_divs_ids).removeClass("hide_divs");
		$('.exp_sectin_'+hide_divs_ids).show()
		$('.exp_input_'+hide_divs_ids).hide();
        $('#error_edit_frm_experience_'+hide_divs_ids).text('');
    });

$("body").on('click',".save-input",function(){
    var hide_divs = $(this).attr('attr-name');
      $('.editable-input_'+hide_divs).each(function() {
      var id=$(this).attr('id');
        $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>front/Profile/ajaxsave",
        data:{value:$(this).val(),key:$(this).attr('id')},
       success: function(data){
        var fullname = data.split(" ");
        if(hide_divs=='description_text')
        $('.hide_'+hide_divs).html(data);
        else
        $('.hide_'+hide_divs).text(data);

        if(id=='first_name')
        {$("#"+id).val(fullname[0]);}
        else if(id=='last_name')
        {$("#"+id).val(fullname[1]);}
        else
        { if(hide_divs=='description_text')
          {
//$('.hide_'+hide_divs).append('<div class="hovericons"><span class="edit-icon icon_'+hide_divs+'" attr-name="'+hide_divs+'" style="visibility:hidden;opacity:0;float:left;text-align:right;width:20px;"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></div>');
$('.icon_'+hide_divs).html('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>');
}
           else
           {$('.hide_'+hide_divs).append('<span class="edit-icon icon_'+hide_divs+'" attr-name="'+hide_divs+'" style="display:inline;"><i class="fa fa-pencil" aria-hidden="true"></i></span>');}
          if(hide_divs!='dob_text')
          $("#"+id).val(data);
         }
       },                   
      failure: function(){}                                                        
               });
      });
    $('.icon_'+hide_divs).removeClass("hide_divs");
    $('.'+hide_divs).hide()
    $('.hide_'+hide_divs).show();
    });

 
    $("body").on('click',".cancel-table",function(){
     var cancelID=$(this).attr('id');
     $('.added-form_'+cancelID).remove();
     $('.error_added-form_'+cancelID).remove();
    });

 $("body").on('click',".educatin_del",function(){
     var delID=$(this).attr('attr-id');
$.ajax({
        type: "POST",
        url: "<?php echo base_url();?>front/Profile/deleteeducationajax",
        data:{deleteid:delID},
        success: function(data){
$('#edu_frm_courses'+delID).remove();
},                   
        failure: function(){}                                                        
  });
    });


$("body").on('click',".removeprev",function(){
$(".inputFile").val('');
});


$("body").on('click',".experience_del",function(){
     var delID=$(this).attr('attr-id');
$.ajax({
        type: "POST",
        url: "<?php echo base_url();?>front/Profile/deleteexperienceajax",
        data:{deleteid:delID},
        success: function(data){
$('#exp_frm_courses'+delID).remove();
},                   
        failure: function(){}                                                        
});
});



    $("body").on('click',".save-button-js",function(){

     var saveID=$(this).attr('id');
     var courseName=$('#course_name'+saveID).val();
     var instituteName=$('#institute_name'+saveID).val();
     var startDate=$('#sdate'+saveID).val();
     var endDate=$('#edate'+saveID).val();
   var reslt=(Date.parse(startDate) > Date.parse(endDate));
   if (reslt){var chk="yes";}else{var chk="no";}
     if(courseName.trim()=='' || instituteName.trim()=='' || startDate.trim()=='' || endDate.trim()==''){
     $('#err_add_education_'+saveID).text('Please fill all the fields.'); return false;}
     if(chk=="yes"){$('#err_add_education_'+saveID).text('Start date must be less than end date.'); return false;}
      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>front/Profile/educationsaveajax",
        data:{course:courseName,institute:instituteName,startdate:startDate,enddate:endDate},
        success: function(data){
$('.profile_edu').prepend('<form class="added-form education-main-div" name="frm_courses" id="edu_frm_courses'+data+'" action="" method="post"><span style="color:red;" id="err_edit_education_'+data+'"></span><div class="col-md-12 col-sm-12 eduhere education-main-div"><div class="row"><div class="hovericons"><span class="edit-icon icon_edu_text'+data+' educatin_edit" attr-id="'+data+'" style="visibility:hidden;opacity:0;" attr-name="edu_text"><i class="fa fa-pencil-square-o" style="margin-right:4px" aria-hidden="true"></i></span><span class="edit-icon icon_edu_text'+data+' educatin_del" style="visibility:hidden;opacity:0;" attr-id="'+data+'"><i class="fa fa-trash-o" aria-hidden="true"></i></span></div><div class="col-sm-6 course-add-section"><label>course name</label><div class="inner-edu-span"><span class="edu_sectin_'+data+'" id="span_course_name_'+data+'" style="display:block">'+courseName+'</span><input name="course_name" style="display:none;" id="course_name_'+data+'" class="edu_input_'+data+'" value="'+courseName+'" type="text"><span id="err_coursename_3"></span></div></div><div class="col-sm-6 course-add-section"><label>institute name</label><div class="inner-edu-span"><span class="edu_sectin_'+data+'" id="span_institute_name_'+data+'" style="display:block">'+instituteName+'</span><input name="institute_name" style="display:none;" id="institute_name_'+data+'" class="edu_input_'+data+'" value="'+instituteName+'" type="text"><span id="err_institutename_3"></span></div></div></div><div class="row"><div class="col-sm-6 course-add-section"><label>start date</label><div class="inner-edu-span"><span class="edu_sectin_'+data+'" id="span_sdate_'+data+'" style="display:block">'+startDate+'</span><input id="sdate_'+data+'" readonly style="display:none;"  class="edu_input_'+data+' edu_datepickr" name="start_date" value="'+startDate+'" type="text"><span id="err_start_date_4" class="icon-validation invalid-text" value=""></span></div></div><div class="col-sm-6 course-add-section"><label>end date</label><div class="inner-edu-span"><span class="edu_sectin_'+data+'" id="span_edate_'+data+'" style="display:block">'+endDate+'</span><input id="edate_'+data+'" readonly style="display:none;" class="edu_input_'+data+' edu_datepickr" name="end_date" value="'+endDate+'" type="text"></div></div></div><div class="row"><div class="col-sm-12 addpadding'+data+'"><span class="btn-wrap edu_input_'+data+'" style="display:none;"><input value="cancel" id="" attr-id="'+data+'" class="hide_cancel submit-btn custom-submit-btm" type="button" attr-name="edu_text"></span><span class="btn-wrap edu_input_'+data+'" style="display:none;"><input value="save" attr-name="edu_text" class="edu-save-button-js save-table submit-btn custom-submit-btm" attr-id="'+data+'" id="" type="button"></span></div></div></div></form>');
$('.edu_datepickr').datepicker({autoclose: true});
$('.addedutext').hide();
 $('#frm_courses'+saveID).remove();
},                   
        failure: function(){}                                                        
           });

    });

 $("body").on('click',".exp-save-button-js",function(){

     var saveID=$(this).attr('id');

if($('#project_name'+saveID).val().trim()=='' ||$('#role_name'+saveID).val().trim()=='' || $('#expsdate'+saveID).val().trim()=='' || $('#expedate'+saveID).val().trim()=='' || $('#description'+saveID).val().trim()=='')
{$('#error_frm_experience'+saveID).text("Please fill all the fields.");return false;}
     var projectName=$('#project_name'+saveID).val();
     var roleName=$('#role_name'+saveID).val();
     var startDate=$('#expsdate'+saveID).val();
     var endDate=$('#expedate'+saveID).val();
     var Description=$('#description'+saveID).val();
  var reslt=(Date.parse(startDate) > Date.parse(endDate));
   if (reslt){var chk="yes";}else{var chk="no";}
 if(chk=="yes"){$('#error_frm_experience'+saveID).text('Start date must be less than end date.'); return false;}

      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>front/Profile/experiencesaveajax",
        data:{project:projectName,role:roleName,stdate:startDate,eddate:endDate,desc:Description},
        success: function(data){
$('.expnotadd').hide();
$('.profile_exp').prepend('<div class="added-form" name="frm_courses" id="exp_frm_courses'+data+'"><p id="error_edit_frm_experience_'+data+'" style="color:red;"></p><div class="hovericons"><span class="edit-icon icon_exp_text'+data+' experience_edit" style="visibility:hidden;opacity:0;float:left;text-align:right;width:20px;" attr-id="'+data+'" attr-name="exp_text"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span><span class="edit-icon icon_exp_text'+data+' experience_del" style="visibility:hidden;opacity:0;float:right;text-align:right;width:20px;" attr-id="'+data+'"><i class="fa fa-trash-o" aria-hidden="true"></i></span></div><div class="col-sm-12 exp-main-div"><div class="row"><div class="col-sm-6 exp-add-section"><label>Project Name</label><div><span class="exp_sectin_'+data+'" id="span_project_name_'+data+'" style="display:block">'+projectName+'</span><input name="course_name" style="display:none;" id="project_name_'+data+'" class="exp_input_'+data+'" value="'+projectName+'" type="text"><span id="err_coursename_3"></span></div></div><div class="col-sm-6 exp-add-section"><label class="color-light">Role</label><div><span class="exp_sectin_'+data+'" id="span_role_name_'+data+'" style="display:block">'+roleName+'</span><input name="institute_name" style="display:none;" id="role_name_'+data+'" class="exp_input_'+data+'" value="'+roleName+'" type="text"><span id="err_institutename_3"></span></div></div></div><div class="row"><div class="col-sm-6 exp-add-section"><label class="color-light">start date</label><div class="exp-course-add-section"><span class="exp_sectin_'+data+'" id="span_expsdate_'+data+'" style="display:block">'+startDate+'</span><input readonly id="expsdate_'+data+'" style="display:none;"  class="exp_input_'+data+' exp_datepickr" name="start_date" value="'+startDate+'" type="text"><span id="err_start_date_4" class="icon-validation invalid-text" value=""></span></div></div><div class="col-sm-6 exp-add-section"><label class="color-light">end date</label><div class="exp-course-add-section"><span class="exp_sectin_'+data+'" id="span_expedate_'+data+'" style="display:block">'+endDate+'</span><input readonly id="expedate_'+data+'" style="display:none;" class="exp_input_'+data+' exp_datepickr" name="end_date" value="'+endDate+'" type="text"></div></div></div><div class="col-sm-12 profdescription"><label class="color-light">Description</label><div class="exp-course-add-section"><span class="exp_sectin_'+data+'" id="span_description_'+data+'" style="display:block">'+Description+'</span><textarea id="description_'+data+'" style="display:none;" class="exp_input_'+data+'" name="end_date" value="'+Description+'">'+Description+'</textarea></div></div><div><span class="btn-wrap exp_input_'+data+'" style="display:none;"><input value="cancel" id="" attr-id="'+data+'" class="hide_cancel_exp submit-btn custom-submit-btm" type="button" attr-name="exp_text"></span><span class="btn-wrap exp_input_'+data+'" style="display:none;"><input value="save" attr-name="exp_text" class="experience-save-button-js save-table submit-btn custom-submit-btm" attr-id="'+data+'" id="" type="button"></span></div></div></div>');
$('.exp_datepickr').datepicker({autoclose: true});
 $('#frm_experience'+saveID).remove();
},                   
        failure: function(){}                                                        
           });

    });



$("body").on('click',".experience_edit",function(){
     var editID=$(this).attr('attr-id');
$('.exp_sectin_'+editID).hide();
$('.exp_input_'+editID).show();
    });


  $("body").on('click',".educatin_edit",function(){
     var editID=$(this).attr('attr-id');
$('.edu_sectin_'+editID).hide();
$('.edu_input_'+editID).show();
$('.addpadding'+editID).addClass('edu-can-save-padding');

    });

  $("body").on('click',".edu-save-button-js",function(){
     var saveID=$(this).attr('attr-id');
var eduDetailArray=[];
  $('.edu_input_'+saveID).each(function() {
eduDetailArray.push($(this).val());
});
var hide_divs_names = $(this).attr('attr-name');
var hide_divs_ids = $(this).attr('attr-id');

  var courseName=eduDetailArray[0];
     var instituteName=eduDetailArray[1];
     var startDate=eduDetailArray[2];
     var endDate=eduDetailArray[3];
   var reslt=(Date.parse(startDate) > Date.parse(endDate));
   if (reslt){var chk="yes";}else{var chk="no";}
     if(courseName.trim()=='' || instituteName.trim()=='' || startDate.trim()=='' || endDate.trim()==''){
     $('#err_edit_education_'+saveID).text('Please fill all the fields.'); return false;}
     if(chk=="yes"){$('#err_edit_education_'+saveID).text('Start date must be less than end date.'); return false;}

$.ajax({
        type: "POST",
        url: "<?php echo base_url();?>front/Profile/editeducationsaveajax",
        data:{alldetails:eduDetailArray,eduid:saveID},
        success: function(data){
 $('#err_edit_education_'+saveID).text('');
$('.icon_'+hide_divs_names+''+hide_divs_ids).removeClass("hide_divs");
		$('.edu_sectin_'+hide_divs_ids).show()
		$('.edu_input_'+hide_divs_ids).hide();
$('#span_course_name_'+saveID).text(eduDetailArray[0]);
$('#span_institute_name_'+saveID).text(eduDetailArray[1]);
$('#span_sdate_'+saveID).text(eduDetailArray[2]);
$('#span_edate_'+saveID).text(eduDetailArray[3]);

 },                   
        failure: function(){}                                                        
  });
 });



$("body").on('click',".experience-save-button-js",function(){
     var saveID=$(this).attr('attr-id');
var expDetailArray=[];
  $('.exp_input_'+saveID).each(function() {
expDetailArray.push($(this).val());
});
var hide_divs_names = $(this).attr('attr-name');
var hide_divs_ids = $(this).attr('attr-id');


     var projectName=expDetailArray[0];
     var roleName=expDetailArray[1];
     var startDate=expDetailArray[2];
     var endDate=expDetailArray[3];
     var desc=expDetailArray[4];
   var reslt=(Date.parse(startDate) > Date.parse(endDate));
   if (reslt){var chk="yes";}else{var chk="no";}
     if(projectName.trim()=='' || roleName.trim()=='' || startDate.trim()=='' || endDate.trim()=='' || desc.trim()==''){
     $('#error_edit_frm_experience_'+saveID).text('Please fill all the fields.'); return false;}
     if(chk=="yes"){$('#error_edit_frm_experience_'+saveID).text('Start date must be less than end date.'); return false;}



$.ajax({
        type: "POST",
        url: "<?php echo base_url();?>front/Profile/editexperiencesaveajax",
        data:{alldetails:expDetailArray,expid:saveID},
        success: function(data){
$('#error_edit_frm_experience_'+saveID).text('');
$('.icon_'+hide_divs_names+''+hide_divs_ids).removeClass("hide_divs");
		$('.exp_sectin_'+hide_divs_ids).show()
		$('.exp_input_'+hide_divs_ids).hide();
$('#span_project_name_'+saveID).text(expDetailArray[0]);
$('#span_role_name_'+saveID).text(expDetailArray[1]);
$('#span_expsdate_'+saveID).text(expDetailArray[2]);
$('#span_expedate_'+saveID).text(expDetailArray[3]);
$('#span_description_'+saveID).text(expDetailArray[4]);

 },                   
        failure: function(){}                                                        
  });
 });
});

   $('#dob').datepicker({
      autoclose: true
    });

 $('.edu_datepickr').datepicker({
      autoclose: true
    });

$('.exp_datepickr').datepicker({autoclose: true});


 $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
       // CKEDITOR.replace('editor1');
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();
      });

$(document).ready(function () {
   var i= 1;
	   $('.edu-tab').on('click',function(){
		   $('.edu-form').append('<form class="added-form_'+i+' education-main-div" name="frm_courses-'+i+'" id="frm_courses'+i+'" action="" method="post"><span style="color:red;" id="err_add_education_'+i+'"></span><input class="datauserId" value="111365" type="hidden"><div class="col-md-12 col-sm-12 education-main-div"><div class="col-sm-6 course-add-section"><label>course name</label><div class="inner-edu-span"><input name="course_name" id="course_name'+i+'" class="table-editable-input show" type="text"><span id="err_coursename_3"></span></div></div><div class="col-sm-6 course-add-section"><label>institute name</label><div class="inner-edu-span"><input name="institute_name" id="institute_name'+i+'" class="table-editable-input show" type="text"><span id="err_institutename_3"></span></div></div><div class="col-sm-6 course-add-section"><label>start date</label><div class="inner-edu-span"><input id="sdate'+i+'" class="table-editable-input show hasDatepicker" readonly name="start_date" value="" type="text"><span id="err_start_date_4" class="icon-validation invalid-text" value=""></span></div></div><div class="col-sm-6 course-add-section"><label>end date</label><div class="inner-edu-span"><input id="edate'+i+'" readonly class="table-editable-input show hasDatepicker" name="end_date" value="" type="text"></div></div><div class="col-sm-12"><span class="btn-wrap"><input value="cancel" id="'+i+'" class="cancel-table submit-btn custom-submit-btm" type="button"></span><span class="btn-wrap"><input value="save" class="save-button-js save-table submit-btn custom-submit-btm" id="'+i+'" type="button"></span></div></div></form>');
		  
          $('#sdate'+i).datepicker({autoclose: true});   
          $('#edate'+i).datepicker({autoclose: true});
            i = i+1;
		     });
			
});


$(document).ready(function () {
   var i= 1;
	   $('.exp-tab').on('click',function(){
		   $('.exp-form').append('<form class="added-form_experience_'+i+'" name="frm_courses-'+i+'" id="frm_experience'+i+'" action="" method="post"><p id="error_frm_experience'+i+'" class="error_added-form_experience_'+i+'" style="color:red;"></p><div class="col-sm-12 ajaxexperience course-add-section"><div class="row"><div class="col-sm-6 form-group"><input name="project_name" id="project_name'+i+'" placeholder="Project Name" class="table-editable-input show form-control" type="text"><span id="err_coursename_3"></span></div><div class="col-sm-6 form-group"><input name="role_name" id="role_name'+i+'"  placeholder="Role" class="table-editable-input show form-control" type="text"><span id="err_institutename_3"></span></div></div><div class="row"><div class="col-sm-6 form-group"><input id="expsdate'+i+'" readonly class="table-editable-input show hasDatepicker form-control" placeholder="Start Date" name="start_date" value="" type="text"><span id="err_start_date_4" class="icon-validation invalid-text" value=""></span></div><div class="col-sm-6 form-group"><input id="expedate'+i+'" readonly class="table-editable-input show hasDatepicker form-control" placeholder="End Date" name="end_date" value="" type="text"></div></div><div class="row"><div class="col-sm-12 form-group"><textarea id="description'+i+'" class="form-control" placeholder="Description" name=""></textarea></div></div><div class="course-add-section"><div class="text-right" colspan="4"><span class="btn-wrap"><input value="cancel" id="experience_'+i+'" class="cancel-table submit-btn custom-submit-btm" type="button"></span><span class="btn-wrap"><input value="save" class="exp-save-button-js save-table submit-btn custom-submit-btm" id="'+i+'" type="button"></span></div></form>');
		  
          $('#expsdate'+i).datepicker({autoclose: true});   
          $('#expedate'+i).datepicker({autoclose: true});
            i = i+1;
		     });
			
});

</script>

 <script>
        $("#load_more_review").click(function(){

   $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>front/Profile/loadreviews",
        data:{start:$('.review_chker').length},
       success: function(data){
      
            $('.review_coverdiv').append(data);
             var totlreviews = $("#totalreviews").val();
            var loaded_reviews = $('.review_chker').length;
           if(loaded_reviews >= totlreviews){
            $("#load_more_review").hide();
            } 
     
            var $input = $('input.rating');
           $input.removeClass('rating-loading').addClass('rating-loading').rating();
    
        },
      failure: function(){}
    }); });


   </script>

 <script>
        $("#load_more_review_userprof").click(function(){

   $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>front/UserProfile/loadreviewsuserprof",
        data:{start:$('.review_chker').length,userid:$('#currentprof').val()},
       success: function(data){
      
            $('.review_coverdiv').append(data);
             var totlreviews = $("#totalreviews").val();
            var loaded_reviews = $('.review_chker').length;
           if(loaded_reviews >= totlreviews){
            $("#load_more_review_userprof").hide();
            } 
     
            var $input = $('input.rating');
           $input.removeClass('rating-loading').addClass('rating-loading').rating();
    
        },
      failure: function(){}
    }); });


   </script>

<script>
  $(document).ready(function(){
  $(".save_testimonial" ).click(function() {
      var text=$("#testimonial_description").val();
    
$.ajax({
        type: "POST",
        url:"<?php echo base_url();?>front/Profile/addTestimonial",
        data:{testmonial:text},
      success: function(data){
          var hide_divs ='testimonial_text';
    $('.icon_'+hide_divs).removeClass("hide_divs");
    $('.'+hide_divs).hide();
    $('.hide_'+hide_divs).text(text);
    $('.hide_'+hide_divs).show();
      },
      error(error){console.log(error);}
    });  
  });
  });
</script>
<script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Datemask2 mm/dd/yyyy
        $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 15, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
              ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
              startDate: moment().subtract(29, 'days'),
              endDate: moment()
            },
        function (start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        );

});
    </script>
   <script>
  $(document).ready(function(){
$(".fancybox").fancybox({
    fitToView: false,
    autoSize: true,
    autoScale: 'true',
    autoDimensions: 'true',
   width: 660,
    height: 700,
    maxWidth: 660,
    maxHeight: 700
});});
   </script>

   
      <script>
  $(document).ready(function(){
$(".fancyboxvideo").fancybox({
    fitToView: false, // avoids scaling the image to fit in the viewport
    beforeShow: function () {
        // set size to (fancybox) img
        $(".fancybox-image").css({
            "width": 650,
            "height": 430
        });
        // set size for parent container
        this.width = 650;
        this.height = 430;
    }
});});
   </script>
<script> jQuery(window).load(function() { 
        jQuery('#preloader').fadeOut('slow', function() {
            jQuery(this).hide();
  }); 
}); 
</script>  
<script>
    $(document).ready(function () {
  $("#phn_no").keypress(function (e) {
   if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
         $("#error_phn").html("[0-9] digits only").show().css("color","red");
               return false;
    }
   });
});
    </script>
<script>
     $(document).ready(function () {
$('#image').bind('change', function() {
    //this.files[0].size gets the size of your file.
 if (!(/\.(gif|jpg|jpeg|png|bmp|GIF|JPG|JPEG|PNG|BMP)$/i).test(this.files[0].name)) {
        $('#image').val(' ');
        alert('You must select an image file only');
   }
});
    });
</script>

 <script type="text/javascript">
    $(document).ready(function(){
       $('#forget_submit').click(function(){
var useremail=$('#forgetemail').val().trim();
var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{1,4})+$/;
if(useremail=='')
{$('.emailf').text("Field is required.");
return false;
}
if(!(regex.test(useremail)))
{$('#err-forgot').text("The email field must contain a vaild email.");
return false;
}

$('#err-forgot').text("");
$('.emailf').text("");
   
    $.ajax({
               url:'<?php echo base_url();?>front/Login/checkemail',
               type:'POST',
                data:{email:useremail},
               success:function(data){
                   if(data=='not match'){$('#err-forgot').text("The email is not registered with us.");}
                   else{
                   $('#success_message').text("Please check your mail to reset password.");
                   $("#success_message").delay(4000).fadeOut();
                   }
               },
                error(error){console.log(error);}
           }); 
       });
    });
    
</script>

<script type="text/javascript">
    $(document).ready(function(){
   $('#createfbsession').click(function(e){
   e.preventDefault();
  var thevalue=$('#hiddenfb').val();
 $.ajax({
               url:'<?php echo base_url();?>Welcome/createfbsession',
               type:'POST',
                data:{check:thevalue},
               success:function(data){
                   if(data=='reload'){window.location.reload(true);}
                   else{  }
               },
                error(error){console.log(error);}
           });
    });});
    
</script>

 <script type="text/javascript">
    $(document).ready(function(){
   $('.removeall').click(function(){
$('#notmatch').text('');
$('#err-forgot').text('');
$('#success_message').text('');
$('#emailf').text('');
$('#notmatch').hide();
$('#email1').val('');
$('#exampleInputPassword1').val('');
$('#forgetemail').val('');
    });});
    
</script>
<script>
$(window).bind("load", function() {
  window.setTimeout(function() {
    $("#flashdata_email").fadeTo(500, 0.7).slideUp(500, function(){
        $(this).remove();
    });
}, 10000);
});
   
</script>
<script>
$(window).bind("load", function() {
  window.setTimeout(function() {
    $("#flashdata_advertise").fadeTo(500, 0.7).slideUp(500, function(){
        $(this).remove();
    });
}, 10000);
});
   
</script>
<script>
$(window).bind("load", function() {
  window.setTimeout(function() {
    $("#flashdata_career").fadeTo(500, 0.7).slideUp(500, function(){
        $(this).remove();
    });
}, 10000);
});
   
</script>
<script>
    $('#nav-expander').on('click', function(){
        if($('#nav-expander').hasClass('show_img1')){
            $('#nav-expander').removeClass('show_img1');
            $('#nav-expander').addClass('show_img2');
            $('#headerimg2').show();
            $('#headerimg1').hide();
        }else{
            $('#nav-expander').removeClass('show_img2');
            $('#nav-expander').addClass('show_img1');
            $('#headerimg2').hide();
            $('#headerimg1').show();
        }
    });
</script>
<script>
    $('.plus-img').on('click', function(){
        $('.icon_testimonial_text').click();
    });

</script>

<script type="text/javascript">
    if (window.location.hash && window.location.hash == '#_=_') {
        window.location.hash = '';
    }
</script>
<script>
    
    $("body").on('click',"#radio1",function(){
        $("#directors_select").val('').trigger('change')
           });
    
    $("body").on('click',"#radio2",function(){
  $("#artist_select").val('').trigger('change')
    });
</script>
</body>

</html>
