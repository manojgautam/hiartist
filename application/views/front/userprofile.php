<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/fancybox/css/fullscreen.css" media="screen">
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/fancybox/css/slideshow.css" media="screen">
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/fancybox/css/thumbs.css" media="screen">
<script type="text/javascript" src="http://platform-api.sharethis.com/js/sharethis.js#property=58eb38b23458130012b72c9f&product=inline-share-buttons"></script>
<!-- Go to www.addthis.com/dashboard to customize your tools -->
<!--<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-58ef8702b5f44890"></script>-->


<?php if(!isset($_SESSION['userip'])){$_SESSION['userip']=$_SERVER['REMOTE_ADDR'];}?>



<div class="slickslide"><img src="<?php echo base_url();?>assets/artists/images/footer-bg-design.jpg" /></div>
<div class="div_profile">

     <?php if(count($userprofdetail)==0){ ?>
	 	 <div class="no_found">
	 <h2 class="error_notfound">No Profile found</h2>
	  </div>
	 <?php } else {?>
	
<div class="container">

<div class="wrapper <?php if($userprofdetail[0]->role_id==1){echo '';} else{echo 'fordiector';} ?>">
    <div class="row">
	<div class="back_btn">
<?php if(isset($_SERVER['HTTP_REFERER'])){?>
               <a href="<?php echo $_SERVER['HTTP_REFERER'] ;?>" class="banner-button-style">BACK</a>
<?php } ?>
</div>
        <div class="col-md-5 img_artist">
 <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php if($userprofdetail[0]->profile_pic==''){echo 'user.jpeg';}else{echo $userprofdetail[0]->profile_pic;}?>&h=430&w=400" alt="artist">
</div>
         <div class="col-md-7 img_artist_right_side text-left">
             
             <h1><span>I am</span> <?php echo ucfirst($userprofdetail[0]->first_name).' '.ucfirst($userprofdetail[0]->last_name); ?> </h1>
             <h5><?php echo $userprofdetail[0]->sub_role_id.' | '.$userprofdetail[0]->district ; ?></h5>
             <hr>
             <h2><?php if($userprofdetail[0]->role_id==1){echo 'Artist';} else{echo 'Director';} ?> details</h2>
<div class="col-md-12 bio_descrpt">
<span class="col-md-4">User Profile</span>
<p class="col-md-8"><?php echo trim($userprofdetail[0]->last_name) ; ?><?php echo trim($userprofdetail[0]->first_name) ; ?>.<?php echo $userprofdetail[0]->user_id ; ?></p>
</div>
<div class="col-md-12 bio_descrpt">
<span class="col-md-4">Category</span>
<p class="col-md-8"><?php echo $userprofdetail[0]->sub_role_id ; ?></p>
</div>
<div class="col-md-12 bio_descrpt">
<span class="col-md-4">Gender</span>
<p class="col-md-8"><?php if($userprofdetail[0]->gender==''){echo 'N A';} else{echo $userprofdetail[0]->gender;}?></p>
</div>
<div class="col-md-12 bio_descrpt">
<span class="col-md-4">Email</span>
<p class="col-md-8" style="text-transform:none;"><?php echo $userprofdetail[0]->email ; ?></p>
</div>
<!--<div class="col-md-12 bio_descrpt">
<span class="col-md-4">Mobile Number</span>
<p class="col-md-8"><?php if($userprofdetail[0]->phone_no==''){echo 'N A';} else echo $userprofdetail[0]->phone_no ; ?></p>
</div>-->
<div class="col-md-12 bio_descrpt">
<span class="col-md-4">Date Of Birth</span>
<p class="col-md-8"><?php if($userprofdetail[0]->dob=='0000-00-00'){echo 'N A';} else echo date_format(date_create($userprofdetail[0]->dob),"d M, Y"); ?></p>
</div>
            
       
            
           <div class="col-md-12">
                  <input id="" value="<?php if($userprofdetail[0]->review_count==0) {echo 0;} else{echo (float)($userprofdetail[0]->reviews)/(float)($userprofdetail[0]->review_count);}?>" type="number" class="rating" readonly min=0 max=5 step=0.1 data-size="xs">
                </div>
             
    </div>
    </div>
    </div>
    <div class="col-md-12 social_icon <?php if($userprofdetail[0]->role_id==1){echo '';} else{echo 'fordiectorshare';} ?>" style="background:#d93333;display:inline-block;">
            <img src="<?php echo base_url();?>assets/artists/images/view_icon.png" alt="artist"> <?php echo $userprofdetail[0]->profile_view ; ?> <span>|</span>
         

  <!-- share code start-->     
<div class="sharethis-inline-share-buttons" style="display:inline;"></div><span class="sharetextt">Share</span>
<!-- share code end -->

<!--<div class="addthis_inline_share_toolbox"></div>-->
            
        </div>
    
    
        
    
        
        <div class=" col-md-12 artist_tab_section">
            
 <ul class="nav nav-pills nav-stacked col-md-3">
  <li class="active"><a href="#tab_a" data-toggle="pill">Bio</a></li>
  <li><a href="#tab_b" data-toggle="pill">Gallery</a></li>
  <li><a href="#tab_c" data-toggle="pill">Experience</a></li>
  <li><a href="#tab_d" data-toggle="pill">Reviews</a></li>
</ul>

<div class="tab-content col-md-9">
        <div class="tab-pane active" id="tab_a">
            <div class="col-md-12 bio_data text-left">
<div class="col-md-12 bio_descrpt">
                <h3>Description</h3>
               <div class="col-md-12 view-text">
               <p><?php if($userprofdetail[0]->description==''){echo "<p style=''>Not updated by the talent yet.</p>";} else echo $userprofdetail[0]->description;?></p>
               </div>
</div>
<hr/>
<div class="col-md-12 bio_descrpt">
                <h3>Education</h3>
              <div class="col-md-12 profile_edu user_profile_edu">
<?php if(count($edudetail)){}else{echo "<p style=''>Not updated by the talent yet.</p>";}?>
<?php foreach($edudetail as $edu){ ?>
<span id="err_education_0"></span>

<div class="col-md-12 col-sm-12 eduhere education-main-div">
<div class="row">
<div class="col-sm-6 course-add-section">
<label>course name</label>
<div class="inner-edu-span"><span class="edu_sectin_<?php echo $edu->edu_id;?>" id="span_course_name_<?php echo $edu->edu_id;?>" style="display:block"><?php echo $edu->course_name;?></span><input name="course_name" style="display:none;" id="course_name_<?php echo $edu->edu_id;?>" class="edu_input_<?php echo $edu->edu_id;?>" value="<?php echo $edu->course_name;?>" type="text">
<!-- <span id="err_coursename_3"></span>-->
</div>
</div>
<div class="col-sm-6 course-add-section">
<label>institute name</label>
<div class="inner-edu-span">
<span class="edu_sectin_<?php echo $edu->edu_id;?>" id="span_institute_name_<?php echo $edu->edu_id;?>" style="display:block"><?php echo $edu->institute_name;?></span><input name="institute_name" style="display:none;" id="institute_name_<?php echo $edu->edu_id;?>" class="edu_input_<?php echo $edu->edu_id;?>" value="<?php echo $edu->institute_name;?>" type="text"></div>
</div>
</div>
<div class="row">
<div class="col-sm-6 course-add-section">
<label>start date</label>
<div class="inner-edu-span">
<span class="edu_sectin_<?php echo $edu->edu_id;?>" id="span_sdate_<?php echo $edu->edu_id;?>" style="display:block"><?php echo date_format(date_create($edu->start_date),'d M, Y');?></span><input id="sdate_<?php echo $edu->edu_id;?>" style="display:none;"  class="edu_input_<?php echo $edu->edu_id;?> edu_datepickr" name="start_date" value="<?php echo date_format(date_create($edu->start_date),'d M, Y');?>" type="text">
</div>
</div>
<div class="col-sm-6 course-add-section">
<label>end date</label>
<div class="inner-edu-span">
<span class="edu_sectin_<?php echo $edu->edu_id;?>" id="span_edate_<?php echo $edu->edu_id;?>" style="display:block"><?php echo date_format(date_create($edu->end_date),'d M, Y');?></span><input id="edate_<?php echo $edu->edu_id;?>" style="display:none;" class="edu_input_<?php echo $edu->edu_id;?> edu_datepickr" name="end_date" value="<?php echo date_format(date_create($edu->end_date),'d M, Y');?>" type="text"></div>
</div>
</div>
</div>


<?php } ?>
 </div>
</div>
<hr/>
<div class="col-md-12 bio_descrpt">
                <h3>Physical Stats</h3>
<?php if(count($physicalstats)){}else{echo "<p>Not updated by the talent yet.</p>";}?>
        <?php foreach($physicalstats as $val){?>
                        <p id="hairlength" style="<?php if($val->hair_length==''){echo 'display:none';}?>"> <span>Hair length</span> : <span id="span_hairlength"><?php echo $val->hair_length;?></span></p>
                        <p id="hairtype" style="<?php if($val->hair_type==''){echo 'display:none';}?>"> <span>Hair type</span> : <span id="span_hairtype"><?php echo $val->hair_type;?></span></p>
                        <p id="biceps" style="<?php if($val->biceps==''){echo 'display:none';}?>"> <span>Biceps</span> : <span id="span_biceps"><?php echo $val->biceps;?></span></p>
                        <p id="chest" style="<?php if($val->chest==''){echo 'display:none';}?>"> <span>Chest</span> : <span id="span_chest"><?php echo $val->chest;?></span></p>
                        <p id="hips" style="<?php if($val->hips==''){echo 'display:none';}?>"> <span>Hips</span> : <span id="span_hips"><?php echo $val->hips;?></span></p>
                        <p id="waist" style="<?php if($val->waist==''){echo 'display:none';}?>"> <span>Waist</span> : <span id="span_waist"><?php echo $val->waist;?></span></p>
                        <p id="bust" style="<?php if($val->bust==''){echo 'display:none';}?>"> <span>Bust</span> : <span id="span_bust"><?php echo $val->bust;?></span></p>
                        <p id="weight" style="<?php if($val->weight==''){echo 'display:none';}?>"> <span>Weight</span> : <span id="span_weight"><?php echo $val->weight;?></span></p>
                        <p id="height" style="<?php if($val->height==''){echo 'display:none';}?>"> <span>Height</span> : <span id="span_height"><?php echo $val->height;?></span></p>
                        <?php } ?>
</div>
<hr/>
<div class="col-md-12 bio_descrpt">
                <h3>Skills</h3>
      <?php if($skills[0]['expert']=='' && $skills[0]['lang']==''){echo "<p>Not updated by the talent yet.</p>";}?>     
 <?php foreach($skills as $val){?>
 <p id="expertise" style="<?php if($val['expert']==''){echo 'display:none';}?>"> <span>Expertise</span> : <span id="span_expertise"><?php echo str_replace(',', ", ",$val['expert']);?></span></p>
                        <p id="languages" style="<?php if($val['lang']==''){echo 'display:none';}?>"> <span>Languages</span> : <span id="span_languages"><?php echo  str_replace(',', ", ",$val['lang']);?></span></p>
<?php } ?>
</div>
<hr/>
<div class="col-md-12 bio_descrpt">
                <h3>Work Preference</h3>
  <?php if(count($workpref)){}else{echo "<p>Not updated by the talent yet.</p>";}?>  
                <?php foreach($workpref as $val){?>
 <p id="workplace" style="<?php if($val->work_places==''){echo 'display:none';}?>"> <span>Preferred Locations</span> : <span id="span_workplace"><?php echo  str_replace(',', ", ",$val->work_places);?></span></p>
                        <p id="workhour" style="<?php if($val->work_hours==''){echo 'display:none';}?>"> <span>Working Hours</span> : <span id="span_workhour"><?php echo $val->work_hours;?></span></p>
<?php } ?>
</div>     
<hr/>  
             
                </div>
           
        </div>


<div class="tab-pane" id="tab_b">
       <div class="col-md-12 col-sm-12 list-group gallerys">
<div class="heading-gallery"><h2>Photos</h2></div>
  <?php if(count($imagedetail)){}else{echo "<p class='image-video'>Not updated by the talent yet.</p>";}?>      
  <?php foreach($imagedetail as $imgs){ ?>
                    <div class="col-md-4">
                        <div class="gallery">
                            <div class="view view-first">
                                <a class="fancybox" rel="ligthbox" href="<?php echo base_url();?>uploads/<?php echo $imgs->url;?>">
                     <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo $imgs->url;?>&h=250&w=300" alt="userImage" class="img-responsive">
                  <div class="mask">
                        <div class="info"><i class="fa fa-plus" aria-hidden="true"></i></div>
                    </div>
                   </a>

                            </div>
                        </div>
                    </div>
          <?php } ?>     
       </div>

<div class="col-md-12 col-sm-12 list-group videogallerys">
<div class="heading-gallery"><h2>Videos</h2></div> 
<?php if(count($video)){}else{echo "<p class='image-video'>Not updated by the talent yet.</p>";}?> 
<?php foreach($video as $vid){ 
          $lastvalue=explode('/',$vid->url);
          $lastval=$lastvalue[count($lastvalue)-1];
    ?>
                    <div class="col-md-4">
                        <div class="gallery">
                            <div class="view view-first">
                              <a class="fancyboxvideo fancybox.iframe" href="<?php echo $vid->url;?>?autoplay=1&wmode=opaque"><img  src="http://img.youtube.com/vi/<?php echo $lastval;?>/0.jpg" alt=""/>
                               <div class="mask">
                        <div class="info"><i class="fa fa-plus" aria-hidden="true"></i></div>
                    </div>
                </a>
                         </div>
                        </div>
                    </div>

                    <?php } ?>
     
</div>

</div>
        <div class="tab-pane" id="tab_c"> 
             <div class="col-md-12 experience_data text-left">
           <div class="profile_exp">
<?php if(count($experiencedetail)){}else{echo "<p class='image-video exp-review'>Not updated by the talent yet.</p>";}?> 
                  <?php foreach($experiencedetail as $edu){ ?>
<div class="added-form" name="frm_courses" id="exp_frm_courses<?php echo $edu->exp_id;?>">
	<span id="err_education_0"></span>
<div class="exp-main-div">
	<div class="row">
	<div class="col-sm-6 exp-add-section">
		<label>Project Name</label>
		<div>
			<span class="exp_sectin_<?php echo $edu->exp_id;?>" id="span_project_name_<?php echo $edu->exp_id;?>" style="display:block"><?php echo $edu->project_name;?></span>
			<input name="course_name" style="display:none;" id="project_name_<?php echo $edu->exp_id;?>" class="exp_input_<?php echo $edu->exp_id;?>" value="<?php echo $edu->project_name;?>" type="text">
				<span id="err_coursename_3"></span>
			</div>
		</div>
		<div class="col-sm-6 exp-add-section">
			<label class="color-light">Role</label>
			<div>
				<span class="exp_sectin_<?php echo $edu->exp_id;?>" id="span_role_name_<?php echo $edu->exp_id;?>" style="display:block"><?php echo $edu->role;?></span>
				<input name="institute_name" style="display:none;" id="role_name_<?php echo $edu->exp_id;?>" class="exp_input_<?php echo $edu->exp_id;?>" value="<?php echo $edu->role;?>" type="text">
					<span id="err_institutename_3"></span>
				</div>
			</div>
			</div>
			<div class="row">
			<div class="col-sm-6 exp-add-section">
				<label class="color-light">start date</label>
				<div class="exp-course-add-section">
					<span class="exp_sectin_<?php echo $edu->exp_id;?>" id="span_expsdate_<?php echo $edu->exp_id;?>" style="display:block"><?php echo date_format(date_create($edu->start_date),'d M, Y');?></span>
					<input id="expsdate_<?php echo $edu->exp_id;?>" style="display:none;"  class="exp_input_<?php echo $edu->exp_id;?> exp_datepickr" name="start_date" value="<?php echo date_format(date_create($edu->start_date),'d M, Y');?>" type="text">
						<span id="err_start_date_4" class="icon-validation invalid-text" value=""></span>
					</div>
				</div>
				<div class="col-sm-6 exp-add-section">
					<label class="color-light">end date</label>
					<div class="exp-course-add-section">
						<span class="exp_sectin_<?php echo $edu->exp_id;?>" id="span_expedate_<?php echo $edu->exp_id;?>" style="display:block"><?php echo date_format(date_create($edu->end_date),'d M, Y');?></span>
						<input id="expedate_<?php echo $edu->exp_id;?>" style="display:none;" class="exp_input_<?php echo $edu->exp_id;?> exp_datepickr" name="end_date" value="<?php echo date_format(date_create($edu->end_date),'d M, Y');?>" type="text">
						</div>
					</div>
					</div>
					<div class="profdescription">
						<label class="color-light">Description</label>
						<div class="exp-course-add-section">
							<span class="exp_sectin_<?php echo $edu->exp_id;?>" id="span_description_<?php echo $edu->exp_id;?>" style="display:block"><?php echo $edu->description;?></span>
							<textarea id="description_<?php echo $edu->exp_id;?>" style="display:none;" class="exp_input_<?php echo $edu->exp_id;?>" name="end_date" value="<?php echo $edu->description;?>"><?php echo $edu->description;?></textarea>
						</div>
					</div>
					<div>
					
							</div>
                       	</div>
						</div>
						
						<hr/>
<?php } ?>

</div>
        </div>
    </div>
        <div class="tab-pane" id="tab_d">
  <div><input type="button" name="reviewbtn" id="reviewbtn" <?php if($this->session->userdata('usersession')!='') {
$Sess = $this->session->userdata('usersession');$sesid=$Sess['user_id'];$profid=$currentprofid;
if($profid==$sesid){echo 'style="display:none;"';}
else{if(count($checkoldreview)){$text='Edit Review';}else{$text='Write a Review';}echo 'value="'.$text.'" data-toggle="modal" data-target="#reviewModal"';}
}else echo 'value="Write a Review" class="showlogin"';?>
/></div>
                  <div class="col-md-12 experience_data review_coverdiv text-left">
<?php if(count($allreviews)){}else{echo "<p class='image-video exp-review' style='text-align:center;padding:0px'>No reviews yet.</p>";}?> 
<?php  if(count($allreviews)>=3) $x=3; else $x=count($allreviews);$y=0;foreach($allreviews as $val){if($y<$x){?>
             <div class="row review-area-inner review_chker">
                            <div class="col-xs-12 col-sm-3 col-md-1">
                            <div class="img_ara">
                                <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php if($val->profile_pic==''){echo 'user.jpeg';} else{echo $val->profile_pic;}?>&h=80&w=75" alt="artist"/>
                            </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-7">
                                <h3><?php echo $val->first_name.' '.$val->last_name;?></h3>
                      
                                <p style="word-wrap:break-word"><?php echo $val->description;?></p>
                                 
                            </div>
                            <div class="col-xs-12 col-sm-5 col-md-4" style="position:relative;left:120px">
                                <input id="" value="<?php echo $val->review;?>" type="number" class="rating" readonly min=0 max=5 step=0.1 data-size="xs">
                            </div>
                        </div>
<?php $y++;}else{break;}} ?>
                </div>
<input type="hidden" id="totalreviews" value="<?php echo count($allreviews);?>">
<?php if(count($allreviews)>3){?>
<div class=" col-md-12 text-center show_more_btn">
  <a class="show_more" style="margin-bottom:30px;margin-top:20px;" id="load_more_review_userprof" href="javascript:void(0);">Show More</a>
</div>
  <?php } ?>
        </div>
</div><!-- tab content --> </div>
    
    </div>
<!------------------------------client-------------------->
<!-- reviews Modal -->
  <div class="modal fade" id="reviewModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Reviews</h4>
        </div>
        <div class="modal-body">
     
<form method="post" action="<?php echo base_url();?>front/UserProfile/addreviews">
    <span>Rating</span>
                           <input id="input-21b" name="rating" value="<?php if(count($checkoldreview)){echo $checkoldreview[0]->review;}else echo 0;?>" type="number" class="rating" min=0 max=5 step=0.1 data-size="xs"><span class="caption"><span class="label label-default"></span></span>
<br><br>                       
<span>Comment</span>
        <div class="form-group">
          <textarea rows="2" onclick="$('#blankcomm').text('');" class="" name="comment" id="usr_about" style="position:relative;left:0px;top:0px;" placeholder="Enter Your reviews"><?php if(count($checkoldreview)){echo $checkoldreview[0]->description;}?></textarea>
<p style="color:red" id="blankcomm"></p>
        </div>
        <div class="form-group text-center">
        <input type="hidden" name="currentprof" id="currentprof" value="<?php echo $currentprofid;?>">
        <input type="submit"  id="commsubmit" name="submit"  value="Submit" style="background:red;color:white;" class="btn btn-primary"/>
</div>
       </form>


        
        <div class="modal-footer">
          <button type="button" class="btn btn-default closeimg" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
    <?php } ?>
</div>
<!-- Modal end -->


