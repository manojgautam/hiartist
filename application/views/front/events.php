

<div class="slickslide_img" style="position:relative"><img src="<?php echo base_url();?>assets/artists/images/footer-bg-design.jpg" />
<!--<div class="registers_btn">
<a href="/register" class="banner-button-style">register as artist</a>
 </div>-->
</div>
   <ul class="nav navbar-nav navbar-right events_sort">
       <div class="container">
        <li class="sort">Sort by</li>
      <li class="dropdown">
          <li class="dropdown">
           <input type="text" readonly id="reservationtime" attr-id="reservationtime"  name="daterange" class="eventDatechange" placeholder="select Date Range">
          </li>
          <li class="dropdown">
              <input type="text" name="location" id="searchTextField" attr-id="searchTextField" class="eventDatechange">
              <input type="hidden" id="lati" name="latitude" >
               <input type="hidden" id="long" name="longitude" >
              <input type="hidden" id="locality" name="locality"/>
              <input type="hidden" id="administrative_area_level_1" name="administrative_area_level_1"/>
              <input type="hidden" id="country" name="country"/>
          </li>
       </div>
     </ul>
  <div class="container">  

<style>
@import "compass/css3";
*, *:before, *:after {box-sizing:  border-box !important;}
        
article {
 -moz-column-width: 13em;
 -webkit-column-width: 13em;
 -moz-column-gap: 1em;
 -webkit-column-gap: 1em; 
  
}

</style>

  <div class="event col-md-12" style="display:inline-block ;float:left">
<div class="subtitle search_result">
 <h1><?php if(count($eventsdata)>0){?>All events<?php } ?></h1>
</div>
</div>
<?php if(count($eventsdata)==0){?>
<div class="no_found">
<h2>&nbsp;No Event Found</h2> 
</div>
 <?php } ?>
 
<div id="event_ajax_search">  
<div id="event_ajax">   
<?php $i=0;foreach($eventsdata as $value){ ?>
      <div class="col-sm-6 col-xs-12 col-md-4 event_box ">
     <section>
      <div class="box_event">
        <div class="itemThumbnail"> 
 
            <a href="<?php echo base_url();?>ViewEvent/<?php echo $value->event_id;?>"> <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo $value->image ; ?>&h=300&w=350" alt="events"/></a>
     
       <div class="ev_time">
       <i class="fa fa-clock-o" aria-hidden="true"></i><?php echo date_format(date_create($value->start_date),'d M Y'); if($value->start_date!=$value->end_date){echo " to ".date_format(date_create($value->end_date),'d M Y');}?> |
       <?php echo date_format(date_create($value->start_time),'h:i A');?> 
        </div>     
       </div>
        <div class=" itemHeader text-left"><h3 class="itemTitle">
         <a href="<?php echo base_url();?>ViewEvent/<?php echo $value->event_id;?>"><?php if(strlen($value->title)>=26){echo substr($value->title,0,26);echo "[..]";}else echo $value->title;?></a></h3>
            <div class="post_details">
<span class="catItemDateCreated"><span> <i class="fa fa-map-marker" aria-hidden="true"></i></span><?php echo $value->place;?></span>
</div></div>
            <p><?php $re = $value->description;$tags=array("<p>","</p>","<br/>","<br>","<b>","</b>","<i>","</i>","<u>","</u>","<li>","</li>","<ul>","</ul>","<ol>","</ol>","<small>","</small>","<h1>","</h1>","<h2>","</h2>","<h3>","</h3>","<h5>","</h5>","<h6>","</h6>");
            $thedesc=str_replace($tags, "", $re);if(strlen($thedesc)>=60){echo substr($thedesc,0,60);echo "[..]";}else echo $thedesc;?></p>
      </div>
  
     </section>
         </div>
  <?php $i++;} ?> 
</div>
<?php
if($count >6){?>
    <div class="row">
             <div class=" col-md-12 text-center show_more_btn">
  <a href="javascript:void(0);" id="load_more"  class="show_more">Show More</a>

   <img id="loading-image" src="<?php echo base_url();?>assets/artists/images/ripple.gif" style="display:none">
    

 
               <!-- <input type="button" id="load_more"  value="show more">-->
               </div>  

   

              </div> 
<?php } ?>
</div>

</div>





   
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
  $( "#load_more" ).click(function() {
$('#loading-image').show();
  //alert(12);
 var divlength = $('.box_event').length;

     //alert(divlength);
      $.ajax({

                url:'<?php echo base_url();?>front/Events/load_more',
                 type:'POST',
                data:{limit:divlength},
                success:function(data)
                {
                  $('#event_ajax').append(data);
                        $('#loading-image').hide();
                     var divtotal = $('.box_event').length;
                    var totalevent='<?php echo $count ;?>';
                    if(totalevent<=divtotal)
                   
                        {

                            $('#load_more').hide();
                          

                        }
                 },
            error(error){console.log(error);}
                    });
});
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
  $('body').on( 'click',"#load_more_eventsearch_ajax",function() {
 var divlength = $('.box_event').length;
     var daterange=$('#reservationtime').val();
       var lati=$('#lati').val();
       var longi=$('#long').val();
      $.ajax({

                url:'<?php echo base_url();?>front/Events/load_more_searchevent',
                 type:'POST',
                data:{limit:divlength,datetime:daterange,lat:lati,lon:longi},
                success:function(data)
                {
                  $('#event_ajax').append(data);
                     var divtotal = $('.box_event').length;
                    var totalevent=$('#count_searchevent').val();
                    if(totalevent<=divtotal)
                        {
                            $('#load_more_eventsearch_ajax').hide();
                        }
                 },
            error(error){console.log(error);}
                    });
});
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
  $('.eventDatechange').change(function() {
   setTimeout(function(){ chcek(); }, 1000);
     
 });
    });
function chcek(){
    var daterange=$('#reservationtime').val();
       var lati=$('#lati').val();
       var longi=$('#long').val();
      //alert(daterange+','+lati+','+long);
        $.ajax({
          url:'<?php echo base_url();?>front/Events/onChangeEventvalue',
          type:'POST',
            data:{datetime:daterange,lat:lati,lon:longi},
                success:function(data)
                {$('#event_ajax_search').html(data);
            
                },
  error(error){console.log(error);}
                    });
    }
</script>


    
 
    
