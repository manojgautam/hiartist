<div class="slickslide"><img src="<?php echo base_url();?>assets/artists/images/footer-bg-design.jpg" /></div>
<div class="top-content">
<div class="inner-bg">
<div class="container">
<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3 signupform">
<?php if($this->session->flashdata('success') != ''){ ?>
<div class="alert alert-success" id="hide_flashdata"><?php echo $this->session->flashdata('success'); ?></div>
 <?php } ?> 
		<form role="form" action="" method="post" enctype="multipart/form-data">
			<h2>Sign Up <span><small>Fill in the form below to get instant access:</small><span></h2>
			<hr class="colorgraph">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
                        <input type="text" name="first_name" id="first_name" class="form-control input-lg" placeholder="First Name" tabindex="1" onkeyup="$('#first_name').css('border','1px solid #ccc');" value="<?php echo set_value('first_name');?>"><?php echo form_error('first_name'); ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="text" name="last_name" id="last_name" class="form-control input-lg" placeholder="Last Name" tabindex="2" onkeyup="$('#last_name').css('border','1px solid #ccc');" value="<?php echo set_value('last_name');?>"><?php echo form_error('last_name'); ?>
					</div>
				</div>
			</div>
			<div class="form-group">
				<input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email Address" tabindex="4" onkeyup="$('#email').css('border','1px solid #ccc');" value="<?php echo set_value('email');?>"><?php echo form_error('email'); ?>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" tabindex="5" onkeyup="$('#password').css('border','1px solid #ccc');" value="<?php echo set_value('password');?>"><?php echo form_error('password'); ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="form-group">
						<input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Confirm Password" tabindex="6" onkeyup="$('#password_confirmation').css('border','1px solid #ccc');" value="<?php echo set_value('password_confirmation');?>"><?php echo form_error('password_confirmation'); ?>
					</div>
				</div>
			</div>
<p id="pwdmatch" class="clienterror"></p>

<input type="hidden" id="locality" name="locality" value="<?php echo set_value('locality');?>"/>
<input type="hidden" id="administrative_area_level_1" name="administrative_area_level_1" value="<?php echo set_value('administrative_area_level_1');?>"/>
<input type="hidden" id="country" name="country" value="<?php echo set_value('country');?>"/>

						<div class="form-group">
				<input type="text" name="phone_no" id="phn_no" class="form-control input-lg" placeholder="Phone Number (not to be shown publicly)" tabindex="3" onkeyup="$('#phn_no').css('border','1px solid #ccc');" value="<?php echo set_value('phone_no');?>"><p id="error_phn"></p><?php echo form_error('phone_no'); ?>
			</div>
			<div class="form-group">
			<div class="row">
			<?php $i =0; foreach($result as $key=>$value){ $i++;?>
                        <div class="col-sm-12 col-sm-4">
                            <div class="radio">
                                <input type="radio" name="radio" id="radio<?php echo $i;?>" data-attr="<?php echo strtolower($key); ?>" value="<?php echo strtolower($key); ?>" <?php if(isset($_POST['radio']) && $_POST['radio'] == strtolower($key))  echo ' checked="checked"';?>>
                                <label for="radio<?php echo $i;?>">
                                    <?php echo $key;?>
                                </label>
                            </div>
                            </div>
			<?php } ?>
                            </div>
<p id="radioerror" class="clienterror"></p>
                            </div>
							<?php $j = 0;foreach($result as $key => $value){ $j++;?>
							 <div id="<?php echo strtolower($key); ?>" class="register_radios" style="display: none">
							 <div class="form-group">
							 <select name="<?php echo strtolower($key); ?>_select[]" data-placeholder="Select Categories" id="<?php echo strtolower($key); ?>_select" class="form-control register_radios_select select2 select2-hidden-accessible" multiple>
							 <?php foreach($value as $options){ ?>
                            <option value="<?php echo $options['sub_role']; ?>" <?php if(isset($_POST['radio'])){ if($_POST['artist_select']==$options['sub_role']){ echo "selected";} else if($_POST['directors_select']==$options['sub_role']){ echo "selected";}}?>><?php echo $options['sub_role']; ?></option>
							 <?php } ?>
							  </select>
							  <button id="b<?php echo $j; ?>" class="btn add-more" type="button" data-id ="<?php echo $j; ?>" data-attr="<?php echo strtolower($key); ?>" data-toggle="tooltip" data-placement="top" title="Add custom <?php echo $key; ?>">+</button>
							  <input style="display:none" autocomplete="off" class="register_fields_input form-control <?php echo strtolower($key); ?>_field" id="field<?php echo $j; ?>" name="<?php echo strtolower($key); ?>_field" type="text" placeholder="Enter <?php echo strtolower($key); ?>" data-items="8" value="<?php echo set_value(strtolower($key));?>"/>
<p class="customerr"  style="color:red;"></p>
								</div>
								</div>
							<?php } ?>
							
								
				<div class="form-group">
				<input type="file" class="filestyle" data-buttonName="btn-primary" name="userfile" id="image" value="<?php echo set_value('userfile');?>">
			   </div>
			    <div class="form-group">
				  <select name="gender" id="gender" class="form-control input-control req" onkeyup="$('#gender').css('border','1px solid #ccc');">
								<option value="">Select Gender</option>
								<option value="Male"<?php if(isset($_POST['gender'])){ if($_POST['gender']=='Male'){echo 'selected';}}?> >Male</option>
								<option value="Female" <?php if(isset($_POST['gender'])){ if($_POST['gender']=='Female'){echo 'selected';}}?>>Female</option>
								<option value="Other" <?php if(isset($_POST['gender'])){ if($_POST['gender']=='Other'){echo 'selected';}}?>>Other</option>
                   </select>
				</div>
				<div class="form-group">
				<span class="req-all-select" data-message="required">
					<select name="date_day" id="day_select" class="form-control input-control day_select">
					<option value="">DD</option>
					<?php for($i = 1; $i <= 31; $i++){?>
						<option value="<?php echo $i;?>" <?php if(isset($_POST['date_day'])){ if($_POST['date_day']==$i){echo 'selected';}}?>><?php echo $i;?></option>
					<?php } ?>
					</select>
					<select name="date_month" id="month_select" style="text-align:left;" class="form-control input-control month_select">
					<option value="">MM</option>
					<option value="January" <?php if(isset($_POST['date_month'])){ if($_POST['date_month']=='January'){echo 'selected';}}?>>January</option>
					<option value="February" <?php if(isset($_POST['date_month'])){ if($_POST['date_month']=='February'){echo 'selected';}}?>>February</option>
					<option value="March" <?php if(isset($_POST['date_month'])){ if($_POST['date_month']=='March'){echo 'selected';}}?>>March</option>
					<option value="April" <?php if(isset($_POST['date_month'])){ if($_POST['date_month']=='April'){echo 'selected';}}?>>April</option>
					<option value="May" <?php if(isset($_POST['date_month'])){ if($_POST['date_month']=='May'){echo 'selected';}}?>>May</option>
					<option value="June" <?php if(isset($_POST['date_month'])){ if($_POST['date_month']=='June'){echo 'selected';}}?>>June</option>
					<option value="July" <?php if(isset($_POST['date_month'])){ if($_POST['date_month']=='July'){echo 'selected';}}?>>July</option>
					<option value="August" <?php if(isset($_POST['date_month'])){ if($_POST['date_month']=='August'){echo 'selected';}}?>>August</option>
					<option value="September" <?php if(isset($_POST['date_month'])){ if($_POST['date_month']=='September'){echo 'selected';}}?>>September</option>
					<option value="October" <?php if(isset($_POST['date_month'])){ if($_POST['date_month']=='October'){echo 'selected';}}?>>October</option>
					<option value="November" <?php if(isset($_POST['date_month'])){ if($_POST['date_month']=='November'){echo 'selected';}}?>>November</option>
					<option value="December" <?php if(isset($_POST['date_month'])){ if($_POST['date_month']=='December'){echo 'selected';}}?>>December</option>
					</select>
					<select name="date_year" id="year_select" class="form-control input-control year_select">
					<option value="">YYYY</option>
					<?php for($i = date("Y"); $i >= 1930; $i--){?>
						<option value="<?php echo $i;?>" <?php if(isset($_POST['date_year'])){ if($_POST['date_year']==  $i){echo 'selected';}}?>><?php echo $i;?></option>
					<?php } ?>
					</select>
				</span>
				</div>
			   <div class="form-group">
			   <input type="text" name="location" class="form-control input-lg" id="searchTextField" value="<?php echo set_value('location');?>"  onkeyup="$('#searchTextField').css('border','1px solid #ccc');" ><p id="error_location" style="color:red;" class="clienterror"></p>
			   <input type="hidden" id="lati" class="form-control" name="latitude" value="">
                <input type="hidden" id="long" class="form-control" name="longitude" value="">
			   </div>
			<div class="row">
				<div class="col-xs-4 col-sm-3 col-md-3">
					<span class="button-checkbox">
						<button type="button" id="chkbtn" class="btn" data-color="info" tabindex="8">I Agree</button>
                        <input type="checkbox" name="t_and_c" id="t_and_c" class="hidden" value="1">
					</span>
				</div>
				<div class="col-xs-8 col-sm-9 col-md-9">
					 By clicking <strong class="label label-primary">Register</strong>, you agree to the <a href="#" data-toggle="modal" data-target="#t_and_c_m">Terms and Conditions</a> set out by this site, including our Cookie Use.
				</div>
			</div>
			<p id="chkerror" class="clienterror"></p>
			<hr class="colorgraph">
			<div class="row">
				<div class="col-xs-12 col-md-6 center-block register_btn">
				<input type="submit" id="advertiseus" name="submit" value="Register" class="btn btn-primary btn-block btn-lg" tabindex="7">
				</div>
				<div class="alredy_account">
				<p>already have an account? <a href="#" class="showlogin"> click here</a> to login </p>
				  </div>
			</div>
		</form>
	</div>
</div>
<!-- Modal -->
<div class="modal fade tnc" id="t_and_c_m" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
			</div>
			<div class="modal-body">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus ipsa porro delectus quidem dolorem ad.</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">I Agree</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>


</div>
</div>
    
