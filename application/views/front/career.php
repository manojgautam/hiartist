<div class="slickslide_img" style="position:relative">
    <img src="<?php echo base_url();?>assets/artists/images/footer-bg-design.jpg">
</div>

<div class="content">
          <div class="container" style="padding:0">
           <div class="top_section" style="position:relative" >
          </div>
        </div>
  
</div>
<section class="carreer_main contact padding-120">
      <div class="container" style="background:#fff">
        <h3>join our <span>team</span></h3>
       <?php
                if($this->session->flashdata('Career')) {  ?>
        <div class="container" id="flashdata_career">
        <div class="alert alert-success">
        <p><?php echo $this->session->flashdata('Career'); ?></p>
  </div>
        </div>
 <?php } ?>
      <div class="row">
           <div class="col-sm-8 col-xs-12">
                <div class="career_box">
                    <form method="POST" action="<?php echo base_url();?>careerPage">
                        <div class="col-sm-6 form-group">
                            <label>Name</label>
                            <input type="text" class="form-control" placeholder="Name" name="name" id="name" onfocus="$('#error_name').text('');">
                        <p id="error_name" class="" style="margin-top:16px;"></p></div>
                        <div class="col-sm-6 form-group">
                            <label>Email Address</label>
                            <input type="email" class="form-control" placeholder="Email Address" name="email" id="email" onfocus="$('#error_email').text('');">
                        <p id="error_email" class="" style="margin-top:16px;"></p></div>
                        <div class="col-sm-12 form-group">
                            <label>Phone Number</label>
                            <input type="text" class="form-control" placeholder="Phone Number" name="contact" id="contact" onfocus="$('#error_contact').text('');" maxlength="10">
                        </div><p id="error_contact" class=""></p>
                        <div class="col-sm-12 form-group">
                            <label>Description</label>
                            <textarea class="form-control" placeholder="Description" id="description" name="description" onfocus="$('#error_description').text('');"></textarea>
                        </div><p id="error_description" class=""></p>
                       <!-- <button type="button" class="career_submit">submit</button>-->
                            <input type="submit" name="submit" id="submit" value="submit" class="career_submit" onclick="return validateCareer();"><span id="error_meassage"></span>
                    </form>
                </div>
            </div>
           
          <div class="col-sm-4 col-xs-12 ">
           <div class="image_join">
               <img src="<?php echo base_url();?>assets/artists/images/joinus_img.png">
              </div>
          </div>
        </div><!-- row -->
      </div><!-- container -->
  <div style="margin-bottom:50px;"></div>
        </section>
