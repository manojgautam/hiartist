<!DOCTYPE html">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1" name="viewport">
<title>Partykarlo</title>
<style type="text/css">
.email-template{
	background: #fff;
	width: 600px;
	margin:auto;
}
.email-template-header{
	background:#000;
	padding: 25px 0;
	margin-top: 20px;
}

.email-template-footer{
	background:#000;
	padding: 25px 0;
}
ul.social-img {
    float: left;
    width: 49%;
}
.email-template-footer ul{
	list-style: none;
	padding: 0;
	margin:0;
	padding-left: 10px;
}
.email-template-footer ul li{
	display: inline-block;
	margin-left: 5px;
}
.copyright-text {
    width: 49%;
    float: right;
    text-align: center;
    font-size: 12px;
    color: #fff;
}
.clearfix{
	clear: both;
}
.email-template-body{
	color: #666;
	padding: 15px;
	margin: 0 1px;
	border-left: 1px solid #ccc;
	border-right: 1px solid #ccc;
}
.email-template-body p{
	margin-bottom: 20px;
	color: #666;
}
.email-template-header img {
    margin-left: 15px;
}
.email-template a{
	color: #d10001;
}
@media screen and (max-width:767px) {
.email-template{
	width: 320px;
}
ul.social-img, .copyright-text{
	float: none;
	width: 100%;
}
}
</style>
</head>
    
<body>
	<div class="email-template">
	 <div class="email-template-header">
	 	<a href="#"><img border="0" src="<?php echo base_url();?>uploads/logo.png"></a>
	 </div><!--email-template-header-->

	 <div class="email-template-body">
	   <h4>Hi <?php echo $name;?></h4>
	   <p><?php echo $message;?></p>
       <?php if(isset($link_text)){ ?>
       <p><a href="<?php echo $link;?>"><?php echo $link_text;?></a></p>
       <?php } ?>
       <p><?php echo $message1; ?></p>

       <div class="email-content-img"><img alt="Thanks" src="<?php echo base_url();?>uploads/thank.png"/></div>
	 </div><!--email-template-body-->

    <div class="email-template-footer">
    	<ul class="social-img">
    	<li><img src="<?php echo base_url();?>uploads/ic_fb.png" alt="facebook" width="28px"></li>
    	<li><img src="<?php echo base_url();?>uploads/ic_twitter.png" alt="twitter" width="28px"></li>
    	<li><img src="<?php echo base_url();?>uploads/ic_linkdin.png" alt="linkdin" width="28px"></li>
    	<li><img src="<?php echo base_url();?>uploads/ic_youtube.png" alt="youtube" width="28px"></li>
    	</ul>

    	<div class="copyright-text">
    		<p>&copy; 2016-17. www.hiartist.com | All rights reserved.</p>
    	</div>
    	<div class="clearfix"></div>
    </div><!--email-template-footer-->

	</div><!--email-template-->
</body>
</html>
