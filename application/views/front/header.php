<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<title><?php if(isset($fronttitle)){echo $fronttitle;} else { echo 'Hi Artist';} ?></title>
<?php if(isset($userprofilepage)){?>
  <meta property="og:url" content="<?php echo base_url();?>userprofile/<?php echo $userid;?>"/>
  <meta property="og:type" content="website"/>
  <meta property="og:title" content="<?php echo $userprofdetail[0]->first_name.' '.$userprofdetail[0]->last_name; ?>"/>
  <meta property="og:description" content="<?php echo $userprofdetail[0]->sub_role_id.' | '.$userprofdetail[0]->district ;?>"/>
  <meta property="og:image" content="<?php echo base_url();?>uploads/<?php if($userprofdetail[0]->profile_pic==''){echo 'user.jpeg';} else {echo $userprofdetail[0]->profile_pic;}?>"/>
<?php } ?>
<?php if(isset($profilepage)){?>
  <meta property="og:url" content="<?php echo base_url();?>profile/<?php echo $userid;?>"/>
  <meta property="og:type" content="website"/>
  <meta property="og:title" content="<?php echo $profdetail[0]->first_name.' '.$profdetail[0]->last_name; ?>"/>
  <meta property="og:description" content="<?php echo $profdetail[0]->sub_role_id.' | '.$profdetail[0]->district ;?>"/>
  <meta property="og:image" content="<?php echo base_url();?>uploads/<?php if($profdetail[0]->profile_pic==''){echo 'user.jpeg';} else {echo $profdetail[0]->profile_pic;}?>"/>
<?php } ?>
<!-- CSS -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/css/fontawesomemin.css">
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">--> 
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/css/cssfontfamily.css">
<!--<link href="https://fonts.googleapis.com/css?family=Ubuntu:300i,400,500,700" rel="stylesheet">-->  
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/css/animate.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/css/build.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/css/slick.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/css/slick-theme.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/css/menu.css">

  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/select2/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/artists/css/style.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
 <link rel="stylesheet" href="<?php echo base_url();?>assets/dist/css/star-rating.css" media="all" type="text/css"/>
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/css/custmscrollbar.css">
    <!--<link rel="stylesheet" href="http://cdn.jsdelivr.net/jquery.mcustomscrollbar/3.0.6/jquery.mCustomScrollbar.min.css" />-->

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Favicon and touch icons -->
 <link rel="shortcut icon" href="<?php echo base_url();?>assets/artists/ico/hiartist_favicon.ico"> 
</head>

<body>
    <div id="preloader">
<img id="loading-image" src="<?php echo base_url();?>assets/artists/images/ripple.gif">
    </div>
<nav class="toggle-menu">
        <ul class="list-unstyled main-menu">
          
          <!--Include your navigation here-->
            <li><a href="<?php echo base_url();?>HowItwork" class="footer-link"><i class="fa fa-briefcase" aria-hidden="true"></i> How it works</a></li>
				<li><a href="<?php echo base_url();?>partner" class="footer-link"><i class="fa fa-sun-o" aria-hidden="true"></i> Featured Partners</a></li>
                <li><a href="<?php echo base_url();?>career" class="footer-link"><i class="fa fa-user" aria-hidden="true"></i> Join Our Team</a></li>
                <li><a href="<?php echo base_url();?>advertise" class="footer-link"><i class="fa fa-random" aria-hidden="true"></i> Advertise With Us</a></li>
             <li><a href="<?php echo base_url();?>Welcome/allstate" class="footer-link"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Cultural Points</a></li>
             <li><a href="<?php echo base_url();?>allevents" class="footer-link"><i class="fa fa-calendar" aria-hidden="true"></i> Events</a></li>
             <li><a href="<?php echo base_url();?>sitemap" class="footer-link"><i class="fa fa-sitemap" aria-hidden="true"></i>
Sitemap</a></li>
             <li><a href="<?php echo base_url();?>contact" class="footer-link"><i class="fa fa-compress" aria-hidden="true"></i>
Contact</a></li>
             <li><a href="<?php echo base_url();?>blog" class="footer-link"><i class="fa fa-rss" aria-hidden="true"></i>
Blog</a></li>
             <li><a href="#" class="footer-link"><i class="fa fa-newspaper-o" aria-hidden="true"></i>
Press</a></li>
        </ul>
      </nav>
	  <header class="color-header-wrap fix-header">
	   <a id="nav-expander" class="nav-expander fixed show_img1">
            <img style="display:block" id="headerimg1" src="<?php echo base_url();?>assets/artists/images/toogle.png" alt="artist" >
           <img style="display:none" id="headerimg2" src="<?php echo base_url();?>assets/artists/images/cross_toggle.png" alt="artist" width="30px" height="30px" >
           
          </a>	
	
<nav class="navbar navbar-fixed-top navbar-bootsnipp animate" role="navigation" style="z-index: 9999999">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="animbrand">
        <a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/artists/images/logo.png" alt="logo"/></a>
        
      </div>
    </div>
	
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
	 <ul class=" nav navbar-nav navbar-right">
                <li class="<?php if(isset($homepagetab) || isset($searcartisthtab)){echo 'active';}?>">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Artists<b class="caret"></b></a>
				<ul class="dropdown-menu multi-level submenu">
		<p class="roll-title">ALL CATEGORIES</p>
            <?php foreach($artist as $item)
            {
          $role_name=$item->sub_role;
            ?>
  <li><a href="<?php echo base_url();?>search/Artists/<?php echo $item->sub_role;?>/all"><?php echo $item->sub_role;?></a></li>  
             <?php
               }
            ?>
			 </ul>
				</li>
<li class="<?php if(isset($searchdirectortab)){echo 'active';}?>"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Directors<b class="caret"></b></a>
				<ul class="dropdown-menu multi-level submenu">
		<p class="roll-title">ALL CATEGORIES</p>
              <?php foreach($director as $value)
            {
            ?>
         <li><a href="<?php echo base_url();?>search/Director/<?php echo $value->sub_role;?>/all"><?php echo $value->sub_role;?></a></li>  
             <?php
               }
            ?>
			 </ul>
				</li>
                <li  class="<?php if(isset($grouptab)){echo 'active';}?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Groups<b class="caret"></b></a>
                    <ul class="dropdown-menu multi-level submenu">
		<p class="roll-title">ALL CATEGORIES</p>
            <?php foreach($groups as $value)
            {
            ?>
         <li style="overflow:hidden;"><a href="<?php echo base_url();?>viewgroup/<?php echo $value->group_id;?>"><?php echo $value->name;?></a></li>  
             <?php
               }
            ?> <a class="view_all" href="<?php echo base_url();?>allgroups">View All</a> 
			 </ul>
                </li>
                 <li class="<?php if(isset($shoptab)){echo 'active';}?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">State<b class="caret"></b></a>
                    <ul class="dropdown-menu state_dropdown-menu">
                        <?php 
                            foreach($state as $cities=>$item)
                        { ?>
                        <li class="dropdown-submenu">
                            
						<a href="#"><?php echo $item->name;?></a>
						<ul class="dropdown-menu">
                            <?php $l=1;foreach($item->cities as $city){ if($city->count_shop!=0){if($l<8){ ?>
                                <li><a href="<?php echo base_url();?>shop/<?php echo $city->id;?>"><?php echo $city->name;?></a></li>
                            <?php } else{break;} $l++;}} ?>
                       
                            </ul>
						</li>
                        <?php } ?>
               <a class="view_all" href="<?php echo base_url();?>allstate"><?php if(count($state)){?> view More <?php }?></a> 
               <?php if(!count($state)){ echo 'No Shops';} ?>
                        
                    </ul>
                </li>
				<li><?php  echo ($this->session->userdata('usersession') == '' ? '<a href="javascript:void(0);" class="showlogin">Login</a>' :'<a href="'.base_url().'profile/'.$this->session->userdata('usersession')['user_id'].'">Dashboard</a>'); ?></li>
          <li class="<?php if(isset($registertab)){echo 'active';}?>">
		  <?php  echo ($this->session->userdata('usersession') == '' ? '<a href="'.base_url().'registeration" class="">Sign Up</a>' : '<a href="'.base_url().'front/Login/logout">Logout</a>'); ?>
		  </li>
            </ul>
    </div>
  </div>
</nav>
</header>

<!--<div class="slickslide">
  <div><img src="<?php //echo base_url();?>assets/artists/images/new-slider-5.jpg" /></div>
    <div><img src="<?php //echo base_url();?>assets/artists/images/slider_bg.jpg" /></div>
  <div><img src="<?php //echo base_url();?>assets/artists/images/new-slider-2.jpg" /></div>
</div>-->

<div class="modal fade login" id="login" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="myLargeModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
			<h2>login with your <span class="logo-hi">hi</span><span class="logo-artist">artist</span> account</h2>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
					<hr class="colorgraph">
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8" style="border-right: 1px dotted #C2C2C2;padding-right: 30px;">
             <p class="alert alert-danger" id="notmatch" style="display:none;"></p>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="Login">
                                <div class="form-horizontal">
                                <div class="form-group">
                                    <label for="email" class="col-sm-3 control-label required-field">
                                        Email</label>
                                    <div class="col-sm-9">
                                        <input type="text" name="email" class="form-control" id="email1" placeholder="Email" />
<p class="erroremail" style="color:red"></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1" class="col-sm-3 control-label required-field">
                                        Password</label>
                                    <div class="col-sm-9">
                                        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" />
<p class="errorpassword" style="color:red"></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">
                                    </div>
                                    <div class="col-sm-10">
                                        <button type="submit" id="login_ajax" class="btn btn-primary btn-md">
                                            Submit</button>
                                        <a href="javascript:;" id="fgt" class="removeall">Forgot your password?</a>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div id="OR" class="hidden-xs">
                            OR</div>
                    </div>
<input type="hidden" id="hiddenfb" value="<?php if(isset($fbfail)){echo 'yes';} else {echo 'no';}?>" />
                    <div class="col-md-4">
                        <div class="row text-center sign-with">
                            <div class="col-md-12">
                                <h3>
                                    Login with</h3>
                            </div>
                            <div class="col-md-12">
                                <div class="btn-group btn-group-justified">
                                    <a href="<?php if(isset($fbfail)){echo '';}else{echo $login_url;}?>" class="btn btn-primary" id="<?php if(isset($fbfail)){echo 'createfbsession';}else{echo '';}?>">Facebook</a> <a href="<?php echo $googlelogin;?>" class="btn btn-danger">Google</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div aria-labelledby="myLargeModalLabel" data-backdrop="static" role="dialog" tabindex="-1" id="forgot" class="modal fade bs-example-modal-md-forget"> <div class="modal-dialog modal-md"> 
<div class="modal-content"> 

<div class="modal-header">
			<h2>Forgot Password</h2>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
					<hr class="colorgraph">
            </div>


			<div class="modal-body">
			    
			<div class="form-group samec">
			<p>When you fill in your registered email address, you will be sent instructions on how to reset your password.</p>
           <p id="err-forgot" style="color:red"></p>
           <p id="success_message" style="color:green"></p> 
			<input type="email" id="forgetemail" name="forgetemail" value="" size="40" class="form-control input" aria-required="true" aria-invalid="false" placeholder="Email" onfocus='$(".emailf").text("");'>
			<p class="emailf" style="color:red;"></p> 
			</div>                

          <div class="form-group samec">
			<input type="submit" value="Get Password" class="btn btn-success btn-md" id="forget_submit" name="forget_submit">
			<p class="already">already have an account?<a class="showlogin removeall">Back to Login</a></p>
			</div>
                
			</div>
	  </div> 
	  </div> 
	  </div> 
	  

	  

