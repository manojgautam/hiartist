
      <style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 100%;
      margin: auto;
      height: 340px;
  }
.carousel.slide {
  width: 50%;
}
  </style>
<div class="slickslide_img" style="position:relative"><img src="<?php echo base_url();?>assets/artists/images/footer-bg-design.jpg" />


</div>
<section class="viewshop_main">
    <div class="container">
        <?php if(count($shopview)==0){ ?><p class="error_notfound">No shop found</p><?php } else {?>
       
        <div class="viewshop_cover">
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12">
                   <!-- <?php if($shopview['image']){?>
                    <div class="viewshop_bx" style="background-image:url(<?php echo base_url();?>uploads/<?php echo $shopview['image'];?>">
                        </div><?php } else { ?>
                    <div class="viewshop_bx" style="background-image:url(<?php echo base_url();?>uploads/<?php echo $shopview['image'];?>">
                    </div>
                    <?php } ?>-->
                    <div class="container">
 
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <?php for($i=1;$i<=$shop_images_count;$i++){?>
      <li data-target="#myCarousel" data-slide-to="<?php echo $i;?>"></li>
     <?php } ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        
      <div class="item active">
        <img style="height:360px" src="<?php echo base_url();?>uploads/<?php if($shopview['image']!=''){echo $shopview['image'];} else { echo 'noimage.png';}?>" alt="image" >
      </div>
         <?php $j=1; foreach($shop_images as $val){?>
      <div class="item ">
        <img src="<?php echo base_url();?>uploads/<?php echo $val->image_name;?>" style="height:360px" alt="image" >
      </div>
        <?php $j++;} ?>
    </div>

    <!-- Left and right controls -->
<?php if(count($shop_images)){ ?>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
<?php } ?>
  </div>
</div>
                </div>
                  <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="viewshop_bx">
                        <h3><?php echo $shopview['shop_name'];?></h3>
                        <ul>
                            <li><i class="fa fa-map-marker" aria-hidden="true"></i>  <?php if($disrtictname[0]->name==$statename[0]->name){?>
                           <?php echo $shopview['address'].' ,'.$disrtictname[0]->name;?>
                              <?php } else{ ?>
                           <?php echo $shopview['address'].' ,'.$disrtictname[0]->name.' ,'.$statename[0]->name;?>
                              <?php } ?></li>
                           <li><i class="fa fa-globe" aria-hidden="true"></i>
<?php if($shopview['website']!=''){ ?>
<a href="<?php echo 'http://'.$shopview['website'];?>" target="_blank"><?php echo $shopview['website'];?></a>
<?php } else {echo "Not Available" ;} ?>
 </li>
<?php if($shopview['contact']!=''){ ?>
<li><i class="fa fa-phone-square" aria-hidden="true"></i><?php echo $shopview['contact'];?></li>
<?php } ?>
<?php if($shopview['email']!=''){ ?>
<li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:<?php echo $shopview['email']?>"><?php echo $shopview['email']?></a></li>
<?php } ?>
                        </ul>
                        <div class="shop_owner">
                            <h4>Shop Owner</h4>
                            <p><?php echo $shopview['owner'];?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="viewshop_dscrpt">
            <h4>description</h4>
            <p><?php echo $shopview['shop_desc'];?></p>
            <a href="<?php echo base_url();?>shop/<?php echo $shopview['district_id'];?>">all stores</a>
        </div>
        <?php } ?>
    </div>
</section>
