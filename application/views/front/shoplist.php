<head><style>
    .artist-ribbon{height:50% !important;}</style></head>
<div class="slickslide_img" style="position:relative"><img src="<?php echo base_url();?>assets/artists/images/footer-bg-design.jpg" />

</div>
 <ul class="nav navbar-nav shoppage_sort">
     <div class="container">
    <li class="sort">Sort by</li>
     <li class="dropdown">     
        <select id="shopstate" class="statedist">   
        <option value="">Select State</option>   
        <?php foreach($states as $val){ ?> 
        <option value="<?php echo $val->id;?>" <?php if(count($stateid)){if($stateid[0]->state_id==$val->id){echo "selected";}}?>><?php echo $val->name;?> (<?php echo $val->citycount;?>) </option>
        <?php } ?>
        </select>
        <select id="shopdistrict" class="statedist">   
        <option value="">Select District</option> 
<?php foreach($alldistrcts as $val){ ?> 
        <option value="<?php echo $val->district_id;?>" <?php if($id==$val->district_id){echo "selected";}?>><?php echo $val->name;?></option>
        <?php } ?>  
        </select>
     </li>
     </div>
  </ul>
<section class="shoplist_main">
    <div class="container filterajax">
  <h2><?php if(count($shop)!=0){?> All Cultural Points <?php } ?></h2>
        <?php if(count($shop)==0){?>
         <div class="row">
        <div class="col-md-12 no_shop_fonud">
       <h2>&nbsp;No Cultural Point Found</h2><p>(Please Select any State or District)</p>
        </div>
</div>
        <?php } ?>
        <div class="shoplist_cover" >
            <div class="row" id="shop_ajax">
                <?php $i=0; foreach($shop as $item){ ?>
                <div class="col-md-4 col-sm-6 col-xs-12 ">
                    <div class="shoplist_bx ajax_data">
                        <?php if($item->image=='')
                            { ?><div class="shoplist_bx_bg">
                        <a href="<?php echo base_url();?>Viewshop/<?php echo $item->shop_id;?>"><img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo 'noimage.png';?>&h=320&w=375" alt="shops">
                           
                        </a>
                         <!--start shoplist stripe-->
                            <div class="shoplist_bx_bg_stripe">
                                <ul>
                                    <li><?php echo $item->address.', '.$disrtict[0]->name;?></li>
                                </ul>
                            </div>
                        </div>
                        <?php
                            } else { ?>
                        <div class="shoplist_bx_bg">
                            <a href="<?php echo base_url();?>Viewshop/<?php echo $item->shop_id;?>"><img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo $item->image; ?>&h=320&w=375" alt="artist">
                            
                            
                            </a>
                               <div class="shoplist_bx_bg_stripe">
                                <ul>
                                    <li><?php echo $item->address.', '.$disrtict[0]->name;?></li>
                                </ul>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="shoplist_bx_detail">
                            <a href="<?php echo base_url();?>Viewshop/<?php echo $item->shop_id;?>"><?php if(strlen($item->shop_name)>=21){echo substr($item->shop_name,0,21);echo "[..]";}else echo $item->shop_name;?></a>
                        </div>
                         <div class="shoplist_bx_description">
                             <p><?php $re = $item->shop_desc;$tags=array("<p>","</p>","<br/>","<br>","<b>","</b>","<i>","</i>","<u>","</u>","<li>","</li>","<ul>","</ul>","<ol>","</ol>","<small>","</small>","<h1>","</h1>","<h2>","</h2>","<h3>","</h3>","<h5>","</h5>","<h6>","</h6>");
            $thedesc=str_replace($tags, "", $re);if(strlen($thedesc)>=60){echo substr($thedesc,0,60);echo "[..]";}else echo $thedesc;?></p>
                        </div>
                    </div>
                </div>
                <?php $i++; } ?>
            </div>
        </div>
           
      <?php if($count > 6)
{?>
        <div class="row">
             <div class="col-md-offset-4 col-md-4 text-center">
                 <a href="javascript:void(0);" id="load_more" class="show_more">Show More</a>
                 <img id="loading-image" src="<?php echo base_url();?>assets/artists/images/ripple.gif" style="display:none">
                 
                </div>  
              </div>
        <?php } ?>
        
    </div>
</section>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
  $( "#load_more" ).click(function() {
         $('#loading-image').show();
     // alert(12);
    var divlength =$('.ajax_data').length;
      var district_id="<?php echo $id;?>";
           $.ajax({
                url:'<?php echo base_url();?>front/Shop/load_more',
                 type:'POST',
                data:{limit:divlength,id:district_id},
                success:function(data)
                {
                     $('#loading-image').hide();
                    $('#shop_ajax').append(data);
                      var divtotal = $('.ajax_data').length;
                    var totalshop='<?php echo $count ;?>';
                    if(divtotal==totalshop)
                        {
                            $('#load_more').hide();
                        }
                 },
            error(error){console.log(error);}
                    });
});
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
  $('body').on('click',"#load_more_ajax_shops" ,function() {
    var divlength =$('.ajax_data').length;
      var stateid=$('#shopstate').val();
      var shopid=$('#shopdistrict').val();
           $.ajax({
                url:'<?php echo base_url();?>front/Shop/load_more_aftershopfilter',
                 type:'POST',
                data:{limit:divlength,statid:stateid,shpid:shopid},
                success:function(data)
                {
                    $('#shop_ajax').append(data);
                      var divtotal = $('.ajax_data').length;
                    var totalshop=$('#totalshoplist').val();
                    if(divtotal==totalshop)
                        {
                            $('#load_more_ajax_shops').hide();
                        }
                 },
            error(error){console.log(error);}
                    });
});
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
  $( "#shopstate" ).change(function() {
  
    var stateid=$('#shopstate').val();
 

           $.ajax({
                url:'<?php echo base_url();?>front/Shop/onchangeval',
                 type:'POST',
                data:{sid:stateid},
                success:function(data)
                {var res=data.split("##@@##",2);
                    $('.filterajax').html(res[1]);
                    $('#shopdistrict').html(res[0]);
                  
                  
                 },
            error(error){console.log(error);}
                    });
});
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
  $( "#shopdistrict" ).change(function() {
    var shopid=$('#shopdistrict').val();
if(shopid!=''){
                $.ajax({
                url:'<?php echo base_url();?>front/Shop/onchangevalshop',
                 type:'POST',
                data:{sid:shopid},
                success:function(data)
                {$('.filterajax').html(data);},
                error(error){console.log(error);}
                 });
              }
else{    
                var stateid=$('#shopstate').val();
                $.ajax({
                url:'<?php echo base_url();?>front/Shop/onchangeval',
                 type:'POST',
                data:{sid:stateid},
                success:function(data)
                {var res=data.split("##@@##",2);
                    $('.filterajax').html(res[1]);
                    $('#shopdistrict').html(res[0]);},
                    error(error){console.log(error);}
                    });



    }
});
    });
</script>

 

