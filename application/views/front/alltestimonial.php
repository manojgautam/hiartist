<div class="slickslide_img" style="position:relative">
    <img src="http://agiledevelopers.in/hiartist/assets/artists/images/footer-bg-design.jpg">
</div>
<section class="testimonials_main">
    <div class="container">
        <h3>Testimonials</h3>
<?php if(!count($testi)){?>
<div class="no_found">
<h2>&nbsp;No Testimonial Found</h2> 
</div>
<?php } ?>
        <div class="testimonail_box_cvr">
<?php foreach($testi as $val){?>
            <div class="testimonial_box">
                <p><?php echo $val->testimonialText;?></p>
                <a style="color:#666" href="<?php echo base_url();?>userprofile/<?php echo $val->testimonialAddedBy;?>"><span>- <?php echo $val->first_name.' '.$val->last_name;?> | <?php echo $val->sub_role_id;?></span></a>
                <div class="testimonial_pic">
                    <a href="<?php echo base_url();?>userprofile/<?php echo $val->testimonialAddedBy;?>"><img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php if($val->profile_pic==''){echo 'user.jpeg';}else{echo $val->profile_pic;}?>&h=280&w=275" alt="actor"></a>
                </div>
            </div>
       <?php } ?>
        </div>
    </div>
    <?php if($count>6){ ?>
<div class="row">
             <div class="col-md-offset-4 col-md-4 text-center">
                <a class="show_more" id="load_more" href="javascript:void(0);">Show More</a>
                <img id="loading-image" src="<?php echo base_url();?>assets/artists/images/ripple.gif" style="display:none">
                </div>  
</div>
<?php } ?>
</section>
<div style="padding-bottom:40px;"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
  $( "#load_more" ).click(function() {
$('#loading-image').show();
 var divlength = $('.testimonial_box').length;

      $.ajax({

                url:'<?php echo base_url();?>front/AllTestimonials/load_more',
                 type:'POST',
                data:{limit:divlength},
                success:function(data)
                {
                  $('.testimonail_box_cvr').append(data);
                       $('#loading-image').hide();
                     var divtotal = $('.testimonial_box').length;
                    var totalevent='<?php echo $count ;?>';
                    if(totalevent<=divtotal)
                        {

                            $('#load_more').hide();
                          

                        }
                 },
            error(error){console.log(error);}
                    });
});
    });
</script>
