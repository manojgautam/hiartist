<style>
  .carousel-inner > .item > img,
  .carousel-inner > .item > a > img {
      width: 100%;
      margin: auto;
      height: 340px;
  }
  .carousel.slide {
  width: 50%;
}
  </style>
<div class="slickslide_img" style="position:relative">
    <img src="<?php echo base_url();?>assets/artists/images/footer-bg-design.jpg">
</div>
<section class="viewgroup_page">
    <div class="container">
         <?php if(count($group)==0){ ?><p class="error_notfound">No Group found</p><?php } else {?>
        <div class="row">
            <div class="col-sm-7 col-xs-12">
                <div class="viewgroup_box">
                    <!--<img src="http://agiledevelopers.in/hiartist/assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo $group['image'];?>&amp;h=357&amp;w=283" alt="image">-->



  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <?php for($i=1;$i<=$group_image_count;$i++){?>
      <li data-target="#myCarousel" data-slide-to="<?php echo $i;?>"></li>
     <?php } ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        
      <div class="item active">
        <img style="height:300px" src="<?php echo base_url();?>uploads/<?php if($group['image']!=''){echo $group['image'];} else { echo 'noimage.png';}?>" alt="image" >
      </div>
         <?php $j=1; foreach($group_images as $val){?>
      <div class="item">
        <img src="<?php echo base_url();?>uploads/<?php echo $val->image_name;?>" style="height:300px" alt="image" >
      </div>
        <?php $j++;} ?>
    </div>

    <!-- Left and right controls -->
<?php if(count($group_images)){ ?>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
<?php } ?>
  </div>

 </div>
            </div>
               <div class="col-sm-5 col-xs-12">
                <div class="viewgroup_box viewgroup_bx2">
                    <h4><?php echo $group['name'];?></h4>
      <?php $members = $group['group_members'];
       $members_array = explode(",", $members);
          $result = count($members_array);?>
                    <ul>
                 
                        <li><?php if($group['website']){ ?><a target='_blank' href="http://<?php echo $group['website'];?>"><?php echo $group['website'];?></a><?php } else { ?><?php echo "Not Available";}?></li>
                        <li><?php if($group['group_members']) echo  $group['group_members'];else echo "Not Available";?></li>        
                     </ul>
                    <div class="blog_link">
                        <h5>blog link</h5>
                  <?php if($group['blog_link']){?><a href="<?php echo 'http://'.$group['blog_link'];?>" target="_blank"><?php } ?><?php if($group['blog_link']){ echo "Click here to visit blog "; } else{echo "Not Available";} ?><?php if($group['blog_link']){?></a><?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="viewgroup_descrp">
            <h3>description</h3>
            <p><?php echo $group['description'];?></p>
        </div>
        <?php } ?>
    </div>
</section>
<div style="padding-bottom:50px;"></div>
