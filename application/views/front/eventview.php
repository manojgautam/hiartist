<div class="slickslide_img" style="position:relative"><img src="<?php echo base_url();?>assets/artists/images/footer-bg-design.jpg" />
    <!--<div class="registers_btn">
    <a href="/register" class="banner-button-style">register as artist</a>
     </div>-->
</div>

<!--view event section-->
<section class="viewevent_main">
    <div class="container">
         <?php if(count($data)==0){ ?><p class="error_notfound">No Event found</p><?php } else {?>
        <div class="viewevent_heading">
            <h3>event details</h3>
        </div>
        <div class="view_event_bxcvr">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="view_event_bx" style="padding:0px;">
                   <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php if($data['image']==''){echo 'noimage.png&h=320&w=350';} else {echo $data['image'].'&h=410&w=850';}?>" alt="events"/>
                </div>
              
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
                <div class="view_event_bx">
                    <h4><?php echo $data['title'];?></h4>
                    <div class="event_details_bx">
                        <ul>
                            
                            <li><i class="fa fa-calendar" aria-hidden="true"></i> <?php if($data['start_date']==$data['end_date']){echo date_format(date_create($data['start_date']),'d M, Y');} else {echo date_format(date_create($data['start_date']),'d M, Y')." to ".date_format(date_create($data['end_date']),'d M, Y');}?></li>
                            <li><i class="fa fa-clock-o" aria-hidden="true"></i> <?php if($data['start_time']){echo date_format(date_create($data['start_time']),'h:i A');} else{ echo "Not Available";}?></li>
                            <li><i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp; <?php echo $data['place'];?></li>
                            <li><i class="fa fa-bookmark-o" aria-hidden="true"></i>&nbsp;&nbsp;<?php if($data['category']==''){ echo "Not Available";} else {echo $data['category'];} ?></li>
                            <li><i class="fa fa-tags" aria-hidden="true"></i> <?php if($data['cost'] && $data['currency'] ){echo $data['cost'] .' '. $data['currency'];} else { echo "Not Available";}?></li>
                          <!--  <li><i class="fa fa-user" aria-hidden="true"></i><?php echo $data['category'];?></li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <div class="dreamday_descripton">
            <h4><?php echo $data['title'];?></h4>
            <p><?php echo $data['description'];?></p>
            <?php if($data['contact_no']){?>
             <p><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $data['contact_no'];?></p><?php } 
            if($data['email']){?>
             <p><i class="fa fa-envelope" aria-hidden="true"></i> <?php echo $data['email'];?></p><?php } ?>
            <a href="<?php echo base_url();?>allevents" class="dreamday_event_btn">all events</a>
        </div>
        <div class="allevent_venue">
            <div class="allevent_venue_bx">
                <h4>venue</h4>
                <address><?php echo $data['venue'].','.$data['place'];?></address>
            </div>
            <div class="allevent_venue_bx">
               <div id="map" style="width:100%;height:175px;"></div>
<div style="text-align:center"> <a  target="_blank" href="https://www.google.com/maps/dir//'<?php echo $data['latitude'];?>,<?php echo $data['longitude'];?>'/@<?php echo $data['latitude'];?>,<?php echo $data['longitude'];?>,17z/"> </a></div>
            </div>
        </div>
         <?php } ?>
    </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script>
//jQuery(document).ready(function(){
var i=0;
jQuery(function(){
//jQuery('.mapid').click(function(){

if(i==0){
i++;

  // Define the latitude and longitude positions
  var latitude = parseFloat("<?php echo $data['latitude'];?>"); // Latitude get from above variable
  var longitude = parseFloat("<?php echo $data['longitude'];?>"); // Longitude from same
  var latlngPos = new google.maps.LatLng(latitude, longitude);
  
  // Set up options for the Google map
  var myOptions = {
    zoom: 14,
    center: latlngPos,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    zoomControlOptions: true,
    zoomControlOptions: {
      style: google.maps.ZoomControlStyle.LARGE
    }
  };
  
  // Define the map
  map = new google.maps.Map(document.getElementById("map"), myOptions);
  
  // Add the marker
         var contentString = '<div id="content" style="background:yellow">'+
      '<div id="siteNotice">'+
      '</div>'+
      '<h3 id="firstHeading" class="firstHeading"><?php echo $data['title'];?></h3>'+
      '<div id="bodyContent">'+
      '<p><?php echo $data['venue'].','.$data['place'];?></p>'+
      '</div>'+
      '</div>';

  var infowindow = new google.maps.InfoWindow({
    content: contentString
  });
  var marker = new google.maps.Marker({
    position: latlngPos,
    map: map,
    title: "Your Location"
  });
        marker.addListener('mouseover', function() {
    infowindow.open(map, marker);
  });
  
}
});
</script>

