<div id="event_ajax">
<?php if(count($eventsdata)==0){?>
<div class="row" style="padding-left:20px;text-align:left">
<h2>&nbsp;No Event Found</h2> 
</div>
 <?php } ?>   
<?php $i=0;foreach($eventsdata as $value){ ?>
      <div class="col-sm-6 col-xs-12 col-md-4 event_box ">
     <section>
      <div class="box_event">
        <div class="itemThumbnail"> 
 
            <a href="<?php echo base_url();?>ViewEvent/<?php echo $value->event_id;?>"> <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php if($value->image==''){echo 'noimage.png';} else {echo $value->image;}?>&h=300&w=350" alt="events"/></a>
     
       <div class="ev_time">
       <i class="fa fa-clock-o" aria-hidden="true"></i><?php echo date_format(date_create($value->start_date),'d M Y');if($value->start_date!=$value->end_date){echo " to ".date_format(date_create($value->end_date),'d M Y');}?> |
       <?php echo date_format(date_create($value->start_time),'h:i A');?> 
        </div>     
       </div>
        <div class=" itemHeader text-left"><h3 class="itemTitle">
         <a href="<?php echo base_url();?>ViewEvent/<?php echo $value->event_id;?>"><?php if(strlen($value->title)>=21){echo substr($value->title,0,21);echo "[..]";}else echo $value->title;?></a></h3>
            <div class="post_details">
<span class="catItemDateCreated"><span> <i class="fa fa-map-marker" aria-hidden="true"></i></span><?php echo $value->place;?></span>
</div></div>
            <p><?php $re = $value->description;$tags=array("<p>","</p>","<br/>","<br>","<b>","</b>","<i>","</i>","<u>","</u>","<li>","</li>","<ul>","</ul>","<ol>","</ol>","<small>","</small>","<h1>","</h1>","<h2>","</h2>","<h3>","</h3>","<h5>","</h5>","<h6>","</h6>");
            $thedesc=str_replace($tags, "", $re);if(strlen($thedesc)>=60){echo substr($thedesc,0,60);echo "[..]";}else echo $thedesc;?></p>
      </div>
  
     </section>
         </div>
  <?php $i++;} ?> 
</div>
<input type="hidden" id="count_searchevent" value="<?php echo count($eventsdatacount);?>"/>
<?php
if(count($eventsdatacount) >6){?>
    <div class="row">
             <div class=" col-md-12 text-center show_more_btn">
  <a href="javascript:void(0);" id="load_more_eventsearch_ajax"  class="show_more">Show More</a>
               <!-- <input type="button" id="load_more"  value="show more">-->
               </div>  
              </div> 
<?php } ?>
