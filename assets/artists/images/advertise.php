<div class="slickslide_img" style="position:relative">
    <img src="http://agiledevelopers.in/hiartist/assets/artists/images/footer-bg-design.jpg">
</div>
<section class="advertise_us">
    <div class="container">
        <h3>advertising <span>with us</span></h3>
    <div class="talent_track_cvr">
        <h5>Talentrack's unique platform offers advertisers an exceptional opportunity to connect to our creative user-base through various integrated advertising solutions including...</h5>
        <div class="talent_track_bx">
            <img src="../hiartist/assets/artists/images/actor.png" class="img-responsive" alt="actor">
            <h5>Actor</h5>
        </div>
        <div class="talent_track_bx">
            <i class="fa fa-calendar" aria-hidden="true"></i>
            <h5>Category Sponsorship</h5>
        </div>
        <div class="talent_track_bx">
            <i class="fa fa-mobile" aria-hidden="true"></i>
            <h5>App Advertising</h5>
        </div>
        <div class="talent_track_bx">
            <i class="fa fa-book" aria-hidden="true"></i>
            <h5>E Marketing Solutions</h5>
        </div>
    </div>
    <!--<div class="hiring_platform">
        <h5>Talentrack is India's fastest-growing talent-aggregator and hiring-platform with...</h5>
        <div class="hiring_platform_bx">
            <span>Over</span>
            <strong>Registered Artist</strong>
            <h2>50,000</h2>
        </div>
        <div class="hiring_platform_bx">
            <span>Over</span>
            <strong>Artist Recruiters</strong>
            <h2>80,000</h2>
        </div>
        <div class="hiring_platform_bx">
            <span>Over</span>
            <strong>Talent Added</strong>
            <h2>120,000</h2>
        </div>
        <div class="hiring_platform_bx">
            <span>Over</span>
            <strong>Job Reviewed</strong>
            <h2>130,000</h2>
        </div>
        <div class="hiring_platform_bx">
            <span>Over</span>
            <strong>Facebook Followers</strong>
            <h2>1,50,000</h2>
        </div>
        <div class="hiring_platform_bx">
            <span>Over</span>
            <strong>Recruiters Added Daily</strong>
            <h2>50</h2>
        </div>
    </div>-->
     <div class="advertise_box_cvr">
       <div class="row">
           <div class="col-md-8 col-sm-6 col-xs-12">
               <div class="advertise_box">
                   <form>
                       <div class="form-group">
                           <label>name</label>
                           <input type="text"  class="form-control" placeholder="Organisation Name">
                       </div>
                       <div class="form-group">
                           <label>Email</label>
                           <input type="email" class="form-control" placeholder="Email">
                       </div>
                       <div class="form-group">
                           <label>Contact Number</label>
                           <input type="text"  class="form-control" placeholder="Contact Number">
                       </div>
                       <div class="form-group">
                           <label>Description</label>
                           <textarea class="form-control"  placeholder="Description"></textarea>
                       </div>
                       <button type="button" name="submit" class="advertise_btn">submit</button>
                   </form>
               </div>
           </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
               <div class="advertise_box advertise_contactxbx">
                   <h3>get in touch <span>with us</span></h3>
                   <ul>
                       <li>
                           <a href="tel:+971564541455">+971 56 454 1455</a>
                       </li>
                        <li>
                           <a href="mailto:info@gmail.com">info@gmail.com</a>
                       </li>
                        <li>
                           <a href="http://agiledevelopers.in/hiartist/advertise">www.website.com</a>
                       </li>
                   </ul>
               </div>
           </div>
        </div>
        </div>
    </div>
</section>