// gallery fancybox
$(document).ready(function(){
    //FANCYBOX
    //https://github.com/fancyapps/fancyBox



    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });

});
   

  //Navigation Menu Slider
    $(document).ready(function(){												
       
     
        $('#nav-expander').on('click',function(e){
      		e.preventDefault();
      		$('body').toggleClass('nav-expanded');
      	});
      	$('#nav-close').on('click',function(e){
      		e.preventDefault();
      		$('body').removeClass('nav-expanded');
      	});
      	
      	
      	// Initialize navgoco with default options
        $(".main-menu").navgoco({
            caret: '<span class="caret"></span>',
            accordion: false,
            openClass: 'open',
            save: true,
            cookie: {
                name: 'navgoco',
                expires: false,
                path: '/'
            },
            slide: {
                duration: 300,
                easing: 'swing'
            }
        });
  
        	
      });

	  
//home page slider  
	  $(document).ready(function(){
  $('.slickslide , .testi-slide').slick({
  dots: true,
  speed: 500
  });
});


 $('.slick-list').slick({
        dots: false,
        infinite: false,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 5,
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 5,
                infinite: true,
                dots: false
            }
        }, {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }]
    });



//register page

var $tabs = $('ul.nav-tabs');
 
// show the appropriate tab content based on url hash
if (window.location.hash) {
  showFormPane(window.location.hash);
  makeTabActive($tabs.find('a[href="' + window.location.hash + '"]').parent());
}
 
// function to show/hide the appropriate content page
function showFormPane(tabContent, paneId) {
  var $paneToHide = $(tabContent).find('div.content-pane').filter('.is-active'),
    $paneToShow = $(paneId);
 
  $paneToHide.removeClass('is-active').addClass('is-animating is-exiting');
  $paneToShow.addClass('is-animating is-active');
 
  $paneToShow.on('transitionend webkitTransitionEnd', function() {
    $paneToHide.removeClass('is-animating is-exiting');
    $paneToShow.removeClass('is-animating').off('transitionend webkitTransitionEnd');
  });
}
 
// change active tab
function makeTabActive($tab) {
  $($tab).parent().addClass('active').siblings('li').removeClass('active');
}
 
// show/hide the tab content when clicking the tab button
$tabs.on('click', 'a', function(e) {
  e.preventDefault();
 
  makeTabActive($(this));
  showFormPane($(this).closest('.container'), this.hash);
})


//header sticky menu
jQuery(window).scroll(function() {
if (jQuery(this).scrollTop() > 1){  
   jQuery('header').addClass("sticky");
  }
  else{
    jQuery('header').removeClass("sticky");
  }
});


(function($){
    var wow = new WOW({
        offset: 50,
        mobile: false
    });

    wow.init();
});



$(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i>');
            }
        }
        init();
    });
});

//registeration radio add new custom fields

    $(function () {
        $("input[name='radio']").click(function () {
			$('.register_radios').hide();
			$('.register_fields_input').hide();
			$('.add-more').html('+');
			var id_name = $(this).attr("data-attr");
if(id_name=='artist'){$('#advertiseus').attr("data-getvalue",'1');$('.addmoreinf').attr("data-getvalue",'1');}
if(id_name=='directors'){$('#advertiseus').attr("data-getvalue",'2');$('.addmoreinf').attr("data-getvalue",'2');}
				$('.register_radios_select').show();
				$('#'+id_name).show();
				$('.add-more').attr('data-original-title','Add custom '+id_name+'');
				$('.'+id_name+'_field').val('');
        });
		$(".add-more").click(function(){
			$('.register_radios_select').hide();
			$('.register_fields_input').hide();
			var id_name = $(this).attr("data-attr");
			var id = $(this).attr("data-id");
			if($(this).hasClass('inputShow')){
				$('#'+id_name+'_select').show();
				$(this).removeClass('inputShow');
				$(this).attr('data-original-title','Add custom '+id_name+'');
				$(this).html('+');
				$('.'+id_name+'_field').val('');
			}else{
				$('#field'+id).show();
				$(this).addClass('inputShow');
				$(this).attr('data-original-title','Remove custom '+id_name+'');
				$(this).html('x');
			}
			
		});
    });


// registeration page validation

$("#advertiseus").click(function(){

var fstx=$('#first_name').val();
var lstx=$('#last_name').val();
var mailx=$('#email').val();
var pwd=$('#password').val();
var c_pwd=$('#password_confirmation').val();
var phn=$('#phn_no').val();
var location=$('#locality').val();
var gender=$('#gender').val();    
var chkkk=$(this).attr("data-getvalue");
var customrolevalue=$('#field'+chkkk).val();
var customrole=$('#field'+chkkk).css('display');
var customerror='';
if(customrole=='block' && customrolevalue==''){customerror='yes';}
else{customerror='no';}

if(fstx.trim()=='' || lstx.trim()=='' || mailx.trim()=='' || pwd.trim()=='' || c_pwd.trim()=='' || phn.trim()=='' || location.trim()=='' ||gender.trim()=='' || !$("input[name='radio']:checked").val() || pwd!=c_pwd || !$("#chkbtn").hasClass("active") || customerror=='yes')
{if(fstx.trim()=='')
{$('#first_name').css('border','1px solid red');}
else
{$('#first_name').css('border','1px solid #ccc');}
if(location.trim()=='')
{$('#searchTextField').css('border','1px solid red');$('#error_location').text('Please enter city/district');}
else
{$('#searchTextField').css('border','1px solid #ccc');$('#error_location').text('');}
 if(gender.trim()=='')
{$('#gender').css('border','1px solid red');}
else
{$('#gender').css('border','1px solid #ccc');}
if(lstx.trim()=='')
{$('#last_name').css('border','1px solid red');}
else
{$('#last_name').css('border','1px solid #ccc');}
if(mailx.trim()=='')
{$('#email').css('border','1px solid red');}
else
{$('#email').css('border','1px solid #ccc');}
if(pwd.trim()=='')
{$('#password').css('border','1px solid red');}
else
{$('#password').css('border','1px solid #ccc');}
if(c_pwd.trim()=='')
{$('#password_confirmation').css('border','1px solid red');}
else
{$('#password_confirmation').css('border','1px solid #ccc');}

if(phn.trim()=='')
{$('#phn_no').css('border','1px solid red');}
else
{$('#phn_no').css('border','1px solid #ccc');}

if(!$("input[name='radio']:checked").val())
{$('#radioerror').text('Please select the category type');}
else{$('#radioerror').text('');}

if(pwd!=c_pwd)
{$('#pwdmatch').text('Password and Confirm password field do not match');}
else{$('#pwdmatch').text('');}

if(!$("#chkbtn").hasClass("active"))
{$('#chkerror').text('Please agree our terms and conditions');}
else{$('#chkerror').text('');}

if(customerror=='yes'){$('.customerr').text('Please add category');}
else{$('.customerr').text('');}

return false;
}
else{$('#first_name').css('border','1px solid #ccc');$('#last_name').css('border','1px solid #ccc');$('#email').css('border','1px solid #ccc');
$('#password').css('border','1px solid #ccc');$('#password_confirmation').css('border','1px solid #ccc'); $('#phn_no').css('border','1px solid #ccc');
$('#radioerror').text('');$('#pwdmatch').text('');$('#chkerror').text('');$('#error_location').text('');$('#searchTextField').css('border','1px solid #ccc');$('.customerr').text('');}


});
$(window).bind("load", function() {
  window.setTimeout(function() {
    $("#hide_flashdata").fadeTo(500, 0.7).slideUp(500, function(){
        $(this).remove();
    });
}, 10000);
});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});


function hidemodal(selector)
{
	$(selector).modal('hide');
	
}
function showmodal(selector)
{
	$(selector).modal('show');
	
}
function showlogin(){
	hidemodal(".modal")
	showmodal("#login")
}

function showforgot(){
	hidemodal(".modal")
	showmodal("#forgot")
}

$( "#fgt" ).click(showforgot);
$( ".showlogin" ).click(showlogin);




//login ajax code

$("#login_ajax").click(function(){
$('#notmatch').text('');
var st1=$('#email1').val();
var st2=$('#exampleInputPassword1').val();
if(st1.trim()=="" || st2.trim()=="")
{
if(st1.trim()=="")
$('.erroremail').text("Field is required");
if(st2.trim()=="")
$('.errorpassword').text("Field is required");

return false;
}
var base_url = $('#base_url').val();
var URL = base_url+"front/Login/login";
   $.ajax({
        type: "POST",
        url: URL,
        data:{emailorname:$('#email1').val().trim(),password:$('#exampleInputPassword1').val()},
       success: function(data){
       
if(data != ''){
 if(data == 'error'){ $('#notmatch').show();
$('#notmatch').text("Email or password does not match"); return false;}
else if(data == 'notverified'){ $('#notmatch').show();
$('#notmatch').text("Please verify your email first to login"); return false;}
else{ $('#notmatch').hide();
var id = data;
window.location.href=base_url+'profile/'+id; }
}
},
failure: function(){$('#tab').html("form not submitted");}
    }); });


//geo location
var mapshow=$('#showmapornot').val();
if(mapshow!=''){
 var placeSearch, autocomplete;
      var componentForm = {
        locality: 'long_name',
        administrative_area_level_1: 'long_name',
        country: 'long_name',
      };
 
               function initialize() {
                       var input = document.getElementById('searchTextField');
                        
                       var autocomplete = new google.maps.places.Autocomplete(input);

 
                        
                        
                        google.maps.event.addListener(autocomplete, 'place_changed', function()
                        {
                           var place = autocomplete.getPlace();


          for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
         }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
                           document.getElementById('lati').value = place.geometry.location.lat();
                           document.getElementById('long').value = place.geometry.location.lng();
                           
                        });
               }

               google.maps.event.addDomListener(window, 'load', initialize);
}
     





