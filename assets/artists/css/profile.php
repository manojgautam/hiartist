<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/fancybox/css/fullscreen.css" media="screen">
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/fancybox/css/slideshow.css" media="screen">
<link rel="stylesheet" href="<?php echo base_url();?>assets/artists/fancybox/css/thumbs.css" media="screen">
<style>
    .gallerys {
   
        margin-top: 2px;
    }
    .alwaysshow{
        visibility:visible !important;
        opacity:1 !important;
    }
</style>
<div class="slickslide"><img src="<?php echo base_url();?>assets/artists/images/footer-bg-design.jpg" /></div>

<!--<div class="registers_btn">
    <a href="/register" class="banner-button-style">register as artist</a>
</div>-->
 <?php $id=$this->uri->segment(2);?>
<input type="hidden" name="user_id" id="user_id" value="<?php echo $id;?>">
<div class="container">
    <div class="wrapper">
        <div class="row">
            <div class="col-md-5 img_artist hover-tab bio_descrpt">
 <span class="edit-icon icon_image_text removeprev" data-toggle="modal" data-target="#profilephoto" attr-name="img_text" style="visibility:hidden;opacity:0;text-align:center;width:40px;"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                <div id="prof-image"><img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php if($profdetail[0]->profile_pic==''){echo 'user.jpeg';}else{echo $profdetail[0]->profile_pic;}?>&h=430&w=400" alt="artist"></div>
    
            </div>
            <div class="col-md-7 img_artist_right_side text-left hover-tab">

                <div class="col-md-12 bio_descrpt heading">
                    <h1><span class="col-md-2" >I am</span> <p class="col-md-8 view-text hide_name_text" style="display: block;"><?php echo $profdetail[0]->first_name.' '.$profdetail[0]->last_name; ?></p> <span class="edit-icon icon_name_text" attr-name="name_text" style="visibility:hidden;opacity:0;float:left;text-align:right;width:20px;"><i class="fa fa-pencil" aria-hidden="true"></i></span></h1>
				<input id="first_name" class="form-control editable-input_name_text name_text" value="<?php echo $profdetail[0]->first_name ; ?>" style="display: none;" type="text">
                    <input id="last_name" class="form-control editable-input_name_text name_text" value="<?php echo $profdetail[0]->last_name ; ?>" style="display: none;" type="text">
                        <div class="save_can">
                        <span class="save-input name_text" attr-name="name_text" style="display: none;"><i class="fa fa-floppy-o" aria-hidden="true"></i></span>
                        <span class="input-cancel name_text" attr-name="name_text" style="display: none;"><i class="fa fa-times" aria-hidden="true"></i></span></div>
						</div>
               
                <h5><?php echo $profdetail[0]->sub_role_id.' | '.$profdetail[0]->district ; ?></h5> 
                <hr>
                <h2><?php if($profdetail[0]->role_id==1){echo 'Artist';} else{echo 'Director';} ?> details</h2>
                <div class="col-md-12 bio_descrpt">
				<span class="col-md-4" >Category</span>
				<p class="col-md-8" ><?php echo $profdetail[0]->sub_role_id ; ?> </p>
				</div>
				
                <div class="col-md-12 bio_descrpt">
				<span class="col-md-4" >Gender</span>
					<p class="col-md-8 view-text hide_gender_text" style="display: block;">
						<?php echo $profdetail[0]->gender ; ?>
                        <span class="edit-icon icon_gender_text" attr-name="gender_text" style="visibility:hidden;opacity:0;text-align:right;width:20px;"><i class="fa fa-pencil" aria-hidden="true"></i></span>
					</p>
                    
					 <select class="form-control editable-input_gender_text gender_text input-control req" style="display: none;" name="gender" id="gender">
								<option value="">Select Gender</option>
								<option value="Male" <?php if($profdetail[0]->gender == "Male") echo "selected" ; ?> >Male</option>
								<option value="Female" <?php if($profdetail[0]->gender == "Female") echo "selected" ; ?>>Female</option>
								<option value="Other" <?php if($profdetail[0]->gender == "Other") echo "selected" ; ?>>Other</option>
                   </select>
                          <div class="save_can">
                        <span class="save-input gender_text" attr-name="gender_text" style="display: none;"><i class="fa fa-floppy-o" aria-hidden="true"></i></span>
                        <span class="input-cancel gender_text" attr-name="gender_text" style="display: none;"><i class="fa fa-times" aria-hidden="true"></i></span></div>

				</div>
				<div class="col-md-12 bio_descrpt">
                <span class="col-md-4">Email</span>
				<p class="col-md-8" style="text-transform:none;"><?php echo $profdetail[0]->email ; ?></p>
				</div>
				<div class="col-md-12 bio_descrpt">
				<span class="col-md-4">Mobile number</span>
					<p class="col-md-8 view-text hide_phoneno_text" style="display: block;">
						<?php echo $profdetail[0]->phone_no ; ?>
						<span class="edit-icon icon_phoneno_text" attr-name="phoneno_text" style="visibility:hidden;opacity:0;text-align:right;width:20px;"><i class="fa fa-pencil" aria-hidden="true"></i></span>
					</p>
                <input class="form-control editable-input_phoneno_text phoneno_text" id="phone_no" value="<?php echo $profdetail[0]->phone_no ; ?>" style="display: none;" type="text">
                          <div class="save_can">
                        <span class="save-input phoneno_text" attr-name="phoneno_text" style="display: none;"><i class="fa fa-floppy-o" aria-hidden="true"></i></span>
                        <span class="input-cancel phoneno_text" attr-name="phoneno_text" style="display: none;"><i class="fa fa-times" aria-hidden="true"></i></span></div>

				</div>
				<div class="col-md-12 bio_descrpt">
				<span class="col-md-4">Date of birth</span>
                <p class="col-md-8 view-text hide_dob_text" style="display: block;">
                    <?php echo $profdetail[0]->dob ; ?>
							<span class="edit-icon icon_dob_text" attr-name="dob_text" style="visibility:hidden;opacity:0;text-align:right;width:20px;"><i class="fa fa-pencil" aria-hidden="true"></i></span>
                </p>
                               <div class="form-control input-group date editable-input dob_text" style="display: none;">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right editable-input_dob_text" id="dob" >
                </div>
				
                          <div class="save_can">
                        <span class="save-input dob_text" attr-name="dob_text" style="display: none;"><i class="fa fa-floppy-o" aria-hidden="true"></i></span>
                        <span class="input-cancel dob_text" attr-name="dob_text" style="display: none;"><i class="fa fa-times" aria-hidden="true"></i></span></div>

						</div>
                <div class="col-md-12">
                  <input id="" value="<?php if($profdetail[0]->review_count==0) {echo 0;} else{echo (float)($profdetail[0]->reviews)/(float)($profdetail[0]->review_count);}?>" type="number" class="rating" readonly min=0 max=5 step=0.1 data-size="xs">
                </div>

            </div>
        </div>
    </div>
    
    
    
    
    <div class="col-md-12 social_icon">
        <img src="<?php echo base_url();?>assets/artists/images/view_icon.png" alt="artist">
        <?php echo $profdetail[0]->profile_view ; ?> 


    </div>

    <div class=" col-md-12 artist_tab_section">

        <ul class="nav nav-pills nav-stacked col-md-3">
            <li class="active"><a href="#tab_a" data-toggle="pill">Bio</a></li>
            <li><a href="#tab_b" data-toggle="pill">Gallery</a></li>
            <li><a href="#tab_c" data-toggle="pill">Experience</a></li>
            <li><a href="#tab_d" data-toggle="pill">Reviews</a></li>
            <li><a href="#tab_e" data-toggle="pill">Testimonials</a></li>
        </ul>

        <div class="tab-content col-md-9">
            <div class="tab-pane active" id="tab_a">
                <div class="col-md-12 bio_data text-left hover-tab">
                    <div class="col-md-12 bio_descrpt">
							<h3>Description</h3>
							<div class="col-md-12 view-text hide_description_text" style="display: block;">
                        <?php echo $profdetail[0]->description;?>
							<div class="hovericons"><span class="edit-icon icon_description_text" attr-name="description_text" style="visibility:hidden;opacity:0;float:left;text-align:right;width:20px;"><i class="fa <?php if($profdetail[0]->description==''){echo 'fa-plus';} else echo 'fa-pencil-square-o';?>" aria-hidden="true"></i></span></div>
							</div>
						
							  <div class="box-body pad editable-input description_text" style="display:none;">
							  <textarea class="textarea editable-input_description_text" id="description" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 0.85em;line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name="description" value="<?php echo $profdetail[0]->description;?>"><?php echo $profdetail[0]->description;?></textarea>
							</div>
							
							<div class="btn-wrap"><span class="save-input description_text custom-submit-btm" attr-name="description_text" style="display: none;">SAVE</i></span></div>
							<div class="btn-wrap"><span class="input-cancel description_text custom-submit-btm" attr-name="description_text" style="display: none;">CANCEL</i></span></div>
                    </div>
					
					 <div class="col-md-12 bio_descrpt">
							<h3>Education</h3><div class="hovericons"><span attr-name="eduplussign" style="visibility:hidden;opacity:0;float:right;text-align:right;width:20px;" class="edit-icon addicon-edu-testi edu-tab"><i class="fa fa-plus" aria-hidden="true"></i></span></div>
							<p class="col-md-12 view-text hide_education_text addedutext" style="display: block;"><?php if(count($edudetail)){}else {echo "Please add Education.";}?>
							
							</p>
						
							    <div class="edu-form">
							    </div>
								<div class="col-md-12 profile_edu">
<?php foreach($edudetail as $edu){ ?>
<form class="added-form education-main-div" name="frm_courses" id="edu_frm_courses<?php echo $edu->edu_id;?>" action="" method="post">	
<span style="color:red;" id="err_edit_education_<?php echo $edu->edu_id;?>"></span>
<div class="hovericons">
<span class="edit-icon icon_edu_text<?php echo $edu->edu_id;?> educatin_edit" style="visibility:hidden;opacity:0;float:left;text-align:right;width:20px;" attr-id="<?php echo $edu->edu_id;?>" attr-name="edu_text"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span>
<span class="edit-icon icon_edu_text<?php echo $edu->edu_id;?> educatin_del" style="visibility:hidden;opacity:0;float:right;text-align:right;width:20px;" attr-id="<?php echo $edu->edu_id;?>"><i class="fa fa-trash-o" aria-hidden="true"></i></span></div>
<div class="col-md-12 col-sm-12 eduhere education-main-div">
<div class="row">
<div class="col-sm-6 course-add-section">
<label>course name</label>
<div class="inner-edu-span"><span class="edu_sectin_<?php echo $edu->edu_id;?>" id="span_course_name_<?php echo $edu->edu_id;?>" style="display:block"><?php echo $edu->course_name;?></span><input name="course_name" style="display:none;" id="course_name_<?php echo $edu->edu_id;?>" class="edu_input_<?php echo $edu->edu_id;?>" value="<?php echo $edu->course_name;?>" type="text"><span id="err_coursename_3"></span></div></div>
<div class="col-sm-6 course-add-section">
<label>institute name</label>
<div class="inner-edu-span"><span class="edu_sectin_<?php echo $edu->edu_id;?>" id="span_institute_name_<?php echo $edu->edu_id;?>" style="display:block"><?php echo $edu->institute_name;?></span><input name="institute_name" style="display:none;" id="institute_name_<?php echo $edu->edu_id;?>" class="edu_input_<?php echo $edu->edu_id;?>" value="<?php echo $edu->institute_name;?>" type="text"><span id="err_institutename_3"></span></div>
</div>
</div>
<div class="row">
<div class="col-sm-6 course-add-section">
<label>start date</label>
<div class="inner-edu-span"><span class="edu_sectin_<?php echo $edu->edu_id;?>" id="span_sdate_<?php echo $edu->edu_id;?>" style="display:block"><?php echo date_format(date_create($edu->start_date),'m/d/Y');?></span><input readonly id="sdate_<?php echo $edu->edu_id;?>" style="display:none;"  class="edu_input_<?php echo $edu->edu_id;?> edu_datepickr" name="start_date" value="<?php echo date_format(date_create($edu->start_date),'m/d/Y');?>" type="text"><span id="err_start_date_4" class="icon-validation invalid-text" value=""></span></div></div>
<div class="col-sm-6 course-add-section">
<label>end date</label>
<div class="inner-edu-span"><span class="edu_sectin_<?php echo $edu->edu_id;?>" id="span_edate_<?php echo $edu->edu_id;?>" style="display:block"><?php echo date_format(date_create($edu->end_date),'m/d/Y');?></span><input readonly id="edate_<?php echo $edu->edu_id;?>" style="display:none;" class="edu_input_<?php echo $edu->edu_id;?> edu_datepickr" name="end_date" value="<?php echo date_format(date_create($edu->end_date),'m/d/Y');?>" type="text"></div>
</div>
</div>
<div class="row">
<div class="col-sm-12 addpadding<?php echo $edu->edu_id;?>">
<span class="btn-wrap edu_input_<?php echo $edu->edu_id;?>" style="display:none;"><input value="cancel" id="" attr-id="<?php echo $edu->edu_id;?>" class="hide_cancel submit-btn custom-submit-btm" type="button" attr-name="edu_text"></span><span class="btn-wrap edu_input_<?php echo $edu->edu_id;?>" style="display:none;"><input value="save" attr-name="edu_text" class="edu-save-button-js save-table submit-btn custom-submit-btm" attr-id="<?php echo $edu->edu_id;?>" id="" type="button"></span>
</div>
</div>
</div>
</form>
<?php } ?>
                                </div>
										
							<span class="save-input education_text" style="display: none;"><i class="fa fa-floppy-o" aria-hidden="true"></i></span>
							<span class="input-cancel education_text" attr-name="education_text" style="display: none;"><i class="fa fa-times" aria-hidden="true"></i></span>
                    </div>
                   
                    <div class="bio_descrpt">
                         <h3>physical stats</h3>
                        <?php if(count($physicalstats)){ ?>
<?php }else{ ?>
<p id="hairlength" style='display:none'> <span>Hair length</span> : <span id="span_hairlength"></span></p>
                        <p id="hairtype" style='display:none'> <span>Hair type</span> : <span id="span_hairtype"></span></p>
                        <p id="biceps" style='display:none'> <span>Biceps</span> : <span id="span_biceps"></span></p>
                        <p id="chest" style='display:none'> <span>Chest</span> : <span id="span_chest"></span></p>
                        <p id="hips" style='display:none'> <span>Hips</span> : <span id="span_hips"></span></p>
                        <p id="waist" style='display:none'> <span>Waist</span> : <span id="span_waist"></span></p>
                        <p id="bust" style='display:none'> <span>Bust</span> : <span id="span_bust"></span></p>
                        <p id="weight" style='display:none'> <span>Weight</span> : <span id="span_weight"></span></p>
                        <p id="height" style='display:none'> <span>Height</span> : <span id="span_height"></span></p>
<?php
echo '<p id="notupdatedphysical">Please add Physical Stats.</p>';}?>
                         <?php foreach($physicalstats as $val){?>
                        <p id="hairlength" style="<?php if($val->hair_length==''){echo 'display:none';}?>"> <span>Hair length</span> : <span id="span_hairlength"><?php echo $val->hair_length;?></span></p>
                        <p id="hairtype" style="<?php if($val->hair_type==''){echo 'display:none';}?>"> <span>Hair type</span> : <span id="span_hairtype"><?php echo $val->hair_type;?></span></p>
                        <p id="biceps" style="<?php if($val->biceps==''){echo 'display:none';}?>"> <span>Biceps</span> : <span id="span_biceps"><?php echo $val->biceps;?></span></p>
                        <p id="chest" style="<?php if($val->chest==''){echo 'display:none';}?>"> <span>Chest</span> : <span id="span_chest"><?php echo $val->chest;?></span></p>
                        <p id="hips" style="<?php if($val->hips==''){echo 'display:none';}?>"> <span>Hips</span> : <span id="span_hips"><?php echo $val->hips;?></span></p>
                        <p id="waist" style="<?php if($val->waist==''){echo 'display:none';}?>"> <span>Waist</span> : <span id="span_waist"><?php echo $val->waist;?></span></p>
                        <p id="bust" style="<?php if($val->bust==''){echo 'display:none';}?>"> <span>Bust</span> : <span id="span_bust"><?php echo $val->bust;?></span></p>
                        <p id="weight" style="<?php if($val->weight==''){echo 'display:none';}?>"> <span>Weight</span> : <span id="span_weight"><?php echo $val->weight;?></span></p>
                        <p id="height" style="<?php if($val->height==''){echo 'display:none';}?>"> <span>Height</span> : <span id="span_height"><?php echo $val->height;?></span></p>
                        <?php } ?>
                        <div class="hovericons"><span class="edit-icon alignright icon_physical_skill_work_text" attr-name="physical_skill_work_text" data-toggle="modal" data-target="#physicalModal" id="" style="visibility:hidden;opacity:0;float:left;text-align:right;width:20px;"><i id="physicaladdedit" class="fa <?php if(count($physicalstats)){echo 'fa-pencil-square-o';}else{echo 'fa-plus';}?>" aria-hidden="true"></i></span></div>
                    </div>
                    <div class="bio_descrpt">
                        <h3>skills</h3>
 <?php if($skills[0]['lang']!='' || $skills[0]['expert']!=''){ ?>
<?php }else{ ?>
<p id="expertise" style='display:none'> <span>Expertise</span> : <span id="span_expertise"></span></p>
<p id="languages" style='display:none'> <span>Languages</span> : <span id="span_languages"></span></p>
 <?php
echo '<p id="notupdatedskill">Please add Skills.</p>';}?>
 <?php foreach($skills as $val){ if($val['expert']!='' || $val['lang']!=''){?>
 <p id="expertise" style="<?php if($val['expert']==''){echo 'display:none';}?>"> <span>Expertise</span> : <span id="span_expertise"><?php echo $val['expert'];?></span></p>
                        <p id="languages" style="<?php if($val['lang']==''){echo 'display:none';}?>"> <span>Languages</span> : <span id="span_languages"><?php echo $val['lang'];?></span></p>
<?php }} ?>
<div class="hovericons"><span class="edit-icon alignright icon_physical_skill_work_text" attr-name="physical_skill_work_text" data-toggle="modal" data-target="#skillModal" id="" style="visibility:hidden;opacity:0;float:left;text-align:right;width:20px;"><i id="skilladdedit" class="fa <?php if($skills[0]['lang']!='' || $skills[0]['expert']!=''){echo 'fa-pencil-square-o';}else{echo 'fa-plus';}?>" aria-hidden="true"></i></span></div>
                    </div>


                    <div class="bio_descrpt">
                        <h3>work preference</h3>
 <?php if(count($workpref)){ ?>
<?php }else{ ?>
<p id="workplace" style='display:none'> <span>Preferred Locations</span> : <span id="span_workplace"></span></p>
<p id="workhour" style='display:none'> <span>Working Hours</span> : <span id="span_workhour"></span></p>
 <?php
echo '<p id="notupdatedwork">Please add Work Preference.</p>';}?>
 <?php foreach($workpref as $val){?>
 <p id="workplace" style="<?php if($val->work_places==''){echo 'display:none';}?>"> <span>Preferred Locations</span> : <span id="span_workplace"><?php echo $val->work_places;?></span></p>
                        <p id="workhour" style="<?php if($val->work_hours==''){echo 'display:none';}?>"> <span>Working Hours</span> : <span id="span_workhour"><?php echo $val->work_hours;?></span></p>
<?php } ?>
                     <div class="hovericons"> <span class="edit-icon alignright icon_physical_skill_work_text" attr-name="physical_skill_work_text" data-toggle="modal" data-target="#workModal" id="" style="visibility:hidden;opacity:0;float:left;text-align:right;width:20px;"><i id="workaddedit" class="fa <?php if(count($workpref)){echo 'fa-pencil-square-o';}else{echo 'fa-plus';}?>" aria-hidden="true"></i></span></div>
                    </div>

                </div>

            </div>
            <div class="tab-pane" id="tab_b">
 <?php if(count($imagedetail)==0){ ?><p class="addimagetext">Please Add Images</p><?php } ?>
  <button type="button" class="btn btn-info btn-lg removeprev" data-toggle="modal" data-target="#myModal">Upload Images</button>

                <div class="col-md-12 col-sm-12 list-group gallerys">
<?php foreach($imagedetail as $imgs){ ?>
                    <div class="col-md-4">
                        <div class="gallery">
                            <div class="view view-first">
                                <a class="fancybox" rel="ligthbox" href="<?php echo base_url();?>uploads/<?php echo $imgs->url;?>">
                     <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo $imgs->url;?>&h=250&w=300" alt="userImage" class="img-responsive">
                    <div class="mask">
                        <div class="info"><i class="fa fa-plus" aria-hidden="true"></i></div>
                    </div>
                   </a>
<div class="removeimage" id="<?php echo $imgs->media_id;?>"><span>X</span></div>
                            </div>
                        </div>
                    </div>
          <?php } ?>

                </div>
    
    <div class="col-md-12">
 <?php if(count($video)==0){ ?><p class="addvideotext">Please Add Videos</p><?php } ?>   
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModalvideo" onclick="$('#videourl').val('');$('#youtubeerr').hide();">Upload Videos</button>
<div class=" list-group videogallerys">
<?php foreach($video as $vid){ 
          $lastvalue=explode('/',$vid->url);
          $lastval=$lastvalue[count($lastvalue)-1];
    ?>
                    <div class="col-md-4">
                        <div class="gallery">
                            <div class="view view-first">
                              <a class="fancyboxvideo fancybox.iframe" href="<?php echo $vid->url;?>?autoplay=1&wmode=opaque"><img  src="http://img.youtube.com/vi/<?php echo $lastval;?>/0.jpg" alt=""/>
                               <div class="mask">
                        <div class="info"><i class="fa fa-plus" aria-hidden="true"></i></div>
                    </div>
                </a>
                    <div class="removeimage" id="<?php echo $vid->media_id;?>"><span>X</span></div>

                            </div>
                        </div>
                    </div>

                    <?php } ?>
     
</div>
</div>


            </div>
            <div class="tab-pane" id="tab_c">
                <div class="col-md-12 experience_data text-left hover-tab">
<?php if(count($experiencedetail)==0){ ?><p class="expnotadd">Please Add Experience</p><?php } ?>
<div class="hovericons" style="max-width:0px;position:relative;top:-35px;left:-30px">
<span class="edit-icon exp-tab alwaysshow" attr-name="expepuussign" style="visibility:hidden;opacity:0;float:left;text-align:right;width:20px;cursor:pointer;"><i class="fa fa-plus" aria-hidden="true"></i></span>
</div>
<div class="exp-form">
</div>
<div class="profile_exp">
                  <?php foreach($experiencedetail as $edu){ ?>
<div class="added-form" name="frm_courses" id="exp_frm_courses<?php echo $edu->exp_id;?>">
<p id="error_edit_frm_experience_<?php echo $edu->exp_id;?>" style="color:red;"></p>
<div class="hovericons">
		<span class="edit-icon icon_exp_text<?php echo $edu->exp_id;?> experience_edit" attr-id="<?php echo $edu->exp_id;?>" style="visibility:hidden;opacity:0;float:left;text-align:right;width:20px;" attr-name="exp_text">
			<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
		</span>
		<span class="edit-icon icon_exp_text<?php echo $edu->exp_id;?> experience_del" style="visibility:hidden;opacity:0;float:right;text-align:right;width:20px;" attr-id="<?php echo $edu->exp_id;?>">
			<i class="fa fa-trash-o" aria-hidden="true"></i>
		</span>
	</div>
<div class="exp-main-div">
	<div class="exp-add-section">
		<label>Project Name</label>
		<div>
			<span class="exp_sectin_<?php echo $edu->exp_id;?>" id="span_project_name_<?php echo $edu->exp_id;?>" style="display:block"><?php echo $edu->project_name;?></span>
			<input name="course_name" style="display:none;" id="project_name_<?php echo $edu->exp_id;?>" class="exp_input_<?php echo $edu->exp_id;?>" value="<?php echo $edu->project_name;?>" type="text">
				<span id="err_coursename_3"></span>
			</div>
		</div>
		<div class="exp-add-section">
			<label class="color-light">Role</label>
			<div>
				<span class="exp_sectin_<?php echo $edu->exp_id;?>" id="span_role_name_<?php echo $edu->exp_id;?>" style="display:block"><?php echo $edu->role;?></span>
				<input name="institute_name" style="display:none;" id="role_name_<?php echo $edu->exp_id;?>" class="exp_input_<?php echo $edu->exp_id;?>" value="<?php echo $edu->role;?>" type="text">
					<span id="err_institutename_3"></span>
				</div>
			</div>
			<div class="exp-add-section">
				<label class="color-light">start date</label>
				<div class="exp-course-add-section">
					<span class="exp_sectin_<?php echo $edu->exp_id;?>" id="span_expsdate_<?php echo $edu->exp_id;?>" style="display:block"><?php echo date_format(date_create($edu->start_date),'m/d/Y');?></span>
					<input id="expsdate_<?php echo $edu->exp_id;?>" readonly style="display:none;"  class="exp_input_<?php echo $edu->exp_id;?> exp_datepickr" name="start_date" value="<?php echo date_format(date_create($edu->start_date),'m/d/Y');?>" type="text">
						<span id="err_start_date_4" class="icon-validation invalid-text" value=""></span>
					</div>
				</div>
				<div class="exp-add-section">
					<label class="color-light">end date</label>
					<div class="exp-course-add-section">
						<span class="exp_sectin_<?php echo $edu->exp_id;?>" id="span_expedate_<?php echo $edu->exp_id;?>" style="display:block"><?php echo date_format(date_create($edu->end_date),'m/d/Y');?></span>
						<input id="expedate_<?php echo $edu->exp_id;?>" style="display:none;" readonly class="exp_input_<?php echo $edu->exp_id;?> exp_datepickr" name="end_date" value="<?php echo date_format(date_create($edu->end_date),'m/d/Y');?>" type="text">
						</div>
					</div>
					<div class="profdescription">
						<label class="color-light">Description</label>
						<div class="exp-course-add-section">
							<span class="exp_sectin_<?php echo $edu->exp_id;?>" id="span_description_<?php echo $edu->exp_id;?>" style="display:block"><?php echo $edu->description;?></span>
							<textarea id="description_<?php echo $edu->exp_id;?>" style="display:none;" class="exp_input_<?php echo $edu->exp_id;?>" name="end_date" value="<?php echo $edu->description;?>"><?php echo $edu->description;?></textarea>
						</div>
					</div>
					<div>
						<span class="btn-wrap exp_input_<?php echo $edu->exp_id;?>" style="display:none;">
							<input value="cancel" id="" attr-id="<?php echo $edu->exp_id;?>" class="hide_cancel_exp submit-btn custom-submit-btm" type="button" attr-name="exp_text">
							</span>
							<span class="btn-wrap exp_input_<?php echo $edu->exp_id;?>" style="display:none;">
								<input value="save" attr-name="exp_text" class="experience-save-button-js save-table submit-btn custom-submit-btm" attr-id="<?php echo $edu->exp_id;?>" id="" type="button">
								</span>
							</div>
						</div>
                      </div>
<?php } ?>
</div>
                </div>
            </div>
            <div class="tab-pane" id="tab_d">
                <div class="col-md-12 experience_data review_coverdiv text-left">
                    <?php if(count($allreviews)){}else{echo "<p class=''>No reviews yet.</p>";}?> 
<?php  if(count($allreviews)>=3) $x=3; else $x=count($allreviews);$y=0;foreach($allreviews as $val){if($y<$x){?>
             <div class="row review-area-inner review_chker">
                            <div class="col-xs-12 col-sm-3 col-md-1">
                            <div class="img_ara">
                                <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php if($val->profile_pic==''){echo 'user.jpeg';} else {echo $val->profile_pic;}?>&h=80&w=75" alt="artist"/>
                            </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-7">
                                <h3><?php echo $val->first_name.' '.$val->last_name;?></h3>
                      
                                <p style="word-wrap:break-word"><?php echo $val->description;?></p>
                                 
                            </div>
                            <div class="col-xs-12 col-sm-5 col-md-4" style="position:relative;left:120px">
                                <input id="" value="<?php echo $val->review;?>" type="number" class="rating" readonly min=0 max=5 step=0.1 data-size="xs">
                            </div>
                        </div>
<?php $y++;}else{break;}} ?>
                </div>
<input type="hidden" id="totalreviews" value="<?php echo count($allreviews);?>">
<?php if(count($allreviews)>3){?>
<div class=" col-md-12 text-center show_more_btn">
  <a class="show_more" id="load_more_review" style="margin-bottom:30px;margin-top:20px;" href="javascript:void(0);">Show More</a>
</div>
  <?php } ?>
            </div>
            
              <div class="tab-pane" id="tab_e">
                        <div class="col-md-12 bio_data text-left hover-tab">
                    <div class="col-md-12 bio_descrpt">
							<h3>Testimonial</h3>
                       
							<p class="col-md-12 view-text hide_testimonial_text" style="display: block;">
                        <?php echo $testimonialtext['testimonialText'];?>
							<div class="hovericons"><span class="edit-icon icon_testimonial_text alwaysshow" attr-name="testimonial_text" style="float:left;text-align:right;width:20px;"><i class="fa <?php if($testimonialtext['testimonialText']==''){echo 'fa-plus';} else echo 'fa-pencil-square-o';?>" aria-hidden="true"></i></span></div>
							</p>
							
							  <div class="box-body pad editable-input testimonial_text" style="display: none;">
							  <textarea class=" editable-input_testimonial_text" id="testimonial_description" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 0.85em;line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name="testimonial_description" value="<?php echo $testimonialtext['testimonialText'];?>"><?php echo $testimonialtext['testimonialText'];?></textarea>
							</div>
							
							<div class="btn-wrap"><span class="save_testimonial testimonial_text custom-submit-btm" attr-name="testimonial_text" style="display: none;">SAVE</span></div>
							<div class="btn-wrap"><span class="input-cancel testimonial_text custom-submit-btm" attr-name="testimonial_text" style="display: none;">CANCEL</span></div>
                    </div>
                  </div>
    <?php if(!$testimonialtext['testimonialText']){ ?>
    <div class="plus-img">
        </div>
    <?php } ?>
            </div>
            
            
            
        </div>
        <!-- tab content -->
    </div>
</div>

<!-- share profile Modal -->
  <div class="modal fade" id="shareprofile" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Share profile</h4>
        </div>
        <div class="modal-body">
 

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<!-- Modal end -->

<!-- photos Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Image Upload</h4>
        </div>
        <div class="modal-body">
     
<form id="uploadForm" action="<?php echo base_url();?>front/Profile/imageupload" method="post">

<div id="uploadFormLayer">
<label>Upload Image File:</label><br/>
<input name="userImage" type="file" class="inputFile" />
<p style="font-size:12px;">file size should not be more than 2 MB. the width and height of the image should be at least 350px.</p>
<input type="submit" value="Submit" class="btnSubmit" />
</div>
</form>


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default closeimg" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<!-- Modal end -->


<!-- profilephoto Modal -->
  <div class="modal fade" id="profilephoto" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit profile Image</h4>
        </div>
        <div class="modal-body">
     
<form id="profileuploadForm" action="<?php echo base_url();?>front/Profile/proflieimageupload" method="post">

<div id="uploadFormLayer">
<label>Upload Image File:</label><br/>
<input name="userImage" type="file" class="inputFile" />
<p style="font-size:12px;">file size should not be more than 2 MB. the width and height of the image should be at least 350px.</p>
<input type="submit" value="Submit" class="btnSubmit" />
</div>
</form>


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default closepimg" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<!-- Modal end -->

<!-- video Modal -->
  <div class="modal fade" id="myModalvideo" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Video Upload</h4>
        </div>
        <div class="modal-body">
     
<form id="videouploadForm" action="<?php echo base_url();?>front/Profile/videoupload" method="post">
<label>Youtube Video URL:</label><br/>
<input name="videourl" type="text" id="videourl"/>
<p style="display:none;color:red;" id="youtubeerr">Please enter Youtube URL</p>
<input type="submit" value="Submit" class="btnSubmit" />
</form>
   </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default closevid" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<!-- Modal end -->


<!-- skill Modal -->
  <div class="modal skillmodal_popup fade" id="skillModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"></button>
          <h4 class="modal-title">Skill</h4>
        </div>
        <div class="modal-body">
     



<div class="expertise_selectbx">
<label class="select-box-wrap">Expertise</label>
<select class="" style="height:70px;" multiple="" id="attr_expertise"  required>
<?php foreach($getexpertiselist as $val){?>
<option value="<?php echo $val->expert_id;?>" <?php if($skills[0]['expert']!=''){if(strpos($skills[0]['expert'], $val->expertise) !== false){echo 'selected';}}?>><?php echo $val->expertise;?></option>
<?php } ?>
</select>
</div>


<div class="expertise_selectbx">
<label class="select-box-wrap">Languages</label>
<select class="" style="height:70px;" multiple="" id="attr_language"  required>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Hindi') !== false){echo 'selected';}}?>>Hindi</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'English') !== false){echo 'selected';}}?>>English</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Bengali') !== false){echo 'selected';}}?>>Bengali</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Telugu') !== false){echo 'selected';}}?>>Telugu</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Tamil') !== false){echo 'selected';}}?>>Tamil</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Marathi') !== false){echo 'selected';}}?>>Marathi</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Urdu') !== false){echo 'selected';}}?>>Urdu</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Gujarati') !== false){echo 'selected';}}?>>Gujarati</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Kannada') !== false){echo 'selected';}}?>>Kannada</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Malayalam') !== false){echo 'selected';}}?>>Malayalam</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Odia') !== false){echo 'selected';}}?>>Odia</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Punjabi') !== false){echo 'selected';}}?>>Punjabi</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Assamese') !== false){echo 'selected';}}?>>Assamese</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Maithili') !== false){echo 'selected';}}?>>Maithili</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Bhilodi') !== false){echo 'selected';}}?>>Bhilodi</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Santali') !== false){echo 'selected';}}?>>Santali</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'kashmiri') !== false){echo 'selected';}}?>>kashmiri</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Nepali') !== false){echo 'selected';}}?>>Nepali</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Gondi') !== false){echo 'selected';}}?>>Gondi</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Sindhi') !== false){echo 'selected';}}?>>Sindhi</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'Konkani') !== false){echo 'selected';}}?>>Konkani</option>
<option <?php if($skills[0]['lang']!=''){if(strpos($skills[0]['lang'], 'International language') !== false){echo 'selected';}}?>>International language</option>
</select>
</div>
<br><span class="italic-font" style="font-size:14px;">(hold ctrl for selecting multiple items)</span><br>
<button type="button" class="btn" name="theskill" id="theskill" value="Submit">Submit</button>


   </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default closeskill" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<!-- Modal end -->


<!-- work Modal -->
  <div class="modal workmodal_popup fade" id="workModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"></button>
          <h4 class="modal-title">Work preference</h4>
        </div>
        <div class="modal-body">
     
<div class="popup-work-section-group"><label class="select-box-wrap">Preferred Locations</label> <div class="select-group-wrap">
<select class="input-common-style" style="height:70px;" multiple="" id="attr_locations">
		<option <?php if(count($workpref)){if(strpos($workpref[0]->work_places, 'Delhi') !== false){echo 'selected';}}?>>Delhi</option>
		<option <?php if(count($workpref)){if(strpos($workpref[0]->work_places, 'Mumbai') !== false){echo 'selected';}}?>>Mumbai</option>
		<option <?php if(count($workpref)){if(strpos($workpref[0]->work_places, 'Bangalore') !== false){echo 'selected';}}?>>Bangalore</option>
        <option <?php if(count($workpref)){if(strpos($workpref[0]->work_places, 'Hyderabad') !== false){echo 'selected';}}?>>Hyderabad</option>
        <option <?php if(count($workpref)){if(strpos($workpref[0]->work_places, 'Ahmedabad') !== false){echo 'selected';}}?>>Ahmedabad</option>
        <option <?php if(count($workpref)){if(strpos($workpref[0]->work_places, 'Chennai') !== false){echo 'selected';}}?>>Chennai</option>
		<option <?php if(count($workpref)){if(strpos($workpref[0]->work_places, 'Kolkata') !== false){echo 'selected';}}?>>Kolkata</option>
		<option <?php if(count($workpref)){if(strpos($workpref[0]->work_places, 'Surat') !== false){echo 'selected';}}?>>Surat</option>
	   <option <?php if(count($workpref)){if(strpos($workpref[0]->work_places, 'Pune') !== false){echo 'selected';}}?>>Pune</option>
		<option <?php if(count($workpref)){if(strpos($workpref[0]->work_places, 'Jaipur') !== false){echo 'selected';}}?>>Jaipur</option>
<option <?php if(count($workpref)){if(strpos($workpref[0]->work_places, 'Anywhere in India') !== false){echo 'selected';}}?>>Anywhere in India</option>
        </select>
        </div><br><span class="italic-font">(hold ctrl for selecting multiple items)</span></div>


<div class="popup-work-section-group"><label class="select-box-wrap">Working Hours</label> <div class="select-group-wrap">
<select class="input-common-style" id="attr_hours">
		 
<option value="">Select</option>
<option <?php if(count($workpref)){if($workpref[0]->work_hours=='Full time'){echo 'selected';}}?>>Full time</option>
<option <?php if(count($workpref)){if($workpref[0]->work_hours=='Part time'){echo 'selected';}}?>>Part time</option>
<option <?php if(count($workpref)){if($workpref[0]->work_hours=='Weekends'){echo 'selected';}}?>>Weekends</option>
<option <?php if(count($workpref)){if($workpref[0]->work_hours=='Short-term'){echo 'selected';}}?>>Short-term</option>
<option <?php if(count($workpref)){if($workpref[0]->work_hours=='Long-term'){echo 'selected';}}?>>Long-term</option>
	</select>
     </div><br></div>
<input type="button" value="Submit" class="btn" id="worksubmit" />
</div>

        <div class="modal-footer">
          <button type="button" class="btn btn-default closework" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
<!-- Modal end -->

<!-- physical stats Modal -->
  <div class="modal physicalmodal_popup fade" id="physicalModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"></button>
          <h4 class="modal-title">physical stats</h4>
        </div>
        <div class="modal-body">
     
<input name="att_sectionid" id="att_sectionid" value="6" type="hidden"><div class="popup-physical-section-group"><label class="select-box-wrap">hair length</label>         <div class="select-group-wrap">
             
 
          
        <select name="attributes" attr-id="hairlength" class="attributes" class="input-common-style">
         
                    <option value="">Select</option>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->hair_length=='long'){echo 'selected';}}?> >long</option>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->hair_length=='medium'){echo 'selected';}}?>>medium</option>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->hair_length=='short'){echo 'selected';}}?>>short</option>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->hair_length=='pixie'){echo 'selected';}}?>>pixie</option>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->hair_length=='bald'){echo 'selected';}}?>>bald</option>
              
        </select>
        </div><br></div><div class="popup-physical-section-group"><label class="select-box-wrap">hair type</label>        <div class="select-group-wrap">
        <select name="attributes" class="attributes" attr-id="hairtype" class="input-common-style">
         
                     <option value="">Select</option>
                      <option <?php if(count($physicalstats)){if($physicalstats[0]->hair_type=='straight'){echo 'selected';}}?>>straight</option>
                      <option <?php if(count($physicalstats)){if($physicalstats[0]->hair_type=='curly'){echo 'selected';}}?>>curly</option>
                      <option <?php if(count($physicalstats)){if($physicalstats[0]->hair_type=='wavy'){echo 'selected';}}?>>wavy</option>
                    
        </select>
        </div><br></div><div class="popup-physical-section-group"><label class="select-box-wrap">biceps</label>       <div class="select-group-wrap">
             
 
          
        <select name="attributes" class="attributes" attr-id="biceps" class="input-common-style">
         
                    <option value="">Select</option>
                                    <option <?php if(count($physicalstats)){if($physicalstats[0]->biceps=='below 10 inches'){echo 'selected';}}?>>below 10 inches</option>
                                    <?php for($i=10;$i<=20;$i++){?>
                                    <option <?php if(count($physicalstats)){if($physicalstats[0]->biceps==$i.' inches'){echo 'selected';}}?>><?php echo $i;?> inches</option>
                                    <?php } ?>
                                    <option <?php if(count($physicalstats)){if($physicalstats[0]->biceps=='above 20 inches'){echo 'selected';}}?>>above 20 inches</option>
                    
                 
        </select>
        </div><br></div><div class="popup-physical-section-group"><label class="select-box-wrap">chest</label>        <div class="select-group-wrap">
             
 
          
        <select name="attributes" class="attributes" attr-id="chest" class="input-common-style">
         
                                    <option value="">Select</option>
                                    <option <?php if(count($physicalstats)){if($physicalstats[0]->chest=='below 20 inches'){echo 'selected';}}?>>below 20 inches</option>
                                    <?php for($i=20;$i<=60;$i++){?>
                                    <option <?php if(count($physicalstats)){if($physicalstats[0]->chest==$i.' inches'){echo 'selected';}}?>><?php echo $i;?> inches</option>
                                    <?php } ?>
                                    <option <?php if(count($physicalstats)){if($physicalstats[0]->chest=='above 60 inches'){echo 'selected';}}?>>above 60 inches</option>
                    
                 
        </select>
        </div><br></div><div class="popup-physical-section-group"><label class="select-box-wrap">hips</label>         <div class="select-group-wrap">
             
 
          
        <select name="attributes" class="attributes" attr-id="hips" class="input-common-style">
         
                                        <option value="">Select</option>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->hips=='below 20 inches'){echo 'selected';}}?>>below 20 inches</option>
                                        <?php for($i=20;$i<=45;$i++){?>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->hips==$i.' inches'){echo 'selected';}}?>><?php echo $i;?> inches</option>
                                        <?php } ?>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->hips=='above 45 inches'){echo 'selected';}}?>>above 45 inches</option>
                    
                 
        </select>
        </div><br></div><div class="popup-physical-section-group"><label class="select-box-wrap">waist</label>        <div class="select-group-wrap">
             
 
          
        <select name="attributes" class="attributes" attr-id="waist" class="input-common-style">
         
                                        <option value="">Select</option>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->waist=='below 20 inches'){echo 'selected';}}?>>below 20 inches</option>
                                        <?php for($i=20;$i<=45;$i++){?>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->waist==$i.' inches'){echo 'selected';}}?>><?php echo $i;?> inches</option>
                                        <?php } ?>
                                       <option <?php if(count($physicalstats)){if($physicalstats[0]->waist=='above 45 inches'){echo 'selected';}}?>>above 45 inches</option>
                    
                 
        </select>
        </div><br></div><div class="popup-physical-section-group"><label class="select-box-wrap">bust</label>         <div class="select-group-wrap">
             
 
          
        <select name="attributes" class="attributes" attr-id="bust" class="input-common-style">
         
                                        <option value="">Select</option>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->bust=='below 20 inches'){echo 'selected';}}?>>below 20 inches</option>
                                        <?php for($i=20;$i<=45;$i++){?>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->bust==$i.' inches'){echo 'selected';}}?>><?php echo $i;?> inches</option>
                                        <?php } ?>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->bust=='above 45 inches'){echo 'selected';}}?>>above 45 inches</option>
                    
                 
        </select>
        </div><br></div><div class="popup-physical-section-group"><label class="select-box-wrap">weight</label>       <div class="select-group-wrap">
             
 
          
        <select name="attributes" class="attributes" attr-id="weight" class="input-common-style">
         
                                        <option value="">Select</option>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->weight=='below 5kg'){echo 'selected';}}?>>below 5kg</option>
                                        <?php for($i=5;$i<=100;$i++){?>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->weight==$i.'kg'){echo 'selected';}}?>><?php echo $i.'kg';?></option>
                                        <?php } ?>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->weight=='above 100kg'){echo 'selected';}}?>>above 100kg</option>
                    
                 
        </select>
        </div><br></div><div class="popup-physical-section-group"><label class="select-box-wrap">height</label>       <div class="select-group-wrap">
             
 
          
        <select name="attributes" class="attributes" attr-id="height" class="input-common-style">
         
                    <option value="">Select</option>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->height=='below 4 feet'){echo 'selected';}}?>>below 4 feet</option>
                                        <?php for($i=4;$i<=7;$i++){?>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->height==$i.' feet'){echo 'selected';}}?>><?php echo $i;?> feet</option>
                                         <?php for($j=1;$j<=11;$j++){?>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->height==$i.' feet '.$j.' inch' || $physicalstats[0]->height==$i.' feet '.$j.' inches'){echo 'selected';}}?>><?php echo $i.' feet ';?><?php if($j==1){echo $j.' inch';}else{echo $j.' inches';}?></option>
                                        <?php } 
                                         }?>
                                        <option <?php if(count($physicalstats)){if($physicalstats[0]->height=='above 8 feet'){echo 'selected';}}?>>above 8 feet</option>
                    
                 
        </select>
        </div><br></div>
        <input type="button" value="Submit" name="physicalstats" id="physicalstats" class="btn popup-physical-btn" />
   </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default closephysical" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>

<!-- Modal end -->
<!------------------------------client-------------------->


