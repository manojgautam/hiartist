<div class="slickslide">
<?php $u=1;foreach( $homeslider as $value ) { ?>
  <div><img src="<?php echo base_url();?>uploads/<?php echo $value->path;?>" /></div>
<?php } ?>
</div>
<div class="container">
    <ul class="nav nav-tabs nav-justified">
      <li class="active"><a href="#tab1" class="text-uppercase">Top Profiles</a></li>
      <li><a href="#tab2" class="text-uppercase">News</a></li>
      <li><a href="#tab3" class="text-uppercase">Event Management</a></li>
    </ul>

    <div class="tab-content">
      <div id="tab1" class="content-pane is-active">
        <div class="inner_tab_content">
		<?php foreach( $homedetails as $value ) { ?>
		 <div class="tab_grid">
		 <div class="boxing">
		<a href="<?php echo base_url();?>userprofile/<?php echo $value->user_id ; ?>"> <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo $value->profile_pic ; ?>&h=280&w=275" alt="artist"/></a>
		 </div>
		 <div class="artist-ribbon">
		 <span><i style="padding-right:2px;" class="fa fa-eye" aria-hidden="true"></i><?php echo $value->profile_view ; ?></span>
		  </div>
		 <div class="artist-content">
		 <h2><?php echo $value->first_name ; ?> <?php echo $value->last_name ; ?></h2>
		 <p><?php echo $value->sub_role_id ; ?></p>
		 </div>
		 </div>
		<?php } ?>
		
   <input type="button" value="View All">
        </div>
        <!-- /.login-box-body -->
      </div>
        
      <!-- /#tab1 -->

      <div id="tab2" class="content-pane">
        <div class="inner_tab_content">
		<?php $u=1;foreach( $post as $value ) { ?>
		
		 <div class="tab_grid">
		 <div class="boxing">
		<a href="<?php echo $value['post']->guid ; ?>"> <img src="<?php echo $value['image']->guid ; ?>" alt="artist"/></a>
		 </div>
		 <div class="artist-ribbon">
		 <span>#<?php echo $u; ?></span>
		  </div>
		 <div class="artist-content">
		 <h2><?php echo $value['post']->post_title ; ?> </h2>
		 </div>
		 </div>
		<?php $u++;} ?>
		
     <input type="button" value="View All">
        </div>
        <!-- /.form-box -->
      </div>
      <!-- /#tab2 -->
 
      <div id="tab3" class="content-pane">
        <div class="inner_tab_content">

<?php $e=1;foreach( $events as $value ) { ?>
         <div class="tab_grid">
		 <div class="boxing">
		 <div class="itemThumbnail">    
       <a href="#"> <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo $value->image ; ?>&h=280&w=275" alt="events"/></a>
        
       <div class="ev_time home_evnts">
       <i aria-hidden="true" class="fa fa-clock-o"></i> <?php echo date_format(date_create($value->start_date),'d F Y');?> |
       <?php echo date_format(date_create($value->start_time),'h:i A');?> 
        </div>     
       </div>
		 </div>
		 <div class="artist-ribbon">
		 <span>#<?php echo $e;?></span>
		  </div>
		 <div class="artist-content">
		 <h2><?php if(strlen($value->title)>=18){echo substr($value->title,0,18);echo "[..]";}else echo $value->title;?></h2>
		 <p><i aria-hidden="true" class="fa fa-map-marker" style="padding-right:2px;"></i><?php echo $value->place ; ?></p>
		 </div>
		 </div>
		<?php $e++;} ?>
              <input type="button" value="View All">
        </div>
         
        <!-- /.form-box -->
      </div>
      <!-- /#tab2 -->
       
    </div>
    <!-- /.tab-content -->
  </div><!-- /.container -->
  
  
  <div class="container">
  <div class="row">
  <div class="col-sm-6">
  <div class="block01 wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0s">
  <?php foreach( $video as  $value){
  if($value->title == "Video-1") {?>
	<iframe width="300" height="200" src="<?php echo $value->path ; ?>" frameborder="0" allowfullscreen></iframe> 
  <? } } ?>
  </div>
  <div class="block02 wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0s">
	<h3>How we work</h3>
  </div>
  <div class="block03 wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0s">
   <?php foreach( $video as  $value){
  if( $value->title == "Video-2") {?>
	<iframe width="560" height="315" src="<?php echo $value->path ; ?>" frameborder="0" allowfullscreen></iframe> 
 <? } } ?>
	</div>
  </div>
  <div class="col-sm-3 wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0s">
	<div class="block04">
	  <?php foreach( $adds as  $value){
  if( $value->title == "Add-1") {?>
	<img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo $value->path ; ?>&h=357&w=283" alt="image"/>
	  <?php } } ?>
  </div>
  <a href="/hiartist/allevents"><div class="block05">
	<h3>Upcoming Events</h3>
  </div></a>
  </div>
  <div class="col-sm-3">
<div class="block06 wow fadeIn" data-wow-duration="1.2s" data-wow-delay="0s">
<div class="testimonial-wrap">
					<div class="testi-slide">
						<?php foreach( $testimonials as $value ) { ?>
						<div>
							<div class="testimonial-img"><span class="border-style-img">
								<img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo $value->profile_pic ; ?>&h=280&w=275" alt="events"/></span></div>
							<p class="testi-quote"><?php echo $value->testimonialText ; ?></p>
							<div class="testi-name"><?php echo $value->first_name ; ?> <?php echo $value->last_name ; ?></div>
							<div class="testi-cat"><?php echo $value->sub_role ; ?></div>
						</div>
						<?php } ?>
					</div>
				</div>
  </div>
  </div>
  </div>
  </div>
	
	<section class="aoh">
   <div class="container">
  <div class="row">
  <div class="slz-main-title">

<h2 class="title">Recommended Artist</h2>
</div>

<div class="col-sm-9">
<div class="h_artist">
<div class="row">
	<?php foreach($recommended as $value){ ?>
 <div class="col-sm-3 nopadding">
 <div class="h_artist_inner">
 <div class="boxing">
<?php if($value->profile_pic==''){?>
     <a href="<?php echo base_url();?>userprofile/<?php echo $value->user_id ; ?>"> <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/noimage.png &h=280&w=275" alt="artist"/></a>
   <?php } else {?>
		 <a href="<?php echo base_url();?>userprofile/<?php echo $value->user_id ; ?>"> <img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo $value->profile_pic ; ?>&h=280&w=275" alt="artist"/></a>
     <?php } ?>
		 </div>
<div class="artist-ribbon">
		 <span><?php echo $value->sub_role_id;?></span>
		  </div>
<h2><?php echo $value->first_name.' '.$value->last_name;?></h2>
 </div>
 

 </div>
<?php } ?>
  </div>
 
 </div>

 </div>
 <div class="col-sm-3 nopadding">
 <div class="advertisement">
	  <?php foreach( $adds as  $value){
  if( $value->title == "Add-2") {?>
	<img src="<?php echo base_url();?>assets/timthumb.php?src=<?php echo base_url();?>uploads/<?php echo $value->path ; ?>&h=674&w=284" alt="add"/>
	  <?php } } ?>
 </div>
 </div>

	 </div>
  </div>
  </section>
	
  
  <section class="partners">

      
  <div class="container">
  <div class="row">
       <h2 class="title">Featured partners</h2>
  <div class="our_partners">
	<div class="slick-list">
<?php $e=1;foreach( $logos as $value ) { ?>
 <div><img src="<?php echo base_url();?>uploads/<?php echo $value->path;?>" alt="partner"/></div>
<?php } ?>
</div>
   </div>
   </div>
   </div>
   </section>
   
